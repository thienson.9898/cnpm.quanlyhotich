﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLHT.Entity;

namespace QLHT.Model
{
    public class CongChucModel
    {
        DataProvider da = new DataProvider();
        public string DangNhap(string TaiKhoan, string MatKhau)
        {
            DataTable dt = da.ExecuteQuery("Proc_CongChuc_DangNhap", new object[] { TaiKhoan, MatKhau }, new List<string>() { "@TaiKhoan", "@Pass" });
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            else
                return "";
        }
        /// <summary>
        /// Trả ra list tên viết tắt các bp của công chưcs
        /// </summary>
        /// <param name="MaCongChuc"></param>
        /// <returns></returns>
        public string GetQuyen(string MaCongChuc)
        {
            DataTable dt = da.ExecuteQuery("Proc_BoPhan_CongChuc_GetData", new object[] { MaCongChuc }, new List<string>() { "@MaCongChuc" });
            if (dt != null && dt.Rows.Count > 0)
            {
                var result = "";
                foreach (DataRow row in dt.Rows)
                {
                    result += row[nameof(BoPhan.TenVietTat)] + ";";
                }
                return result;
            }
            else
                return "";
        }

        public List<CongChuc> GetData()
        {
            try
            {
                List<CongChuc> dsCongChuc = new List<CongChuc>();
                DataTable dt = da.ExecuteQuery("Proc_CongChuc_GetData", null, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CongChuc congChuc = new CongChuc();
                        congChuc.MaCongChuc = String.IsNullOrEmpty(row["MaCongChuc"].ToString()) ? 0 : int.Parse(row["MaCongChuc"].ToString());
                        congChuc.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                        congChuc.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(row["NgaySinh"].ToString());
                        congChuc.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                        congChuc.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                        congChuc.ChucVu = String.IsNullOrEmpty(row["ChucVu"].ToString()) ? "" : row["ChucVu"].ToString();
                        congChuc.TaiKhoan = String.IsNullOrEmpty(row["TaiKhoan"].ToString()) ? "" : row["TaiKhoan"].ToString(); ;
                        congChuc.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? "" : row["TrangThai"].ToString();
                        congChuc.CMND = String.IsNullOrEmpty(row["CMND"].ToString()) ? "" : row["CMND"].ToString();
                        dsCongChuc.Add(congChuc);
                    }
                }
                return dsCongChuc;
            }
            catch (Exception ex)
            {
                return new List<CongChuc>();
            }
        }

        public List<CongChuc> TimKiem(object HoTen, object GioiTinh, object QueQuan, object ChucVu)
        {
            try
            {
                List<CongChuc> dsCongChuc = new List<CongChuc>();
                DataTable dt = da.ExecuteQuery("Proc_CongChuc_GetData", new object[] { HoTen, GioiTinh, QueQuan, ChucVu }, new List<string>() { "HoTen", "GioiTinh", "QueQuan", "ChucVu" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CongChuc congChuc = new CongChuc();
                        congChuc.MaCongChuc = String.IsNullOrEmpty(row["MaCongChuc"].ToString()) ? 0 : int.Parse(row["MaCongChuc"].ToString());
                        congChuc.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                        congChuc.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(row["NgaySinh"].ToString());
                        congChuc.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                        congChuc.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                        congChuc.ChucVu = String.IsNullOrEmpty(row["ChucVu"].ToString()) ? "" : row["ChucVu"].ToString();
                        congChuc.TaiKhoan = String.IsNullOrEmpty(row["TaiKhoan"].ToString()) ? "" : row["TaiKhoan"].ToString(); ;
                        congChuc.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? "" : row["TrangThai"].ToString();
                        congChuc.CMND = String.IsNullOrEmpty(row["CMND"].ToString()) ? "" : row["CMND"].ToString();
                        dsCongChuc.Add(congChuc);
                    }
                }
                return dsCongChuc;
            }
            catch (Exception ex)
            {
                return new List<CongChuc>();
            }
        }

        public List<CongChuc> isGetCongChucChuaCoTaiKhoan()
        {
            try
            {
                List<CongChuc> dsCongChuc = new List<CongChuc>();
                DataTable dt = da.ExecuteQuery("Proc_CongChuc_GetData", new object[] { "" }, new List<string>() { "@TaiKhoan" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CongChuc congChuc = new CongChuc();
                        congChuc.MaCongChuc = String.IsNullOrEmpty(row["MaCongChuc"].ToString()) ? 0 : int.Parse(row["MaCongChuc"].ToString());
                        congChuc.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                        congChuc.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(row["NgaySinh"].ToString());
                        congChuc.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                        congChuc.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                        congChuc.ChucVu = String.IsNullOrEmpty(row["ChucVu"].ToString()) ? "" : row["ChucVu"].ToString();
                        congChuc.TaiKhoan = String.IsNullOrEmpty(row["TaiKhoan"].ToString()) ? "" : row["TaiKhoan"].ToString(); ;
                        congChuc.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? "" : row["TrangThai"].ToString();
                        congChuc.CMND = String.IsNullOrEmpty(row["CMND"].ToString()) ? "" : row["CMND"].ToString();
                        dsCongChuc.Add(congChuc);
                    }
                }
                return dsCongChuc;
            }
            catch (Exception ex)
            {
                return new List<CongChuc>();
            }
        }

        public string ThemCongChuc(CongChuc congChuc)
        {
            try
            {
                string rs = "";
                if (String.IsNullOrEmpty(congChuc.TrangThai))
                {
                    congChuc.TrangThai = "HoatDong";
                }
                rs = da.ExecuteScalar("Proc_CongChuc_Insert", new object[] { congChuc.HoTen, congChuc.NgaySinh, congChuc.GioiTinh, congChuc.QueQuan, congChuc.ChucVu, congChuc.CMND, null, null, "", congChuc.TrangThai },
                  new List<string>() { "@HoTen", "@NgaySinh", "@GioiTinh", "@QueQuan", "@ChucVu", "@CMND", "@TaiKhoan", "@Pass", "@Quyen", "@TrangThai" });
                return rs;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public bool CheckTaiKhoan(int maCongChuc, string TaiKhoan)
        {
            try
            {
                string rs = da.ExecuteScalar("Proc_CongChuc_CheckTK", new object[] { maCongChuc, TaiKhoan },
                 new List<string>() { "@MaCongChuc", "@TaiKhoan" });
                if (int.Parse(rs) > 0)
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public string CapNhatCongChuc(CongChuc congChuc)
        {
            try
            {
                string rs = "";
                if (String.IsNullOrEmpty(congChuc.TrangThai))
                {
                    congChuc.TrangThai = "HoatDong";
                }
                int kq = da.ExecuteNonQuery("Proc_CongChuc_Update", new object[] { congChuc.MaCongChuc, congChuc.HoTen, congChuc.NgaySinh, congChuc.GioiTinh, congChuc.QueQuan, congChuc.ChucVu, congChuc.CMND, congChuc.TrangThai },
                  new List<string>() { "@MaCongChuc", "@HoTen", "@NgaySinh", "@GioiTinh", "@QueQuan", "@ChucVu", "@CMND", "@TrangThai" });
                return kq.ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public bool CapTaiKhoan(int MaCongChuc, string TaiKhoan, string MatKhau)
        {
            int kq = da.ExecuteNonQuery("Proc_CongChuc_CapTaiKhoan", new object[] { MaCongChuc, TaiKhoan, MatKhau },
                  new List<string>() { "@MaCongChuc", "@TaiKhoan", "@Pass" });
            if (kq > 0)
                return true;
            return false;
        }


        public bool CapNhatMatKhau(int MaCongChuc, string MatKhau)
        {
            int kq = da.ExecuteNonQuery("Proc_CongChuc_CapNhatMatKhau", new object[] { MaCongChuc, MatKhau },
                  new List<string>() { "@MaCongChuc", "@Pass" });
            if (kq > 0)
                return true;
            return false;
        }

        public CongChuc GetCongChucByMaCongChuc(int MaCongChuc)
        {
            try
            {
                CongChuc congChuc = null;
                DataTable dt = da.ExecuteQuery("Proc_CongChuc_GetData", new object[] { MaCongChuc }, new List<string>() { "MaCongChuc" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    var row = dt.Rows[0];
                    congChuc = new CongChuc();
                    congChuc.MaCongChuc = String.IsNullOrEmpty(row["MaCongChuc"].ToString()) ? 0 : int.Parse(row["MaCongChuc"].ToString());
                    congChuc.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                    congChuc.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(row["NgaySinh"].ToString());
                    congChuc.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                    congChuc.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                    congChuc.ChucVu = String.IsNullOrEmpty(row["ChucVu"].ToString()) ? "" : row["ChucVu"].ToString();
                    congChuc.TaiKhoan = String.IsNullOrEmpty(row["TaiKhoan"].ToString()) ? "" : row["TaiKhoan"].ToString(); ;
                    congChuc.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? "" : row["TrangThai"].ToString();
                    congChuc.CMND = String.IsNullOrEmpty(row["CMND"].ToString()) ? "" : row["CMND"].ToString();
                }
                return congChuc;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
