﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Model
{
    public class TrangThaiModel
    {
        public string GetTrangThaiByMaTrangThai(string MaTrangThai)
        {
            string result = "";
            switch (MaTrangThai.Trim())
            {
                case "0": result = "Hồ sơ được tiếp nhận, chờ xác minh"; break;
                case "1": result = "Hồ sơ đã được xác minh, chờ duyệt"; break;
                case "2": result = "Hồ sơ không được xác minh"; break;
                case "3": result = "Hồ sơ đã được duyệt, chờ trả lại cho tổ chức/cá nhân"; break;
                case "4": result = "Hồ sơ không được duyệt"; break;
                case "5": result = "Đã trả cho tổ chức/cá nhân"; break;
                case "6": result = "Đã thanh toán. Xong"; break;
            }
            return result;
        }

        public string GetTrangThaiVietTatByMaTrangThai(string MaTrangThai)
        {
            string result = "";
            switch (MaTrangThai.Trim())
            {
                case "0": result = "Đã tiếp nhận"; break;
                case "1": result = "Đã xác minh"; break;
                case "2": result = "Không xác minh"; break;
                case "3": result = "Đã duyệt"; break;
                case "4": result = "Không duyệt"; break;
                case "5": result = "Đã trả"; break;
                case "6": result = "Đã thanh toán"; break;
            }
            return result;
        }

        public List<QLHT.Entity.TrangThai> dsTrangThai()
        {
            var list = new List<QLHT.Entity.TrangThai>();
            list.Add(new QLHT.Entity.TrangThai(-1, "Tất cả"));
            list.Add(new QLHT.Entity.TrangThai(0, GetTrangThaiVietTatByMaTrangThai("0")));
            list.Add(new QLHT.Entity.TrangThai(1, GetTrangThaiVietTatByMaTrangThai("1")));
            list.Add(new QLHT.Entity.TrangThai(2, GetTrangThaiVietTatByMaTrangThai("2")));
            list.Add(new QLHT.Entity.TrangThai(3, GetTrangThaiVietTatByMaTrangThai("3")));
            list.Add(new QLHT.Entity.TrangThai(4, GetTrangThaiVietTatByMaTrangThai("4")));
            list.Add(new QLHT.Entity.TrangThai(5, GetTrangThaiVietTatByMaTrangThai("5")));
            list.Add(new QLHT.Entity.TrangThai(6, GetTrangThaiVietTatByMaTrangThai("6")));
            return list;
        }

        public enum TrangThai : int
        {
            TiepNhan,
            XacMinh,
            KhongXacMinh,
            Duyet,
            KhongDuyet,
            DaTra,
            DaThanhToan
        }
    }
}

/*
 
     Tiếp nhận hồ sơ
Đã xác minh hồ sơ
Xác minh hồ sơ thất bại
Đã duyệt
Duyệt thất bại
Xong
Ðã thanh toán
     */
