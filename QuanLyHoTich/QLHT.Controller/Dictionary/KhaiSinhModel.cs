﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLHT.Entity;


namespace QLHT.Model
{
    public class KhaiSinhModel
    {
        DataProvider dataProvider = new DataProvider();
        public bool Insert(int MaCongDan, int MaCongChucTiepNhan, int IDCha, int IDMe, DateTime NgaySinh, bool GioiTinh, DateTime NgayHen, string HoTen = "" ,string DanToc = "Kinh", string NoiSinh = "", string SoDinhDanh = "000", string QuocTich = "VietNam", string QueQuan = "Ninh Khang")
        {
            try
            {
                if (MaCongDan == -1) MaCongDan = IDCha;
                int rs = 0;
                rs = dataProvider.ExecuteNonQuery("Proc_KhaiSinh_Insert", new object[] { HoTen, NgaySinh,GioiTinh, DanToc, QuocTich, QueQuan, NoiSinh, SoDinhDanh, IDCha, IDMe, 1, NgayHen,MaCongDan, MaCongChucTiepNhan},
                  new List<string>() { "HoTen", "NgaySinh","GioiTinh", "DanToc", "QuocTich", "QueQuan", "NoiSinh", "SoDinhDanhCaNhan", "IDNguoiCha", "IDNguoiMe", "MaThuTuc","NgayHen", "MaCongDan", "MaCongChucTiepNhan" });
                if (rs > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<KhaiSinh> GetData()
        {
            try
            {
                List<KhaiSinh> dsKhaiSinh = new List<KhaiSinh>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_KhaiSinh_GetData", null, null);
                if(dt!=null && dt.Rows.Count > 0)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        KhaiSinh khaiSinh = new KhaiSinh();
                        khaiSinh.MaKhaiSinh = String.IsNullOrEmpty(row["MaKhaiSinh"].ToString()) ? 0 : int.Parse(row["MaKhaiSinh"].ToString());
                        khaiSinh.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                        khaiSinh.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgaySinh"].ToString());
                        khaiSinh.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                        khaiSinh.DanToc = String.IsNullOrEmpty(row["DanToc"].ToString()) ? "Ninh Khang" : row["DanToc"].ToString();
                        khaiSinh.QuocTich = String.IsNullOrEmpty(row["QuocTich"].ToString()) ? "Ninh Khang" : row["QuocTich"].ToString();
                        khaiSinh.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                        khaiSinh.NoiSinh = String.IsNullOrEmpty(row["NoiSinh"].ToString()) ? "Ninh Khang" : row["NoiSinh"].ToString();
                        khaiSinh.SoDinhDanhCaNhan = String.IsNullOrEmpty(row["SoDinhDanhCaNhan"].ToString()) ? "000" : row["SoDinhDanhCaNhan"].ToString();
                        khaiSinh.IDNguoiCha = String.IsNullOrEmpty(row["IDNguoiCha"].ToString()) ? 0 : int.Parse(row["IDNguoiCha"].ToString());
                        khaiSinh.IDNguoiMe = String.IsNullOrEmpty(row["IDNguoiMe"].ToString()) ? 0 : int.Parse(row["IDNguoiMe"].ToString());
                        khaiSinh.MaThuTuc = String.IsNullOrEmpty(row["MaThuTuc"].ToString()) ? 0 : int.Parse(row["MaThuTuc"].ToString());
                        khaiSinh.NgayDangKy = String.IsNullOrEmpty(row["NgayDangKy"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayDangKy"].ToString());
                        //khaiSinh.NgayHen = String.IsNullOrEmpty(row["NgayHen"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayHen"].ToString());
                        //khaiSinh.NgayTra = String.IsNullOrEmpty(row["NgayTra"].ToString()) ?  : Convert.ToDateTime(row["NgayTra"].ToString());
                        if (!String.IsNullOrEmpty(row["NgayHen"].ToString()))
                        {
                            khaiSinh.NgayHen = Convert.ToDateTime(row["NgayHen"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["NgayTra"].ToString()))
                        {
                            khaiSinh.NgayTra = Convert.ToDateTime(row["NgayTra"].ToString());
                        }
                        khaiSinh.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        khaiSinh.MaCongChucTiepNhan = String.IsNullOrEmpty(row["MaCongChucTiepNhan"].ToString()) ? 0 : int.Parse(row["MaCongChucTiepNhan"].ToString());
                        khaiSinh.MaCongChucXacMinh = String.IsNullOrEmpty(row["MaCongChucXacMinh"].ToString()) ? 0 : int.Parse(row["MaCongChucXacMinh"].ToString());
                        khaiSinh.MaLanhDao = String.IsNullOrEmpty(row["MaLanhDao"].ToString()) ? 0 : int.Parse(row["MaLanhDao"].ToString());
                        khaiSinh.MaCongChucVanPhong = String.IsNullOrEmpty(row["MaCongChucVanPhong"].ToString()) ? 0 : int.Parse(row["MaCongChucVanPhong"].ToString());
                        khaiSinh.MaCongChucKTTC = String.IsNullOrEmpty(row["MaCongChucKTTC"].ToString()) ? 0 : int.Parse(row["MaCongChucKTTC"].ToString());
                        khaiSinh.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? 0 : int.Parse(row["TrangThai"].ToString());
                        //khaiSinh.MaKhaiSinh = String.IsNullOrEmpty(row["MaKhaiSinh"].ToString()) ? 0 : int.Parse(row["MaKhaiSinh"].ToString());
                        dsKhaiSinh.Add(khaiSinh);
                    }
                    return dsKhaiSinh;
                }
                return new List<KhaiSinh>();
            }
            catch(Exception ex)
            {
                return new List<KhaiSinh>();
            }
            
        }
        /// <summary>
        /// Cần truyền vào : = '' hết. Mỗi GioiTinh = -1
        /// "HoTen","NgaySinh","GioiTinh","DanToc","QuocTich","QueQuan","NoiSinh","SoDinhDanhCaNhan", "TrangThai"
        /// </summary>
        /// <returns></returns>
        public List<KhaiSinh> TimKiem(object[] param)
        {
            try
            {
                List<KhaiSinh> dsKhaiSinh = new List<KhaiSinh>();
                List<string> listParam = new List<string>() { "HoTen","NgaySinh","GioiTinh","DanToc","QuocTich","QueQuan","NoiSinh","SoDinhDanhCaNhan","TrangThai"};
                DataTable dt = dataProvider.ExecuteQuery("Proc_KhaiSinh_GetData", param, listParam);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        KhaiSinh khaiSinh = new KhaiSinh();
                        khaiSinh.MaKhaiSinh = String.IsNullOrEmpty(row["MaKhaiSinh"].ToString()) ? 0 : int.Parse(row["MaKhaiSinh"].ToString());
                        khaiSinh.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                        khaiSinh.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgaySinh"].ToString());
                        khaiSinh.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                        khaiSinh.DanToc = String.IsNullOrEmpty(row["DanToc"].ToString()) ? "Ninh Khang" : row["DanToc"].ToString();
                        khaiSinh.QuocTich = String.IsNullOrEmpty(row["QuocTich"].ToString()) ? "Ninh Khang" : row["QuocTich"].ToString();
                        khaiSinh.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                        khaiSinh.NoiSinh = String.IsNullOrEmpty(row["NoiSinh"].ToString()) ? "Ninh Khang" : row["NoiSinh"].ToString();
                        khaiSinh.SoDinhDanhCaNhan = String.IsNullOrEmpty(row["SoDinhDanhCaNhan"].ToString()) ? "000" : row["SoDinhDanhCaNhan"].ToString();
                        khaiSinh.IDNguoiCha = String.IsNullOrEmpty(row["IDNguoiCha"].ToString()) ? 0 : int.Parse(row["IDNguoiCha"].ToString());
                        khaiSinh.IDNguoiMe = String.IsNullOrEmpty(row["IDNguoiMe"].ToString()) ? 0 : int.Parse(row["IDNguoiMe"].ToString());
                        khaiSinh.MaThuTuc = String.IsNullOrEmpty(row["MaThuTuc"].ToString()) ? 0 : int.Parse(row["MaThuTuc"].ToString());
                        khaiSinh.NgayDangKy = String.IsNullOrEmpty(row["NgayDangKy"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayDangKy"].ToString());
                        //khaiSinh.NgayHen = String.IsNullOrEmpty(row["NgayHen"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayHen"].ToString());
                        //khaiSinh.NgayTra = String.IsNullOrEmpty(row["NgayTra"].ToString()) ?  : Convert.ToDateTime(row["NgayTra"].ToString());
                        if (!String.IsNullOrEmpty(row["NgayHen"].ToString()))
                        {
                            khaiSinh.NgayHen = Convert.ToDateTime(row["NgayHen"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["NgayTra"].ToString()))
                        {
                            khaiSinh.NgayTra = Convert.ToDateTime(row["NgayTra"].ToString());
                        }
                        khaiSinh.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        khaiSinh.MaCongChucTiepNhan = String.IsNullOrEmpty(row["MaCongChucTiepNhan"].ToString()) ? 0 : int.Parse(row["MaCongChucTiepNhan"].ToString());
                        khaiSinh.MaCongChucXacMinh = String.IsNullOrEmpty(row["MaCongChucXacMinh"].ToString()) ? 0 : int.Parse(row["MaCongChucXacMinh"].ToString());
                        khaiSinh.MaLanhDao = String.IsNullOrEmpty(row["MaLanhDao"].ToString()) ? 0 : int.Parse(row["MaLanhDao"].ToString());
                        khaiSinh.MaCongChucVanPhong = String.IsNullOrEmpty(row["MaCongChucVanPhong"].ToString()) ? 0 : int.Parse(row["MaCongChucVanPhong"].ToString());
                        khaiSinh.MaCongChucKTTC = String.IsNullOrEmpty(row["MaCongChucKTTC"].ToString()) ? 0 : int.Parse(row["MaCongChucKTTC"].ToString());
                        khaiSinh.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? 0 : int.Parse(row["TrangThai"].ToString());
                        //khaiSinh.MaKhaiSinh = String.IsNullOrEmpty(row["MaKhaiSinh"].ToString()) ? 0 : int.Parse(row["MaKhaiSinh"].ToString());
                        dsKhaiSinh.Add(khaiSinh);
                    }
                    return dsKhaiSinh;
                }
                return new List<KhaiSinh>();
            }
            catch (Exception ex)
            {
                return new List<KhaiSinh>();
            }

        }

        public List<KhaiSinh> GetDataKSGiamHo()
        {
            try
            {
                List<KhaiSinh> dsKhaiSinh = new List<KhaiSinh>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_KhaiSinh_GetDataKSGiamHo", null, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        KhaiSinh khaiSinh = new KhaiSinh();
                        khaiSinh.MaKhaiSinh = String.IsNullOrEmpty(row["MaKhaiSinh"].ToString()) ? 0 : int.Parse(row["MaKhaiSinh"].ToString());
                        khaiSinh.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                        khaiSinh.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgaySinh"].ToString());
                       
                        khaiSinh.DanToc = String.IsNullOrEmpty(row["DanToc"].ToString()) ? "Ninh Khang" : row["DanToc"].ToString();
                        khaiSinh.QuocTich = String.IsNullOrEmpty(row["QuocTich"].ToString()) ? "Ninh Khang" : row["QuocTich"].ToString();
                        khaiSinh.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                       
                        
                        dsKhaiSinh.Add(khaiSinh);
                    }
                    return dsKhaiSinh;
                }
                return new List<KhaiSinh>();
            }
            catch (Exception ex)
            {
                return new List<KhaiSinh>();
            }

        }


        public bool UpdateTrangThai(int MaKhaiSinh, string MaCongChucXacMinh, string MaLanhDao, int TrangThai)
        {
            int kq = 0;
            if (!String.IsNullOrEmpty(MaCongChucXacMinh))
            {
                kq = dataProvider.ExecuteNonQuery("Proc_KhaiSinh_UpdateTrangThai", new object[] {MaKhaiSinh, MaCongChucXacMinh, TrangThai }, new List<string>() {"MaKhaiSinh", "MaCongChucXacMinh", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                kq = dataProvider.ExecuteNonQuery("Proc_KhaiSinh_UpdateTrangThai", new object[] { MaKhaiSinh, MaLanhDao, TrangThai }, new List<string>() { "MaKhaiSinh","MaLanhDao", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UpdateTrangThai(int MaKhaiSinh, string MaCongChuc, DateTime NgayTra ,int TrangThai)
        {
            int kq = 0;
            kq = dataProvider.ExecuteNonQuery("Proc_KhaiSinh_UpdateTrangThai", new object[] { MaKhaiSinh, MaCongChuc, NgayTra ,TrangThai }, new List<string>() { "MaKhaiSinh", "MaCongChucVanPhong", "NgayTra" ,"TrangThai" });
            if (kq > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool update_KhaiSinh_FromDangKyThayDoi(string CMND , string HoTen ,DateTime NgaySinh, string DanToc,bool GioiTinh , string QueQuan , string QuocTich )
        {
            int kq = 0;
            kq = dataProvider.ExecuteNonQuery("update_KhaiSinh_FromDangKyThayDoi", new object[] {CMND,HoTen,NgaySinh,DanToc,GioiTinh,QueQuan, QuocTich }, new List<string>() { "CMND", "HoTen", "NgaySinh", "DanToc", "GioiTinh", "QueQuan", "QuocTich" });
            if (kq > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdateThuTuc(int MaKhaiSinh, int MaThuTuc)
        {
            int kq = 0;
            kq = dataProvider.ExecuteNonQuery("Proc_KhaiSinh_UpdateThuTuc", new object[] { MaKhaiSinh, MaThuTuc}, new List<string>() { "MaKhaiSinh", "MaThuTuc" });
            if (kq > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
