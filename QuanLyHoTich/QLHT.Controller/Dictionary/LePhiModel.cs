﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLHT.Entity;


namespace QLHT.Model
{
    public class LePhiModel
    {
        DataProvider dataProvider = new DataProvider();
        public List<LePhi> GetData()
        {
            try
            {
                List<LePhi> dsLePhi = new List<LePhi>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_LePhi_GetData", null, null);
                if(dt!=null && dt.Rows.Count > 0)
                {
                    foreach(DataRow row in dt.Rows) 
                    {
                        LePhi lePhi = new LePhi();
                        lePhi.MaLePhi = String.IsNullOrEmpty(row["MaLePhi"].ToString()) ? 0 : int.Parse(row["MaLePhi"].ToString());
                        lePhi.NgayLap = String.IsNullOrEmpty(row["NgayLap"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayLap"].ToString());
                        lePhi.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        lePhi.MaCongChuc = String.IsNullOrEmpty(row["MaCongChuc"].ToString()) ? 0 : int.Parse(row["MaCongChuc"].ToString());
                        lePhi.HoTenCC = String.IsNullOrEmpty(row["HoTenCC"].ToString()) ? "" : row["HoTenCC"].ToString();
                        lePhi.HoTenCD = String.IsNullOrEmpty(row["HoTenCD"].ToString()) ? "" : row["HoTenCD"].ToString();
                        dsLePhi.Add(lePhi);
                    }
                    return dsLePhi;
                }
                return new List<LePhi>();
            }
            catch(Exception ex)
            {
                return new List<LePhi>();
            }
            
        }

        public string ThemLePhi(LePhi lePhi)
        {
            try
            {
                string rs = "";
                rs = dataProvider.ExecuteScalar("Proc_LePhi_Insert", new object[] { lePhi.MaCongDan, lePhi.MaCongChuc, lePhi.NgayLap},
                  new List<string>() { "@MaCongDan", "@MaCongChuc", "@NgayLap"});
                return rs;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public bool UpdateLePhi(LePhi lePhi)
        {
            try
            {
                int kq = 0;
                kq = dataProvider.ExecuteNonQuery("Proc_LePhi_Update", new object[] { lePhi.MaLePhi, lePhi.MaCongDan, lePhi.MaCongChuc, lePhi.NgayLap},
                  new List<string>() { "@MaLePhi", "@MaCongDan", "@MaCongChuc", "@NgayLap"});
                if (kq > 0)
                    return true;
                else return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
