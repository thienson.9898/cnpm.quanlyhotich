﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLHT.Entity;


namespace QLHT.Model
{
    public class KetHonModel
    {
        DataProvider dataProvider = new DataProvider();
        public bool Insert(int MaCongDan, int MaCongChucTiepNhan, int IDNguoiChong, int IDNguoiVo, DateTime NgayHen)
        {
            try
            {
                if (MaCongDan == -1) MaCongDan = IDNguoiChong;
                int rs = 0;
                rs = dataProvider.ExecuteNonQuery("Proc_KetHon_Insert", new object[] { IDNguoiChong, IDNguoiVo, 1, NgayHen,MaCongDan, MaCongChucTiepNhan},
                  new List<string>() {"IDNguoiChong", "IDNguoiVo", "MaThuTuc","NgayHen", "MaCongDan", "MaCongChucTiepNhan" });
                if (rs > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<KetHon> GetData()
        {
            try
            {
                List<KetHon> dsKetHon = new List<KetHon>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_KetHon_GetData", null, null);
                if(dt!=null && dt.Rows.Count > 0)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        KetHon ketHon = new KetHon();
                        ketHon.MaKH = String.IsNullOrEmpty(row["MaKH"].ToString()) ? 0 : int.Parse(row["MaKH"].ToString());
                        ketHon.IDNguoiChong = String.IsNullOrEmpty(row["IDNguoiChong"].ToString()) ? 0 : int.Parse(row["IDNguoiChong"].ToString());
                        ketHon.IDNguoiVo = String.IsNullOrEmpty(row["IDNguoiVo"].ToString()) ? 0 : int.Parse(row["IDNguoiVo"].ToString());
                        ketHon.MaThuTuc = String.IsNullOrEmpty(row["MaThuTuc"].ToString()) ? 0 : int.Parse(row["MaThuTuc"].ToString());
                        ketHon.NgayDangKy = String.IsNullOrEmpty(row["NgayDangKy"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayDangKy"].ToString());
                        //khaiSinh.NgayHen = String.IsNullOrEmpty(row["NgayHen"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayHen"].ToString());
                        //khaiSinh.NgayTra = String.IsNullOrEmpty(row["NgayTra"].ToString()) ?  : Convert.ToDateTime(row["NgayTra"].ToString());
                        if (!String.IsNullOrEmpty(row["NgayHen"].ToString()))
                        {
                            ketHon.NgayHen = Convert.ToDateTime(row["NgayHen"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["NgayTra"].ToString()))
                        {
                            ketHon.NgayTra = Convert.ToDateTime(row["NgayTra"].ToString());
                        }
                        ketHon.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        ketHon.MaCongChucTiepNhan = String.IsNullOrEmpty(row["MaCongChucTiepNhan"].ToString()) ? 0 : int.Parse(row["MaCongChucTiepNhan"].ToString());
                        ketHon.MaCongChucXacMinh = String.IsNullOrEmpty(row["MaCongChucXacMinh"].ToString()) ? 0 : int.Parse(row["MaCongChucXacMinh"].ToString());
                        ketHon.MaLanhDao = String.IsNullOrEmpty(row["MaLanhDao"].ToString()) ? 0 : int.Parse(row["MaLanhDao"].ToString());
                        ketHon.MaCongChucVanPhong = String.IsNullOrEmpty(row["MaCongChucVanPhong"].ToString()) ? 0 : int.Parse(row["MaCongChucVanPhong"].ToString());
                        ketHon.MaCongChucKTTC = String.IsNullOrEmpty(row["MaCongChucKTTC"].ToString()) ? 0 : int.Parse(row["MaCongChucKTTC"].ToString());
                        ketHon.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? 0 : int.Parse(row["TrangThai"].ToString());
                        //khaiSinh.MaKhaiSinh = String.IsNullOrEmpty(row["MaKhaiSinh"].ToString()) ? 0 : int.Parse(row["MaKhaiSinh"].ToString());
                        dsKetHon.Add(ketHon);
                    }
                    return dsKetHon;
                }
                return new List<KetHon>();
            }
            catch(Exception ex)
            {
                return new List<KetHon>();
            }
            
        }

        public bool UpdateTrangThai(int MaKH, string MaCongChucXacMinh, string MaLanhDao, int TrangThai)
        {
            int kq = 0;
            if (!String.IsNullOrEmpty(MaCongChucXacMinh))
            {
                kq = dataProvider.ExecuteNonQuery("Proc_KetHon_UpdateTrangThai", new object[] {MaKH, MaCongChucXacMinh, TrangThai }, new List<string>() {"MaKH", "MaCongChucXacMinh", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                kq = dataProvider.ExecuteNonQuery("Proc_KetHon_UpdateTrangThai", new object[] { MaKH, MaLanhDao, TrangThai }, new List<string>() { "MaKH","MaLanhDao", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UpdateTrangThai(int MaKH, string MaCongChuc, DateTime NgayTra ,int TrangThai)
        {
            int kq = 0;
            kq = dataProvider.ExecuteNonQuery("Proc_KetHon_UpdateTrangThai", new object[] { MaKH, MaCongChuc, NgayTra ,TrangThai }, new List<string>() { "MaKH", "MaCongChucVanPhong", "NgayTra" ,"TrangThai" });
            if (kq > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
