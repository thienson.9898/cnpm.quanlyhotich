﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLHT.Entity;

namespace QLHT.Model.Dictionary
{
    public class KhaiTuModel
    {
        DataProvider dataProvider = new DataProvider();
        public bool Insert(int IdNguoiChet, int MaCongDan, int MaCongChucTiepNhan, DateTime ThoiGianChet, DateTime NgayHen, string NoiCuTruCuoiCung, string NoiChet, string NguyenNhan)
        {
            try
            {

                int rs = 0;
                rs = dataProvider.ExecuteNonQuery("Proc_KhaiTu_Insert", new object[] { IdNguoiChet, NoiCuTruCuoiCung, ThoiGianChet, NoiChet, NguyenNhan, 1, NgayHen, MaCongDan, MaCongChucTiepNhan },
                  new List<string>() { "IdNguoiChet", "NoiCuTruCuoiCung", "ThoiGianChet", "NoiChet", "NguyenNhan", "MaThuTuc", "NgayHen", "MaCongDan", "MaCongChucTiepNhan" });
                if (rs > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<KhaiTu> GetData()
        {
            try
            {
                List<KhaiTu> dsKhaiTu = new List<KhaiTu>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_KhaiTu_GetData", null, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        KhaiTu khaiTu = new KhaiTu();
                        khaiTu.MaKT = String.IsNullOrEmpty(row["MaKT"].ToString()) ? 0 : int.Parse(row["MaKT"].ToString());

                        if (!String.IsNullOrEmpty(row["NgayHen"].ToString()))
                        {
                            khaiTu.NgayHen = Convert.ToDateTime(row["NgayHen"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["NgayTra"].ToString()))
                        {
                            khaiTu.NgayTra = Convert.ToDateTime(row["NgayTra"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["ThoiGianChet"].ToString()))
                        {
                            khaiTu.ThoiGianChet = Convert.ToDateTime(row["ThoiGianChet"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["NgayDangKy"].ToString()))
                        {
                            khaiTu.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"].ToString());
                        }
                        khaiTu.MaThuTuc = String.IsNullOrEmpty(row["MaThuTuc"].ToString()) ? 0 : int.Parse(row["MaThuTuc"].ToString());
                        khaiTu.NguyenNhan = String.IsNullOrEmpty(row["NguyenNhan"].ToString()) ? "Không rõ" : row["NguyenNhan"].ToString();
                        khaiTu.NoiChet = String.IsNullOrEmpty(row["NoiChet"].ToString()) ? "Không rõ" : row["NoiChet"].ToString();
                        khaiTu.NoiCuTruCuoiCung = String.IsNullOrEmpty(row["NoiCuTruCuoiCung"].ToString()) ? "Không rõ" : row["NoiCuTruCuoiCung"].ToString();
                        khaiTu.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        khaiTu.IDNguoiChet = String.IsNullOrEmpty(row["IdNguoiChet"].ToString()) ? 0 : int.Parse(row["IdNguoiChet"].ToString());
                        khaiTu.MaCongChucTiepNhan = String.IsNullOrEmpty(row["MaCongChucTiepNhan"].ToString()) ? 0 : int.Parse(row["MaCongChucTiepNhan"].ToString());
                        khaiTu.MaCongChucXacMinh = String.IsNullOrEmpty(row["MaCongChucXacMinh"].ToString()) ? 0 : int.Parse(row["MaCongChucXacMinh"].ToString());
                        khaiTu.MaLanhDao = String.IsNullOrEmpty(row["MaLanhDao"].ToString()) ? 0 : int.Parse(row["MaLanhDao"].ToString());
                        khaiTu.MaCongChucVanPhong = String.IsNullOrEmpty(row["MaCongChucVanPhong"].ToString()) ? 0 : int.Parse(row["MaCongChucVanPhong"].ToString());
                        khaiTu.MaCongChucKTTC = String.IsNullOrEmpty(row["MaCongChucKTTC"].ToString()) ? 0 : int.Parse(row["MaCongChucKTTC"].ToString());
                        khaiTu.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? 0 : int.Parse(row["TrangThai"].ToString());
                        dsKhaiTu.Add(khaiTu);
                    }
                    return dsKhaiTu;
                }
                return new List<KhaiTu>();
            }
            catch (Exception ex)
            {
                return new List<KhaiTu>();
            }

        }

        public bool UpdateTrangThai(int MaKhaiTu, string MaCongChucXacMinh, string MaLanhDao, int TrangThai)
        {
            int kq = 0;
            if (!String.IsNullOrEmpty(MaCongChucXacMinh))
            {
                kq = dataProvider.ExecuteNonQuery("Proc_KhaiTu_UpdateTrangThai", new object[] { MaKhaiTu, MaCongChucXacMinh, TrangThai }, new List<string>() { "MaKT", "MaCongChucXacMinh", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                kq = dataProvider.ExecuteNonQuery("Proc_KhaiTu_UpdateTrangThai", new object[] { MaKhaiTu, MaLanhDao, TrangThai }, new List<string>() { "MaKT", "MaLanhDao", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UpdateTrangThai(int MaKhaiTu, string MaCongChuc, DateTime NgayTra, int TrangThai)
        {
            int kq = 0;
            kq = dataProvider.ExecuteNonQuery("Proc_KhaiTu_UpdateTrangThai", new object[] { MaKhaiTu, MaCongChuc, NgayTra, TrangThai }, new List<string>() { "MaKT", "MaCongChucVanPhong", "NgayTra", "TrangThai" });
            if (kq > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<KhaiTu> TimKiem(int MaTrangThai)
        {


            try
            {
               
                        List<KhaiTu> dsKhaiTu = new List<KhaiTu>();
                DataTable dt = dataProvider.ExecuteQuery2("TimKiemKhaiTu ","TrangThai",MaTrangThai );
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        KhaiTu khaiTu = new KhaiTu();
                        khaiTu.MaKT = String.IsNullOrEmpty(row["MaKT"].ToString()) ? 0 : int.Parse(row["MaKT"].ToString());

                        if (!String.IsNullOrEmpty(row["NgayHen"].ToString()))
                        {
                            khaiTu.NgayHen = Convert.ToDateTime(row["NgayHen"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["NgayTra"].ToString()))
                        {
                            khaiTu.NgayTra = Convert.ToDateTime(row["NgayTra"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["ThoiGianChet"].ToString()))
                        {
                            khaiTu.ThoiGianChet = Convert.ToDateTime(row["ThoiGianChet"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["NgayDangKy"].ToString()))
                        {
                            khaiTu.NgayDangKy = Convert.ToDateTime(row["NgayDangKy"].ToString());
                        }
                        khaiTu.MaThuTuc = String.IsNullOrEmpty(row["MaThuTuc"].ToString()) ? 0 : int.Parse(row["MaThuTuc"].ToString());
                        khaiTu.NguyenNhan = String.IsNullOrEmpty(row["NguyenNhan"].ToString()) ? "Không rõ" : row["NguyenNhan"].ToString();
                        khaiTu.NoiChet = String.IsNullOrEmpty(row["NoiChet"].ToString()) ? "Không rõ" : row["NoiChet"].ToString();
                        khaiTu.NoiCuTruCuoiCung = String.IsNullOrEmpty(row["NoiCuTruCuoiCung"].ToString()) ? "Không rõ" : row["NoiCuTruCuoiCung"].ToString();
                        khaiTu.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        khaiTu.IDNguoiChet = String.IsNullOrEmpty(row["IdNguoiChet"].ToString()) ? 0 : int.Parse(row["IdNguoiChet"].ToString());
                        khaiTu.MaCongChucTiepNhan = String.IsNullOrEmpty(row["MaCongChucTiepNhan"].ToString()) ? 0 : int.Parse(row["MaCongChucTiepNhan"].ToString());
                        khaiTu.MaCongChucXacMinh = String.IsNullOrEmpty(row["MaCongChucXacMinh"].ToString()) ? 0 : int.Parse(row["MaCongChucXacMinh"].ToString());
                        khaiTu.MaLanhDao = String.IsNullOrEmpty(row["MaLanhDao"].ToString()) ? 0 : int.Parse(row["MaLanhDao"].ToString());
                        khaiTu.MaCongChucVanPhong = String.IsNullOrEmpty(row["MaCongChucVanPhong"].ToString()) ? 0 : int.Parse(row["MaCongChucVanPhong"].ToString());
                        khaiTu.MaCongChucKTTC = String.IsNullOrEmpty(row["MaCongChucKTTC"].ToString()) ? 0 : int.Parse(row["MaCongChucKTTC"].ToString());
                        khaiTu.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? 0 : int.Parse(row["TrangThai"].ToString());
                        dsKhaiTu.Add(khaiTu);
                    }
                    return dsKhaiTu;
                }
                return new List<KhaiTu>();
            }
            catch (Exception ex)
            {
                return new List<KhaiTu>();
            }

        
    } 










    }
}
