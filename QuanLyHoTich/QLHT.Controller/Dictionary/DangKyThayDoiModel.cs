﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLHT.Entity;
using QLHT.Entity.Dictionary;

namespace QLHT.Model
{
    public class DangKyThayDoiModel
    {    
        DataProvider dataProvider = new DataProvider();
        public List<DangKyThayDoi> GetData()
        {
            try
            {
                List<DangKyThayDoi> dsDangKyThayDoi = new List<DangKyThayDoi>();
                DataTable dt = dataProvider.ExecuteQuery("select_DSDangKyThayDoi", null, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        DangKyThayDoi dangKyThayDoi = new DangKyThayDoi();
                        dangKyThayDoi.MaThayDoi = String.IsNullOrEmpty(row["MaThayDoi"].ToString()) ? 0 : int.Parse(row["MaThayDoi"].ToString());
                        dangKyThayDoi.MaCongDanYC = String.IsNullOrEmpty(row["MaCongDanYC"].ToString()) ? 0 : int.Parse(row["MaCongDanYC"].ToString());
                        dangKyThayDoi.MaCongDanDN = String.IsNullOrEmpty(row["MaCongDanDN"].ToString()) ? 0 : int.Parse(row["MaCongDanDN"].ToString());
                        dangKyThayDoi.QuanHeVoiNDN = String.IsNullOrEmpty(row["QuanHeVoiNDN"].ToString()) ? "Quan hệ" : row["QuanHeVoiNDN"].ToString();
                        dangKyThayDoi.MucCanThayDoi = String.IsNullOrEmpty(row["MucCanThayDoi"].ToString()) ? "" : row["MucCanThayDoi"].ToString();
                        dangKyThayDoi.LyDo = String.IsNullOrEmpty(row["LyDo"].ToString()) ? "" : row["LyDo"].ToString();
                        dangKyThayDoi.GiaTriCu = String.IsNullOrEmpty(row["GiaTriCu"].ToString()) ? "" : row["GiaTriCu"].ToString();
                        dangKyThayDoi.GiaTriMoi = String.IsNullOrEmpty(row["GiaTriMoi"].ToString()) ? "" : row["GiaTriMoi"].ToString();
                        dangKyThayDoi.MaThuTuc = String.IsNullOrEmpty(row["MaThuTuc"].ToString()) ? 0 : int.Parse(row["MaThuTuc"].ToString());
                        dangKyThayDoi.NgayDangKy = String.IsNullOrEmpty(row["NgayDangKy"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayDangKy"].ToString());
                        //khaiSinh.NgayHen = String.IsNullOrEmpty(row["NgayHen"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayHen"].ToString());
                        //dangKyThayDoi.NgayTra = String.IsNullOrEmpty(row["NgayTra"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayTra"].ToString());
                        if (!String.IsNullOrEmpty(row["NgayHen"].ToString()))
                        {
                            dangKyThayDoi.NgayHen = Convert.ToDateTime(row["NgayHen"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["NgayTra"].ToString()))
                        {
                            dangKyThayDoi.NgayTra = Convert.ToDateTime(row["NgayTra"].ToString());
                        }
                        //  khaiSinh.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        dangKyThayDoi.MaCongChucTiepNhan = String.IsNullOrEmpty(row["MaCongChucTiepNhan"].ToString()) ? 0 : int.Parse(row["MaCongChucTiepNhan"].ToString());
                        dangKyThayDoi.MaCongChucXacMinh = String.IsNullOrEmpty(row["MaCongChucXacMinh"].ToString()) ? 0 : int.Parse(row["MaCongChucXacMinh"].ToString());
                        dangKyThayDoi.MaLanhDao = String.IsNullOrEmpty(row["MaLanhDao"].ToString()) ? 0 : int.Parse(row["MaLanhDao"].ToString());
                        dangKyThayDoi.MaCongChucVanPhong = String.IsNullOrEmpty(row["MaCongChucVanPhong"].ToString()) ? 0 : int.Parse(row["MaCongChucVanPhong"].ToString());
                        dangKyThayDoi.MaCongChucKTTC = String.IsNullOrEmpty(row["MaCongChucKTTC"].ToString()) ? 0 : int.Parse(row["MaCongChucKTTC"].ToString());
                        dangKyThayDoi.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? 0 : int.Parse(row["TrangThai"].ToString());
                        //khaiSinh.MaKhaiSinh = String.IsNullOrEmpty(row["MaKhaiSinh"].ToString()) ? 0 : int.Parse(row["MaKhaiSinh"].ToString());
                        dsDangKyThayDoi.Add(dangKyThayDoi);
                    }
                    return dsDangKyThayDoi;
                }
                return new List<DangKyThayDoi>();
            }
            catch (Exception ex)
            {
                return new List<DangKyThayDoi>();
            }

        }
        public List<DangKyThayDoi> TimKiem(object[] param)
        {
            try
            {
                List<DangKyThayDoi> dsDangKyThayDoi = new List<DangKyThayDoi>();
                List<string> listParam = new List<string> { "MaTrangThai" };
                DataTable dt = dataProvider.ExecuteQuery("select_DSDangKyThayDoi_TheoMa", param,listParam);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        DangKyThayDoi dangKyThayDoi = new DangKyThayDoi();
                        dangKyThayDoi.MaThayDoi = String.IsNullOrEmpty(row["MaThayDoi"].ToString()) ? 0 : int.Parse(row["MaThayDoi"].ToString());
                        dangKyThayDoi.MaCongDanYC = String.IsNullOrEmpty(row["MaCongDanYC"].ToString()) ? 0 : int.Parse(row["MaCongDanYC"].ToString());
                        dangKyThayDoi.MaCongDanDN = String.IsNullOrEmpty(row["MaCongDanDN"].ToString()) ? 0 : int.Parse(row["MaCongDanDN"].ToString());
                        dangKyThayDoi.QuanHeVoiNDN = String.IsNullOrEmpty(row["QuanHeVoiNDN"].ToString()) ? "Quan hệ" : row["QuanHeVoiNDN"].ToString();
                        dangKyThayDoi.MucCanThayDoi = String.IsNullOrEmpty(row["MucCanThayDoi"].ToString()) ? "" : row["MucCanThayDoi"].ToString();
                        dangKyThayDoi.LyDo = String.IsNullOrEmpty(row["LyDo"].ToString()) ? "" : row["LyDo"].ToString();
                        dangKyThayDoi.GiaTriCu = String.IsNullOrEmpty(row["GiaTriCu"].ToString()) ? "" : row["GiaTriCu"].ToString();
                        dangKyThayDoi.GiaTriMoi = String.IsNullOrEmpty(row["GiaTriMoi"].ToString()) ? "" : row["GiaTriMoi"].ToString();
                        dangKyThayDoi.MaThuTuc = String.IsNullOrEmpty(row["MaThuTuc"].ToString()) ? 0 : int.Parse(row["MaThuTuc"].ToString());
                        dangKyThayDoi.NgayDangKy = String.IsNullOrEmpty(row["NgayDangKy"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayDangKy"].ToString());
                        //khaiSinh.NgayHen = String.IsNullOrEmpty(row["NgayHen"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayHen"].ToString());
                        //dangKyThayDoi.NgayTra = String.IsNullOrEmpty(row["NgayTra"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayTra"].ToString());
                        if (!String.IsNullOrEmpty(row["NgayHen"].ToString()))
                        {
                            dangKyThayDoi.NgayHen = Convert.ToDateTime(row["NgayHen"].ToString());
                        }
                        if (!String.IsNullOrEmpty(row["NgayTra"].ToString()))
                        {
                            dangKyThayDoi.NgayTra = Convert.ToDateTime(row["NgayTra"].ToString());
                        }
                        //  khaiSinh.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        dangKyThayDoi.MaCongChucTiepNhan = String.IsNullOrEmpty(row["MaCongChucTiepNhan"].ToString()) ? 0 : int.Parse(row["MaCongChucTiepNhan"].ToString());
                        dangKyThayDoi.MaCongChucXacMinh = String.IsNullOrEmpty(row["MaCongChucXacMinh"].ToString()) ? 0 : int.Parse(row["MaCongChucXacMinh"].ToString());
                        dangKyThayDoi.MaLanhDao = String.IsNullOrEmpty(row["MaLanhDao"].ToString()) ? 0 : int.Parse(row["MaLanhDao"].ToString());
                        dangKyThayDoi.MaCongChucVanPhong = String.IsNullOrEmpty(row["MaCongChucVanPhong"].ToString()) ? 0 : int.Parse(row["MaCongChucVanPhong"].ToString());
                        dangKyThayDoi.MaCongChucKTTC = String.IsNullOrEmpty(row["MaCongChucKTTC"].ToString()) ? 0 : int.Parse(row["MaCongChucKTTC"].ToString());
                        dangKyThayDoi.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? 0 : int.Parse(row["TrangThai"].ToString());
                        //khaiSinh.MaKhaiSinh = String.IsNullOrEmpty(row["MaKhaiSinh"].ToString()) ? 0 : int.Parse(row["MaKhaiSinh"].ToString());
                        dsDangKyThayDoi.Add(dangKyThayDoi);
                    }
                    return dsDangKyThayDoi;
                }
                return new List<DangKyThayDoi>();
            }
            catch (Exception ex)
            {
                return new List<DangKyThayDoi>();
            }

        }
        public bool UpdateTrangThai(int MaThayDoi,int MaCongDanDN, string HoTen, string QueQuan, string DanToc,string QuocTich,DateTime NgaySinh,  string MaCongChucXacMinh, string MaLanhDao, int TrangThai)
        {
            int kq = 0;
            if (!String.IsNullOrEmpty(MaCongChucXacMinh))
            {
                kq = dataProvider.ExecuteNonQuery("update_DangKyThayDoi_Update_TrangThai", new object[] { MaThayDoi, MaCongDanDN, HoTen, QueQuan, DanToc, QuocTich, NgaySinh, MaCongChucXacMinh,MaLanhDao, TrangThai }, new List<string>() { "MaThayDoi", "MaCongDanDN", "HoTen", "QueQuan", "DanToc", "QuocTich", "NgaySinh", "MaCongChucXacMinh", "MaLanhDao", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                kq = dataProvider.ExecuteNonQuery("update_DangKyThayDoi_Update_TrangThai", new object[] { MaThayDoi, MaCongDanDN, HoTen, QueQuan, DanToc, QuocTich, NgaySinh, MaCongChucXacMinh,MaLanhDao, TrangThai }, new List<string>() { "MaThayDoi", "MaCongDanDN", "HoTen", "QueQuan", "DanToc", "QuocTich", "NgaySinh", "MaCongChucXacMinh", "MaLanhDao", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UpdateTrangThai(int MaThayDoi, string MaCongChucXacMinh, string MaLanhDao, int TrangThai)
        {
            int kq = 0;
            if (!String.IsNullOrEmpty(MaCongChucXacMinh))
            {
                kq = dataProvider.ExecuteNonQuery("update_DangKyThayDoi_Update_TrangThai", new object[] { MaThayDoi, MaCongChucXacMinh, TrangThai }, new List<string>() { "MaThayDoi", "MaCongChucXacMinh", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                kq = dataProvider.ExecuteNonQuery("update_DangKyThayDoi_Update_TrangThai", new object[] { MaThayDoi, MaLanhDao, TrangThai }, new List<string>() { "MaThayDoi", "MaLanhDao", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public bool UpdateTrangThai(int MaThayDoi, string MaCongChuc, DateTime NgayTra, int TrangThai)
        {
            int kq = 0;
            kq = dataProvider.ExecuteNonQuery("update_DangKyThayDoi_Update_TrangThai", new object[] { MaThayDoi, MaCongChuc, NgayTra, TrangThai }, new List<string>() { "MaThayDoi", "MaCongChucVanPhong", "NgayTra", "TrangThai" });
            if (kq > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Update_CongDan_FromDangKyThaDoi( int MaCongDanDN, string HoTen, DateTime NgaySinh, string DanToc, string QueQuan, string QuocTich)
        {
            int kq = 0;
         //  if (!String.IsNullOrEmpty(MaCongChucXacMinh))
         //   {
                kq = dataProvider.ExecuteNonQuery("update_CongDan_FromDangKyThayDoi", new object[] {MaCongDanDN, HoTen, NgaySinh, DanToc, QueQuan, QuocTich}, new List<string>() {"MaCongDanDN", "HoTen", "NgaySinh", "DanToc", "QueQuan", "QuocTich"});
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
         
        }
    }
}
