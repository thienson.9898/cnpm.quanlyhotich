﻿using QLHT.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Model
{
    public static class BoPhanModel
    {       
        public static string BoPhanTiepNhanVaTraKQ = "Bộ phận tiếp nhận và trả KQ";
        public static string CongChucCapXa = "Công chức cấp xã";
        public static string LanhDao = "Lãnh đạo";
        public static string BoPhanVanPhong = "Bộ phận văn phòng";
        public static string BoPhanKTTC = "Bộ phận tài chính - kế toán";
        
        public static string TenVietTatBoPhanTiepNhanVaTraKQ = "TN&TKQ";
        public static string TenVietTatCongChucCapXa = "CCCX";
        public static string TenVietTatLanhDao = "LD";
        public static string TenVietTatBoPhanVanPhong = "VP";
        public static string TenVietTatBoPhanKTTC = "KTTC";

        public static List<BoPhan> GetData()
        {
            DataProvider dataProvider = new DataProvider();
            DataTable dt = dataProvider.ExecuteQuery("Proc_BoPhan_GetData");
            List<BoPhan> dsBoPhan = new List<BoPhan>();
            if(dt!=null && dt.Rows.Count > 0)
            {
                foreach(DataRow row in dt.Rows)
                {
                    dsBoPhan.Add(new BoPhan(int.Parse(row["MaBoPhan"].ToString()), row["TenBoPhan"].ToString(), row["TenVietTat"].ToString()));
                }
                return dsBoPhan;
            }
            return null;
        }
    }
}
