﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Model.Dictionary
{
    public class ThuTucModel
    {
        DataProvider dataProvider = new DataProvider();
        public int Insert(string TenThuTuc)
        {
            int kq = -1;
            try
            {
                var rs = dataProvider.ExecuteScalar("Proc_ThuTuc_Insert", new object[] { TenThuTuc }, new List<string>() { "TenThuTuc" });
                kq = int.Parse(rs.ToString());
            }
            catch (Exception ex)
            {
                kq = -1;
            }
            return kq;

        }
    }
}
