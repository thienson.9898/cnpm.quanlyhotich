﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLHT.Entity;

namespace QLHT.Model
{
    public class CTLPModel
    {
        DataProvider dataProvider = new DataProvider();
        public List<CTLP> GetData()
        {
            try
            {
                List<CTLP> dsCTLP = new List<CTLP>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_CTLePhi_GetData", null, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CTLP ctlp = new CTLP();
                        ctlp.MaCTLePhi = String.IsNullOrEmpty(row["MaLePhi"].ToString()) ? 0 : int.Parse(row["MaCTLePhi"].ToString());
                        ctlp.MaThuTuc = String.IsNullOrEmpty(row["MaThuTuc"].ToString()) ? 0 : int.Parse(row["MaThuTuc"].ToString());
                        ctlp.MaLePhi = String.IsNullOrEmpty(row["MaLePhi"].ToString()) ? 0 : int.Parse(row["MaLePhi"].ToString());
                        ctlp.LyDoNop = String.IsNullOrEmpty(row["LyDoNop"].ToString()) ? "" : row["LyDoNop"].ToString();
                        ctlp.HoTenTT = String.IsNullOrEmpty(row["TenThuTuc"].ToString()) ? "" : row["TenThuTuc"].ToString();
                        ctlp.SoTienNop = String.IsNullOrEmpty(row["SoTienNop"].ToString()) ? 0 : float.Parse(row["SoTienNop"].ToString());
                        dsCTLP.Add(ctlp);
                    }
                    return dsCTLP;
                }
                return new List<CTLP>();
            }
            catch (Exception ex)
            {
                return new List<CTLP>();
            }

        }

        public string ThemCTLePhi(CTLP ctlp)
        {
            try
            {
                string rs = "";
                rs = dataProvider.ExecuteScalar("Proc_CTLePhi_Insert", new object[] { ctlp.MaThuTuc, ctlp.MaLePhi, ctlp.SoTienNop, ctlp.LyDoNop },
                  new List<string>() { "@MaThuTuc", "@MaLePhi", "@SoTienNop", "@LyDoNop" });
                return rs;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public bool UpdateCTLePhi(CTLP ctlp)
        {
            try
            {
                int kq = 0;
                kq = dataProvider.ExecuteNonQuery("Proc_CTLePhi_Update", new object[] { ctlp.MaCTLePhi, ctlp.MaThuTuc, ctlp.MaLePhi, ctlp.SoTienNop, ctlp.LyDoNop},
                  new List<string>() { "@MaCTLePhi", "@MaThuTuc", "@MaLePhi", "@SoTienNop", "@LyDoNop" });
                if (kq > 0)
                    return true;
                else return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
