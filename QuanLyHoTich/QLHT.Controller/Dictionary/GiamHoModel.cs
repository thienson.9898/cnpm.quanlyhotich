﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLHT.Entity;

namespace QLHT.Model.Dictionary
{
   public class GiamHoModel
    {
        DataProvider dataProvider = new DataProvider();
        public bool Insert(int idNgGiamHo, int idNGDcGiamHo, int maTHuTuc, DateTime NgayHen, int  maCD,int maCCTN, int trangThai)
        {
            try
            {
               
                int rs = 0;
                rs = dataProvider.ExecuteNonQuery("proc_insertGiamHo", new object[] { idNgGiamHo, idNGDcGiamHo, maTHuTuc, NgayHen, maCD,maCCTN, trangThai},
                  new List<string>() { "idNgGiamHo", "idNgDcGiamHo", "maThuTuc", "ngHen", "maCongDan", "maCongCHucTN", "trangThai" });
                if (rs > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        

        
        }

        public List<GiamHo> GetDataKSGiamHo()
        {
            try
            {
                List<GiamHo> dsKhaiSinh = new List<GiamHo>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_GIamHo_GetData", null, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        GiamHo khaiSinh = new GiamHo();
                        
                        khaiSinh.MaGH = String.IsNullOrEmpty(row["MaGH"].ToString()) ? 0 : int.Parse(row["MaGH"].ToString());
                        khaiSinh.IDNguoiGiamHo = String.IsNullOrEmpty(row["IDNguoiGiamHo"].ToString()) ? 0 : int.Parse(row["IDNguoiGiamHo"].ToString());
                        khaiSinh.IDNguoiDuocGiamHo = String.IsNullOrEmpty(row["IDNguoiDuocGiamHo"].ToString()) ? 0 : int.Parse(row["IDNguoiDuocGiamHo"].ToString());
                        khaiSinh.NgayDangKy = String.IsNullOrEmpty(row["NgayDangKy"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayDangKy"].ToString());
                        khaiSinh.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? 0 : int.Parse(row["TrangThai"].ToString());
                        khaiSinh.NgayHen = String.IsNullOrEmpty(row["NgayHen"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayHen"].ToString());



                        dsKhaiSinh.Add(khaiSinh);
                    }
                    return dsKhaiSinh;
                }
                return new List<GiamHo>();
            }
            catch (Exception ex)
            {
                return new List<GiamHo>();
            }

        }

        public List<GiamHo> GetDataKSGiamHoCBox(int TrangThai)
        {
            try
            {
                List<GiamHo> dsKhaiSinh = new List<GiamHo>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_GIamHo_GetDataComBoBox", new object[] {TrangThai }, new List<string>() { "TrangThai" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        GiamHo khaiSinh = new GiamHo();

                        khaiSinh.MaGH = String.IsNullOrEmpty(row["MaGH"].ToString()) ? 0 : int.Parse(row["MaGH"].ToString());
                        khaiSinh.IDNguoiGiamHo = String.IsNullOrEmpty(row["IDNguoiGiamHo"].ToString()) ? 0 : int.Parse(row["IDNguoiGiamHo"].ToString());
                        khaiSinh.IDNguoiDuocGiamHo = String.IsNullOrEmpty(row["IDNguoiDuocGiamHo"].ToString()) ? 0 : int.Parse(row["IDNguoiDuocGiamHo"].ToString());
                        khaiSinh.NgayDangKy = String.IsNullOrEmpty(row["NgayDangKy"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayDangKy"].ToString());
                        khaiSinh.TrangThai = String.IsNullOrEmpty(row["TrangThai"].ToString()) ? 0 : int.Parse(row["TrangThai"].ToString());
                        khaiSinh.NgayHen = String.IsNullOrEmpty(row["NgayHen"].ToString()) ? DateTime.Now : Convert.ToDateTime(row["NgayHen"].ToString());



                        dsKhaiSinh.Add(khaiSinh);
                    }
                    return dsKhaiSinh;
                }
                return new List<GiamHo>();
            }
            catch (Exception ex)
            {
                return new List<GiamHo>();
            }

        }

        public CongDan GetCongDanByMaCongDan(int MaCongDan)
        {
            try
            {
                DataTable dt = dataProvider.ExecuteQuery("Proc_GiamHoCD_getDATA", new object[] { MaCongDan }, new List<string>() { "MaCongDan" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    CongDan congDan = new CongDan();
                    congDan.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                    congDan.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                    congDan.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(row["NgaySinh"].ToString());
                    congDan.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                    congDan.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                    congDan.ThuongTru = String.IsNullOrEmpty(row["ThuongTru"].ToString()) ? "xã Ninh Khang" : row["ThuongTru"].ToString();
                    congDan.DanToc = String.IsNullOrEmpty(row["DanToc"].ToString()) ? "Kinh" : row["DanToc"].ToString();
                    congDan.QuocTich = String.IsNullOrEmpty(row["QuocTich"].ToString()) ? "Việt Nam" : row["QuocTich"].ToString();
                    congDan.CMND = String.IsNullOrEmpty(row["CMND"].ToString()) ? "" : row["CMND"].ToString();
                    return congDan;
                }
                return new CongDan();
            }
            catch (Exception ex)
            {
                return new CongDan();
            }
        }

        public bool UpdateTrangThai(int MaKhaiSinh, int TrangThai)
        {
            int kq = 0;
           
                kq = dataProvider.ExecuteNonQuery("Proc_GiamHo_UpdateTrangThai", new object[] { MaKhaiSinh, TrangThai }, new List<string>() { "MaGH", "TrangThai" });
                if (kq > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            
           
        }

        public bool UpdateTrangThaiDuyet(int MaKhaiSinh, int MaCongChucXacMinh, int TrangThai)
        {
            int kq = 0;

            kq = dataProvider.ExecuteNonQuery("Proc_GiamHo_UpdateTrangThaiDuyetGH", new object[] { MaKhaiSinh, MaCongChucXacMinh, TrangThai }, new List<string>() { "MaGH", "MaLanhDao", "TrangThai" });
            if (kq > 0)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        public bool UpdateTrangThaiTraKQ(int MaKhaiSinh, int MaCongChucXacMinh, int TrangThai)
        {
            int kq = 0;

            kq = dataProvider.ExecuteNonQuery("Proc_GiamHo_UpdateTrangThaiTraKQ", new object[] { MaKhaiSinh, MaCongChucXacMinh, TrangThai }, new List<string>() { "MaGH", "MaCongChucTiepNhan", "TrangThai" });
            if (kq > 0)
            {
                return true;
            }
            else
            {
                return false;
            }


        }


    }
}
