﻿using QLHT.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Model{
    public class BoPhan_CongChucModel
    {
        DataProvider dataProvider = new DataProvider();
        public void Insert(int IDCongChuc, List<int> IDsBoPhan)
        {
            DeleteByIDCongChuc(IDCongChuc);
            if (IDsBoPhan.Count > 0)
            {
                foreach(int i in IDsBoPhan)
                {
                    dataProvider.ExecuteNonQuery("Proc_BoPhan_CongChuc_Insert", new object[] { IDCongChuc, i }, new List<string>() { "MaCongChuc", "MaBoPhan" });
                }
            }
        }

        private void DeleteByIDCongChuc(int IDCongChuc)
        {
            dataProvider.ExecuteNonQuery("Proc_BoPhan_CongChuc_DeleteByCongChuc", new object[] { IDCongChuc }, new List<string>() { "MaCongChuc" });
        }

        public List<BoPhan_CongChuc> GetData(int IDCongChuc)
        {
            try
            {
                List<BoPhan_CongChuc> dsBoPhan_CongChuc = new List<BoPhan_CongChuc>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_BoPhan_CongChuc_GetData", new object[] { IDCongChuc }, new List<string>() { "MaCongChuc" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        BoPhan_CongChuc boPhan_CongChuc = new BoPhan_CongChuc();
                        boPhan_CongChuc.MaCongChuc = String.IsNullOrEmpty(row["MaCongChuc"].ToString()) ? -1 : int.Parse(row["MaCongChuc"].ToString());
                        boPhan_CongChuc.MaBoPhan = String.IsNullOrEmpty(row["MaBoPhan"].ToString()) ? -1 : int.Parse(row["MaBoPhan"].ToString());
                        dsBoPhan_CongChuc.Add(boPhan_CongChuc);
                    }
                }
                return dsBoPhan_CongChuc;
            }
            catch (Exception ex)
            {
                return new List<BoPhan_CongChuc>();
            }
        }

        public List<int> GetIDsBoPhanByMaCongChuc(int IDCongChuc)
        {
            try
            {
                List<int> dsBoPhan_CongChuc = new List<int>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_BoPhan_CongChuc_GetData", new object[] { IDCongChuc }, new List<string>() { "MaCongChuc" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        int MaBoPhan = String.IsNullOrEmpty(row["MaBoPhan"].ToString()) ? -1 : int.Parse(row["MaBoPhan"].ToString());
                        dsBoPhan_CongChuc.Add(MaBoPhan);
                    }
                }
                return dsBoPhan_CongChuc;
            }
            catch (Exception ex)
            {
                return new List<int>();
            }
        }
    }
}
