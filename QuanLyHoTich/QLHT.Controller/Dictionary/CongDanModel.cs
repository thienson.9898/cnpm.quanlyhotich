﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLHT.Entity;


namespace QLHT.Model
{
    public class CongDanModel
    {
        DataProvider dataProvider = new DataProvider();
        public List<CongDan> GetData()
        {
            try
            {
                List<CongDan> dsCongDan = new List<CongDan>();
                DataTable dt = dataProvider.ExecuteQuery("Proc_CongDan_GetData", null, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CongDan congDan = new CongDan();
                        congDan.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        congDan.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                        congDan.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(row["NgaySinh"].ToString());
                        congDan.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                        congDan.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                        congDan.ThuongTru = String.IsNullOrEmpty(row["ThuongTru"].ToString()) ? "xã Ninh Khang" : row["ThuongTru"].ToString();
                        congDan.DanToc = String.IsNullOrEmpty(row["DanToc"].ToString()) ? "Kinh" : row["DanToc"].ToString();
                        congDan.QuocTich = String.IsNullOrEmpty(row["QuocTich"].ToString()) ? "Việt Nam" : row["QuocTich"].ToString();
                        congDan.CMND = String.IsNullOrEmpty(row["CMND"].ToString()) ? "" : row["CMND"].ToString();
                        dsCongDan.Add(congDan);
                    }
                }
                return dsCongDan;
            }
            catch (Exception ex)
            {
                return new List<CongDan>();
            }
        }

        public List<CongDan> GetData(string hoTen, string ngaySinh, string gioiTinh, string queQuan, string thuongTru, string danToc, string quocTich, string CMND, string ContainKhaiSinh = "-1", string ContainAdult = "1", string trangThai = "-1", string MaCongDan = "-1")
        {
            try
            {
                List<CongDan> dsCongDan = new List<CongDan>();
                object GioiTinh = "";
                if (String.IsNullOrEmpty(gioiTinh))
                {
                    GioiTinh = DBNull.Value;
                }
                else
                {
                    GioiTinh = gioiTinh;
                }
                DataTable dt = dataProvider.ExecuteQuery("Proc_CongDan_GetData", new object[] { MaCongDan, hoTen, ngaySinh, GioiTinh, queQuan, thuongTru, danToc, quocTich, CMND, ContainKhaiSinh, ContainAdult, trangThai },
                  new List<string>() { "@MaCongDan", "@HoTen", "@NgaySinh", "@GioiTinh", "@QueQuan", "@ThuongTru", "@DanToc", "@QuocTich", "@CMND", "@ContainKhaiSinh","@ContainAdult","@TrangThai" });
               Console.Out.WriteLine(dt.Rows.Count);
                if (dt != null && dt.Rows.Count > 0)
                {

                    

                    foreach (DataRow row in dt.Rows)
                    {                       
                        CongDan congDan = new CongDan();
                        congDan.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                        congDan.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                        congDan.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(row["NgaySinh"].ToString());
                        congDan.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                        congDan.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                        congDan.ThuongTru = String.IsNullOrEmpty(row["ThuongTru"].ToString()) ? "xã Ninh Khang" : row["ThuongTru"].ToString();
                        congDan.DanToc = String.IsNullOrEmpty(row["DanToc"].ToString()) ? "Kinh" : row["DanToc"].ToString();
                        congDan.QuocTich = String.IsNullOrEmpty(row["QuocTich"].ToString()) ? "Việt Nam" : row["QuocTich"].ToString();
                        congDan.CMND = String.IsNullOrEmpty(row["CMND"].ToString()) ? "" : row["CMND"].ToString();
                        dsCongDan.Add(congDan);
                    }
                }
                return dsCongDan;
            }
            catch (Exception ex)
            {
                return new List<CongDan>();
            }
        }

        public string GetHoTenByMaCongDan(int MaCongDan)
        {
            Console.Out.WriteLine("aaaaaaaaa");
            try
            {
                DataTable dt = dataProvider.ExecuteQuery("Proc_CongDan_GetData", new object[] { MaCongDan }, new List<string>() { "MaCongDan" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    Console.Out.WriteLine("" + dt.Rows[0]["HoTen"]);
                   return dt.Rows[0]["HoTen"].ToString();
                }
                Console.Out.WriteLine("no name");
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string GetHoTenByMaCongDan_KetHon(int MaCongDan)
        {
            Console.Out.WriteLine("aaaaaaaaa");
            try
            {
                DataTable dt = dataProvider.ExecuteQuery("Proc_CongDan_GetData", new object[] { MaCongDan,-1,1 }, new List<string>() { "MaCongDan","ContainKhaiSinh","ContainAdult" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    Console.Out.WriteLine("" + dt.Rows[0]["HoTen"]);
                    return dt.Rows[0]["HoTen"].ToString();
                }
                Console.Out.WriteLine("no name");
                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }/*

        public CongDan GetCongDanByMaCongDan(int MaCongDan, bool ContainKhaiSinh = false, bool ContainAdult = true)
        {
            try
            {
                int iContainKhaiSinh = ContainKhaiSinh ? 1:-1;
                int iContainAdult = ContainAdult? 1:-1;
                
                DataTable dt = dataProvider.ExecuteQuery("Proc_CongDan_GetData", new object[] { MaCongDan , iContainKhaiSinh, iContainAdult}, new List<string>() { "MaCongDan" , "ContainKhaiSinh" , "ContainAdult"});
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    CongDan congDan = new CongDan();
                    congDan.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                    congDan.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                    congDan.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(row["NgaySinh"].ToString());
                    congDan.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                    congDan.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                    congDan.ThuongTru = String.IsNullOrEmpty(row["ThuongTru"].ToString()) ? "xã Ninh Khang" : row["ThuongTru"].ToString();
                    congDan.DanToc = String.IsNullOrEmpty(row["DanToc"].ToString()) ? "Kinh" : row["DanToc"].ToString();
                    congDan.QuocTich = String.IsNullOrEmpty(row["QuocTich"].ToString()) ? "Việt Nam" : row["QuocTich"].ToString();
                    congDan.CMND = String.IsNullOrEmpty(row["CMND"].ToString()) ? "" : row["CMND"].ToString();
                    return congDan;
                }
                return new CongDan();
            }
            catch (Exception ex)
            {
                return new CongDan();
            }
        }*/

        public string ThemCongDan(CongDan congDan)
        {
            try
            {
                string rs = "";
                rs = dataProvider.ExecuteScalar("Proc_CongDan_Insert", new object[] { congDan.HoTen, congDan.NgaySinh, congDan.GioiTinh, congDan.QueQuan, congDan.ThuongTru, congDan.DanToc, congDan.QuocTich, congDan.CMND, congDan.TrangThai },
                  new List<string>() { "@HoTen", "@NgaySinh", "@GioiTinh", "@QueQuan", "@ThuongTru", "@DanToc", "@QuocTich", "@CMND", "@TrangThai" });
                return rs;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public CongDan GetCongDanByMaCongDan(int MaCongDan, bool ContainKhaiSinh = false, bool ContainAdult = true)
        {
            try
            {
                int iContainKhaiSinh = ContainKhaiSinh ? 1 : -1;
                int iContainAdult = ContainAdult ? 1 : -1;

                DataTable dt = dataProvider.ExecuteQuery("Proc_CongDan_GetData", new object[] { MaCongDan, iContainKhaiSinh, iContainAdult }, new List<string>() { "MaCongDan", "ContainKhaiSinh", "ContainAdult" });
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    CongDan congDan = new CongDan();
                    congDan.MaCongDan = String.IsNullOrEmpty(row["MaCongDan"].ToString()) ? 0 : int.Parse(row["MaCongDan"].ToString());
                    congDan.HoTen = String.IsNullOrEmpty(row["HoTen"].ToString()) ? "" : row["HoTen"].ToString();
                    congDan.NgaySinh = String.IsNullOrEmpty(row["NgaySinh"].ToString()) ? DateTime.MinValue : Convert.ToDateTime(row["NgaySinh"].ToString());
                    congDan.GioiTinh = String.IsNullOrEmpty(row["GioiTinh"].ToString()) ? true : row["GioiTinh"].ToString().ToLower() == "true" ? true : false;
                    congDan.QueQuan = String.IsNullOrEmpty(row["QueQuan"].ToString()) ? "Ninh Khang" : row["QueQuan"].ToString();
                    congDan.ThuongTru = String.IsNullOrEmpty(row["ThuongTru"].ToString()) ? "xã Ninh Khang" : row["ThuongTru"].ToString();
                    congDan.DanToc = String.IsNullOrEmpty(row["DanToc"].ToString()) ? "Kinh" : row["DanToc"].ToString();
                    congDan.QuocTich = String.IsNullOrEmpty(row["QuocTich"].ToString()) ? "Việt Nam" : row["QuocTich"].ToString();
                    congDan.CMND = String.IsNullOrEmpty(row["CMND"].ToString()) ? "" : row["CMND"].ToString();
                    return congDan;
                }
                return new CongDan();
            }
            catch (Exception ex)
            {
                return new CongDan();
            }
        }

        public bool UpdateCongDan(CongDan congDan)
        {
            try
            {
                int kq = 0;
                kq = dataProvider.ExecuteNonQuery("Proc_CongDan_Update", new object[] { congDan.MaCongDan, congDan.HoTen, congDan.NgaySinh, congDan.GioiTinh, congDan.QueQuan, congDan.ThuongTru, congDan.DanToc, congDan.QuocTich, congDan.CMND, congDan.TrangThai },
                  new List<string>() { "@MaCongDan", "@HoTen", "@NgaySinh", "@GioiTinh", "@QueQuan", "@ThuongTru", "@DanToc", "@QuocTich", "@CMND", "@TrangThai" });
                if (kq > 0)
                    return true;
                else return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Update_CongDan_FromDangKyThaDoi(int MaCongDanDN, string HoTen, DateTime NgaySinh, string DanToc,bool GioiTinh, string QueQuan, string QuocTich)
        {
            int kq = 0;
            //  if (!String.IsNullOrEmpty(MaCongChucXacMinh))
            //   {
            kq = dataProvider.ExecuteNonQuery("update_DangKyThayDoi_Update_TrangThai", new object[] { MaCongDanDN, HoTen, NgaySinh, DanToc, GioiTinh, QueQuan, QuocTich }, 
                new List<string>() { "MaCongDanDN", "HoTen", "NgaySinh", "DanToc", "GioiTinh", "QueQuan", "QuocTich" });
            if (kq > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}