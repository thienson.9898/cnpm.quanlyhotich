﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity
{
    public class BoPhan_CongChuc
    {
        private int _MaBoPhan;
        private int _MaCongChuc;

        public int MaBoPhan { get => _MaBoPhan; set => _MaBoPhan = value; }
        public int MaCongChuc { get => _MaCongChuc; set => _MaCongChuc = value; }
    }
}
