﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity
{
    public class KhaiTu
    {
        private int _MaKT;
        private int _IDNguoiChet;
        private string _NoiCuTruCuoiCung;
        private DateTime _ThoiGianChet;
        private string _NoiChet;
        private string _NguyenNhan;
        private int _MaThuTuc;
        private DateTime _NgayDangKy;
        private DateTime? _NgayHen;
        private DateTime? _NgayTra;
        private int _MaCongDan;
        private int _MaCongChucTiepNhan;
        private int _MaCongChucXacMinh;
        private int _MaLanhDao;
        private int _MaCongChucVanPhong;
        private int _MaCongChucKTTC;
        private int _TrangThai;


        public int MaKT { get => _MaKT; set => _MaKT = value; }
        public int IDNguoiChet { get => _IDNguoiChet; set => _IDNguoiChet = value; }
        public string NoiCuTruCuoiCung { get => _NoiCuTruCuoiCung; set => _NoiCuTruCuoiCung = value; }
        public DateTime ThoiGianChet { get => _ThoiGianChet; set => _ThoiGianChet = value; }
        public string NoiChet { get => _NoiChet; set => _NoiChet = value; }
        public string NguyenNhan { get => _NguyenNhan; set => _NguyenNhan = value; }
        public int MaThuTuc { get => _MaThuTuc; set => _MaThuTuc = value; }
        public DateTime NgayDangKy { get => _NgayDangKy; set => _NgayDangKy = value; }
        public int MaCongDan { get => _MaCongDan; set => _MaCongDan = value; }
        public int MaCongChucTiepNhan { get => _MaCongChucTiepNhan; set => _MaCongChucTiepNhan = value; }
        public int MaCongChucXacMinh { get => _MaCongChucXacMinh; set => _MaCongChucXacMinh = value; }
        public int MaLanhDao { get => _MaLanhDao; set => _MaLanhDao = value; }
        public int MaCongChucVanPhong { get => _MaCongChucVanPhong; set => _MaCongChucVanPhong = value; }
        public int MaCongChucKTTC { get => _MaCongChucKTTC; set => _MaCongChucKTTC = value; }
        public int TrangThai { get => _TrangThai; set => _TrangThai = value; }
        public DateTime? NgayHen { get => _NgayHen; set => _NgayHen = value; }
        public DateTime? NgayTra { get => _NgayTra; set => _NgayTra = value; }
    }
}
