﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity
{
    public class ThuTuc
    {
        private int _MaThuTuc;
        private string _TenThuTuc;

        public int MaThuTuc { get => _MaThuTuc; set => _MaThuTuc = value; }
        public string TenThuTuc { get => _TenThuTuc; set => _TenThuTuc = value; }
    }
}
