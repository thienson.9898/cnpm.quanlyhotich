﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity.Dictionary
{
    public class DangKyThayDoi
    {
        private int _MaThayDoi;
        private int _MaCongDanYC;
        private int _MaCongDanDN;
        private string _QuanHeVoiNDN;
        private string _MucCanThayDoi;
        private string _LyDo;
        private string _GiaTriCu;
        private string _GiaTriMoi;
        private int _MaThuTuc;
        private DateTime _NgayDangKy;
        private DateTime _NgayHen;
        private DateTime _NgayTra;
        private int _MaCongChucTiepNhan;
        private int _MaCongChucXacMinh;
        private int _MaCongChucVanPhong;
        private int _MaCongChucKTTC;
        private int _MaLanhDao;
        private int _TrangThai;
    
        public int MaThayDoi { get => _MaThayDoi; set => _MaThayDoi = value; }
        public int MaCongDanYC { get => _MaCongDanYC; set => _MaCongDanYC = value; }
        public int MaCongDanDN { get => _MaCongDanDN; set => _MaCongDanDN = value; }
        public string QuanHeVoiNDN { get => _QuanHeVoiNDN; set => _QuanHeVoiNDN = value; }
        public string MucCanThayDoi { get => _MucCanThayDoi; set => _MucCanThayDoi = value; }
        public string LyDo { get => _LyDo; set => _LyDo = value; }
        public string GiaTriCu { get => _GiaTriCu; set => _GiaTriCu = value; }
        public string GiaTriMoi { get => _GiaTriMoi; set => _GiaTriMoi = value; }
        public int MaThuTuc { get => _MaThuTuc; set => _MaThuTuc = value; }
        public DateTime NgayDangKy { get => _NgayDangKy; set => _NgayDangKy = value; }
        public DateTime NgayHen { get => _NgayHen; set => _NgayHen = value; }
        public DateTime NgayTra { get => _NgayTra; set => _NgayTra = value; }
        public int MaCongChucTiepNhan { get => _MaCongChucTiepNhan; set => _MaCongChucTiepNhan = value; }
        public int MaCongChucXacMinh { get => _MaCongChucXacMinh; set => _MaCongChucXacMinh = value; }
        public int MaCongChucVanPhong { get => _MaCongChucVanPhong; set => _MaCongChucVanPhong = value; }
        public int MaCongChucKTTC { get => _MaCongChucKTTC; set => _MaCongChucKTTC = value; }       
        public int TrangThai { get => _TrangThai; set => _TrangThai = value; }
        public int MaLanhDao { get => _MaLanhDao; set => _MaLanhDao = value; }
    }
}
