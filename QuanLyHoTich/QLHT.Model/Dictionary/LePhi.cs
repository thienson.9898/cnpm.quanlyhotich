﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity
{
    public class LePhi
    {
        private int _MaLePhi;
        private DateTime _NgayLap;
        private int _MaCongDan;
        private int _MaCongChuc;

        public int MaLePhi { get => _MaLePhi; set => _MaLePhi = value; }
        public DateTime NgayLap { get => _NgayLap; set => _NgayLap = value; }
        public int MaCongDan { get => _MaCongDan; set => _MaCongDan = value; }
        public int MaCongChuc { get => _MaCongChuc; set => _MaCongChuc = value; }

        public string HoTenCD { get; set; }
        public string HoTenCC { get; set; }

        public LePhi()
        {
        }

        public LePhi(int maLePhi, int maCongDan, int maCongChuc, DateTime ngayLap)
        {
            MaLePhi = maLePhi;
            MaCongDan = maCongDan;
            MaCongChuc = maCongChuc;
            NgayLap = ngayLap;
        }

        public LePhi(int maCongDan, int maCongChuc, DateTime ngayLap)
        {
            MaCongDan = maCongDan;
            MaCongChuc = maCongChuc;
            NgayLap = ngayLap;
        }
    }
}
