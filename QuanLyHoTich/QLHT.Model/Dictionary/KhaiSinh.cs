﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity
{
    public class KhaiSinh
    {
        private int _MaKhaiSinh;
        private string _HoTen;
        private DateTime _NgaySinh;
        private bool _GioiTinh;
        private string _DanToc;
        private string _QuocTich;
        private string _QueQuan;
        private string _NoiSinh;
        private string _SoDinhDanhCaNhan;
        private int _IDNguoiMe;
        private int _IDNguoiCha;
        private int _MaThuTuc;
        private DateTime _NgayDangKy;
        private DateTime? _NgayHen;
        private DateTime? _NgayTra;
        private int _MaCongDan;
        private int _MaCongChucTiepNhan;
        private int _MaCongChucXacMinh;
        private int _MaLanhDao;
        private int _MaCongChucVanPhong;
        private int _MaCongChucKTTC;
        private int _TrangThai;

        public KhaiSinh()
        {
        }

        public KhaiSinh(int maCongDan, string hoTen, string quequan, string quoctich, string dantocc, DateTime ngay)
        {
            MaCongDan = maCongDan;
            HoTen = hoTen;
            QueQuan = quequan;
            QuocTich = quoctich;
            DanToc = dantocc;
            NgaySinh = ngay;
        }

        public int MaKhaiSinh { get => _MaKhaiSinh; set => _MaKhaiSinh = value; }
        public string HoTen { get => _HoTen; set => _HoTen = value; }
        public DateTime NgaySinh { get => _NgaySinh; set => _NgaySinh = value; }
        public bool GioiTinh { get => _GioiTinh; set => _GioiTinh = value; }
        public string DanToc { get => _DanToc; set => _DanToc = value; }
        public string QueQuan { get => _QueQuan; set => _QueQuan = value; }
        public int IDNguoiMe { get => _IDNguoiMe; set => _IDNguoiMe = value; }
        public int IDNguoiCha { get => _IDNguoiCha; set => _IDNguoiCha = value; }
        public int MaThuTuc { get => _MaThuTuc; set => _MaThuTuc = value; }
        public DateTime NgayDangKy { get => _NgayDangKy; set => _NgayDangKy = value; }
        public int MaCongDan { get => _MaCongDan; set => _MaCongDan = value; }
        public int MaCongChucTiepNhan { get => _MaCongChucTiepNhan; set => _MaCongChucTiepNhan = value; }
        public int MaCongChucXacMinh { get => _MaCongChucXacMinh; set => _MaCongChucXacMinh = value; }
        public int MaLanhDao { get => _MaLanhDao; set => _MaLanhDao = value; }
        public int MaCongChucVanPhong { get => _MaCongChucVanPhong; set => _MaCongChucVanPhong = value; }
        public int MaCongChucKTTC { get => _MaCongChucKTTC; set => _MaCongChucKTTC = value; }
        public int TrangThai { get => _TrangThai; set => _TrangThai = value; }
        public string NoiSinh { get => _NoiSinh; set => _NoiSinh = value; }
        public string SoDinhDanhCaNhan { get => _SoDinhDanhCaNhan; set => _SoDinhDanhCaNhan = value; }
        public string QuocTich { get => _QuocTich; set => _QuocTich = value; }
        public DateTime? NgayHen { get => _NgayHen; set => _NgayHen = value; }
        public DateTime? NgayTra { get => _NgayTra; set => _NgayTra = value; }
    }
}
