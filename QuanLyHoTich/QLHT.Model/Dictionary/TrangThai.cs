﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity
{
    public class TrangThai
    {
        private int _MaTrangThai;
        private string _TrangThai;

        public int MaTrangThai { get => _MaTrangThai; set => _MaTrangThai = value; }
        public string MoTaTrangThai { get => _TrangThai; set => _TrangThai = value; }

        public TrangThai(int maTrangThai, string moTaTrangThai)
        {
            MaTrangThai = maTrangThai;
            MoTaTrangThai = moTaTrangThai;
        }
    }
}
