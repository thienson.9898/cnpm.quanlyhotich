﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity
{
    public class KetHon
    {
        private int _MaKH;
        private int _IDNguoiChong;
        private int _LanKetHonChong;
        private int _IDNguoiVo;
        private int _LanKetHonVo;
        private int _MaThuTuc;
        private DateTime _NgayDangKy;
        private DateTime? _NgayHen;
        private DateTime? _NgayTra;
        private int _MaCongDan;
        private int _MaCongChucTiepNhan;
        private int _MaCongChucXacMinh;
        private int _MaLanhDao;
        private int _MaCongChucVanPhong;
        private int _MaCongChucKTTC;
        private int _TrangThai;

        public int MaKH { get => _MaKH; set => _MaKH = value; }
        public int IDNguoiChong { get => _IDNguoiChong; set => _IDNguoiChong = value; }
        public int LanKetHonChong { get => _LanKetHonChong; set => _LanKetHonChong = value; }
        public int IDNguoiVo { get => _IDNguoiVo; set => _IDNguoiVo = value; }
        public int LanKetHonVo { get => _LanKetHonVo; set => _LanKetHonVo = value; }
        public int MaThuTuc { get => _MaThuTuc; set => _MaThuTuc = value; }
        public DateTime NgayDangKy { get => _NgayDangKy; set => _NgayDangKy = value; }
        public int MaCongDan { get => _MaCongDan; set => _MaCongDan = value; }
        public int MaCongChucTiepNhan { get => _MaCongChucTiepNhan; set => _MaCongChucTiepNhan = value; }
        public int MaCongChucXacMinh { get => _MaCongChucXacMinh; set => _MaCongChucXacMinh = value; }
        public int MaLanhDao { get => _MaLanhDao; set => _MaLanhDao = value; }
        public int MaCongChucVanPhong { get => _MaCongChucVanPhong; set => _MaCongChucVanPhong = value; }
        public int MaCongChucKTTC { get => _MaCongChucKTTC; set => _MaCongChucKTTC = value; }
        public int TrangThai { get => _TrangThai; set => _TrangThai = value; }
        public DateTime? NgayHen { get => _NgayHen; set => _NgayHen = value; }
        public DateTime? NgayTra { get => _NgayTra; set => _NgayTra = value; }
    }
}
