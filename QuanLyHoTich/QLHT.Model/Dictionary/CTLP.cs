﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity
{
    public class CTLP
    {
        private int _MaCTLePhi;
        private int _MaThuTuc;
        private int _MaLePhi;
        private string _LyDoNop;
        private float _SoTienNop;

        public int MaCTLePhi { get => _MaCTLePhi; set => _MaCTLePhi = value; }
        public int MaThuTuc { get => _MaThuTuc; set => _MaThuTuc = value; }
        public int MaLePhi { get => _MaLePhi; set => _MaLePhi = value; }
        public string LyDoNop { get => _LyDoNop; set => _LyDoNop = value; }
        public float SoTienNop { get => _SoTienNop; set => _SoTienNop = value; }
        public string HoTenTT { get; set; }

        public CTLP()
        {
        }

        public CTLP(int maCTLePhi, int maThuTuc, int maLePhi, string lyDoNop, float soTienNop)
        {
            MaCTLePhi = maCTLePhi;
            MaThuTuc = maThuTuc;
            MaLePhi = maLePhi;
            LyDoNop = lyDoNop;
            SoTienNop = soTienNop;
        }

        public CTLP(int maThuTuc, int maLePhi, string lyDoNop, float soTienNop)
        {
            MaThuTuc = maThuTuc;
            MaLePhi = maLePhi;
            LyDoNop = lyDoNop;
            SoTienNop = soTienNop;
        }
    }
}
