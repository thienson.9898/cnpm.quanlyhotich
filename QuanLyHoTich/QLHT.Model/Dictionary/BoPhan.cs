﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLHT.Entity
{
    public class BoPhan
    {
        private int _MaBoPhan;
        private string _TenVietTat;
        private string _TenBoPhan;

        public int MaBoPhan { get => _MaBoPhan; set => _MaBoPhan = value; }
        public string TenBoPhan { get => _TenBoPhan; set => _TenBoPhan = value; }
        public string TenVietTat { get => _TenVietTat; set => _TenVietTat = value; }

        public BoPhan(int maBoPhan, string tenBoPhan, string tenVietTat)
        {
            MaBoPhan = maBoPhan;
            TenBoPhan = tenBoPhan;
            TenVietTat = tenVietTat;
        }
    }
}
