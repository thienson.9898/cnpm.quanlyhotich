﻿namespace QuanLyHoTich.FormQLThayDoiCaiChinhBoSungHoTich
{
    partial class FormQLThayDoiBoSung
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.formQLThayDoi_lblTTHS = new System.Windows.Forms.Label();
            this.formQLThayDoi_TT = new System.Windows.Forms.Label();
            this.formThayDoi_grbTTNYC = new System.Windows.Forms.GroupBox();
            this.formThayDoi_grbTTNYC_txtMaCongDan = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.formThayDoi_grbTTNYC_txtQuanHe = new System.Windows.Forms.TextBox();
            this.formThayDoi_grbTTNYC_btnChonNYC = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.formThayDoi_grbTTNYC_rdNu = new System.Windows.Forms.RadioButton();
            this.formThayDoi_grbTTNYC_rdNam = new System.Windows.Forms.RadioButton();
            this.label23 = new System.Windows.Forms.Label();
            this.formThayDoi_grbTTNYC_txtbHoTen = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.formThayDoi_grbTTNYC_txtDanToc = new System.Windows.Forms.TextBox();
            this.formThayDoi_grbTTNYC_txtQuocTich = new System.Windows.Forms.TextBox();
            this.formThayDoi_grbTTNYC_txtQueQuan = new System.Windows.Forms.TextBox();
            this.formThayDoi_grbTTNYC_dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.formQLThayDoi_grbTTNDN = new System.Windows.Forms.GroupBox();
            this.formQLThayDoi_grbTTNDN_txtMaCDDN = new System.Windows.Forms.TextBox();
            this.formQLThayDoi_grbRadioButtonChoose = new System.Windows.Forms.GroupBox();
            this.formQLThayDoi_rdChooseHoTen = new System.Windows.Forms.RadioButton();
            this.formQLThayDoi_rdChooseNgaySinh = new System.Windows.Forms.RadioButton();
            this.formQLThayDoi_rdChooseDanToc = new System.Windows.Forms.RadioButton();
            this.formQLThayDoi_rdChooseQuocTich = new System.Windows.Forms.RadioButton();
            this.formQLThayDoi_rdChooseQueQuan = new System.Windows.Forms.RadioButton();
            this.formQLThayDoi_rdChooseGioiTinh = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.formQLThayDoi_grbTTNDN_btnHuy = new System.Windows.Forms.Button();
            this.formQLThayDoi_grbTTNDN_btnDongY = new System.Windows.Forms.Button();
            this.formQLThayDoi_grbTTNDN_btnTTNDN = new System.Windows.Forms.Button();
            this.formQLThayDoi_grbTTNDN_grbGioiTinh = new System.Windows.Forms.GroupBox();
            this.formQLThayDoi_grbTTNDN_rdNu = new System.Windows.Forms.RadioButton();
            this.formQLThayDoi_grbTTNDN_rdNam = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.formQLThayDoi_grbTTNDN_txtHoTen = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.formQLThayDoi_grbTTNDN_txtDanToc = new System.Windows.Forms.TextBox();
            this.formQLThayDoi_grbTTNDN_txtQuocTich = new System.Windows.Forms.TextBox();
            this.formQLThayDoi_grbTTNDN_txtQueQuan = new System.Windows.Forms.TextBox();
            this.formQLThayDoi_grbTTNDN_dtpkNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.FormQLThayDoiBoSung_dtpkHenTra = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.FormQLThayDoiBoSung_btnBackToMenu = new System.Windows.Forms.Button();
            this.FormQLThayDoiBoSung_btnHuy = new System.Windows.Forms.Button();
            this.FormQLThayDoiBoSung_btnTimKiem = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.FormQLThayDoiBoSung_cbbTrangThai = new System.Windows.Forms.ComboBox();
            this.FormQLThayDoiBoSung_btnTraKetQua = new System.Windows.Forms.Button();
            this.FormQLThayDoiBoSung_btnDuyetHoSo = new System.Windows.Forms.Button();
            this.FormQLThayDoiBoSung_btnXacNhan = new System.Windows.Forms.Button();
            this.FormQLThayDoiBoSung_btnThemHoSo = new System.Windows.Forms.Button();
            this.grvDSDangKiThayDoi = new System.Windows.Forms.DataGridView();
            this.MaThayDoi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaCongDanYC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaCongDanDN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuanHeVoiNDN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MucThayDoi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GTC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GTM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayDangKy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayHen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayTra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrangThai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MTT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grb_giaTriMoi = new System.Windows.Forms.GroupBox();
            this.txtGiaTriMoiHoTen = new System.Windows.Forms.TextBox();
            this.txtGiaTriMoiNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.grbGiaTriMoiGioiTinh = new System.Windows.Forms.GroupBox();
            this.rdGiaTriMoi_GTNau = new System.Windows.Forms.RadioButton();
            this.rdGiaTriMoi_GTNam = new System.Windows.Forms.RadioButton();
            this.txtGiaTriMoiDanToc = new System.Windows.Forms.TextBox();
            this.txtGiaTriMoiQuocTich = new System.Windows.Forms.TextBox();
            this.txtGiaTriMoiQueQuan = new System.Windows.Forms.TextBox();
            this.formThayDoi_grbTTNYC.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.formQLThayDoi_grbTTNDN.SuspendLayout();
            this.formQLThayDoi_grbRadioButtonChoose.SuspendLayout();
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvDSDangKiThayDoi)).BeginInit();
            this.grb_giaTriMoi.SuspendLayout();
            this.grbGiaTriMoiGioiTinh.SuspendLayout();
            this.SuspendLayout();
            // 
            // formQLThayDoi_lblTTHS
            // 
            this.formQLThayDoi_lblTTHS.AutoSize = true;
            this.formQLThayDoi_lblTTHS.Location = new System.Drawing.Point(21, 20);
            this.formQLThayDoi_lblTTHS.Name = "formQLThayDoi_lblTTHS";
            this.formQLThayDoi_lblTTHS.Size = new System.Drawing.Size(90, 13);
            this.formQLThayDoi_lblTTHS.TabIndex = 60;
            this.formQLThayDoi_lblTTHS.Text = "Trạng thái hồ sơ: ";
            this.formQLThayDoi_lblTTHS.Visible = false;
            // 
            // formQLThayDoi_TT
            // 
            this.formQLThayDoi_TT.AutoSize = true;
            this.formQLThayDoi_TT.Location = new System.Drawing.Point(117, 20);
            this.formQLThayDoi_TT.Name = "formQLThayDoi_TT";
            this.formQLThayDoi_TT.Size = new System.Drawing.Size(29, 13);
            this.formQLThayDoi_TT.TabIndex = 61;
            this.formQLThayDoi_TT.Text = "label";
            this.formQLThayDoi_TT.Visible = false;
            // 
            // formThayDoi_grbTTNYC
            // 
            this.formThayDoi_grbTTNYC.Controls.Add(this.formThayDoi_grbTTNYC_txtMaCongDan);
            this.formThayDoi_grbTTNYC.Controls.Add(this.label17);
            this.formThayDoi_grbTTNYC.Controls.Add(this.label16);
            this.formThayDoi_grbTTNYC.Controls.Add(this.formThayDoi_grbTTNYC_txtQuanHe);
            this.formThayDoi_grbTTNYC.Controls.Add(this.formThayDoi_grbTTNYC_btnChonNYC);
            this.formThayDoi_grbTTNYC.Controls.Add(this.groupBox4);
            this.formThayDoi_grbTTNYC.Controls.Add(this.label23);
            this.formThayDoi_grbTTNYC.Controls.Add(this.formThayDoi_grbTTNYC_txtbHoTen);
            this.formThayDoi_grbTTNYC.Controls.Add(this.label3);
            this.formThayDoi_grbTTNYC.Controls.Add(this.label4);
            this.formThayDoi_grbTTNYC.Controls.Add(this.label5);
            this.formThayDoi_grbTTNYC.Controls.Add(this.label6);
            this.formThayDoi_grbTTNYC.Controls.Add(this.label7);
            this.formThayDoi_grbTTNYC.Controls.Add(this.formThayDoi_grbTTNYC_txtDanToc);
            this.formThayDoi_grbTTNYC.Controls.Add(this.formThayDoi_grbTTNYC_txtQuocTich);
            this.formThayDoi_grbTTNYC.Controls.Add(this.formThayDoi_grbTTNYC_txtQueQuan);
            this.formThayDoi_grbTTNYC.Controls.Add(this.formThayDoi_grbTTNYC_dtpNgaySinh);
            this.formThayDoi_grbTTNYC.Location = new System.Drawing.Point(40, 54);
            this.formThayDoi_grbTTNYC.Name = "formThayDoi_grbTTNYC";
            this.formThayDoi_grbTTNYC.Size = new System.Drawing.Size(270, 277);
            this.formThayDoi_grbTTNYC.TabIndex = 62;
            this.formThayDoi_grbTTNYC.TabStop = false;
            this.formThayDoi_grbTTNYC.Text = "Thông tin người yêu cầu";
            // 
            // formThayDoi_grbTTNYC_txtMaCongDan
            // 
            this.formThayDoi_grbTTNYC_txtMaCongDan.Location = new System.Drawing.Point(97, 59);
            this.formThayDoi_grbTTNYC_txtMaCongDan.Name = "formThayDoi_grbTTNYC_txtMaCongDan";
            this.formThayDoi_grbTTNYC_txtMaCongDan.Size = new System.Drawing.Size(151, 20);
            this.formThayDoi_grbTTNYC_txtMaCongDan.TabIndex = 39;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 62);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(70, 13);
            this.label17.TabIndex = 38;
            this.label17.Text = "Mã công dân";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(24, 247);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(133, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "Quan hệ với người đề nghị";
            // 
            // formThayDoi_grbTTNYC_txtQuanHe
            // 
            this.formThayDoi_grbTTNYC_txtQuanHe.Location = new System.Drawing.Point(164, 240);
            this.formThayDoi_grbTTNYC_txtQuanHe.Name = "formThayDoi_grbTTNYC_txtQuanHe";
            this.formThayDoi_grbTTNYC_txtQuanHe.Size = new System.Drawing.Size(84, 20);
            this.formThayDoi_grbTTNYC_txtQuanHe.TabIndex = 37;
            // 
            // formThayDoi_grbTTNYC_btnChonNYC
            // 
            this.formThayDoi_grbTTNYC_btnChonNYC.Location = new System.Drawing.Point(10, 21);
            this.formThayDoi_grbTTNYC_btnChonNYC.Name = "formThayDoi_grbTTNYC_btnChonNYC";
            this.formThayDoi_grbTTNYC_btnChonNYC.Size = new System.Drawing.Size(170, 23);
            this.formThayDoi_grbTTNYC_btnChonNYC.TabIndex = 35;
            this.formThayDoi_grbTTNYC_btnChonNYC.Text = "Chọn thông tin người yêu cầu";
            this.formThayDoi_grbTTNYC_btnChonNYC.UseVisualStyleBackColor = true;
            this.formThayDoi_grbTTNYC_btnChonNYC.Click += new System.EventHandler(this.formThayDoi_grbTTNYC_btnChonNYC_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.formThayDoi_grbTTNYC_rdNu);
            this.groupBox4.Controls.Add(this.formThayDoi_grbTTNYC_rdNam);
            this.groupBox4.Location = new System.Drawing.Point(97, 137);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(151, 20);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            // 
            // formThayDoi_grbTTNYC_rdNu
            // 
            this.formThayDoi_grbTTNYC_rdNu.AutoSize = true;
            this.formThayDoi_grbTTNYC_rdNu.Location = new System.Drawing.Point(93, 2);
            this.formThayDoi_grbTTNYC_rdNu.Name = "formThayDoi_grbTTNYC_rdNu";
            this.formThayDoi_grbTTNYC_rdNu.Size = new System.Drawing.Size(39, 17);
            this.formThayDoi_grbTTNYC_rdNu.TabIndex = 1;
            this.formThayDoi_grbTTNYC_rdNu.TabStop = true;
            this.formThayDoi_grbTTNYC_rdNu.Text = "Nữ";
            this.formThayDoi_grbTTNYC_rdNu.UseVisualStyleBackColor = true;
            // 
            // formThayDoi_grbTTNYC_rdNam
            // 
            this.formThayDoi_grbTTNYC_rdNam.AutoSize = true;
            this.formThayDoi_grbTTNYC_rdNam.Location = new System.Drawing.Point(7, 2);
            this.formThayDoi_grbTTNYC_rdNam.Name = "formThayDoi_grbTTNYC_rdNam";
            this.formThayDoi_grbTTNYC_rdNam.Size = new System.Drawing.Size(47, 17);
            this.formThayDoi_grbTTNYC_rdNam.TabIndex = 0;
            this.formThayDoi_grbTTNYC_rdNam.TabStop = true;
            this.formThayDoi_grbTTNYC_rdNam.Text = "Nam";
            this.formThayDoi_grbTTNYC_rdNam.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(24, 140);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 13);
            this.label23.TabIndex = 26;
            this.label23.Text = "Giới tính";
            // 
            // formThayDoi_grbTTNYC_txtbHoTen
            // 
            this.formThayDoi_grbTTNYC_txtbHoTen.Location = new System.Drawing.Point(97, 85);
            this.formThayDoi_grbTTNYC_txtbHoTen.Name = "formThayDoi_grbTTNYC_txtbHoTen";
            this.formThayDoi_grbTTNYC_txtbHoTen.Size = new System.Drawing.Size(151, 20);
            this.formThayDoi_grbTTNYC_txtbHoTen.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Họ tên";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Ngày sinh";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Dân tộc";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 222);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Quốc tịch";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Quê quán";
            // 
            // formThayDoi_grbTTNYC_txtDanToc
            // 
            this.formThayDoi_grbTTNYC_txtDanToc.Location = new System.Drawing.Point(97, 163);
            this.formThayDoi_grbTTNYC_txtDanToc.Name = "formThayDoi_grbTTNYC_txtDanToc";
            this.formThayDoi_grbTTNYC_txtDanToc.Size = new System.Drawing.Size(151, 20);
            this.formThayDoi_grbTTNYC_txtDanToc.TabIndex = 18;
            // 
            // formThayDoi_grbTTNYC_txtQuocTich
            // 
            this.formThayDoi_grbTTNYC_txtQuocTich.Location = new System.Drawing.Point(96, 214);
            this.formThayDoi_grbTTNYC_txtQuocTich.Name = "formThayDoi_grbTTNYC_txtQuocTich";
            this.formThayDoi_grbTTNYC_txtQuocTich.Size = new System.Drawing.Size(151, 20);
            this.formThayDoi_grbTTNYC_txtQuocTich.TabIndex = 19;
            // 
            // formThayDoi_grbTTNYC_txtQueQuan
            // 
            this.formThayDoi_grbTTNYC_txtQueQuan.Location = new System.Drawing.Point(96, 188);
            this.formThayDoi_grbTTNYC_txtQueQuan.Name = "formThayDoi_grbTTNYC_txtQueQuan";
            this.formThayDoi_grbTTNYC_txtQueQuan.Size = new System.Drawing.Size(151, 20);
            this.formThayDoi_grbTTNYC_txtQueQuan.TabIndex = 20;
            // 
            // formThayDoi_grbTTNYC_dtpNgaySinh
            // 
            this.formThayDoi_grbTTNYC_dtpNgaySinh.CustomFormat = "dd/MM/yyyy";
            this.formThayDoi_grbTTNYC_dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.formThayDoi_grbTTNYC_dtpNgaySinh.Location = new System.Drawing.Point(97, 111);
            this.formThayDoi_grbTTNYC_dtpNgaySinh.Name = "formThayDoi_grbTTNYC_dtpNgaySinh";
            this.formThayDoi_grbTTNYC_dtpNgaySinh.Size = new System.Drawing.Size(151, 20);
            this.formThayDoi_grbTTNYC_dtpNgaySinh.TabIndex = 21;
            // 
            // formQLThayDoi_grbTTNDN
            // 
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_txtMaCDDN);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbRadioButtonChoose);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.label1);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_btnHuy);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_btnDongY);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_btnTTNDN);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_grbGioiTinh);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.label8);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_txtHoTen);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.label11);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.label12);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.label13);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.label14);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.label15);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_txtDanToc);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_txtQuocTich);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_txtQueQuan);
            this.formQLThayDoi_grbTTNDN.Controls.Add(this.formQLThayDoi_grbTTNDN_dtpkNgaySinh);
            this.formQLThayDoi_grbTTNDN.Location = new System.Drawing.Point(325, 54);
            this.formQLThayDoi_grbTTNDN.Name = "formQLThayDoi_grbTTNDN";
            this.formQLThayDoi_grbTTNDN.Size = new System.Drawing.Size(278, 277);
            this.formQLThayDoi_grbTTNDN.TabIndex = 63;
            this.formQLThayDoi_grbTTNDN.TabStop = false;
            this.formQLThayDoi_grbTTNDN.Text = "Thông tin người đề nghị";
            // 
            // formQLThayDoi_grbTTNDN_txtMaCDDN
            // 
            this.formQLThayDoi_grbTTNDN_txtMaCDDN.Enabled = false;
            this.formQLThayDoi_grbTTNDN_txtMaCDDN.Location = new System.Drawing.Point(104, 63);
            this.formQLThayDoi_grbTTNDN_txtMaCDDN.Name = "formQLThayDoi_grbTTNDN_txtMaCDDN";
            this.formQLThayDoi_grbTTNDN_txtMaCDDN.Size = new System.Drawing.Size(151, 20);
            this.formQLThayDoi_grbTTNDN_txtMaCDDN.TabIndex = 41;
            // 
            // formQLThayDoi_grbRadioButtonChoose
            // 
            this.formQLThayDoi_grbRadioButtonChoose.Controls.Add(this.formQLThayDoi_rdChooseHoTen);
            this.formQLThayDoi_grbRadioButtonChoose.Controls.Add(this.formQLThayDoi_rdChooseNgaySinh);
            this.formQLThayDoi_grbRadioButtonChoose.Controls.Add(this.formQLThayDoi_rdChooseDanToc);
            this.formQLThayDoi_grbRadioButtonChoose.Controls.Add(this.formQLThayDoi_rdChooseQuocTich);
            this.formQLThayDoi_grbRadioButtonChoose.Controls.Add(this.formQLThayDoi_rdChooseQueQuan);
            this.formQLThayDoi_grbRadioButtonChoose.Controls.Add(this.formQLThayDoi_rdChooseGioiTinh);
            this.formQLThayDoi_grbRadioButtonChoose.Location = new System.Drawing.Point(8, 85);
            this.formQLThayDoi_grbRadioButtonChoose.Name = "formQLThayDoi_grbRadioButtonChoose";
            this.formQLThayDoi_grbRadioButtonChoose.Size = new System.Drawing.Size(20, 154);
            this.formQLThayDoi_grbRadioButtonChoose.TabIndex = 67;
            this.formQLThayDoi_grbRadioButtonChoose.TabStop = false;
            this.formQLThayDoi_grbRadioButtonChoose.Visible = false;
            // 
            // formQLThayDoi_rdChooseHoTen
            // 
            this.formQLThayDoi_rdChooseHoTen.AutoSize = true;
            this.formQLThayDoi_rdChooseHoTen.Location = new System.Drawing.Point(4, 11);
            this.formQLThayDoi_rdChooseHoTen.Name = "formQLThayDoi_rdChooseHoTen";
            this.formQLThayDoi_rdChooseHoTen.Size = new System.Drawing.Size(14, 13);
            this.formQLThayDoi_rdChooseHoTen.TabIndex = 28;
            this.formQLThayDoi_rdChooseHoTen.TabStop = true;
            this.formQLThayDoi_rdChooseHoTen.UseVisualStyleBackColor = true;
            // 
            // formQLThayDoi_rdChooseNgaySinh
            // 
            this.formQLThayDoi_rdChooseNgaySinh.AutoSize = true;
            this.formQLThayDoi_rdChooseNgaySinh.Location = new System.Drawing.Point(4, 37);
            this.formQLThayDoi_rdChooseNgaySinh.Name = "formQLThayDoi_rdChooseNgaySinh";
            this.formQLThayDoi_rdChooseNgaySinh.Size = new System.Drawing.Size(14, 13);
            this.formQLThayDoi_rdChooseNgaySinh.TabIndex = 29;
            this.formQLThayDoi_rdChooseNgaySinh.TabStop = true;
            this.formQLThayDoi_rdChooseNgaySinh.UseVisualStyleBackColor = true;
            // 
            // formQLThayDoi_rdChooseDanToc
            // 
            this.formQLThayDoi_rdChooseDanToc.AutoSize = true;
            this.formQLThayDoi_rdChooseDanToc.Location = new System.Drawing.Point(4, 89);
            this.formQLThayDoi_rdChooseDanToc.Name = "formQLThayDoi_rdChooseDanToc";
            this.formQLThayDoi_rdChooseDanToc.Size = new System.Drawing.Size(14, 13);
            this.formQLThayDoi_rdChooseDanToc.TabIndex = 31;
            this.formQLThayDoi_rdChooseDanToc.TabStop = true;
            this.formQLThayDoi_rdChooseDanToc.UseVisualStyleBackColor = true;
            // 
            // formQLThayDoi_rdChooseQuocTich
            // 
            this.formQLThayDoi_rdChooseQuocTich.AutoSize = true;
            this.formQLThayDoi_rdChooseQuocTich.Location = new System.Drawing.Point(4, 141);
            this.formQLThayDoi_rdChooseQuocTich.Name = "formQLThayDoi_rdChooseQuocTich";
            this.formQLThayDoi_rdChooseQuocTich.Size = new System.Drawing.Size(14, 13);
            this.formQLThayDoi_rdChooseQuocTich.TabIndex = 33;
            this.formQLThayDoi_rdChooseQuocTich.TabStop = true;
            this.formQLThayDoi_rdChooseQuocTich.UseVisualStyleBackColor = true;
            // 
            // formQLThayDoi_rdChooseQueQuan
            // 
            this.formQLThayDoi_rdChooseQueQuan.AutoSize = true;
            this.formQLThayDoi_rdChooseQueQuan.Location = new System.Drawing.Point(4, 113);
            this.formQLThayDoi_rdChooseQueQuan.Name = "formQLThayDoi_rdChooseQueQuan";
            this.formQLThayDoi_rdChooseQueQuan.Size = new System.Drawing.Size(14, 13);
            this.formQLThayDoi_rdChooseQueQuan.TabIndex = 32;
            this.formQLThayDoi_rdChooseQueQuan.TabStop = true;
            this.formQLThayDoi_rdChooseQueQuan.UseVisualStyleBackColor = true;
            // 
            // formQLThayDoi_rdChooseGioiTinh
            // 
            this.formQLThayDoi_rdChooseGioiTinh.AutoSize = true;
            this.formQLThayDoi_rdChooseGioiTinh.Location = new System.Drawing.Point(4, 63);
            this.formQLThayDoi_rdChooseGioiTinh.Name = "formQLThayDoi_rdChooseGioiTinh";
            this.formQLThayDoi_rdChooseGioiTinh.Size = new System.Drawing.Size(14, 13);
            this.formQLThayDoi_rdChooseGioiTinh.TabIndex = 30;
            this.formQLThayDoi_rdChooseGioiTinh.TabStop = true;
            this.formQLThayDoi_rdChooseGioiTinh.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 40;
            this.label1.Text = "Mã công dân";
            // 
            // formQLThayDoi_grbTTNDN_btnHuy
            // 
            this.formQLThayDoi_grbTTNDN_btnHuy.Location = new System.Drawing.Point(195, 248);
            this.formQLThayDoi_grbTTNDN_btnHuy.Name = "formQLThayDoi_grbTTNDN_btnHuy";
            this.formQLThayDoi_grbTTNDN_btnHuy.Size = new System.Drawing.Size(60, 23);
            this.formQLThayDoi_grbTTNDN_btnHuy.TabIndex = 66;
            this.formQLThayDoi_grbTTNDN_btnHuy.Text = "Hủy";
            this.formQLThayDoi_grbTTNDN_btnHuy.UseVisualStyleBackColor = true;
            this.formQLThayDoi_grbTTNDN_btnHuy.Visible = false;
            this.formQLThayDoi_grbTTNDN_btnHuy.Click += new System.EventHandler(this.formQLThayDoi_grbTTNDN_btnHuy_Click);
            // 
            // formQLThayDoi_grbTTNDN_btnDongY
            // 
            this.formQLThayDoi_grbTTNDN_btnDongY.Location = new System.Drawing.Point(129, 248);
            this.formQLThayDoi_grbTTNDN_btnDongY.Name = "formQLThayDoi_grbTTNDN_btnDongY";
            this.formQLThayDoi_grbTTNDN_btnDongY.Size = new System.Drawing.Size(60, 23);
            this.formQLThayDoi_grbTTNDN_btnDongY.TabIndex = 65;
            this.formQLThayDoi_grbTTNDN_btnDongY.Text = "Đồng ý";
            this.formQLThayDoi_grbTTNDN_btnDongY.UseVisualStyleBackColor = true;
            this.formQLThayDoi_grbTTNDN_btnDongY.Visible = false;
            this.formQLThayDoi_grbTTNDN_btnDongY.Click += new System.EventHandler(this.formQLThayDoi_grbTTNDN_btnDongY_Click);
            // 
            // formQLThayDoi_grbTTNDN_btnTTNDN
            // 
            this.formQLThayDoi_grbTTNDN_btnTTNDN.Location = new System.Drawing.Point(18, 21);
            this.formQLThayDoi_grbTTNDN_btnTTNDN.Name = "formQLThayDoi_grbTTNDN_btnTTNDN";
            this.formQLThayDoi_grbTTNDN_btnTTNDN.Size = new System.Drawing.Size(170, 23);
            this.formQLThayDoi_grbTTNDN_btnTTNDN.TabIndex = 40;
            this.formQLThayDoi_grbTTNDN_btnTTNDN.Text = "Chọn thông tin người đề nghị";
            this.formQLThayDoi_grbTTNDN_btnTTNDN.UseVisualStyleBackColor = true;
            this.formQLThayDoi_grbTTNDN_btnTTNDN.Click += new System.EventHandler(this.formQLThayDoi_grbTTNDN_btnTTNDN_Click);
            // 
            // formQLThayDoi_grbTTNDN_grbGioiTinh
            // 
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.Controls.Add(this.formQLThayDoi_grbTTNDN_rdNu);
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.Controls.Add(this.formQLThayDoi_grbTTNDN_rdNam);
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.Enabled = false;
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.Location = new System.Drawing.Point(105, 141);
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.Name = "formQLThayDoi_grbTTNDN_grbGioiTinh";
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.Size = new System.Drawing.Size(151, 20);
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.TabIndex = 27;
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.TabStop = false;
            // 
            // formQLThayDoi_grbTTNDN_rdNu
            // 
            this.formQLThayDoi_grbTTNDN_rdNu.AutoSize = true;
            this.formQLThayDoi_grbTTNDN_rdNu.Location = new System.Drawing.Point(93, 2);
            this.formQLThayDoi_grbTTNDN_rdNu.Name = "formQLThayDoi_grbTTNDN_rdNu";
            this.formQLThayDoi_grbTTNDN_rdNu.Size = new System.Drawing.Size(39, 17);
            this.formQLThayDoi_grbTTNDN_rdNu.TabIndex = 1;
            this.formQLThayDoi_grbTTNDN_rdNu.TabStop = true;
            this.formQLThayDoi_grbTTNDN_rdNu.Text = "Nữ";
            this.formQLThayDoi_grbTTNDN_rdNu.UseVisualStyleBackColor = true;
            // 
            // formQLThayDoi_grbTTNDN_rdNam
            // 
            this.formQLThayDoi_grbTTNDN_rdNam.AutoSize = true;
            this.formQLThayDoi_grbTTNDN_rdNam.Location = new System.Drawing.Point(7, 2);
            this.formQLThayDoi_grbTTNDN_rdNam.Name = "formQLThayDoi_grbTTNDN_rdNam";
            this.formQLThayDoi_grbTTNDN_rdNam.Size = new System.Drawing.Size(47, 17);
            this.formQLThayDoi_grbTTNDN_rdNam.TabIndex = 0;
            this.formQLThayDoi_grbTTNDN_rdNam.TabStop = true;
            this.formQLThayDoi_grbTTNDN_rdNam.Text = "Nam";
            this.formQLThayDoi_grbTTNDN_rdNam.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(32, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Giới tính";
            // 
            // formQLThayDoi_grbTTNDN_txtHoTen
            // 
            this.formQLThayDoi_grbTTNDN_txtHoTen.Location = new System.Drawing.Point(105, 89);
            this.formQLThayDoi_grbTTNDN_txtHoTen.Name = "formQLThayDoi_grbTTNDN_txtHoTen";
            this.formQLThayDoi_grbTTNDN_txtHoTen.Size = new System.Drawing.Size(151, 20);
            this.formQLThayDoi_grbTTNDN_txtHoTen.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Họ tên";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(32, 118);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Ngày sinh";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(32, 170);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Dân tộc";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(34, 226);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Quốc tịch";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(33, 198);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Quê quán";
            // 
            // formQLThayDoi_grbTTNDN_txtDanToc
            // 
            this.formQLThayDoi_grbTTNDN_txtDanToc.Enabled = false;
            this.formQLThayDoi_grbTTNDN_txtDanToc.Location = new System.Drawing.Point(105, 167);
            this.formQLThayDoi_grbTTNDN_txtDanToc.Name = "formQLThayDoi_grbTTNDN_txtDanToc";
            this.formQLThayDoi_grbTTNDN_txtDanToc.Size = new System.Drawing.Size(151, 20);
            this.formQLThayDoi_grbTTNDN_txtDanToc.TabIndex = 18;
            // 
            // formQLThayDoi_grbTTNDN_txtQuocTich
            // 
            this.formQLThayDoi_grbTTNDN_txtQuocTich.Enabled = false;
            this.formQLThayDoi_grbTTNDN_txtQuocTich.Location = new System.Drawing.Point(104, 219);
            this.formQLThayDoi_grbTTNDN_txtQuocTich.Name = "formQLThayDoi_grbTTNDN_txtQuocTich";
            this.formQLThayDoi_grbTTNDN_txtQuocTich.Size = new System.Drawing.Size(151, 20);
            this.formQLThayDoi_grbTTNDN_txtQuocTich.TabIndex = 19;
            // 
            // formQLThayDoi_grbTTNDN_txtQueQuan
            // 
            this.formQLThayDoi_grbTTNDN_txtQueQuan.Enabled = false;
            this.formQLThayDoi_grbTTNDN_txtQueQuan.Location = new System.Drawing.Point(104, 193);
            this.formQLThayDoi_grbTTNDN_txtQueQuan.Name = "formQLThayDoi_grbTTNDN_txtQueQuan";
            this.formQLThayDoi_grbTTNDN_txtQueQuan.Size = new System.Drawing.Size(151, 20);
            this.formQLThayDoi_grbTTNDN_txtQueQuan.TabIndex = 20;
            // 
            // formQLThayDoi_grbTTNDN_dtpkNgaySinh
            // 
            this.formQLThayDoi_grbTTNDN_dtpkNgaySinh.CustomFormat = "dd/MM/yyyy";
            this.formQLThayDoi_grbTTNDN_dtpkNgaySinh.Enabled = false;
            this.formQLThayDoi_grbTTNDN_dtpkNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.formQLThayDoi_grbTTNDN_dtpkNgaySinh.Location = new System.Drawing.Point(105, 115);
            this.formQLThayDoi_grbTTNDN_dtpkNgaySinh.Name = "formQLThayDoi_grbTTNDN_dtpkNgaySinh";
            this.formQLThayDoi_grbTTNDN_dtpkNgaySinh.Size = new System.Drawing.Size(151, 20);
            this.formQLThayDoi_grbTTNDN_dtpkNgaySinh.TabIndex = 21;
            // 
            // FormQLThayDoiBoSung_dtpkHenTra
            // 
            this.FormQLThayDoiBoSung_dtpkHenTra.CustomFormat = "dd/MM/yyyy";
            this.FormQLThayDoiBoSung_dtpkHenTra.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FormQLThayDoiBoSung_dtpkHenTra.Location = new System.Drawing.Point(123, 343);
            this.FormQLThayDoiBoSung_dtpkHenTra.Name = "FormQLThayDoiBoSung_dtpkHenTra";
            this.FormQLThayDoiBoSung_dtpkHenTra.Size = new System.Drawing.Size(117, 20);
            this.FormQLThayDoiBoSung_dtpkHenTra.TabIndex = 65;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(47, 347);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 64;
            this.label19.Text = "Ngày hẹn trả";
            // 
            // FormQLThayDoiBoSung_btnBackToMenu
            // 
            this.FormQLThayDoiBoSung_btnBackToMenu.Location = new System.Drawing.Point(508, 402);
            this.FormQLThayDoiBoSung_btnBackToMenu.Name = "FormQLThayDoiBoSung_btnBackToMenu";
            this.FormQLThayDoiBoSung_btnBackToMenu.Size = new System.Drawing.Size(95, 23);
            this.FormQLThayDoiBoSung_btnBackToMenu.TabIndex = 74;
            this.FormQLThayDoiBoSung_btnBackToMenu.Text = "Quay về Menu";
            this.FormQLThayDoiBoSung_btnBackToMenu.UseVisualStyleBackColor = true;
            this.FormQLThayDoiBoSung_btnBackToMenu.Click += new System.EventHandler(this.FormQLThayDoiBoSung_btnBackToMenu_Click);
            // 
            // FormQLThayDoiBoSung_btnHuy
            // 
            this.FormQLThayDoiBoSung_btnHuy.Location = new System.Drawing.Point(508, 373);
            this.FormQLThayDoiBoSung_btnHuy.Name = "FormQLThayDoiBoSung_btnHuy";
            this.FormQLThayDoiBoSung_btnHuy.Size = new System.Drawing.Size(95, 23);
            this.FormQLThayDoiBoSung_btnHuy.TabIndex = 73;
            this.FormQLThayDoiBoSung_btnHuy.Text = "Hủy";
            this.FormQLThayDoiBoSung_btnHuy.UseVisualStyleBackColor = true;
            this.FormQLThayDoiBoSung_btnHuy.Click += new System.EventHandler(this.FormQLThayDoiBoSung_btnHuy_Click);
            // 
            // FormQLThayDoiBoSung_btnTimKiem
            // 
            this.FormQLThayDoiBoSung_btnTimKiem.Location = new System.Drawing.Point(282, 368);
            this.FormQLThayDoiBoSung_btnTimKiem.Name = "FormQLThayDoiBoSung_btnTimKiem";
            this.FormQLThayDoiBoSung_btnTimKiem.Size = new System.Drawing.Size(75, 23);
            this.FormQLThayDoiBoSung_btnTimKiem.TabIndex = 72;
            this.FormQLThayDoiBoSung_btnTimKiem.Text = "Tìm kiếm";
            this.FormQLThayDoiBoSung_btnTimKiem.UseVisualStyleBackColor = true;
            this.FormQLThayDoiBoSung_btnTimKiem.Click += new System.EventHandler(this.FormQLThayDoiBoSung_btnTimKiem_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(47, 378);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 66;
            this.label18.Text = "Trạng thái";
            // 
            // FormQLThayDoiBoSung_cbbTrangThai
            // 
            this.FormQLThayDoiBoSung_cbbTrangThai.FormattingEnabled = true;
            this.FormQLThayDoiBoSung_cbbTrangThai.Items.AddRange(new object[] {
            "Tất cả",
            "Tiếp nhận, chờ xác nhận",
            "Đã xác minh, chờ duyệt",
            "Không xác minh",
            "Đã duyệt",
            "Không duyệt",
            "Đã trả",
            "Đã thanh toán"});
            this.FormQLThayDoiBoSung_cbbTrangThai.Location = new System.Drawing.Point(123, 370);
            this.FormQLThayDoiBoSung_cbbTrangThai.Name = "FormQLThayDoiBoSung_cbbTrangThai";
            this.FormQLThayDoiBoSung_cbbTrangThai.Size = new System.Drawing.Size(152, 21);
            this.FormQLThayDoiBoSung_cbbTrangThai.TabIndex = 71;
            this.FormQLThayDoiBoSung_cbbTrangThai.SelectionChangeCommitted += new System.EventHandler(this.FormQLThayDoiBoSung_cbbTrangThai_SelectionChangeCommitted);
            // 
            // FormQLThayDoiBoSung_btnTraKetQua
            // 
            this.FormQLThayDoiBoSung_btnTraKetQua.Location = new System.Drawing.Point(335, 402);
            this.FormQLThayDoiBoSung_btnTraKetQua.Name = "FormQLThayDoiBoSung_btnTraKetQua";
            this.FormQLThayDoiBoSung_btnTraKetQua.Size = new System.Drawing.Size(97, 23);
            this.FormQLThayDoiBoSung_btnTraKetQua.TabIndex = 70;
            this.FormQLThayDoiBoSung_btnTraKetQua.Text = "Trả kết quả";
            this.FormQLThayDoiBoSung_btnTraKetQua.UseVisualStyleBackColor = true;
            this.FormQLThayDoiBoSung_btnTraKetQua.Click += new System.EventHandler(this.FormQLThayDoiBoSung_btnTraKetQua_Click);
            // 
            // FormQLThayDoiBoSung_btnDuyetHoSo
            // 
            this.FormQLThayDoiBoSung_btnDuyetHoSo.Enabled = false;
            this.FormQLThayDoiBoSung_btnDuyetHoSo.Location = new System.Drawing.Point(232, 402);
            this.FormQLThayDoiBoSung_btnDuyetHoSo.Name = "FormQLThayDoiBoSung_btnDuyetHoSo";
            this.FormQLThayDoiBoSung_btnDuyetHoSo.Size = new System.Drawing.Size(97, 23);
            this.FormQLThayDoiBoSung_btnDuyetHoSo.TabIndex = 69;
            this.FormQLThayDoiBoSung_btnDuyetHoSo.Text = "Duyệt hồ sơ";
            this.FormQLThayDoiBoSung_btnDuyetHoSo.UseVisualStyleBackColor = true;
            this.FormQLThayDoiBoSung_btnDuyetHoSo.Click += new System.EventHandler(this.FormQLThayDoiBoSung_btnDuyetHoSo_Click);
            // 
            // FormQLThayDoiBoSung_btnXacNhan
            // 
            this.FormQLThayDoiBoSung_btnXacNhan.Enabled = false;
            this.FormQLThayDoiBoSung_btnXacNhan.Location = new System.Drawing.Point(129, 402);
            this.FormQLThayDoiBoSung_btnXacNhan.Name = "FormQLThayDoiBoSung_btnXacNhan";
            this.FormQLThayDoiBoSung_btnXacNhan.Size = new System.Drawing.Size(97, 23);
            this.FormQLThayDoiBoSung_btnXacNhan.TabIndex = 68;
            this.FormQLThayDoiBoSung_btnXacNhan.Text = "Xác nhận hồ sơ";
            this.FormQLThayDoiBoSung_btnXacNhan.UseVisualStyleBackColor = true;
            this.FormQLThayDoiBoSung_btnXacNhan.Click += new System.EventHandler(this.FormQLThayDoiBoSung_btnXacNhan_Click);
            // 
            // FormQLThayDoiBoSung_btnThemHoSo
            // 
            this.FormQLThayDoiBoSung_btnThemHoSo.Location = new System.Drawing.Point(48, 402);
            this.FormQLThayDoiBoSung_btnThemHoSo.Name = "FormQLThayDoiBoSung_btnThemHoSo";
            this.FormQLThayDoiBoSung_btnThemHoSo.Size = new System.Drawing.Size(75, 23);
            this.FormQLThayDoiBoSung_btnThemHoSo.TabIndex = 67;
            this.FormQLThayDoiBoSung_btnThemHoSo.Text = "Thêm hồ sơ";
            this.FormQLThayDoiBoSung_btnThemHoSo.UseVisualStyleBackColor = true;
            this.FormQLThayDoiBoSung_btnThemHoSo.Click += new System.EventHandler(this.FormQLThayDoiBoSung_btnThemHoSo_Click);
            // 
            // grvDSDangKiThayDoi
            // 
            this.grvDSDangKiThayDoi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvDSDangKiThayDoi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaThayDoi,
            this.MaCongDanYC,
            this.MaCongDanDN,
            this.QuanHeVoiNDN,
            this.MucThayDoi,
            this.LD,
            this.GTC,
            this.GTM,
            this.NgayDangKy,
            this.NgayHen,
            this.NgayTra,
            this.TrangThai,
            this.MTT});
            this.grvDSDangKiThayDoi.Location = new System.Drawing.Point(12, 449);
            this.grvDSDangKiThayDoi.Name = "grvDSDangKiThayDoi";
            this.grvDSDangKiThayDoi.Size = new System.Drawing.Size(668, 150);
            this.grvDSDangKiThayDoi.TabIndex = 75;
            this.grvDSDangKiThayDoi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grvDSDangKiThayDoi_CellClick);
            // 
            // MaThayDoi
            // 
            this.MaThayDoi.HeaderText = "Mã thay đổi";
            this.MaThayDoi.Name = "MaThayDoi";
            // 
            // MaCongDanYC
            // 
            this.MaCongDanYC.HeaderText = "Mã CDYC";
            this.MaCongDanYC.Name = "MaCongDanYC";
            // 
            // MaCongDanDN
            // 
            this.MaCongDanDN.HeaderText = "Mã CDDN";
            this.MaCongDanDN.Name = "MaCongDanDN";
            // 
            // QuanHeVoiNDN
            // 
            this.QuanHeVoiNDN.HeaderText = "Quan hệ";
            this.QuanHeVoiNDN.Name = "QuanHeVoiNDN";
            // 
            // MucThayDoi
            // 
            this.MucThayDoi.HeaderText = "Mục thay đổi";
            this.MucThayDoi.Name = "MucThayDoi";
            // 
            // LD
            // 
            this.LD.HeaderText = "Lý do";
            this.LD.Name = "LD";
            // 
            // GTC
            // 
            this.GTC.HeaderText = "Giá trị cũ";
            this.GTC.Name = "GTC";
            // 
            // GTM
            // 
            this.GTM.HeaderText = "Giá trị mới";
            this.GTM.Name = "GTM";
            // 
            // NgayDangKy
            // 
            this.NgayDangKy.HeaderText = "Ngày đăng ký";
            this.NgayDangKy.Name = "NgayDangKy";
            // 
            // NgayHen
            // 
            this.NgayHen.HeaderText = "Ngày hẹn";
            this.NgayHen.Name = "NgayHen";
            // 
            // NgayTra
            // 
            this.NgayTra.HeaderText = "Ngày trả";
            this.NgayTra.Name = "NgayTra";
            // 
            // TrangThai
            // 
            this.TrangThai.HeaderText = "Trạng thái";
            this.TrangThai.Name = "TrangThai";
            // 
            // MTT
            // 
            this.MTT.HeaderText = "Mã trạng thái";
            this.MTT.Name = "MTT";
            // 
            // grb_giaTriMoi
            // 
            this.grb_giaTriMoi.Controls.Add(this.txtGiaTriMoiHoTen);
            this.grb_giaTriMoi.Controls.Add(this.txtGiaTriMoiNgaySinh);
            this.grb_giaTriMoi.Controls.Add(this.grbGiaTriMoiGioiTinh);
            this.grb_giaTriMoi.Controls.Add(this.txtGiaTriMoiDanToc);
            this.grb_giaTriMoi.Controls.Add(this.txtGiaTriMoiQuocTich);
            this.grb_giaTriMoi.Controls.Add(this.txtGiaTriMoiQueQuan);
            this.grb_giaTriMoi.Enabled = false;
            this.grb_giaTriMoi.Location = new System.Drawing.Point(609, 54);
            this.grb_giaTriMoi.Name = "grb_giaTriMoi";
            this.grb_giaTriMoi.Size = new System.Drawing.Size(122, 277);
            this.grb_giaTriMoi.TabIndex = 76;
            this.grb_giaTriMoi.TabStop = false;
            this.grb_giaTriMoi.Text = "Giá trị mới";
            this.grb_giaTriMoi.Visible = false;
            // 
            // txtGiaTriMoiHoTen
            // 
            this.txtGiaTriMoiHoTen.Location = new System.Drawing.Point(7, 89);
            this.txtGiaTriMoiHoTen.Name = "txtGiaTriMoiHoTen";
            this.txtGiaTriMoiHoTen.Size = new System.Drawing.Size(108, 20);
            this.txtGiaTriMoiHoTen.TabIndex = 42;
            // 
            // txtGiaTriMoiNgaySinh
            // 
            this.txtGiaTriMoiNgaySinh.CustomFormat = "dd/MM/yyyy";
            this.txtGiaTriMoiNgaySinh.Enabled = false;
            this.txtGiaTriMoiNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtGiaTriMoiNgaySinh.Location = new System.Drawing.Point(7, 115);
            this.txtGiaTriMoiNgaySinh.Name = "txtGiaTriMoiNgaySinh";
            this.txtGiaTriMoiNgaySinh.Size = new System.Drawing.Size(108, 20);
            this.txtGiaTriMoiNgaySinh.TabIndex = 46;
            // 
            // grbGiaTriMoiGioiTinh
            // 
            this.grbGiaTriMoiGioiTinh.Controls.Add(this.rdGiaTriMoi_GTNau);
            this.grbGiaTriMoiGioiTinh.Controls.Add(this.rdGiaTriMoi_GTNam);
            this.grbGiaTriMoiGioiTinh.Enabled = false;
            this.grbGiaTriMoiGioiTinh.Location = new System.Drawing.Point(7, 141);
            this.grbGiaTriMoiGioiTinh.Name = "grbGiaTriMoiGioiTinh";
            this.grbGiaTriMoiGioiTinh.Size = new System.Drawing.Size(108, 20);
            this.grbGiaTriMoiGioiTinh.TabIndex = 47;
            this.grbGiaTriMoiGioiTinh.TabStop = false;
            // 
            // rdGiaTriMoi_GTNau
            // 
            this.rdGiaTriMoi_GTNau.AutoSize = true;
            this.rdGiaTriMoi_GTNau.Location = new System.Drawing.Point(63, 3);
            this.rdGiaTriMoi_GTNau.Name = "rdGiaTriMoi_GTNau";
            this.rdGiaTriMoi_GTNau.Size = new System.Drawing.Size(39, 17);
            this.rdGiaTriMoi_GTNau.TabIndex = 1;
            this.rdGiaTriMoi_GTNau.TabStop = true;
            this.rdGiaTriMoi_GTNau.Text = "Nữ";
            this.rdGiaTriMoi_GTNau.UseVisualStyleBackColor = true;
            // 
            // rdGiaTriMoi_GTNam
            // 
            this.rdGiaTriMoi_GTNam.AutoSize = true;
            this.rdGiaTriMoi_GTNam.Location = new System.Drawing.Point(7, 2);
            this.rdGiaTriMoi_GTNam.Name = "rdGiaTriMoi_GTNam";
            this.rdGiaTriMoi_GTNam.Size = new System.Drawing.Size(47, 17);
            this.rdGiaTriMoi_GTNam.TabIndex = 0;
            this.rdGiaTriMoi_GTNam.TabStop = true;
            this.rdGiaTriMoi_GTNam.Text = "Nam";
            this.rdGiaTriMoi_GTNam.UseVisualStyleBackColor = true;
            // 
            // txtGiaTriMoiDanToc
            // 
            this.txtGiaTriMoiDanToc.Enabled = false;
            this.txtGiaTriMoiDanToc.Location = new System.Drawing.Point(7, 167);
            this.txtGiaTriMoiDanToc.Name = "txtGiaTriMoiDanToc";
            this.txtGiaTriMoiDanToc.Size = new System.Drawing.Size(108, 20);
            this.txtGiaTriMoiDanToc.TabIndex = 43;
            // 
            // txtGiaTriMoiQuocTich
            // 
            this.txtGiaTriMoiQuocTich.Enabled = false;
            this.txtGiaTriMoiQuocTich.Location = new System.Drawing.Point(6, 219);
            this.txtGiaTriMoiQuocTich.Name = "txtGiaTriMoiQuocTich";
            this.txtGiaTriMoiQuocTich.Size = new System.Drawing.Size(108, 20);
            this.txtGiaTriMoiQuocTich.TabIndex = 44;
            // 
            // txtGiaTriMoiQueQuan
            // 
            this.txtGiaTriMoiQueQuan.Enabled = false;
            this.txtGiaTriMoiQueQuan.Location = new System.Drawing.Point(6, 193);
            this.txtGiaTriMoiQueQuan.Name = "txtGiaTriMoiQueQuan";
            this.txtGiaTriMoiQueQuan.Size = new System.Drawing.Size(108, 20);
            this.txtGiaTriMoiQueQuan.TabIndex = 45;
            // 
            // FormQLThayDoiBoSung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 624);
            this.ControlBox = false;
            this.Controls.Add(this.grb_giaTriMoi);
            this.Controls.Add(this.grvDSDangKiThayDoi);
            this.Controls.Add(this.FormQLThayDoiBoSung_btnBackToMenu);
            this.Controls.Add(this.FormQLThayDoiBoSung_btnHuy);
            this.Controls.Add(this.FormQLThayDoiBoSung_btnTimKiem);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.FormQLThayDoiBoSung_cbbTrangThai);
            this.Controls.Add(this.FormQLThayDoiBoSung_btnTraKetQua);
            this.Controls.Add(this.FormQLThayDoiBoSung_btnDuyetHoSo);
            this.Controls.Add(this.FormQLThayDoiBoSung_btnXacNhan);
            this.Controls.Add(this.FormQLThayDoiBoSung_btnThemHoSo);
            this.Controls.Add(this.FormQLThayDoiBoSung_dtpkHenTra);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.formQLThayDoi_grbTTNDN);
            this.Controls.Add(this.formThayDoi_grbTTNYC);
            this.Controls.Add(this.formQLThayDoi_TT);
            this.Controls.Add(this.formQLThayDoi_lblTTHS);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormQLThayDoiBoSung";
            this.Text = "Đăng kí thay đổi cải chính bổ sung hộ tịch";
            this.formThayDoi_grbTTNYC.ResumeLayout(false);
            this.formThayDoi_grbTTNYC.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.formQLThayDoi_grbTTNDN.ResumeLayout(false);
            this.formQLThayDoi_grbTTNDN.PerformLayout();
            this.formQLThayDoi_grbRadioButtonChoose.ResumeLayout(false);
            this.formQLThayDoi_grbRadioButtonChoose.PerformLayout();
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.ResumeLayout(false);
            this.formQLThayDoi_grbTTNDN_grbGioiTinh.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvDSDangKiThayDoi)).EndInit();
            this.grb_giaTriMoi.ResumeLayout(false);
            this.grb_giaTriMoi.PerformLayout();
            this.grbGiaTriMoiGioiTinh.ResumeLayout(false);
            this.grbGiaTriMoiGioiTinh.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label formQLThayDoi_lblTTHS;
        private System.Windows.Forms.Label formQLThayDoi_TT;
        private System.Windows.Forms.GroupBox formThayDoi_grbTTNYC;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton formThayDoi_grbTTNYC_rdNu;
        private System.Windows.Forms.RadioButton formThayDoi_grbTTNYC_rdNam;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox formThayDoi_grbTTNYC_txtbHoTen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox formThayDoi_grbTTNYC_txtDanToc;
        private System.Windows.Forms.TextBox formThayDoi_grbTTNYC_txtQuocTich;
        private System.Windows.Forms.TextBox formThayDoi_grbTTNYC_txtQueQuan;
        private System.Windows.Forms.DateTimePicker formThayDoi_grbTTNYC_dtpNgaySinh;
        private System.Windows.Forms.GroupBox formQLThayDoi_grbTTNDN;
        private System.Windows.Forms.GroupBox formQLThayDoi_grbTTNDN_grbGioiTinh;
        private System.Windows.Forms.RadioButton formQLThayDoi_grbTTNDN_rdNu;
        private System.Windows.Forms.RadioButton formQLThayDoi_grbTTNDN_rdNam;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox formQLThayDoi_grbTTNDN_txtHoTen;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox formQLThayDoi_grbTTNDN_txtDanToc;
        private System.Windows.Forms.TextBox formQLThayDoi_grbTTNDN_txtQuocTich;
        private System.Windows.Forms.TextBox formQLThayDoi_grbTTNDN_txtQueQuan;
        private System.Windows.Forms.DateTimePicker formQLThayDoi_grbTTNDN_dtpkNgaySinh;
        private System.Windows.Forms.Button formThayDoi_grbTTNYC_btnChonNYC;
        private System.Windows.Forms.TextBox formThayDoi_grbTTNYC_txtMaCongDan;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox formThayDoi_grbTTNYC_txtQuanHe;
        private System.Windows.Forms.RadioButton formQLThayDoi_rdChooseHoTen;
        private System.Windows.Forms.RadioButton formQLThayDoi_rdChooseQuocTich;
        private System.Windows.Forms.RadioButton formQLThayDoi_rdChooseQueQuan;
        private System.Windows.Forms.RadioButton formQLThayDoi_rdChooseDanToc;
        private System.Windows.Forms.RadioButton formQLThayDoi_rdChooseGioiTinh;
        private System.Windows.Forms.RadioButton formQLThayDoi_rdChooseNgaySinh;
        private System.Windows.Forms.Button formQLThayDoi_grbTTNDN_btnTTNDN;
        private System.Windows.Forms.Button formQLThayDoi_grbTTNDN_btnHuy;
        private System.Windows.Forms.Button formQLThayDoi_grbTTNDN_btnDongY;
        private System.Windows.Forms.DateTimePicker FormQLThayDoiBoSung_dtpkHenTra;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button FormQLThayDoiBoSung_btnBackToMenu;
        private System.Windows.Forms.Button FormQLThayDoiBoSung_btnHuy;
        private System.Windows.Forms.Button FormQLThayDoiBoSung_btnTimKiem;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox FormQLThayDoiBoSung_cbbTrangThai;
        private System.Windows.Forms.Button FormQLThayDoiBoSung_btnTraKetQua;
        private System.Windows.Forms.Button FormQLThayDoiBoSung_btnDuyetHoSo;
        private System.Windows.Forms.Button FormQLThayDoiBoSung_btnXacNhan;
        private System.Windows.Forms.Button FormQLThayDoiBoSung_btnThemHoSo;
        private System.Windows.Forms.GroupBox formQLThayDoi_grbRadioButtonChoose;
        private System.Windows.Forms.TextBox formQLThayDoi_grbTTNDN_txtMaCDDN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView grvDSDangKiThayDoi;
        private System.Windows.Forms.GroupBox grb_giaTriMoi;
        private System.Windows.Forms.TextBox txtGiaTriMoiHoTen;
        private System.Windows.Forms.DateTimePicker txtGiaTriMoiNgaySinh;
        private System.Windows.Forms.GroupBox grbGiaTriMoiGioiTinh;
        private System.Windows.Forms.RadioButton rdGiaTriMoi_GTNau;
        private System.Windows.Forms.RadioButton rdGiaTriMoi_GTNam;
        private System.Windows.Forms.TextBox txtGiaTriMoiDanToc;
        private System.Windows.Forms.TextBox txtGiaTriMoiQuocTich;
        private System.Windows.Forms.TextBox txtGiaTriMoiQueQuan;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaThayDoi;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCongDanYC;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCongDanDN;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuanHeVoiNDN;
        private System.Windows.Forms.DataGridViewTextBoxColumn MucThayDoi;
        private System.Windows.Forms.DataGridViewTextBoxColumn LD;
        private System.Windows.Forms.DataGridViewTextBoxColumn GTC;
        private System.Windows.Forms.DataGridViewTextBoxColumn GTM;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayDangKy;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayHen;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayTra;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai;
        private System.Windows.Forms.DataGridViewTextBoxColumn MTT;
    }
}