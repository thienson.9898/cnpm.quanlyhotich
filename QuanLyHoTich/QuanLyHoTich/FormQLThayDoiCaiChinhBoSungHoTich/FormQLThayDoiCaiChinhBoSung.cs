﻿using QLHT.Entity;
using QLHT.Entity.Dictionary;
using QLHT.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHoTich.FormQLThayDoiCaiChinhBoSungHoTich
{
    public partial class FormQLThayDoiBoSung : Form
    {
        string chuoikn = @"Data Source=.\SQLEXPRESS;Initial Catalog=qlhotich;Integrated Security=True";
        public static FormQLThayDoiBoSung instance;
        CongDan congDanYeuCau = new CongDan();
        CongDan congDanDeNghi = new CongDan();
        CongDanModel CongDanModel = new CongDanModel();
        KhaiSinhModel khaiSinhModel = new KhaiSinhModel();
        int maDangKiCurrent = 0;
        int MaCongDanNDNCurrent = 0;
        string HoTenNDNCurrent = "";
        string QueQuanCurrent = "";
        string DanTocCurrent = "";
        string QuocTichCurrent = "";
        DateTime NgaySinhCurrent;
        DangKyThayDoiModel DangKyThayDoi = new DangKyThayDoiModel();
        TrangThaiModel TrangThaiModel = new TrangThaiModel();
        String Quyen = "";
        public bool isCheckChanged = false;
        public CongDan congDan_NguoiYeuCau = new CongDan();
        public CongDan congDan_NguoiDeNghi = new CongDan();
        public bool choose_CongDan_NguoiYeuCau = false;
        public bool choose_CongDan_NguoiDeNghi = false;
        public int kt;
        public bool isClickedAccept = false;
        public String mucCanThayDoi = "";
        string lyDo = "";      
        private int rowIndex;
        int maTrangThai;
        public  String giaTriCu ;
        public String giaTriMoi ;
        public static FormQLThayDoiBoSung Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormQLThayDoiBoSung();
                }
                return instance;
            }
        }
     
        public FormQLThayDoiBoSung()
        {
            InitializeComponent();
            GetQuyen();
            load_cbbTrangThai();
            setFalseAllControlsTTNDN();
            setFalseAllControlsTTNYC();
            RadioButtonEvent();
            loadData();
            formThayDoi_grbTTNYC_dtpNgaySinh.CustomFormat = "dd/MM/yyyy";
            formQLThayDoi_grbTTNDN_dtpkNgaySinh.CustomFormat = "dd/MM/yyyy";
            FormQLThayDoiBoSung_dtpkHenTra.CustomFormat = "dd/MM/yyyy";
        }
        public void load_cbbTrangThai()
        {
            DataTable dataTrangThai = new DataTable();
            //dataTrabgThai = chuoikn.getDataTable("Select_CbbMaCanBo");
           // DataTable dataTable = new DataTable();
            SqlDataAdapter dataAdapter;
            using (SqlConnection connection = new SqlConnection(chuoikn))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "select_MaTrangThai";
                    dataAdapter = new SqlDataAdapter(command);
                    dataAdapter.Fill(dataTrangThai);
                    FormQLThayDoiBoSung_cbbTrangThai.DataSource = dataTrangThai;
                    FormQLThayDoiBoSung_cbbTrangThai.DisplayMember = "TrangThai";
                    FormQLThayDoiBoSung_cbbTrangThai.ValueMember = "MaTrangThai";
                }
            }
          
           
        }
        void RadioButtonEvent()
        {
            foreach (Control ctl in formQLThayDoi_grbRadioButtonChoose.Controls)
            {
                if (ctl is RadioButton)
                {
                    var rdBtn = (RadioButton)ctl;
                    rdBtn.CheckedChanged += new EventHandler(setEnableControlsTTNDN);
                }
            }
        }

        private void loadData()
        {
            try
            {
                grvDSDangKiThayDoi.Rows.Clear();
                List<DangKyThayDoi> dsDangKyThayDoi = DangKyThayDoi.GetData();
                foreach (var dangKy in dsDangKyThayDoi)
                {
                    var index = grvDSDangKiThayDoi.Rows.Add();
                    grvDSDangKiThayDoi.Rows[index].Cells["MaThayDoi"].Value = dangKy.MaThayDoi;
                    grvDSDangKiThayDoi.Rows[index].Cells["MaCongDanYC"].Value = dangKy.MaCongDanYC;
                    grvDSDangKiThayDoi.Rows[index].Cells["MaCongDanDN"].Value = dangKy.MaCongDanDN;
                    grvDSDangKiThayDoi.Rows[index].Cells["QuanHeVoiNDN"].Value = dangKy.QuanHeVoiNDN;
                    grvDSDangKiThayDoi.Rows[index].Cells["MucThayDoi"].Value = dangKy.MucCanThayDoi;
                    grvDSDangKiThayDoi.Rows[index].Cells["LD"].Value = dangKy.LyDo;
                    grvDSDangKiThayDoi.Rows[index].Cells["GTC"].Value = dangKy.GiaTriCu;
                    grvDSDangKiThayDoi.Rows[index].Cells["GTM"].Value = dangKy.GiaTriMoi;

                    //   dgrv_KhaiSinh.Rows[index].Cells["NgaySinh"].Value = khaiSinh.NgaySinh.ToString("dd/MM/yyyy");

                  
                    grvDSDangKiThayDoi.Rows[index].Cells["MTT"].Value = (dangKy.TrangThai);
                    grvDSDangKiThayDoi.Rows[index].Cells["TrangThai"].Value = TrangThaiModel.GetTrangThaiVietTatByMaTrangThai(dangKy.TrangThai.ToString());
                    grvDSDangKiThayDoi.Rows[index].Cells["NgayDangKy"].Value = dangKy.NgayDangKy == null ? "" : Convert.ToDateTime(dangKy.NgayDangKy).ToString("dd/MM/yyyy");
                    grvDSDangKiThayDoi.Rows[index].Cells["NgayHen"].Value = dangKy.NgayHen == null ? "" : Convert.ToDateTime(dangKy.NgayHen).ToString("dd/MM/yyyy");
                    grvDSDangKiThayDoi.Rows[index].Cells["NgayTra"].Value = dangKy.NgayTra == null ? "" : Convert.ToDateTime(dangKy.NgayTra).ToString("dd/MM/yyyy");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra!");
            }
        }
        void SetFalseAllButton()
        {
            FormQLThayDoiBoSung_btnXacNhan.Enabled = false;
            FormQLThayDoiBoSung_btnThemHoSo.Enabled = false;
            FormQLThayDoiBoSung_btnDuyetHoSo.Enabled = false;
            FormQLThayDoiBoSung_btnTraKetQua.Enabled = false;
        }

        private void FormQLThayDoiBoSung_btnHuy_Click(object sender, EventArgs e)
        {
            formQLThayDoi_lblTTHS.Visible = false;
            formQLThayDoi_TT.Visible = false;

            foreach (Control ctr in formQLThayDoi_grbTTNDN.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }
            foreach (Control ctr in formThayDoi_grbTTNYC.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }
            formQLThayDoi_grbRadioButtonChoose.Enabled = false;
            SetFalseAllButton();
            if (Constants.isTN_TKQ)
            {
                FormQLThayDoiBoSung_btnThemHoSo.Enabled = true;
            }
            ////
            
        }

        private void FormQLThayDoiBoSung_btnBackToMenu_Click(object sender, EventArgs e)
        {
            FormMenu.Instance.Show();
            FormQLThayDoiBoSung.Instance.Close();
        }
        public void GetQuyen()
        {
            Quyen = new CongChucModel().GetQuyen(Constants.MaCongChucDangNhap);
            SetFalseAllButton();
            //Ktra 5 quyền
            Debug.WriteLine("Send to debug output.");

            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanTiepNhanVaTraKQ))
            {
                Debug.WriteLine("isTN_TKQ");
                Constants.isTN_TKQ = true;
                FormQLThayDoiBoSung_btnThemHoSo.Enabled = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatCongChucCapXa))
            {

                Constants.isCCCX = true;
                Debug.WriteLine("isCCCX");
                //  btnXacNhan.Enabled = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatLanhDao))
            {
                // btnDuyet.Enabled = true;
                Debug.WriteLine("isLD");
                Constants.isLD = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanVanPhong))
            {
                Debug.WriteLine("isVP");
                // btnTraKQ.Enabled = true;
                Constants.isVP = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanKTTC))
            {
                Debug.WriteLine("isKTTC");
                Constants.isKTTC = true;
            }
            //Get trạng thái hồ sơ để bật/tắt nút
        }
        //public void GetQuyen()
        //{
        //    Quyen = new CongChucModel().GetQuyen(Constants.MaCongChucDangNhap);
        //    SetFalseAllButton();
        //    //Ktra 5 quyền
        //    if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanTiepNhanVaTraKQ))
        //    {
        //        Constants.isTN_TKQ = true;
        //        FormQLThayDoiBoSung_btnThemHoSo.Enabled = true;
        //    }
        //    if (Quyen.Contains(BoPhanModel.TenVietTatCongChucCapXa))
        //    {
        //        Constants.isCCCX = true;
              
        //        //  btnXacNhan.Enabled = true;
        //    }
        //    if (Quyen.Contains(BoPhanModel.TenVietTatLanhDao))
        //    {
        //        // btnDuyet.Enabled = true;
        //        Constants.isLD = true;
              
        //    }
        //    if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanVanPhong))
        //    {
        //        // btnTraKQ.Enabled = true;
        //        Constants.isVP = true;
              
        //    }
        //    if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanKTTC))
        //    {
        //        Constants.isKTTC = true;
               
        //    }
        //    //Get trạng thái hồ sơ để bật/tắt nút
        //}
        public void setFalseAllControlsTTNDN()
        {
            formQLThayDoi_grbTTNDN_txtMaCDDN.Enabled = false;
            formQLThayDoi_grbTTNDN_txtHoTen.Enabled = false;
            formQLThayDoi_grbTTNDN_dtpkNgaySinh.Enabled = false;
            formQLThayDoi_grbTTNDN_grbGioiTinh.Enabled = false;
            formQLThayDoi_grbTTNDN_txtDanToc.Enabled = false;
            formQLThayDoi_grbTTNDN_txtQuocTich.Enabled = false;
            formQLThayDoi_grbTTNDN_txtQueQuan.Enabled = false;


        }
        public void setFalseAllControlsTTNYC()
        {
            formThayDoi_grbTTNYC_txtMaCongDan.Enabled = false;
            formThayDoi_grbTTNYC_txtbHoTen.Enabled = false;
            formThayDoi_grbTTNYC_dtpNgaySinh.Enabled = false;
            groupBox4.Enabled = false;
            formThayDoi_grbTTNYC_txtDanToc.Enabled = false;
            formThayDoi_grbTTNYC_txtQueQuan.Enabled = false;
            formThayDoi_grbTTNYC_txtQuocTich.Enabled = false;


        }

        private void formQLThayDoi_grbTTNDN_btnHuy_Click(object sender, EventArgs e)
        {
            setValueForTTNDN();
            setFalseAllControlsTTNDN();
            setFalseAllChooseThayDoiNDN();

        }
 
        private void formThayDoi_grbTTNYC_btnChonNYC_Click(object sender, EventArgs e)
        {
            // fixbug lần 1 nvHieu
            FormQLCongDan.Instance.isFormThayDoiClick = true; 
            FormQLCongDan.Instance.isChonCongDan = true;
            ////
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong && FormQLCongDan.Instance.isBackToFormThayDoi)
            {
                choose_CongDan_NguoiYeuCau = true;
                MessageBox.Show("Chọn thành công");
                congDan_NguoiYeuCau = FormQLCongDan.Instance.congDan;
                formThayDoi_grbTTNYC_txtMaCongDan.Text = congDan_NguoiYeuCau.MaCongDan.ToString();
                formThayDoi_grbTTNYC_txtbHoTen.Text = String.IsNullOrEmpty(congDan_NguoiYeuCau.HoTen) ? "" : congDan_NguoiYeuCau.HoTen;
                formThayDoi_grbTTNYC_dtpNgaySinh.Value = congDan_NguoiYeuCau.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(congDan_NguoiYeuCau.NgaySinh);
                formThayDoi_grbTTNYC_txtDanToc.Text = String.IsNullOrEmpty(congDan_NguoiYeuCau.DanToc) ? "" : congDan_NguoiYeuCau.DanToc;
                formThayDoi_grbTTNYC_rdNam.Checked = congDan_NguoiYeuCau.GioiTinh == true ? true : false;
                formThayDoi_grbTTNYC_rdNu.Checked = congDan_NguoiYeuCau.GioiTinh == false ? true : false;
                formThayDoi_grbTTNYC_txtQueQuan.Text = String.IsNullOrEmpty(congDan_NguoiYeuCau.QueQuan) ? "" : congDan_NguoiYeuCau.QueQuan;
                formThayDoi_grbTTNYC_txtQuocTich.Text = String.IsNullOrEmpty(congDan_NguoiYeuCau.QuocTich) ? "" : congDan_NguoiYeuCau.QuocTich;

            }
            FormQLCongDan.Instance.Close();
        }
        
     
        private void formQLThayDoi_grbTTNDN_btnTTNDN_Click(object sender, EventArgs e)
        {
            //isClickedAccept = false;
            // fixbug lần 1 nvHieu
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.isFormThayDoiClick = true;
            ////////
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong && FormQLCongDan.Instance.isBackToFormThayDoi)
            {
                choose_CongDan_NguoiDeNghi = true;
                MessageBox.Show("Chọn thành công");
                congDan_NguoiDeNghi = FormQLCongDan.Instance.congDan;
                formQLThayDoi_grbTTNDN_txtMaCDDN.Text = congDan_NguoiDeNghi.MaCongDan.ToString();
                formQLThayDoi_grbTTNDN_txtHoTen.Text = String.IsNullOrEmpty(congDan_NguoiDeNghi.HoTen) ? "" : congDan_NguoiDeNghi.HoTen;
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Value = congDan_NguoiDeNghi.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(congDan_NguoiDeNghi.NgaySinh);
                formQLThayDoi_grbTTNDN_txtDanToc.Text = String.IsNullOrEmpty(congDan_NguoiDeNghi.DanToc) ? "" : congDan_NguoiDeNghi.DanToc;
                formQLThayDoi_grbTTNDN_rdNam.Checked = congDan_NguoiDeNghi.GioiTinh == true ? true : false;
                formQLThayDoi_grbTTNDN_rdNu.Checked = congDan_NguoiDeNghi.GioiTinh == false ? true : false;
                formQLThayDoi_grbTTNDN_txtQueQuan.Text = String.IsNullOrEmpty(congDan_NguoiDeNghi.QueQuan) ? "" : congDan_NguoiDeNghi.QueQuan;
                formQLThayDoi_grbTTNDN_txtQuocTich.Text = String.IsNullOrEmpty(congDan_NguoiDeNghi.QuocTich) ? "" : congDan_NguoiDeNghi.QuocTich;
                setFalseAllControlsTTNDN();
                setFalseAllChooseThayDoiNDN();
                setValueTagForTTNDN();               
                formQLThayDoi_grbRadioButtonChoose.Visible = true;             
                // formQLThayDoi_grbTTNDN_grbGioiTinh.Tag = formQLThayDoi_grbTTNDN_grbGioiTinh.Text.ToString();

            }
            FormQLCongDan.Instance.Close();
        }
        string hoTenTag = "";
        private void setValueTagForTTNDN()
        {
            formQLThayDoi_grbTTNDN_txtHoTen.Tag = formQLThayDoi_grbTTNDN_txtHoTen.Text.ToString();
            formQLThayDoi_grbTTNDN_dtpkNgaySinh.Tag = formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text.ToString();
            formQLThayDoi_grbTTNDN_txtDanToc.Tag = formQLThayDoi_grbTTNDN_txtDanToc.Text.ToString();
            formQLThayDoi_grbTTNDN_txtQueQuan.Tag = formQLThayDoi_grbTTNDN_txtQueQuan.Text.ToString();
            formQLThayDoi_grbTTNDN_txtQuocTich.Tag = formQLThayDoi_grbTTNDN_txtQuocTich.Text.ToString();
            hoTenTag = formQLThayDoi_grbTTNDN_txtHoTen.Text.ToString();
            if (formQLThayDoi_grbTTNDN_rdNu.Checked == true)
            {
                kt = 0;
            }
            else
            {
                kt = 1;
            }
        }
        private void setValueForTTNDN()
        {
            formQLThayDoi_grbTTNDN_txtHoTen.Text = formQLThayDoi_grbTTNDN_txtHoTen.Tag.ToString();
            formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text = formQLThayDoi_grbTTNDN_dtpkNgaySinh.Tag.ToString();
            formQLThayDoi_grbTTNDN_txtDanToc.Text = formQLThayDoi_grbTTNDN_txtDanToc.Tag.ToString();
            formQLThayDoi_grbTTNDN_txtQueQuan.Text = formQLThayDoi_grbTTNDN_txtQueQuan.Tag.ToString();
            formQLThayDoi_grbTTNDN_txtQuocTich.Text = formQLThayDoi_grbTTNDN_txtQuocTich.Tag.ToString();
            if (kt == 0)
            {
                formQLThayDoi_grbTTNDN_rdNu.Checked = true;
            }
            else
            {
                formQLThayDoi_grbTTNDN_rdNam.Checked = true;
            }
        }
        private void setFalseAllChooseThayDoiNDN()
        {
            formQLThayDoi_rdChooseHoTen.Checked = false;
            formQLThayDoi_rdChooseNgaySinh.Checked = false;
            formQLThayDoi_rdChooseGioiTinh.Checked = false;
            formQLThayDoi_rdChooseDanToc.Checked = false;
            formQLThayDoi_rdChooseQueQuan.Checked = false;
            formQLThayDoi_rdChooseQuocTich.Checked = false;
        }
     
        private void setValueLyDo(string ld)
        {
            if(ld.ToString().Trim().Equals(""))
            {
                lyDo = "Bổ sung";
            }
            else
            {
                lyDo = "Thay đổi";
            }
        }
        public int thayDoiHT, thayDoiGT, thayDoiQQ, thayDoiNS, thayDoiDT, thayDoiQT;
        private void setEnableControlsTTNDN(object sender, EventArgs e)
        {
            
            // if(isClickedAccept == false)
            //   {
            if (formQLThayDoi_rdChooseHoTen.Checked == true)
            {
                isCheckChanged = true;             
                formQLThayDoi_grbTTNDN_txtHoTen.Enabled = true;
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Enabled = false;
                formQLThayDoi_grbTTNDN_grbGioiTinh.Enabled = false;
                formQLThayDoi_grbTTNDN_txtDanToc.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQueQuan.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQuocTich.Enabled = false;    
                //if(hoTenTag.Equals(formQLThayDoi_grbTTNDN_txtHoTen.Text.ToString().Trim()))
                //{
                //    thayDoiHT = 1;
                //}
                //else
                //{
                //    thayDoiHT = 0;
                //}
                giaTriCu = formQLThayDoi_grbTTNDN_txtHoTen.Tag.ToString().Trim();
                giaTriMoi = formQLThayDoi_grbTTNDN_txtHoTen.Text.ToString().Trim();
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text = formQLThayDoi_grbTTNDN_dtpkNgaySinh.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtDanToc.Text = formQLThayDoi_grbTTNDN_txtDanToc.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQueQuan.Text = formQLThayDoi_grbTTNDN_txtQueQuan.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQuocTich.Text = formQLThayDoi_grbTTNDN_txtQuocTich.Tag.ToString();
                if (kt == 0)
                {
                    formQLThayDoi_grbTTNDN_rdNu.Checked = true;
                }
                else
                {
                    formQLThayDoi_grbTTNDN_rdNam.Checked = true;
                }
                mucCanThayDoi = "Họ tên";
                
                setValueLyDo(formQLThayDoi_grbTTNDN_txtHoTen.Text.ToString().Trim());
            }
            if (formQLThayDoi_rdChooseNgaySinh.Checked == true)
            {
                isCheckChanged = true;
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Enabled = true;
                formQLThayDoi_grbTTNDN_txtHoTen.Enabled = false;
                formQLThayDoi_grbTTNDN_grbGioiTinh.Enabled = false;
                formQLThayDoi_grbTTNDN_txtDanToc.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQueQuan.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQuocTich.Enabled = false;
                formQLThayDoi_grbTTNDN_txtHoTen.Text = formQLThayDoi_grbTTNDN_txtHoTen.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtDanToc.Text = formQLThayDoi_grbTTNDN_txtDanToc.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQueQuan.Text = formQLThayDoi_grbTTNDN_txtQueQuan.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQuocTich.Text = formQLThayDoi_grbTTNDN_txtQuocTich.Tag.ToString();
                if (kt == 0)
                {
                    formQLThayDoi_grbTTNDN_rdNu.Checked = true;
                }
                else
                {
                    formQLThayDoi_grbTTNDN_rdNam.Checked = true;
                }
                mucCanThayDoi = "Ngày sinh";
                giaTriCu = formQLThayDoi_grbTTNDN_dtpkNgaySinh.Tag.ToString().Trim();
                giaTriMoi = formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text.ToString().Trim();
                setValueLyDo(formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text.ToString().Trim());
            }
            if (formQLThayDoi_rdChooseGioiTinh.Checked == true)
            {
                isCheckChanged = true;
                formQLThayDoi_grbTTNDN_grbGioiTinh.Enabled = true;
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Enabled = false;
                formQLThayDoi_grbTTNDN_txtHoTen.Enabled = false;
                formQLThayDoi_grbTTNDN_txtDanToc.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQueQuan.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQuocTich.Enabled = false;
                formQLThayDoi_grbTTNDN_txtHoTen.Text = formQLThayDoi_grbTTNDN_txtHoTen.Tag.ToString();
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text = formQLThayDoi_grbTTNDN_dtpkNgaySinh.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtDanToc.Text = formQLThayDoi_grbTTNDN_txtDanToc.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQueQuan.Text = formQLThayDoi_grbTTNDN_txtQueQuan.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQuocTich.Text = formQLThayDoi_grbTTNDN_txtQuocTich.Tag.ToString();
                mucCanThayDoi = "Giới tính";
                if(kt==0 && formQLThayDoi_grbTTNDN_rdNam.Checked == true)
                {
                    giaTriCu = "Nữ";
                    giaTriMoi = "Nam";
                }
                if (kt == 1 && formQLThayDoi_grbTTNDN_rdNu.Checked == true)
                {
                    giaTriCu = "Nam";
                    giaTriMoi = "Nữ";
                }
                if (formQLThayDoi_grbTTNDN_rdNam.Checked == false && formQLThayDoi_grbTTNDN_rdNu.Checked==false)
                {
                    lyDo = "Bổ sung";
                }
                else
                {
                    lyDo = "Thay đổi";
                }
            }
            if (formQLThayDoi_rdChooseDanToc.Checked == true)
            {
                isCheckChanged = true;
                formQLThayDoi_grbTTNDN_txtDanToc.Enabled = true;
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Enabled = false;
                formQLThayDoi_grbTTNDN_txtHoTen.Enabled = false;
                formQLThayDoi_grbTTNDN_grbGioiTinh.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQueQuan.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQuocTich.Enabled = false;
                formQLThayDoi_grbTTNDN_txtHoTen.Text = formQLThayDoi_grbTTNDN_txtHoTen.Tag.ToString();
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text = formQLThayDoi_grbTTNDN_dtpkNgaySinh.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQueQuan.Text = formQLThayDoi_grbTTNDN_txtQueQuan.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQuocTich.Text = formQLThayDoi_grbTTNDN_txtQuocTich.Tag.ToString();
                if (kt == 0)
                {
                    formQLThayDoi_grbTTNDN_rdNu.Checked = true;
                }
                else
                {
                    formQLThayDoi_grbTTNDN_rdNam.Checked = true;
                }
                mucCanThayDoi = "Dân tộc";
                giaTriCu = formQLThayDoi_grbTTNDN_txtDanToc.Tag.ToString().Trim();
                giaTriMoi = formQLThayDoi_grbTTNDN_txtDanToc.Text.ToString().Trim();
                setValueLyDo(formQLThayDoi_grbTTNDN_txtDanToc.Text.ToString().Trim());
            }
            if (formQLThayDoi_rdChooseQueQuan.Checked == true)
            {
                isCheckChanged = true;
                formQLThayDoi_grbTTNDN_txtQueQuan.Enabled = true;
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Enabled = false;
                formQLThayDoi_grbTTNDN_txtHoTen.Enabled = false;
                formQLThayDoi_grbTTNDN_grbGioiTinh.Enabled = false;
                formQLThayDoi_grbTTNDN_txtDanToc.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQuocTich.Enabled = false;
                formQLThayDoi_grbTTNDN_txtHoTen.Text = formQLThayDoi_grbTTNDN_txtHoTen.Tag.ToString();
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text = formQLThayDoi_grbTTNDN_dtpkNgaySinh.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtDanToc.Text = formQLThayDoi_grbTTNDN_txtDanToc.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQuocTich.Text = formQLThayDoi_grbTTNDN_txtQuocTich.Tag.ToString();
                if (kt == 0)
                {
                    formQLThayDoi_grbTTNDN_rdNu.Checked = true;
                }
                else
                {
                    formQLThayDoi_grbTTNDN_rdNam.Checked = true;
                }
                mucCanThayDoi = "Quê quán";
                giaTriCu = formQLThayDoi_grbTTNDN_txtQueQuan.Tag.ToString().Trim();
                giaTriMoi = formQLThayDoi_grbTTNDN_txtQueQuan.Text.ToString().Trim();
                setValueLyDo(formQLThayDoi_grbTTNDN_txtQueQuan.Text.ToString().Trim());
            }
            if (formQLThayDoi_rdChooseQuocTich.Checked == true)
            {
                isCheckChanged = true;
                formQLThayDoi_grbTTNDN_txtQuocTich.Enabled = true;
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Enabled = false;
                formQLThayDoi_grbTTNDN_txtHoTen.Enabled = false;
                formQLThayDoi_grbTTNDN_grbGioiTinh.Enabled = false;
                formQLThayDoi_grbTTNDN_txtDanToc.Enabled = false;
                formQLThayDoi_grbTTNDN_txtQueQuan.Enabled = false;
                formQLThayDoi_grbTTNDN_txtHoTen.Text = formQLThayDoi_grbTTNDN_txtHoTen.Tag.ToString();
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text = formQLThayDoi_grbTTNDN_dtpkNgaySinh.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtDanToc.Text = formQLThayDoi_grbTTNDN_txtDanToc.Tag.ToString();
                formQLThayDoi_grbTTNDN_txtQueQuan.Text = formQLThayDoi_grbTTNDN_txtQueQuan.Tag.ToString();
                if (kt == 0)
                {
                    formQLThayDoi_grbTTNDN_rdNu.Checked = true;
                }
                else
                {
                    formQLThayDoi_grbTTNDN_rdNam.Checked = true;
                }
                mucCanThayDoi = "Quốc tịch";
                giaTriCu = formQLThayDoi_grbTTNDN_txtQuocTich.Tag.ToString().Trim();
                giaTriMoi = formQLThayDoi_grbTTNDN_txtQuocTich.Text.ToString().Trim();
                setValueLyDo(formQLThayDoi_grbTTNDN_txtQuocTich.Text.ToString().Trim());
            }
            if (isCheckChanged == true)
            {
                formQLThayDoi_grbTTNDN_btnDongY.Visible = true;
                formQLThayDoi_grbTTNDN_btnHuy.Visible = true;
            }
        }
         public void setValueGiaTriMoi()
        {
            switch(layRaMucThayDoi)
            {
                case "Họ tên" :
                    txtGiaTriMoiHoTen.Text = layRaGiTriMoi.ToString();
                    txtGiaTriMoiNgaySinh.Value = DateTime.Now;
                    rdGiaTriMoi_GTNam.Checked = false;
                    rdGiaTriMoi_GTNau.Checked = false;
                    txtGiaTriMoiDanToc.Text = "";
                    txtGiaTriMoiQueQuan.Text = "";
                    txtGiaTriMoiQuocTich.Text = "";
                    break;
                case "Ngày sinh":
                    txtGiaTriMoiNgaySinh.Value = DateTime.ParseExact(layRaGiTriMoi.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                    txtGiaTriMoiHoTen.Text = "";
                    rdGiaTriMoi_GTNam.Checked = false;
                    rdGiaTriMoi_GTNau.Checked = false;
                    txtGiaTriMoiDanToc.Text = "";
                    txtGiaTriMoiQueQuan.Text = "";
                    txtGiaTriMoiQuocTich.Text = "";
                    break;
                case "Giới tính":
                   if(layRaGiTriMoi.ToString().Equals("Nam"))
                    {
                        rdGiaTriMoi_GTNam.Checked = true;
                    }
                   else
                    {
                        rdGiaTriMoi_GTNau.Checked = true;
                    }
                    txtGiaTriMoiNgaySinh.Value = DateTime.Now;
                    txtGiaTriMoiHoTen.Text = "";
                    txtGiaTriMoiQueQuan.Text = "";
                    txtGiaTriMoiDanToc.Text = "";
                    txtGiaTriMoiQuocTich.Text = "";
                    break;
                case "Dân tộc":
                    txtGiaTriMoiDanToc.Text = layRaGiTriMoi.ToString();
                    txtGiaTriMoiNgaySinh.Value = DateTime.Now;
                    txtGiaTriMoiHoTen.Text = "";
                    txtGiaTriMoiQueQuan.Text = "";
                    txtGiaTriMoiQuocTich.Text = "";
                    rdGiaTriMoi_GTNam.Checked = false;
                    rdGiaTriMoi_GTNau.Checked = false;
                    txtGiaTriMoiQuocTich.Text = "";
                    break;
                case "Quê quán" :
                    txtGiaTriMoiQueQuan.Text = layRaGiTriMoi.ToString();
                    txtGiaTriMoiHoTen.Text = "";
                    txtGiaTriMoiNgaySinh.Value = DateTime.Now;
                    txtGiaTriMoiQuocTich.Text = "";
                    rdGiaTriMoi_GTNam.Checked = false;
                    rdGiaTriMoi_GTNau.Checked = false;
                    txtGiaTriMoiQuocTich.Text = "";
                    txtGiaTriMoiDanToc.Text = "";
                    break;
                case "Quốc tịch" :
                    txtGiaTriMoiQuocTich.Text = layRaGiTriMoi.ToString();
                    txtGiaTriMoiHoTen.Text = "";
                    txtGiaTriMoiNgaySinh.Value = DateTime.Now;    
                    rdGiaTriMoi_GTNam.Checked = false;
                    rdGiaTriMoi_GTNau.Checked = false;
                    txtGiaTriMoiQueQuan.Text = "";
                    txtGiaTriMoiDanToc.Text = "";
                    break;
            }
           
        }



        bool isThayDoi = false;
        private void formQLThayDoi_grbTTNDN_btnDongY_Click(object sender, EventArgs e)
        {
            setEnableControlsTTNDN(null, null);
            isClickedAccept = true;
            if (formQLThayDoi_grbTTNDN_txtHoTen.Tag.ToString().Trim().Equals(formQLThayDoi_grbTTNDN_txtHoTen.Text.ToString().Trim()) &&
               formQLThayDoi_grbTTNDN_dtpkNgaySinh.Tag.ToString().Trim().Equals(formQLThayDoi_grbTTNDN_dtpkNgaySinh.Text.ToString().Trim()) &&
                formQLThayDoi_grbTTNDN_txtDanToc.Tag.ToString().Trim().Equals(formQLThayDoi_grbTTNDN_txtDanToc.Text.ToString().Trim()) &&
                formQLThayDoi_grbTTNDN_txtQueQuan.Tag.ToString().Trim().Equals(formQLThayDoi_grbTTNDN_txtQueQuan.Text.ToString().Trim()) &&
                 formQLThayDoi_grbTTNDN_txtQuocTich.Tag.ToString().Trim().Equals(formQLThayDoi_grbTTNDN_txtQuocTich.Text.ToString().Trim()) &&
                 (kt == 0 && formQLThayDoi_grbTTNDN_rdNu.Checked == true || kt == 1 && formQLThayDoi_grbTTNDN_rdNam.Checked == true))
            {
                isThayDoi = false;
                MessageBox.Show("Bạn đã không thay đổi thông tin gì !");
            }
            else
            {

                if (MessageBox.Show("Bạn chắc chắn với những thông tin vừa thay đổi ?", "Xác nhận", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    isThayDoi = true;
                    formQLThayDoi_grbRadioButtonChoose.Visible = false;
                    setFalseAllControlsTTNDN();
                    formQLThayDoi_grbTTNDN_btnDongY.Visible = false;
                    formQLThayDoi_grbTTNDN_btnHuy.Visible = false;
        }
    }

}

        void ToggleButton(bool btnThemHoSo, bool btnXacNhanKQ, bool btnDuyetKQ, bool btnTraKQ)
        {
            FormQLThayDoiBoSung_btnThemHoSo.Enabled = btnThemHoSo;
            FormQLThayDoiBoSung_btnXacNhan.Enabled = btnXacNhanKQ;
            FormQLThayDoiBoSung_btnDuyetHoSo.Enabled = btnDuyetKQ;
            this.FormQLThayDoiBoSung_btnTraKetQua.Enabled = btnTraKQ;
        }
        private void ToggleButtonByTrangThai(string TrangThai)
        {
            int iTrangThai = int.Parse(TrangThai);
            SetFalseAllButton();
            if (Constants.isCCCX)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.TiepNhan)
                {
                    FormQLThayDoiBoSung_btnXacNhan.Enabled = true;
                }
            }
            if (Constants.isLD)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.XacMinh)
                {
                    FormQLThayDoiBoSung_btnDuyetHoSo.Enabled = true;
                }
            }
            if (Constants.isVP)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.Duyet)
                {
                    FormQLThayDoiBoSung_btnTraKetQua.Enabled = true;
                }
            }
        }

        private void FormQLThayDoiBoSung_btnTraKetQua_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn trả kết quả hồ sơ đăng ký thay đổi này chứ?";
            CustomYesNoCancelMsgBox.Instance.Yes = "OK";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (DangKyThayDoi.UpdateTrangThai(maDangKiCurrent, Constants.MaCongChucDangNhap, DateTime.Now, (int)TrangThaiModel.TrangThai.DaTra))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.DaTra + "");
                    loadData();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {

            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }
        }
        private string select_CMND(int maCongDanDN)
        {
            // int ketQua =0;
            string[] ketQua_Split = new string[0];
            using (SqlConnection connection = new SqlConnection(chuoikn))
            {
                connection.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandText = "select_CMND_CongDan";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@MaCongDan", maCongDanDN));
                        string ketQua = (string)command.ExecuteScalar();
                        ketQua_Split = ketQua.Split(',');
                      //  MessageBox.Show(ketQua_Split[1].ToString());
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
                return ketQua_Split[1].ToString();
        }
        private void FormQLThayDoiBoSung_btnDuyetHoSo_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn duyệt hồ sơ đăng ký thay đổi này chứ?";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (DangKyThayDoi.UpdateTrangThai(maDangKiCurrent, "", Constants.MaCongChucDangNhap, (int)TrangThaiModel.TrangThai.Duyet))
                {
                    switch(layRaMucThayDoi)
                    {
                        case "Họ tên":
                            CongDanModel.Update_CongDan_FromDangKyThaDoi(MaCongDanNDNCurrent, layRaGiTriMoi.ToString(),GiaTriNgaySinh_Update, GiaTriDanToc_Update,GiaTriGioiTinh_Update, GiaTriQueQuan_Update, GiaTriQuocTich_Update);
                            khaiSinhModel.update_KhaiSinh_FromDangKyThayDoi(select_CMND(MaCongDanNDNCurrent), layRaGiTriMoi.ToString(), GiaTriNgaySinh_Update, GiaTriDanToc_Update, GiaTriGioiTinh_Update, GiaTriQueQuan_Update, GiaTriQuocTich_Update);
                            break;
                        case "Ngày sinh":
                            CongDanModel.Update_CongDan_FromDangKyThaDoi(MaCongDanNDNCurrent, GiaTriHoTen_Update, DateTime.ParseExact(layRaGiTriMoi.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture), GiaTriDanToc_Update, GiaTriGioiTinh_Update, GiaTriQueQuan_Update, GiaTriQuocTich_Update);
                            khaiSinhModel.update_KhaiSinh_FromDangKyThayDoi(select_CMND(MaCongDanNDNCurrent),GiaTriHoTen_Update, DateTime.ParseExact(layRaGiTriMoi.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture), GiaTriDanToc_Update, GiaTriGioiTinh_Update, GiaTriQueQuan_Update, GiaTriQuocTich_Update);
                            break;
                        case "Giới tính":
                            CongDanModel.Update_CongDan_FromDangKyThaDoi(MaCongDanNDNCurrent,GiaTriHoTen_Update , GiaTriNgaySinh_Update, GiaTriDanToc_Update,bool.Parse(layRaGiTriMoi.ToString()), GiaTriQueQuan_Update, GiaTriQuocTich_Update);
                            khaiSinhModel.update_KhaiSinh_FromDangKyThayDoi(select_CMND(MaCongDanNDNCurrent), GiaTriHoTen_Update, GiaTriNgaySinh_Update, GiaTriDanToc_Update, bool.Parse(layRaGiTriMoi.ToString()), GiaTriQueQuan_Update, GiaTriQuocTich_Update);
                            break;
                        case "Dân tộc":
                            CongDanModel.Update_CongDan_FromDangKyThaDoi(MaCongDanNDNCurrent,GiaTriHoTen_Update, GiaTriNgaySinh_Update, layRaGiTriMoi.ToString(), GiaTriGioiTinh_Update, GiaTriQueQuan_Update, GiaTriQuocTich_Update);
                            khaiSinhModel.update_KhaiSinh_FromDangKyThayDoi(select_CMND(MaCongDanNDNCurrent), GiaTriHoTen_Update, GiaTriNgaySinh_Update, layRaGiTriMoi.ToString(), GiaTriGioiTinh_Update, GiaTriQueQuan_Update, GiaTriQuocTich_Update);
                            break;
                        case "Quê quán":
                            CongDanModel.Update_CongDan_FromDangKyThaDoi(MaCongDanNDNCurrent, GiaTriHoTen_Update, GiaTriNgaySinh_Update,GiaTriDanToc_Update, GiaTriGioiTinh_Update, layRaGiTriMoi.ToString(), GiaTriQuocTich_Update);
                            khaiSinhModel.update_KhaiSinh_FromDangKyThayDoi(select_CMND(MaCongDanNDNCurrent), GiaTriHoTen_Update, GiaTriNgaySinh_Update, GiaTriDanToc_Update, GiaTriGioiTinh_Update, layRaGiTriMoi.ToString(), GiaTriQuocTich_Update);
                            break;
                        case "Quốc tịch":
                            CongDanModel.Update_CongDan_FromDangKyThaDoi(MaCongDanNDNCurrent, GiaTriHoTen_Update, GiaTriNgaySinh_Update, GiaTriDanToc_Update, GiaTriGioiTinh_Update, GiaTriQueQuan_Update, layRaGiTriMoi.ToString());
                            khaiSinhModel.update_KhaiSinh_FromDangKyThayDoi(select_CMND(MaCongDanNDNCurrent), GiaTriHoTen_Update, GiaTriNgaySinh_Update, GiaTriDanToc_Update, GiaTriGioiTinh_Update, GiaTriQueQuan_Update, layRaGiTriMoi.ToString());
                            break;
                            
                    }
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.Duyet + "");
                    loadData();

                    //Cần UPDATE lại trạng thái cho công dân. Hàm này viết sau
                    //string IDCongDanKhaiSinh = CongDanModel.ThemCongDan(congDanKhaiSinh);
                    //CongDan congDanCha = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KhaiSinh["IDCha", selectedRowIndex].Value.ToString()));
                    //congDanCha.TrangThai = (int)Constants.TrangThaiCongDan.DaDuyet;
                    //CongDanModel.UpdateCongDan(congDanCha);
                    //CongDan congDanMe = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KhaiSinh["IDMe", selectedRowIndex].Value.ToString()));
                    //congDanMe.TrangThai = (int)Constants.TrangThaiCongDan.DaDuyet;
                    //CongDanModel.UpdateCongDan(congDanMe);

                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {
                if (DangKyThayDoi.UpdateTrangThai(maDangKiCurrent, "", Constants.MaCongChucDangNhap, (int)TrangThaiModel.TrangThai.KhongDuyet))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.KhongDuyet + "");
                    loadData();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }
        }
        string layRaMucThayDoi;
        string layRaGiTriMoi;
        string GiaTriHoTen_Update, GiaTriDanToc_Update, GiaTriQueQuan_Update, GiaTriQuocTich_Update;
        bool GiaTriGioiTinh_Update;
        DateTime GiaTriNgaySinh_Update;
        private void grvDSDangKiThayDoi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;
                if (rowIndex >= 0)
                {
                //int Selected_index = grvDSDangKiThayDoi.CurrentRow.Index;
                maDangKiCurrent = int.Parse(grvDSDangKiThayDoi["MaThayDoi",rowIndex].Value.ToString());
                MaCongDanNDNCurrent = int.Parse(grvDSDangKiThayDoi["MaCongDanDN",rowIndex].Value.ToString());
                layRaMucThayDoi = grvDSDangKiThayDoi.Rows[rowIndex].Cells["MucThayDoi"].Value.ToString();
                layRaGiTriMoi = grvDSDangKiThayDoi["GTM",rowIndex].Value.ToString();
                CongDan congDanYeuCau = CongDanModel.GetCongDanByMaCongDan(int.Parse(grvDSDangKiThayDoi["MaCongDanYC",rowIndex].Value.ToString()));
                CongDan congDanDeNghi = CongDanModel.GetCongDanByMaCongDan(int.Parse(grvDSDangKiThayDoi["MaCongDanDN",rowIndex].Value.ToString()));

                setValueGiaTriMoi();
                grb_giaTriMoi.Visible = true;
                if(FormQLThayDoiBoSung_btnXacNhan.Enabled == true || FormQLThayDoiBoSung_btnDuyetHoSo.Enabled == true || 
                    FormQLThayDoiBoSung_btnTraKetQua.Enabled == true)
                {
                    FormQLThayDoiBoSung_btnThemHoSo.Enabled = false;
                }

                formThayDoi_grbTTNYC_txtMaCongDan.Text = grvDSDangKiThayDoi["MaCongDanYC",rowIndex].Value.ToString();
                formThayDoi_grbTTNYC_txtbHoTen.Text = String.IsNullOrEmpty(congDanYeuCau.HoTen) ? "" : congDanYeuCau.HoTen;
                formThayDoi_grbTTNYC_dtpNgaySinh.Value = congDanYeuCau.NgaySinh;
                formThayDoi_grbTTNYC_rdNam.Checked = congDanYeuCau.GioiTinh == true ? true : false;
                formThayDoi_grbTTNYC_rdNu.Checked = congDanYeuCau.GioiTinh == false ? true : false;
                formThayDoi_grbTTNYC_txtDanToc.Text = String.IsNullOrEmpty(congDanYeuCau.DanToc) ? "" : congDanYeuCau.DanToc;
                formThayDoi_grbTTNYC_txtQueQuan.Text = String.IsNullOrEmpty(congDanYeuCau.QueQuan) ? "" : congDanYeuCau.QueQuan;
                formThayDoi_grbTTNYC_txtQuocTich.Text = String.IsNullOrEmpty(congDanYeuCau.QuocTich) ? "" : congDanYeuCau.QuocTich;
                formThayDoi_grbTTNYC_txtQuanHe.Text = grvDSDangKiThayDoi.Rows[rowIndex].Cells[3].Value.ToString();

                formQLThayDoi_grbTTNDN_txtMaCDDN.Text = grvDSDangKiThayDoi["MaCongDanDN",rowIndex].Value.ToString();
                formQLThayDoi_grbTTNDN_txtHoTen.Text = String.IsNullOrEmpty(congDanDeNghi.HoTen) ? "" : congDanDeNghi.HoTen;
                formQLThayDoi_grbTTNDN_dtpkNgaySinh.Value = congDanDeNghi.NgaySinh;
                formQLThayDoi_grbTTNDN_rdNam.Checked = congDanDeNghi.GioiTinh == true ? true : false;
                formQLThayDoi_grbTTNDN_rdNu.Checked = congDanDeNghi.GioiTinh == false ? true : false;
                formQLThayDoi_grbTTNDN_txtDanToc.Text = String.IsNullOrEmpty(congDanDeNghi.DanToc) ? "" : congDanDeNghi.DanToc;
                formQLThayDoi_grbTTNDN_txtQueQuan.Text = String.IsNullOrEmpty(congDanDeNghi.QueQuan) ? "" : congDanDeNghi.QueQuan;
                formQLThayDoi_grbTTNDN_txtQuocTich.Text = String.IsNullOrEmpty(congDanDeNghi.QuocTich) ? "" : congDanDeNghi.QuocTich;

                FormQLThayDoiBoSung_dtpkHenTra.Value = String.IsNullOrEmpty(grvDSDangKiThayDoi.Rows[rowIndex].Cells["NgayHen"].Value.ToString()) ? DateTime.Now : DateTime.ParseExact(grvDSDangKiThayDoi.Rows[rowIndex].Cells["NgayHen"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);

                GiaTriHoTen_Update = String.IsNullOrEmpty(congDanDeNghi.HoTen) ? "" : congDanDeNghi.HoTen;
                GiaTriNgaySinh_Update = congDanDeNghi.NgaySinh;
                GiaTriGioiTinh_Update = congDanDeNghi.GioiTinh == true ? true : false;
                GiaTriDanToc_Update = String.IsNullOrEmpty(congDanDeNghi.DanToc) ? "" : congDanDeNghi.DanToc;
                GiaTriQueQuan_Update = String.IsNullOrEmpty(congDanDeNghi.QueQuan) ? "" : congDanDeNghi.QueQuan;
                GiaTriQuocTich_Update = String.IsNullOrEmpty(congDanDeNghi.QuocTich) ? "" : congDanDeNghi.QuocTich;

                formQLThayDoi_lblTTHS.Visible = true;
                formQLThayDoi_TT.Visible = true;
                formQLThayDoi_TT.Text = String.IsNullOrEmpty(grvDSDangKiThayDoi["TrangThai",rowIndex].Value.ToString()) ? "" : TrangThaiModel.GetTrangThaiByMaTrangThai(grvDSDangKiThayDoi["MTT", rowIndex].Value.ToString());
                ToggleButtonByTrangThai(grvDSDangKiThayDoi.Rows[rowIndex].Cells["MTT"].Value.ToString());
                }
            }
            catch (Exception ex)
            {

            }
        }
       // DangKyThayDoi dangKyThayDoi = new DangKyThayDoi();
      
        private void FormQLThayDoiBoSung_btnXacNhan_Click(object sender, EventArgs e)
        {

            
            CustomYesNoCancelMsgBox.Instance.title = "Bạn xác nhận hồ sơ đăng kí thay đổi này chứ?";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
              
                if (DangKyThayDoi.UpdateTrangThai(maDangKiCurrent, Constants.MaCongChucDangNhap, "", (int)TrangThaiModel.TrangThai.XacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.XacMinh + "");
                    loadData();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {
                if (DangKyThayDoi.UpdateTrangThai(maDangKiCurrent, Constants.MaCongChucDangNhap, "", (int)TrangThaiModel.TrangThai.XacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.KhongXacMinh + "");
                    loadData();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }
        }

        private void FormQLThayDoiBoSung_btnThemHoSo_Click(object sender, EventArgs e)
        {

            if (choose_CongDan_NguoiYeuCau == true && choose_CongDan_NguoiDeNghi == true)
            {
                if (isClickedAccept == true)
                {
                   if(isThayDoi==true)
                    {
                        using (SqlConnection connection = new SqlConnection(chuoikn))
                        {
                            connection.Open();
                            try
                            {
                                using (SqlCommand command = new SqlCommand())
                                {

                                    command.Connection = connection;
                                    command.CommandText = "Insert_DangKyThayDoi";
                                    command.CommandType = CommandType.StoredProcedure;
                                    command.Parameters.Add(new SqlParameter("@MaCongDanYC", formThayDoi_grbTTNYC_txtMaCongDan.Text));
                                    command.Parameters.Add(new SqlParameter("@MaCongDanDN", formQLThayDoi_grbTTNDN_txtMaCDDN.Text));
                                    command.Parameters.Add(new SqlParameter("@QuanHeVoiNDN", formThayDoi_grbTTNYC_txtQuanHe.Text));
                                    command.Parameters.Add(new SqlParameter("@MucCanThayDoi", mucCanThayDoi));
                                    command.Parameters.Add(new SqlParameter("@LyDo", lyDo));
                                    command.Parameters.Add(new SqlParameter("@GiaTriCu", giaTriCu));
                                    command.Parameters.Add(new SqlParameter("@GiaTriMoi", giaTriMoi));
                                    command.Parameters.Add(new SqlParameter("@MaThuTuc", 1));
                                    command.Parameters.Add(new SqlParameter("@NgayHen", Convert.ToDateTime(FormQLThayDoiBoSung_dtpkHenTra.Value)));
                                    command.Parameters.Add(new SqlParameter("@MaCongChucTiepNhan", int.Parse(Constants.MaCongChucDangNhap)));
                                    DialogResult = MessageBox.Show("Bạn chắc chắn muốn thêm hồ sơ đăng ký thay đổi này chứ?", "Xác nhận", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                    if (DialogResult == DialogResult.OK)
                                    {

                                        int i = command.ExecuteNonQuery();
                                        if (i > 0)
                                        {
                                            MessageBox.Show("Thêm thành công !");
                                            loadData();
                                            // lvDSNhanKhau.Refresh();
                                        }
                                        else
                                        {
                                            MessageBox.Show("Thêm thất bại !");
                                        }
                                    }
                                    else
                                    {
                                        return;
                                    }

                                }

                            }

                            catch (SqlException ex)
                            {
                                MessageBox.Show(ex.Message);
                            }

                        }
                    }
                   else
                    {
                        MessageBox.Show("Hãy thay đổi thông tin trước khi thêm hồ sơ !");
                    }
                }
                else
                {
                    MessageBox.Show("Hãy thay đổi thông tin và đồng ý trước khi thêm hồ sơ này !");
                }
            }
            else
            {
                MessageBox.Show("Hãy điền thông tin người yêu cầu và người đề nghị !");
            }
            
        }
       
        private void FormQLThayDoiBoSung_cbbTrangThai_SelectionChangeCommitted(object sender, EventArgs e)
        {
            maTrangThai = int.Parse(FormQLThayDoiBoSung_cbbTrangThai.SelectedValue.ToString());
        }

        private void FormQLThayDoiBoSung_btnTimKiem_Click(object sender, EventArgs e)
        {
          
            object[] param = new object[] { maTrangThai };
            try
            {
                grvDSDangKiThayDoi.Rows.Clear();
                List<DangKyThayDoi> dsDangKyThayDoi = DangKyThayDoi.TimKiem(param);
                foreach (var dangKy in dsDangKyThayDoi)
                {
                    var index = grvDSDangKiThayDoi.Rows.Add();
                    grvDSDangKiThayDoi.Rows[index].Cells["MaThayDoi"].Value = dangKy.MaThayDoi;
                    int mcdyc = dangKy.MaCongDanYC;
                    grvDSDangKiThayDoi.Rows[index].Cells["MaCongDanYC"].Value = dangKy.MaCongDanYC;
                    grvDSDangKiThayDoi.Rows[index].Cells["MaCongDanDN"].Value = dangKy.MaCongDanDN;
                    grvDSDangKiThayDoi.Rows[index].Cells["QuanHeVoiNDN"].Value = dangKy.QuanHeVoiNDN;
                    grvDSDangKiThayDoi.Rows[index].Cells["MucThayDoi"].Value = dangKy.MucCanThayDoi;
                    grvDSDangKiThayDoi.Rows[index].Cells["LD"].Value = dangKy.LyDo;
                    grvDSDangKiThayDoi.Rows[index].Cells["GTC"].Value = dangKy.GiaTriCu;
                    grvDSDangKiThayDoi.Rows[index].Cells["GTM"].Value = dangKy.GiaTriMoi;

                    //   dgrv_KhaiSinh.Rows[index].Cells["NgaySinh"].Value = khaiSinh.NgaySinh.ToString("dd/MM/yyyy");


                    grvDSDangKiThayDoi.Rows[index].Cells["MTT"].Value = (dangKy.TrangThai);
                    grvDSDangKiThayDoi.Rows[index].Cells["TrangThai"].Value = TrangThaiModel.GetTrangThaiVietTatByMaTrangThai(dangKy.TrangThai.ToString());
                    grvDSDangKiThayDoi.Rows[index].Cells["NgayDangKy"].Value = dangKy.NgayDangKy == null ? "" : Convert.ToDateTime(dangKy.NgayDangKy).ToString("dd/MM/yyyy");
                    grvDSDangKiThayDoi.Rows[index].Cells["NgayHen"].Value = dangKy.NgayHen == null ? "" : Convert.ToDateTime(dangKy.NgayHen).ToString("dd/MM/yyyy");
                    grvDSDangKiThayDoi.Rows[index].Cells["NgayTra"].Value = dangKy.NgayTra == null ? "" : Convert.ToDateTime(dangKy.NgayTra).ToString("dd/MM/yyyy");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra!");
            }
        }
    }

   
}
