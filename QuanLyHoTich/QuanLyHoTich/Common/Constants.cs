﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHoTich
{
    static class Constants
    {
        //Theo như bài PTTK. Có các bộ phận. Tương ứng các quyền
       /* MaBoPhan TenVietTat  TenBoPhan
                1	TN&TKQ     Bộ phận tiếp nhận và trả KQ
                2	CCCX       Công chức cấp xã
                3	LD         Lãnh đạo
                4	VP         Bộ phận văn phòng
                5	KTTC       Bộ phận tài chính - kế toán*/

        public static bool isTN_TKQ = false;
        public static bool isCCCX = false;
        public static bool isLD = false;
        public static bool isVP = false;
        public static bool isKTTC = false;

        //ID Người đăng nhập
        public static string MaCongChucDangNhap = "";

        public enum CustomDialogResult:int{
            Yes,
            No, 
            Cancel
        }

        public enum TrangThaiCongDan : int
        {
            Xoa = -1,
            ChuaDuyet = 0,
            DaDuyet = 1
        }
    }
}
