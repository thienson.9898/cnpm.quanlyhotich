﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHoTich
{
    public partial class CustomYesNoCancelMsgBox : Form
    {
        public CustomYesNoCancelMsgBox()
        {
            InitializeComponent();
        }
        private static CustomYesNoCancelMsgBox instance;
        public static CustomYesNoCancelMsgBox Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new CustomYesNoCancelMsgBox();
                }
                return instance;
            }
        }

        public int DialogResult = -1;
        public string title = "";
        public string Yes = "Có";
        public string No = "Không";
        public string Cancel = "Hủy";

        private void CustomYesNoCancelMsgBox_Load(object sender, EventArgs e)
        {
            
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            DialogResult = 0;
            CustomYesNoCancelMsgBox.Instance.Hide();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            DialogResult = 1;
            CustomYesNoCancelMsgBox.Instance.Hide();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = 2;
            CustomYesNoCancelMsgBox.Instance.Hide();
        }

        private void CustomYesNoCancelMsgBox_Shown(object sender, EventArgs e)
        {
            lblThongBao.Text = title;
        }
    }
}
