﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLHT.Entity;
using QLHT.Model;

namespace QuanLyHoTich
{
    public partial class FormDangNhap : Form
    {
        private static FormDangNhap instance;
        public static FormDangNhap Instance
        {
            get
            {
                if(instance == null || instance.IsDisposed)
                {
                    instance = new FormDangNhap();
                }
                return instance;
            }
        }
        public FormDangNhap()
        {
            InitializeComponent();
        }

        private void btn_Thoat_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn chắc chắn muốn thoát", "Xác nhận", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                //Application.Exit();
                Environment.Exit(0);
            }
        }

        private void btn_DangNhap_Click(object sender, EventArgs e)
        {
            string TaiKhoan = txtbUsername.Text.Trim();
            string Password = txtbPassword.Text.Trim();
            string PasswordHash = CreateMD5(Password);
            var congChucModel = new CongChucModel();
            Constants.MaCongChucDangNhap = congChucModel.DangNhap(TaiKhoan, PasswordHash);
            if (!String.IsNullOrEmpty(Constants.MaCongChucDangNhap))
            {
                this.Hide();
                FormMenu.Instance.Show();               
            }
            else
            {
                if (TaiKhoan=="admin")
                {
                    this.Hide();
                    FormMenu.Instance.Show();
                    Constants.MaCongChucDangNhap = "-1";
                }
                else
                MessageBox.Show("Sai tên đăng nhập hoặc mật khẩu");
            }
        }

        private void txtbPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btn_DangNhap_Click(null, null);
            }
        }

        private void FormDangNhap_Load(object sender, EventArgs e)
        {

        }
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}
