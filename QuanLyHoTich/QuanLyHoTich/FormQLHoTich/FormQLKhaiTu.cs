﻿using QLHT.Entity;
using QLHT.Model;
using QLHT.Model.Dictionary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHoTich.FormQLHoTich
{
    public partial class FormQLKhaiTu : Form
    {


        String Quyen = "";
        CongDanModel CongDanModel = new CongDanModel();
        KhaiTuModel khaiTuModel = new KhaiTuModel();
        TrangThaiModel TrangThaiModel = new TrangThaiModel();
        public CongDan CongDanDiKhaiTu = new CongDan();
        public CongDan CongDanDuocKhaiTu = new CongDan();
        string MaCongDanYeuCau = "-1";
        bool IsChooseCongDanDuocKhaiTu = false;
        bool IsChooseCongDanDiKhaiTu = false;
        KhaiTu currentKhaiTu = new KhaiTu();




        private static FormQLKhaiTu instance;
        public static FormQLKhaiTu Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormQLKhaiTu();
                }
                return instance;
            }
        }
        public FormQLKhaiTu()
        {
            InitializeComponent();
            GetQuyen();
            dtpHenTra.CustomFormat = "dd/MM/yyyy";
            dtpNgaySinh.CustomFormat = "dd/MM/yyyy";
            dtpNgaySinh_NDRKT.CustomFormat = "dd/MM/yyyy";
            TextBoxReadOnly();
            LoadDuLieu();
        }
        //Quy định các trường không nhập 
        public void TextBoxReadOnly()
        {
            txtbCMND.ReadOnly = true;
            txtbCMND_NDRKT.ReadOnly = true;
            txtbDanToc.ReadOnly = true;
            txtbDanToc_NDRKT.ReadOnly = true;
            txtbHoTen.ReadOnly = true;
            txtbHoTen_NDRKT.ReadOnly = true;
            txtbQueQuan.ReadOnly = true;
            txtbQueQuan_NDRKT.ReadOnly = true;
            txtbQuocTich.ReadOnly = true;
            txtbQuocTich_NDRKT.ReadOnly = true;
            dtpNgaySinh.Enabled = false;
            dtpNgaySinh_NDRKT.Enabled = false;
            rdoNam.Enabled = false;
            rdoNu.Enabled = false;
            rdoNam_NDRKT.Enabled = false;
            rdoNu_NDRKT.Enabled = false;
        }

     
        // Lấy quyền các User login
        public void GetQuyen()
        {
            Quyen = new CongChucModel().GetQuyen(Constants.MaCongChucDangNhap);
            SetFalseAllButton();
            //Ktra 5 quyền
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanTiepNhanVaTraKQ))
            {
                Constants.isTN_TKQ = true;
                btnThem.Enabled = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatCongChucCapXa))
            {
                Constants.isCCCX = true;
                //  btnXacNhan.Enabled = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatLanhDao))
            {
                // btnDuyet.Enabled = true;
                Constants.isLD = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanVanPhong))
            {
                // btnTraKQ.Enabled = true;
                Constants.isVP = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanKTTC))
            {
                Constants.isKTTC = true;
            }
            //Get trạng thái hồ sơ để bật/tắt nút
        }

        //Set Value các button
        void SetFalseAllButton()
        {
            btnThem.Enabled = false;
            btnXacNhan.Enabled = false;
            btnDuyet.Enabled = false;
            btnTraKQ.Enabled = false;
        }

      //Chọn thông tin ngừoi đi khai tử

        private void btn_ChooseNguoiDungRaKhaiTu_Click(object sender, EventArgs e)
        {

            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.AllGioiTinh = true;
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                MessageBox.Show("Chọn thành công");
                CongDanDiKhaiTu = FormQLCongDan.Instance.congDan;
                this.MaCongDanYeuCau = CongDanDiKhaiTu.MaCongDan.ToString();
                txtbHoTen_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu.HoTen) ? "" : CongDanDiKhaiTu.HoTen; ;
                txtbCMND_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu.CMND) ? "" : CongDanDiKhaiTu.CMND;
                txtbHoTen_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu.HoTen) ? "" : CongDanDiKhaiTu.HoTen;
                dtpNgaySinh_NDRKT.Value = CongDanDiKhaiTu.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(CongDanDiKhaiTu.NgaySinh);
               txtbDanToc_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu.DanToc) ? "" : CongDanDiKhaiTu.DanToc;
                txtbQuocTich_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu.QuocTich) ? "" : CongDanDiKhaiTu.QuocTich;
                txtbQueQuan_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu.QueQuan) ? "" : CongDanDiKhaiTu.QueQuan;
                bool bGioitinh = CongDanDiKhaiTu.GioiTinh;

                if(bGioitinh)
                {
                    rdoNam_NDRKT.Checked=true;
                }
                else
                {
                    rdoNu_NDRKT.Checked = true;
                }
            }
            FormQLCongDan.Instance.Close();
        }


        //Chọn thông tin ngừoi được khai tử

        private void btn_ChooseThongTinKhaiTu_Click(object sender, EventArgs e)
        {

            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.AllGioiTinh = true;
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                MessageBox.Show("Chọn thành công");
                CongDanDuocKhaiTu = FormQLCongDan.Instance.congDan;
                this.MaCongDanYeuCau = CongDanDuocKhaiTu.MaCongDan.ToString();
                txtbCMND.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu.CMND) ? "" : CongDanDuocKhaiTu.CMND;
                txtbHoTen.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu.HoTen) ? "" : CongDanDuocKhaiTu.HoTen; 
                dtpNgaySinh.Value = CongDanDuocKhaiTu.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(CongDanDuocKhaiTu.NgaySinh);
                txtbDanToc.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu.DanToc) ? "" : CongDanDuocKhaiTu.DanToc;
                txtbQuocTich.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu.QuocTich) ? "" : CongDanDuocKhaiTu.QuocTich;
                txtbQueQuan.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu.QueQuan) ? "" : CongDanDuocKhaiTu.QueQuan;
               
                bool bGioitinh = CongDanDuocKhaiTu.GioiTinh;

                if (bGioitinh)
                {
                    rdoNam.Checked = true;
                }
                else
                {
                    rdoNu.Checked = true;
                }
            }
            FormQLCongDan.Instance.Close();

        }
        //Validate 
        private bool ValidateKhaiTu()
        {
            bool check = true;
            if(txtbHoTen.Text=="")
            {
                check = false;
            }
            if(txtbHoTen_NDRKT.Text=="")
            {
                check = false;

                }  
            if(txtbNoiCuChu.Text=="")
            {
                check = false;
            }
            if(txtbNoiChet.Text=="")
            {
                check = false;
            }
            if(txtbNguyenNhan.Text=="")
            {
                check = false;
            }


            return check;
        }




        //Thêm mới hồ sơ khai tử 
        private void btnThem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn chắc chắn sẽ nhập thông tin hồ sơ khai tử này chứ?", "Xác nhận", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int IdNguoiChet = CongDanDuocKhaiTu.MaCongDan;
                int MaCongDan = CongDanDiKhaiTu.MaCongDan;
                string NoiCuTruCuoiCung = txtbNoiCuChu.Text.Trim();
                string NoiChet = txtbNoiChet.Text.Trim();
                string NguyenNhanChet = txtbNguyenNhan.Text.Trim();
                DateTime ThoiGianChet = dtp_ThoiGianChet.Value;
                DateTime NgayHenTra = dtpHenTra.Value;
                if (khaiTuModel.Insert(IdNguoiChet, MaCongDan,int.Parse(Constants.MaCongChucDangNhap), ThoiGianChet, NgayHenTra, NoiCuTruCuoiCung, NoiChet, NguyenNhanChet)&&ValidateKhaiTu()) 
                {
                    MessageBox.Show("Thêm hồ sơ đăng ký khai tử thành công");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thêm hồ sơ đăng ký khai tử thất bại, vui lòng nhập đầy đủ thông tin !");
                }
            }
            }
        //Load data ra grigview 
        private void LoadDuLieu()
        {
            try
            {
                dgrv_KhaiTu.Rows.Clear();
                List<KhaiTu> dsKhaiTu  =khaiTuModel.GetData();
                foreach (var khaiTu in dsKhaiTu)
                {
                    var index = dgrv_KhaiTu.Rows.Add();
                    dgrv_KhaiTu.Rows[index].Cells["MaKT"].Value = khaiTu.MaKT;
                    dgrv_KhaiTu.Rows[index].Cells["IdNguoiChet"].Value = khaiTu.IDNguoiChet;
                    dgrv_KhaiTu.Rows[index].Cells["NoiCuTruCuoiCung"].Value = khaiTu.NoiCuTruCuoiCung;
                    dgrv_KhaiTu.Rows[index].Cells["ThoiGianChet"].Value = khaiTu.ThoiGianChet == null ? "" : Convert.ToDateTime(khaiTu.ThoiGianChet).ToString("dd/MM/yyyy");
                    dgrv_KhaiTu.Rows[index].Cells["NoiChet"].Value = khaiTu.NoiChet;
                    dgrv_KhaiTu.Rows[index].Cells["NguyenNhan"].Value = khaiTu.NguyenNhan;
                    dgrv_KhaiTu.Rows[index].Cells["MaThuTuc"].Value = khaiTu.MaThuTuc;
                    dgrv_KhaiTu.Rows[index].Cells["NgayDangKy"].Value = khaiTu.NgayDangKy == null ? "" : Convert.ToDateTime(khaiTu.NgayDangKy).ToString("dd/MM/yyyy");
                    dgrv_KhaiTu.Rows[index].Cells["NgayHen"].Value = khaiTu.NgayHen == null ? "" : Convert.ToDateTime(khaiTu.NgayHen).ToString("dd/MM/yyyy");
                    dgrv_KhaiTu.Rows[index].Cells["NgayTra"].Value = khaiTu.NgayTra == null ? "" : Convert.ToDateTime(khaiTu.NgayTra).ToString("dd/MM/yyyy");
                    dgrv_KhaiTu.Rows[index].Cells["MaCongDan"].Value = khaiTu.MaCongDan;
                    dgrv_KhaiTu.Rows[index].Cells["MaCongChucTiepNhan"].Value = khaiTu.MaCongChucTiepNhan;
                    dgrv_KhaiTu.Rows[index].Cells["TrangThai"].Value = khaiTu.TrangThai;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra!" +ex.ToString());
            }
        }

        private void dgrv_KhaiTu_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                int rowIndex = e.RowIndex;
                if (rowIndex >= 0)
                {
                    ToggleButtonByTrangThai(dgrv_KhaiTu["TrangThai", rowIndex].Value.ToString());


                    CongDan CongDanDiKhaiTu2 = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KhaiTu["MaCongDan", rowIndex].Value.ToString()));
                    CongDan CongDanDuocKhaiTu2 = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KhaiTu["IdNguoiChet", rowIndex].Value.ToString()));

                    currentKhaiTu.MaKT = String.IsNullOrEmpty(dgrv_KhaiTu["MaKT", rowIndex].Value.ToString()) ? 0 : int.Parse(dgrv_KhaiTu["MaKT", rowIndex].Value.ToString());
                    txtbHoTen_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu2.HoTen) ? "" : CongDanDiKhaiTu2.HoTen; ;
                    txtbCMND_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu2.CMND) ? "" : CongDanDiKhaiTu2.CMND;
                    txtbHoTen_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu2.HoTen) ? "" : CongDanDiKhaiTu2.HoTen;
                    dtpNgaySinh_NDRKT.Value = CongDanDiKhaiTu2.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(CongDanDiKhaiTu2.NgaySinh);
                    txtbDanToc_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu2.DanToc) ? "" : CongDanDiKhaiTu2.DanToc;
                    txtbQuocTich_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu2.QuocTich) ? "" : CongDanDiKhaiTu2.QuocTich;
                    txtbQueQuan_NDRKT.Text = String.IsNullOrEmpty(CongDanDiKhaiTu2.QueQuan) ? "" : CongDanDiKhaiTu2.QueQuan;



                    bool bGioitinh2 = CongDanDiKhaiTu2.GioiTinh;
                    if (bGioitinh2)
                    {
                        rdoNam_NDRKT.Checked = true;
                    }
                    else
                    {
                        rdoNu_NDRKT.Checked = true;
                    }

                    //---Hien Cong Dan Duoc Khai Tu
              
                    txtbHoTen.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu2.HoTen) ? "" : CongDanDuocKhaiTu2.HoTen;
                    txtbCMND.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu2.CMND) ? "" : CongDanDuocKhaiTu2.CMND;
                    dtpNgaySinh.Value = CongDanDuocKhaiTu2.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(CongDanDuocKhaiTu2.NgaySinh);
                    txtbDanToc.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu2.DanToc) ? "" : CongDanDuocKhaiTu2.DanToc;
                    txtbQuocTich.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu2.QuocTich) ? "" : CongDanDuocKhaiTu2.QuocTich;
                    txtbQueQuan.Text = String.IsNullOrEmpty(CongDanDuocKhaiTu2.QueQuan) ? "" : CongDanDuocKhaiTu2.QueQuan;

                    bool bGioitinh3 = CongDanDuocKhaiTu2.GioiTinh;
                    if (bGioitinh3)
                    {
                        rdoNam.Checked = true;
                    }
                    else
                    {
                        rdoNu.Checked = true;
                    }
                    //Enabled các nút 
                    //txtbNguyenNhan.Enabled = false;
                    //txtbNoiChet.Enabled = false;
                    //dtp_ThoiGianChet.Enabled = false;
                    //dtpHenTra.Enabled = false;
                    txtbNguyenNhan.Text = String.IsNullOrEmpty(dgrv_KhaiTu["NguyenNhan", rowIndex].Value.ToString()) ? "" : dgrv_KhaiTu["NguyenNhan", rowIndex].Value.ToString();
                    txtbNoiChet.Text = String.IsNullOrEmpty(dgrv_KhaiTu["NoiChet", rowIndex].Value.ToString()) ? "" : dgrv_KhaiTu["NoiChet", rowIndex].Value.ToString();
                    txtbNoiCuChu.Text = String.IsNullOrEmpty(dgrv_KhaiTu["NoiCuTruCuoiCung", rowIndex].Value.ToString()) ? "" : dgrv_KhaiTu["NoiCuTruCuoiCung", rowIndex].Value.ToString();
                    dtp_ThoiGianChet.Value = DateTime.ParseExact(dgrv_KhaiTu.Rows[rowIndex].Cells["ThoiGianChet"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                    dtpHenTra.Value = DateTime.ParseExact(dgrv_KhaiTu.Rows[rowIndex].Cells["NgayHen"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);

                }
            }
            catch (Exception ex)
            {

            }

        }

        private void ToggleButtonByTrangThai(string TrangThai)
        {
            int iTrangThai = int.Parse(TrangThai);
            SetFalseAllButton();
            if (Constants.isCCCX)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.TiepNhan)
                {
                    btnXacNhan.Enabled = true;
                }
            }
            if (Constants.isLD)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.XacMinh)
                {
                    btnDuyet.Enabled = true;
                }
            }
            if (Constants.isVP)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.Duyet)
                {
                    btnTraKQ.Enabled = true;
                }
            }
        }

        //Xác nhận 

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn xác nhận hồ sơ khai tử này chứ?";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (khaiTuModel.UpdateTrangThai(currentKhaiTu.MaKT, Constants.MaCongChucDangNhap, "", (int)TrangThaiModel.TrangThai.XacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.XacMinh + "");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại !");
                    
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {
                if (khaiTuModel.UpdateTrangThai(currentKhaiTu.MaKT, Constants.MaCongChucDangNhap, "", (int)TrangThaiModel.TrangThai.KhongXacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.KhongXacMinh + "");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại !");
                }
            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }

        }

        private void btnDuyet_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn duyệt hồ sơ khai tử này chứ?";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (khaiTuModel.UpdateTrangThai(currentKhaiTu.MaKT, "", Constants.MaCongChucDangNhap, (int)TrangThaiModel.TrangThai.Duyet))
                {
                   
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.Duyet + "");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {
                if (khaiTuModel.UpdateTrangThai(currentKhaiTu.MaKT, "", Constants.MaCongChucDangNhap, (int)TrangThaiModel.TrangThai.KhongDuyet))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.KhongDuyet + "");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }

        }

        private void btnTraKQ_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn trả kết quả hồ sơ khai tử này chứ?";
            CustomYesNoCancelMsgBox.Instance.Yes = "OK";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (khaiTuModel.UpdateTrangThai(currentKhaiTu.MaKT, Constants.MaCongChucDangNhap, DateTime.Now, (int)TrangThaiModel.TrangThai.XacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.DaTra + "");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {

            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }

        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {
            FormMenu.Instance.Show();
            FormQLKhaiTu.Instance.Close();

        }

        private void btnHuy_Click(object sender, EventArgs e)
        {

            foreach (Control ctr in grbNguoiDungRaKT.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }

            foreach (Control ctr in grbKhaiTu.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }
            SetFalseAllButton();
            btnThem.Enabled = true;

        }

        private void btn_TimKiem_Click(object sender, EventArgs e)
        {
            List<KhaiTu> dsKhaiTu = khaiTuModel.GetData();

            if (comboBox1.Text=="Tất cả")
            {
                dgrv_KhaiTu.Rows.Clear();
                dsKhaiTu = khaiTuModel.GetData();
            }    

            if(comboBox1.Text== "Tiếp nhận, chờ xác nhận")
            {
                dsKhaiTu = khaiTuModel.TimKiem(0);
             
            }
            if(comboBox1.Text== "Đã xác minh, chờ duyệt")
            {
                dsKhaiTu = khaiTuModel.TimKiem(1);
            }
            if (comboBox1.Text == "Đã duyệt")
            {
                dsKhaiTu = khaiTuModel.TimKiem(3);
            }




            try
            {
                dgrv_KhaiTu.Rows.Clear();
             
                foreach (var khaiTu in dsKhaiTu)
                {
                    var index = dgrv_KhaiTu.Rows.Add();
                    dgrv_KhaiTu.Rows[index].Cells["MaKT"].Value = khaiTu.MaKT;
                    dgrv_KhaiTu.Rows[index].Cells["IdNguoiChet"].Value = khaiTu.IDNguoiChet;
                    dgrv_KhaiTu.Rows[index].Cells["NoiCuTruCuoiCung"].Value = khaiTu.NoiCuTruCuoiCung;
                    dgrv_KhaiTu.Rows[index].Cells["ThoiGianChet"].Value = khaiTu.ThoiGianChet == null ? "" : Convert.ToDateTime(khaiTu.ThoiGianChet).ToString("dd/MM/yyyy");
                    dgrv_KhaiTu.Rows[index].Cells["NoiChet"].Value = khaiTu.NoiChet;
                    dgrv_KhaiTu.Rows[index].Cells["NguyenNhan"].Value = khaiTu.NguyenNhan;
                    dgrv_KhaiTu.Rows[index].Cells["MaThuTuc"].Value = khaiTu.MaThuTuc;
                    dgrv_KhaiTu.Rows[index].Cells["NgayDangKy"].Value = khaiTu.NgayDangKy == null ? "" : Convert.ToDateTime(khaiTu.NgayDangKy).ToString("dd/MM/yyyy");
                    dgrv_KhaiTu.Rows[index].Cells["NgayHen"].Value = khaiTu.NgayHen == null ? "" : Convert.ToDateTime(khaiTu.NgayHen).ToString("dd/MM/yyyy");
                    dgrv_KhaiTu.Rows[index].Cells["NgayTra"].Value = khaiTu.NgayTra == null ? "" : Convert.ToDateTime(khaiTu.NgayTra).ToString("dd/MM/yyyy");
                    dgrv_KhaiTu.Rows[index].Cells["MaCongDan"].Value = khaiTu.MaCongDan;
                    dgrv_KhaiTu.Rows[index].Cells["MaCongChucTiepNhan"].Value = khaiTu.MaCongChucTiepNhan;
                    dgrv_KhaiTu.Rows[index].Cells["TrangThai"].Value = khaiTu.TrangThai;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra!" + ex.ToString());
            }


        }
    }

}
