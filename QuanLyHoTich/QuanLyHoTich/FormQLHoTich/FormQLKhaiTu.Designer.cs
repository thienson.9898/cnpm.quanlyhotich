﻿namespace QuanLyHoTich.FormQLHoTich
{
    partial class FormQLKhaiTu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblTTTHS = new System.Windows.Forms.Label();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.grbKhaiTu = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtbQueQuan = new System.Windows.Forms.TextBox();
            this.txtbDanToc = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtbCMND = new System.Windows.Forms.TextBox();
            this.btn_ChooseThongTinKhaiTu = new System.Windows.Forms.Button();
            this.dtp_ThoiGianChet = new System.Windows.Forms.DateTimePicker();
            this.txtbNguyenNhan = new System.Windows.Forms.TextBox();
            this.txtbNoiChet = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtbNoiCuChu = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.noicutru = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdoNu = new System.Windows.Forms.RadioButton();
            this.rdoNam = new System.Windows.Forms.RadioButton();
            this.label23 = new System.Windows.Forms.Label();
            this.txtbHoTen = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbQuocTich = new System.Windows.Forms.TextBox();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.grbNguoiDungRaKT = new System.Windows.Forms.GroupBox();
            this.groupBox_NDRKT = new System.Windows.Forms.GroupBox();
            this.rdoNu_NDRKT = new System.Windows.Forms.RadioButton();
            this.rdoNam_NDRKT = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.txtbCMND_NDRKT = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dtpNgaySinh_NDRKT = new System.Windows.Forms.DateTimePicker();
            this.txtbQueQuan_NDRKT = new System.Windows.Forms.TextBox();
            this.txtbQuocTich_NDRKT = new System.Windows.Forms.TextBox();
            this.txtbDanToc_NDRKT = new System.Windows.Forms.TextBox();
            this.txtbHoTen_NDRKT = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btn_ChooseNguoiDungRaKhaiTu = new System.Windows.Forms.Button();
            this.dtpHenTra = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.btnTraKQ = new System.Windows.Forms.Button();
            this.btnDuyet = new System.Windows.Forms.Button();
            this.btnXacNhan = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.btn_TimKiem = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.dgrv_KhaiTu = new System.Windows.Forms.DataGridView();
            this.MaKT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdNguoiChet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoiCuTruCuoiCung = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ThoiGianChet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoiChet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NguyenNhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaThuTuc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayDangKy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayHen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayTra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaCongDan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaCongChucTiepNhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrangThai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnBackToMenu = new System.Windows.Forms.Button();
            this.grbKhaiTu.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.grbNguoiDungRaKT.SuspendLayout();
            this.groupBox_NDRKT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrv_KhaiTu)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 20);
            this.label1.TabIndex = 0;
            // 
            // lblTTTHS
            // 
            this.lblTTTHS.AutoSize = true;
            this.lblTTTHS.Location = new System.Drawing.Point(13, 9);
            this.lblTTTHS.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTTTHS.Name = "lblTTTHS";
            this.lblTTTHS.Size = new System.Drawing.Size(131, 20);
            this.lblTTTHS.TabIndex = 56;
            this.lblTTTHS.Text = "Trạng thái hồ sơ: ";
            this.lblTTTHS.Visible = false;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Location = new System.Drawing.Point(152, 9);
            this.lblTrangThai.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(72, 20);
            this.lblTrangThai.TabIndex = 57;
            this.lblTrangThai.Text = "trangthai";
            this.lblTrangThai.Visible = false;
            // 
            // grbKhaiTu
            // 
            this.grbKhaiTu.Controls.Add(this.label17);
            this.grbKhaiTu.Controls.Add(this.txtbQueQuan);
            this.grbKhaiTu.Controls.Add(this.txtbDanToc);
            this.grbKhaiTu.Controls.Add(this.label15);
            this.grbKhaiTu.Controls.Add(this.label14);
            this.grbKhaiTu.Controls.Add(this.txtbCMND);
            this.grbKhaiTu.Controls.Add(this.btn_ChooseThongTinKhaiTu);
            this.grbKhaiTu.Controls.Add(this.dtp_ThoiGianChet);
            this.grbKhaiTu.Controls.Add(this.txtbNguyenNhan);
            this.grbKhaiTu.Controls.Add(this.txtbNoiChet);
            this.grbKhaiTu.Controls.Add(this.textBox2);
            this.grbKhaiTu.Controls.Add(this.txtbNoiCuChu);
            this.grbKhaiTu.Controls.Add(this.label7);
            this.grbKhaiTu.Controls.Add(this.label4);
            this.grbKhaiTu.Controls.Add(this.label6);
            this.grbKhaiTu.Controls.Add(this.noicutru);
            this.grbKhaiTu.Controls.Add(this.groupBox4);
            this.grbKhaiTu.Controls.Add(this.label23);
            this.grbKhaiTu.Controls.Add(this.txtbHoTen);
            this.grbKhaiTu.Controls.Add(this.label2);
            this.grbKhaiTu.Controls.Add(this.label3);
            this.grbKhaiTu.Controls.Add(this.label5);
            this.grbKhaiTu.Controls.Add(this.txtbQuocTich);
            this.grbKhaiTu.Controls.Add(this.dtpNgaySinh);
            this.grbKhaiTu.Location = new System.Drawing.Point(35, 38);
            this.grbKhaiTu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grbKhaiTu.Name = "grbKhaiTu";
            this.grbKhaiTu.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grbKhaiTu.Size = new System.Drawing.Size(437, 498);
            this.grbKhaiTu.TabIndex = 58;
            this.grbKhaiTu.TabStop = false;
            this.grbKhaiTu.Text = "Thông tin khai tử";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 406);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 20);
            this.label17.TabIndex = 47;
            this.label17.Text = "Quê quán";
            // 
            // txtbQueQuan
            // 
            this.txtbQueQuan.Location = new System.Drawing.Point(141, 400);
            this.txtbQueQuan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbQueQuan.Name = "txtbQueQuan";
            this.txtbQueQuan.Size = new System.Drawing.Size(224, 26);
            this.txtbQueQuan.TabIndex = 49;
            // 
            // txtbDanToc
            // 
            this.txtbDanToc.Location = new System.Drawing.Point(139, 183);
            this.txtbDanToc.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbDanToc.Name = "txtbDanToc";
            this.txtbDanToc.Size = new System.Drawing.Size(224, 26);
            this.txtbDanToc.TabIndex = 47;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(24, 186);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 20);
            this.label15.TabIndex = 48;
            this.label15.Text = "Dân tộc";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 364);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 20);
            this.label14.TabIndex = 47;
            this.label14.Text = "CMND";
            // 
            // txtbCMND
            // 
            this.txtbCMND.Location = new System.Drawing.Point(141, 364);
            this.txtbCMND.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbCMND.Name = "txtbCMND";
            this.txtbCMND.Size = new System.Drawing.Size(224, 26);
            this.txtbCMND.TabIndex = 47;
            // 
            // btn_ChooseThongTinKhaiTu
            // 
            this.btn_ChooseThongTinKhaiTu.Location = new System.Drawing.Point(141, 443);
            this.btn_ChooseThongTinKhaiTu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_ChooseThongTinKhaiTu.Name = "btn_ChooseThongTinKhaiTu";
            this.btn_ChooseThongTinKhaiTu.Size = new System.Drawing.Size(207, 35);
            this.btn_ChooseThongTinKhaiTu.TabIndex = 37;
            this.btn_ChooseThongTinKhaiTu.Text = "Chọn thông tin ";
            this.btn_ChooseThongTinKhaiTu.UseVisualStyleBackColor = true;
            this.btn_ChooseThongTinKhaiTu.Click += new System.EventHandler(this.btn_ChooseThongTinKhaiTu_Click);
            // 
            // dtp_ThoiGianChet
            // 
            this.dtp_ThoiGianChet.CustomFormat = "dd/MM/yyyy";
            this.dtp_ThoiGianChet.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_ThoiGianChet.Location = new System.Drawing.Point(141, 249);
            this.dtp_ThoiGianChet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtp_ThoiGianChet.Name = "dtp_ThoiGianChet";
            this.dtp_ThoiGianChet.Size = new System.Drawing.Size(224, 26);
            this.dtp_ThoiGianChet.TabIndex = 36;
            // 
            // txtbNguyenNhan
            // 
            this.txtbNguyenNhan.Location = new System.Drawing.Point(141, 327);
            this.txtbNguyenNhan.Name = "txtbNguyenNhan";
            this.txtbNguyenNhan.Size = new System.Drawing.Size(224, 26);
            this.txtbNguyenNhan.TabIndex = 35;
            // 
            // txtbNoiChet
            // 
            this.txtbNoiChet.Location = new System.Drawing.Point(141, 288);
            this.txtbNoiChet.Name = "txtbNoiChet";
            this.txtbNoiChet.Size = new System.Drawing.Size(224, 26);
            this.txtbNoiChet.TabIndex = 34;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(141, 249);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(0, 26);
            this.textBox2.TabIndex = 33;
            // 
            // txtbNoiCuChu
            // 
            this.txtbNoiCuChu.Location = new System.Drawing.Point(139, 215);
            this.txtbNoiCuChu.Name = "txtbNoiCuChu";
            this.txtbNoiCuChu.Size = new System.Drawing.Size(224, 26);
            this.txtbNoiCuChu.TabIndex = 32;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 330);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 20);
            this.label7.TabIndex = 31;
            this.label7.Text = "Nguyên nhân";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 288);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 20);
            this.label4.TabIndex = 30;
            this.label4.Text = "Nơi chết";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 252);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 20);
            this.label6.TabIndex = 29;
            this.label6.Text = "Thời gian chết";
            // 
            // noicutru
            // 
            this.noicutru.AutoSize = true;
            this.noicutru.Location = new System.Drawing.Point(24, 221);
            this.noicutru.Name = "noicutru";
            this.noicutru.Size = new System.Drawing.Size(76, 20);
            this.noicutru.TabIndex = 28;
            this.noicutru.Text = "Nơi cư trú";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rdoNu);
            this.groupBox4.Controls.Add(this.rdoNam);
            this.groupBox4.Location = new System.Drawing.Point(139, 112);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox4.Size = new System.Drawing.Size(226, 31);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            // 
            // rdoNu
            // 
            this.rdoNu.AutoSize = true;
            this.rdoNu.Location = new System.Drawing.Point(139, 7);
            this.rdoNu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdoNu.Name = "rdoNu";
            this.rdoNu.Size = new System.Drawing.Size(54, 24);
            this.rdoNu.TabIndex = 1;
            this.rdoNu.TabStop = true;
            this.rdoNu.Text = "Nữ";
            this.rdoNu.UseVisualStyleBackColor = true;
            // 
            // rdoNam
            // 
            this.rdoNam.AutoSize = true;
            this.rdoNam.Location = new System.Drawing.Point(8, 6);
            this.rdoNam.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdoNam.Name = "rdoNam";
            this.rdoNam.Size = new System.Drawing.Size(67, 24);
            this.rdoNam.TabIndex = 0;
            this.rdoNam.TabStop = true;
            this.rdoNam.Text = "Nam";
            this.rdoNam.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(24, 119);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(67, 20);
            this.label23.TabIndex = 26;
            this.label23.Text = "Giới tính";
            // 
            // txtbHoTen
            // 
            this.txtbHoTen.Location = new System.Drawing.Point(139, 32);
            this.txtbHoTen.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbHoTen.Name = "txtbHoTen";
            this.txtbHoTen.Size = new System.Drawing.Size(224, 26);
            this.txtbHoTen.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 37);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Họ tên";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Ngày sinh";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 152);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Quốc tịch";
            // 
            // txtbQuocTich
            // 
            this.txtbQuocTich.Location = new System.Drawing.Point(139, 152);
            this.txtbQuocTich.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbQuocTich.Name = "txtbQuocTich";
            this.txtbQuocTich.Size = new System.Drawing.Size(224, 26);
            this.txtbQuocTich.TabIndex = 19;
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.CustomFormat = "dd/MM/yyyy";
            this.dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh.Location = new System.Drawing.Point(139, 72);
            this.dtpNgaySinh.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(224, 26);
            this.dtpNgaySinh.TabIndex = 21;
            // 
            // grbNguoiDungRaKT
            // 
            this.grbNguoiDungRaKT.Controls.Add(this.groupBox_NDRKT);
            this.grbNguoiDungRaKT.Controls.Add(this.label13);
            this.grbNguoiDungRaKT.Controls.Add(this.txtbCMND_NDRKT);
            this.grbNguoiDungRaKT.Controls.Add(this.label16);
            this.grbNguoiDungRaKT.Controls.Add(this.dtpNgaySinh_NDRKT);
            this.grbNguoiDungRaKT.Controls.Add(this.txtbQueQuan_NDRKT);
            this.grbNguoiDungRaKT.Controls.Add(this.txtbQuocTich_NDRKT);
            this.grbNguoiDungRaKT.Controls.Add(this.txtbDanToc_NDRKT);
            this.grbNguoiDungRaKT.Controls.Add(this.txtbHoTen_NDRKT);
            this.grbNguoiDungRaKT.Controls.Add(this.label8);
            this.grbNguoiDungRaKT.Controls.Add(this.label9);
            this.grbNguoiDungRaKT.Controls.Add(this.label10);
            this.grbNguoiDungRaKT.Controls.Add(this.label11);
            this.grbNguoiDungRaKT.Controls.Add(this.label12);
            this.grbNguoiDungRaKT.Controls.Add(this.btn_ChooseNguoiDungRaKhaiTu);
            this.grbNguoiDungRaKT.Location = new System.Drawing.Point(730, 38);
            this.grbNguoiDungRaKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grbNguoiDungRaKT.Name = "grbNguoiDungRaKT";
            this.grbNguoiDungRaKT.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grbNguoiDungRaKT.Size = new System.Drawing.Size(384, 419);
            this.grbNguoiDungRaKT.TabIndex = 59;
            this.grbNguoiDungRaKT.TabStop = false;
            this.grbNguoiDungRaKT.Text = "Thông tin người đứng ra khai tử ";
            // 
            // groupBox_NDRKT
            // 
            this.groupBox_NDRKT.Controls.Add(this.rdoNu_NDRKT);
            this.groupBox_NDRKT.Controls.Add(this.rdoNam_NDRKT);
            this.groupBox_NDRKT.Location = new System.Drawing.Point(102, 157);
            this.groupBox_NDRKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox_NDRKT.Name = "groupBox_NDRKT";
            this.groupBox_NDRKT.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox_NDRKT.Size = new System.Drawing.Size(226, 31);
            this.groupBox_NDRKT.TabIndex = 29;
            this.groupBox_NDRKT.TabStop = false;
            // 
            // rdoNu_NDRKT
            // 
            this.rdoNu_NDRKT.AutoSize = true;
            this.rdoNu_NDRKT.Location = new System.Drawing.Point(140, 3);
            this.rdoNu_NDRKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdoNu_NDRKT.Name = "rdoNu_NDRKT";
            this.rdoNu_NDRKT.Size = new System.Drawing.Size(54, 24);
            this.rdoNu_NDRKT.TabIndex = 1;
            this.rdoNu_NDRKT.TabStop = true;
            this.rdoNu_NDRKT.Text = "Nữ";
            this.rdoNu_NDRKT.UseVisualStyleBackColor = true;
            // 
            // rdoNam_NDRKT
            // 
            this.rdoNam_NDRKT.AutoSize = true;
            this.rdoNam_NDRKT.Location = new System.Drawing.Point(10, 3);
            this.rdoNam_NDRKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdoNam_NDRKT.Name = "rdoNam_NDRKT";
            this.rdoNam_NDRKT.Size = new System.Drawing.Size(67, 24);
            this.rdoNam_NDRKT.TabIndex = 0;
            this.rdoNam_NDRKT.TabStop = true;
            this.rdoNam_NDRKT.Text = "Nam";
            this.rdoNam_NDRKT.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 160);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 20);
            this.label13.TabIndex = 28;
            this.label13.Text = "Giới tính";
            // 
            // txtbCMND_NDRKT
            // 
            this.txtbCMND_NDRKT.Location = new System.Drawing.Point(102, 37);
            this.txtbCMND_NDRKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbCMND_NDRKT.Name = "txtbCMND_NDRKT";
            this.txtbCMND_NDRKT.Size = new System.Drawing.Size(224, 26);
            this.txtbCMND_NDRKT.TabIndex = 46;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 40);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 20);
            this.label16.TabIndex = 45;
            this.label16.Text = "CMND";
            // 
            // dtpNgaySinh_NDRKT
            // 
            this.dtpNgaySinh_NDRKT.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh_NDRKT.Location = new System.Drawing.Point(102, 119);
            this.dtpNgaySinh_NDRKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtpNgaySinh_NDRKT.Name = "dtpNgaySinh_NDRKT";
            this.dtpNgaySinh_NDRKT.Size = new System.Drawing.Size(224, 26);
            this.dtpNgaySinh_NDRKT.TabIndex = 44;
            // 
            // txtbQueQuan_NDRKT
            // 
            this.txtbQueQuan_NDRKT.Location = new System.Drawing.Point(102, 279);
            this.txtbQueQuan_NDRKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbQueQuan_NDRKT.Name = "txtbQueQuan_NDRKT";
            this.txtbQueQuan_NDRKT.Size = new System.Drawing.Size(224, 26);
            this.txtbQueQuan_NDRKT.TabIndex = 43;
            // 
            // txtbQuocTich_NDRKT
            // 
            this.txtbQuocTich_NDRKT.Location = new System.Drawing.Point(102, 240);
            this.txtbQuocTich_NDRKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbQuocTich_NDRKT.Name = "txtbQuocTich_NDRKT";
            this.txtbQuocTich_NDRKT.Size = new System.Drawing.Size(224, 26);
            this.txtbQuocTich_NDRKT.TabIndex = 42;
            // 
            // txtbDanToc_NDRKT
            // 
            this.txtbDanToc_NDRKT.Location = new System.Drawing.Point(102, 203);
            this.txtbDanToc_NDRKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbDanToc_NDRKT.Name = "txtbDanToc_NDRKT";
            this.txtbDanToc_NDRKT.Size = new System.Drawing.Size(224, 26);
            this.txtbDanToc_NDRKT.TabIndex = 41;
            // 
            // txtbHoTen_NDRKT
            // 
            this.txtbHoTen_NDRKT.Location = new System.Drawing.Point(102, 79);
            this.txtbHoTen_NDRKT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtbHoTen_NDRKT.Name = "txtbHoTen_NDRKT";
            this.txtbHoTen_NDRKT.Size = new System.Drawing.Size(224, 26);
            this.txtbHoTen_NDRKT.TabIndex = 40;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 282);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 20);
            this.label8.TabIndex = 39;
            this.label8.Text = "Quê quán";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 243);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 20);
            this.label9.TabIndex = 38;
            this.label9.Text = "Quốc tịch";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 206);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 20);
            this.label10.TabIndex = 37;
            this.label10.Text = "Dân tộc";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 122);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 20);
            this.label11.TabIndex = 36;
            this.label11.Text = "Ngày sinh";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 82);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 20);
            this.label12.TabIndex = 35;
            this.label12.Text = "Họ tên";
            // 
            // btn_ChooseNguoiDungRaKhaiTu
            // 
            this.btn_ChooseNguoiDungRaKhaiTu.Location = new System.Drawing.Point(102, 374);
            this.btn_ChooseNguoiDungRaKhaiTu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_ChooseNguoiDungRaKhaiTu.Name = "btn_ChooseNguoiDungRaKhaiTu";
            this.btn_ChooseNguoiDungRaKhaiTu.Size = new System.Drawing.Size(207, 35);
            this.btn_ChooseNguoiDungRaKhaiTu.TabIndex = 34;
            this.btn_ChooseNguoiDungRaKhaiTu.Text = "Chọn thông tin ";
            this.btn_ChooseNguoiDungRaKhaiTu.UseVisualStyleBackColor = true;
            this.btn_ChooseNguoiDungRaKhaiTu.Click += new System.EventHandler(this.btn_ChooseNguoiDungRaKhaiTu_Click);
            // 
            // dtpHenTra
            // 
            this.dtpHenTra.CustomFormat = "dd/MM/yyyy";
            this.dtpHenTra.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHenTra.Location = new System.Drawing.Point(145, 639);
            this.dtpHenTra.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtpHenTra.Name = "dtpHenTra";
            this.dtpHenTra.Size = new System.Drawing.Size(174, 26);
            this.dtpHenTra.TabIndex = 62;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(31, 645);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(99, 20);
            this.label19.TabIndex = 61;
            this.label19.Text = "Ngày hẹn trả";
            // 
            // btnTraKQ
            // 
            this.btnTraKQ.Location = new System.Drawing.Point(845, 630);
            this.btnTraKQ.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnTraKQ.Name = "btnTraKQ";
            this.btnTraKQ.Size = new System.Drawing.Size(146, 35);
            this.btnTraKQ.TabIndex = 66;
            this.btnTraKQ.Text = "Trả kết quả";
            this.btnTraKQ.UseVisualStyleBackColor = true;
            this.btnTraKQ.Click += new System.EventHandler(this.btnTraKQ_Click);
            // 
            // btnDuyet
            // 
            this.btnDuyet.Location = new System.Drawing.Point(690, 630);
            this.btnDuyet.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDuyet.Name = "btnDuyet";
            this.btnDuyet.Size = new System.Drawing.Size(146, 35);
            this.btnDuyet.TabIndex = 65;
            this.btnDuyet.Text = "Duyệt hồ sơ";
            this.btnDuyet.UseVisualStyleBackColor = true;
            this.btnDuyet.Click += new System.EventHandler(this.btnDuyet_Click);
            // 
            // btnXacNhan
            // 
            this.btnXacNhan.Location = new System.Drawing.Point(535, 630);
            this.btnXacNhan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnXacNhan.Name = "btnXacNhan";
            this.btnXacNhan.Size = new System.Drawing.Size(146, 35);
            this.btnXacNhan.TabIndex = 64;
            this.btnXacNhan.Text = "Xác nhận hồ sơ";
            this.btnXacNhan.UseVisualStyleBackColor = true;
            this.btnXacNhan.Click += new System.EventHandler(this.btnXacNhan_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(414, 630);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(112, 35);
            this.btnThem.TabIndex = 63;
            this.btnThem.Text = "Thêm hồ sơ";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Location = new System.Drawing.Point(844, 563);
            this.btnHuy.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(142, 35);
            this.btnHuy.TabIndex = 70;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // btn_TimKiem
            // 
            this.btn_TimKiem.Location = new System.Drawing.Point(721, 564);
            this.btn_TimKiem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_TimKiem.Name = "btn_TimKiem";
            this.btn_TimKiem.Size = new System.Drawing.Size(112, 35);
            this.btn_TimKiem.TabIndex = 69;
            this.btn_TimKiem.Text = "Tìm kiếm";
            this.btn_TimKiem.UseVisualStyleBackColor = true;
            this.btn_TimKiem.Click += new System.EventHandler(this.btn_TimKiem_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(397, 570);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 20);
            this.label18.TabIndex = 67;
            this.label18.Text = "Trạng thái";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Tất cả",
            "Tiếp nhận, chờ xác nhận",
            "Đã xác minh, chờ duyệt",
            "Không xác minh",
            "Đã duyệt",
            "Không duyệt",
            "Đã trả",
            "Đã thanh toán"});
            this.comboBox1.Location = new System.Drawing.Point(483, 565);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(226, 28);
            this.comboBox1.TabIndex = 68;
            // 
            // dgrv_KhaiTu
            // 
            this.dgrv_KhaiTu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrv_KhaiTu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaKT,
            this.IdNguoiChet,
            this.NoiCuTruCuoiCung,
            this.ThoiGianChet,
            this.NoiChet,
            this.NguyenNhan,
            this.MaThuTuc,
            this.NgayDangKy,
            this.NgayHen,
            this.NgayTra,
            this.MaCongDan,
            this.MaCongChucTiepNhan,
            this.TrangThai});
            this.dgrv_KhaiTu.Location = new System.Drawing.Point(13, 692);
            this.dgrv_KhaiTu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgrv_KhaiTu.Name = "dgrv_KhaiTu";
            this.dgrv_KhaiTu.RowHeadersWidth = 62;
            this.dgrv_KhaiTu.Size = new System.Drawing.Size(1128, 352);
            this.dgrv_KhaiTu.TabIndex = 71;
            this.dgrv_KhaiTu.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrv_KhaiTu_CellClick);
            // 
            // MaKT
            // 
            this.MaKT.HeaderText = "IDKT";
            this.MaKT.MinimumWidth = 8;
            this.MaKT.Name = "MaKT";
            this.MaKT.Width = 150;
            // 
            // IdNguoiChet
            // 
            this.IdNguoiChet.HeaderText = "IdNguoiChet";
            this.IdNguoiChet.MinimumWidth = 8;
            this.IdNguoiChet.Name = "IdNguoiChet";
            this.IdNguoiChet.Width = 150;
            // 
            // NoiCuTruCuoiCung
            // 
            this.NoiCuTruCuoiCung.HeaderText = "Nơi cư trú cuối cùng";
            this.NoiCuTruCuoiCung.MinimumWidth = 8;
            this.NoiCuTruCuoiCung.Name = "NoiCuTruCuoiCung";
            this.NoiCuTruCuoiCung.Width = 150;
            // 
            // ThoiGianChet
            // 
            this.ThoiGianChet.HeaderText = "Thời gian chết";
            this.ThoiGianChet.MinimumWidth = 8;
            this.ThoiGianChet.Name = "ThoiGianChet";
            this.ThoiGianChet.Width = 150;
            // 
            // NoiChet
            // 
            this.NoiChet.HeaderText = "Nơi chết";
            this.NoiChet.MinimumWidth = 8;
            this.NoiChet.Name = "NoiChet";
            this.NoiChet.Width = 150;
            // 
            // NguyenNhan
            // 
            this.NguyenNhan.HeaderText = "Nguyên Nhân Chết";
            this.NguyenNhan.MinimumWidth = 8;
            this.NguyenNhan.Name = "NguyenNhan";
            this.NguyenNhan.Width = 150;
            // 
            // MaThuTuc
            // 
            this.MaThuTuc.HeaderText = "Mã Thủ Tục";
            this.MaThuTuc.MinimumWidth = 8;
            this.MaThuTuc.Name = "MaThuTuc";
            this.MaThuTuc.Width = 150;
            // 
            // NgayDangKy
            // 
            this.NgayDangKy.HeaderText = "Ngày Đăng Ký";
            this.NgayDangKy.MinimumWidth = 8;
            this.NgayDangKy.Name = "NgayDangKy";
            this.NgayDangKy.Width = 150;
            // 
            // NgayHen
            // 
            this.NgayHen.HeaderText = "Ngày Hẹn";
            this.NgayHen.MinimumWidth = 8;
            this.NgayHen.Name = "NgayHen";
            this.NgayHen.Width = 150;
            // 
            // NgayTra
            // 
            this.NgayTra.HeaderText = "Ngày trả";
            this.NgayTra.MinimumWidth = 8;
            this.NgayTra.Name = "NgayTra";
            this.NgayTra.Width = 150;
            // 
            // MaCongDan
            // 
            this.MaCongDan.HeaderText = "Mã Công Dân";
            this.MaCongDan.MinimumWidth = 8;
            this.MaCongDan.Name = "MaCongDan";
            this.MaCongDan.Width = 150;
            // 
            // MaCongChucTiepNhan
            // 
            this.MaCongChucTiepNhan.HeaderText = "Mã Công Chức ";
            this.MaCongChucTiepNhan.MinimumWidth = 8;
            this.MaCongChucTiepNhan.Name = "MaCongChucTiepNhan";
            this.MaCongChucTiepNhan.Width = 150;
            // 
            // TrangThai
            // 
            this.TrangThai.HeaderText = "Mã trạng thái";
            this.TrangThai.MinimumWidth = 8;
            this.TrangThai.Name = "TrangThai";
            this.TrangThai.Visible = false;
            this.TrangThai.Width = 150;
            // 
            // btnBackToMenu
            // 
            this.btnBackToMenu.Location = new System.Drawing.Point(999, 630);
            this.btnBackToMenu.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnBackToMenu.Name = "btnBackToMenu";
            this.btnBackToMenu.Size = new System.Drawing.Size(142, 35);
            this.btnBackToMenu.TabIndex = 72;
            this.btnBackToMenu.Text = "Quay về Menu";
            this.btnBackToMenu.UseVisualStyleBackColor = true;
            this.btnBackToMenu.Click += new System.EventHandler(this.btnBackToMenu_Click);
            // 
            // FormQLKhaiTu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1153, 1058);
            this.ControlBox = false;
            this.Controls.Add(this.btnBackToMenu);
            this.Controls.Add(this.dgrv_KhaiTu);
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.btn_TimKiem);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btnTraKQ);
            this.Controls.Add(this.btnDuyet);
            this.Controls.Add(this.btnXacNhan);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.dtpHenTra);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.grbNguoiDungRaKT);
            this.Controls.Add(this.grbKhaiTu);
            this.Controls.Add(this.lblTrangThai);
            this.Controls.Add(this.lblTTTHS);
            this.Controls.Add(this.label1);
            this.Name = "FormQLKhaiTu";
            this.Text = "Đăng ký khai tử";
            this.grbKhaiTu.ResumeLayout(false);
            this.grbKhaiTu.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.grbNguoiDungRaKT.ResumeLayout(false);
            this.grbNguoiDungRaKT.PerformLayout();
            this.groupBox_NDRKT.ResumeLayout(false);
            this.groupBox_NDRKT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrv_KhaiTu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTTTHS;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.GroupBox grbKhaiTu;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rdoNu;
        private System.Windows.Forms.RadioButton rdoNam;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtbHoTen;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbQuocTich;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label noicutru;
        private System.Windows.Forms.TextBox txtbNguyenNhan;
        private System.Windows.Forms.TextBox txtbNoiChet;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txtbNoiCuChu;
        private System.Windows.Forms.DateTimePicker dtp_ThoiGianChet;
        private System.Windows.Forms.Button btn_ChooseThongTinKhaiTu;
        private System.Windows.Forms.GroupBox grbNguoiDungRaKT;
        private System.Windows.Forms.TextBox txtbCMND_NDRKT;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh_NDRKT;
        private System.Windows.Forms.TextBox txtbQueQuan_NDRKT;
        private System.Windows.Forms.TextBox txtbQuocTich_NDRKT;
        private System.Windows.Forms.TextBox txtbDanToc_NDRKT;
        private System.Windows.Forms.TextBox txtbHoTen_NDRKT;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_ChooseNguoiDungRaKhaiTu;
        private System.Windows.Forms.GroupBox groupBox_NDRKT;
        private System.Windows.Forms.RadioButton rdoNu_NDRKT;
        private System.Windows.Forms.RadioButton rdoNam_NDRKT;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dtpHenTra;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnTraKQ;
        private System.Windows.Forms.Button btnDuyet;
        private System.Windows.Forms.Button btnXacNhan;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Button btn_TimKiem;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridView dgrv_KhaiTu;
        private System.Windows.Forms.Button btnBackToMenu;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtbQueQuan;
        private System.Windows.Forms.TextBox txtbDanToc;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtbCMND;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaKT;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdNguoiChet;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoiCuTruCuoiCung;
        private System.Windows.Forms.DataGridViewTextBoxColumn ThoiGianChet;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoiChet;
        private System.Windows.Forms.DataGridViewTextBoxColumn NguyenNhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaThuTuc;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayDangKy;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayHen;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayTra;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCongDan;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCongChucTiepNhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai;
    }
}