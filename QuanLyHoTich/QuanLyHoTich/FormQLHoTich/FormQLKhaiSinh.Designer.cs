﻿namespace QuanLyHoTich
{
    partial class FormQLKhaiSinh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbHoTen = new System.Windows.Forms.TextBox();
            this.txtbDanToc = new System.Windows.Forms.TextBox();
            this.txtbQuocTich = new System.Windows.Forms.TextBox();
            this.txtbQueQuan = new System.Windows.Forms.TextBox();
            this.dtpNgaySinh = new System.Windows.Forms.DateTimePicker();
            this.grbKhaiSinh = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdNu = new System.Windows.Forms.RadioButton();
            this.rdNam = new System.Windows.Forms.RadioButton();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtbNoiSinh = new System.Windows.Forms.TextBox();
            this.txtbSoDinhDanh = new System.Windows.Forms.TextBox();
            this.grbCha = new System.Windows.Forms.GroupBox();
            this.txtbCMND_Cha = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dtpNgaySinh_Cha = new System.Windows.Forms.DateTimePicker();
            this.txtbQueQuan_Cha = new System.Windows.Forms.TextBox();
            this.txtbQuocTich_Cha = new System.Windows.Forms.TextBox();
            this.txtbDanToc_Cha = new System.Windows.Forms.TextBox();
            this.txtbHoTen_Cha = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_ChooseCha = new System.Windows.Forms.Button();
            this.grbMe = new System.Windows.Forms.GroupBox();
            this.txtbCMND_Me = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpNgaySinh_Me = new System.Windows.Forms.DateTimePicker();
            this.txtbQueQuan_Me = new System.Windows.Forms.TextBox();
            this.txtbQuocTich_Me = new System.Windows.Forms.TextBox();
            this.txtbDanToc_Me = new System.Windows.Forms.TextBox();
            this.txtbHoTen_Me = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btn_ChooseMe = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnXacNhan = new System.Windows.Forms.Button();
            this.btnDuyet = new System.Windows.Forms.Button();
            this.btnTraKQ = new System.Windows.Forms.Button();
            this.dgrv_KhaiSinh = new System.Windows.Forms.DataGridView();
            this.MaKhaiSinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgaySinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GioiTinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DanToc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoiSinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoDinhDanhCaNhan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QuocTich = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QueQuan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTenCha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTenMe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrangThai_MoTa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDCha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayHen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayTra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDMe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrangThai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbTrangThai = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.lblTTTHS = new System.Windows.Forms.Label();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.btn_TimKiem = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.btnBackToMenu = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.dtpHenTra = new System.Windows.Forms.DateTimePicker();
            this.btnChonCongDan = new System.Windows.Forms.Button();
            this.lblNguoiYeuCau = new System.Windows.Forms.Label();
            this.lblHoTenNguoiYeuCau = new System.Windows.Forms.Label();
            this.grbKhaiSinh.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.grbCha.SuspendLayout();
            this.grbMe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrv_KhaiSinh)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Họ tên";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngày sinh";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Dân tộc";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Quốc tịch";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 203);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Quê quán";
            // 
            // txtbHoTen
            // 
            this.txtbHoTen.Location = new System.Drawing.Point(77, 21);
            this.txtbHoTen.Name = "txtbHoTen";
            this.txtbHoTen.Size = new System.Drawing.Size(151, 20);
            this.txtbHoTen.TabIndex = 1;
            // 
            // txtbDanToc
            // 
            this.txtbDanToc.Location = new System.Drawing.Point(77, 99);
            this.txtbDanToc.Name = "txtbDanToc";
            this.txtbDanToc.Size = new System.Drawing.Size(151, 20);
            this.txtbDanToc.TabIndex = 5;
            // 
            // txtbQuocTich
            // 
            this.txtbQuocTich.Location = new System.Drawing.Point(77, 175);
            this.txtbQuocTich.Name = "txtbQuocTich";
            this.txtbQuocTich.Size = new System.Drawing.Size(151, 20);
            this.txtbQuocTich.TabIndex = 8;
            // 
            // txtbQueQuan
            // 
            this.txtbQueQuan.Location = new System.Drawing.Point(77, 200);
            this.txtbQueQuan.Name = "txtbQueQuan";
            this.txtbQueQuan.Size = new System.Drawing.Size(151, 20);
            this.txtbQueQuan.TabIndex = 9;
            // 
            // dtpNgaySinh
            // 
            this.dtpNgaySinh.CustomFormat = "dd/MM/yyyy";
            this.dtpNgaySinh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh.Location = new System.Drawing.Point(77, 47);
            this.dtpNgaySinh.Name = "dtpNgaySinh";
            this.dtpNgaySinh.Size = new System.Drawing.Size(151, 20);
            this.dtpNgaySinh.TabIndex = 2;
            // 
            // grbKhaiSinh
            // 
            this.grbKhaiSinh.Controls.Add(this.groupBox4);
            this.grbKhaiSinh.Controls.Add(this.label23);
            this.grbKhaiSinh.Controls.Add(this.label21);
            this.grbKhaiSinh.Controls.Add(this.label22);
            this.grbKhaiSinh.Controls.Add(this.txtbNoiSinh);
            this.grbKhaiSinh.Controls.Add(this.txtbSoDinhDanh);
            this.grbKhaiSinh.Controls.Add(this.txtbHoTen);
            this.grbKhaiSinh.Controls.Add(this.label1);
            this.grbKhaiSinh.Controls.Add(this.label2);
            this.grbKhaiSinh.Controls.Add(this.label3);
            this.grbKhaiSinh.Controls.Add(this.label4);
            this.grbKhaiSinh.Controls.Add(this.label5);
            this.grbKhaiSinh.Controls.Add(this.txtbDanToc);
            this.grbKhaiSinh.Controls.Add(this.txtbQuocTich);
            this.grbKhaiSinh.Controls.Add(this.txtbQueQuan);
            this.grbKhaiSinh.Controls.Add(this.dtpNgaySinh);
            this.grbKhaiSinh.Location = new System.Drawing.Point(35, 38);
            this.grbKhaiSinh.Name = "grbKhaiSinh";
            this.grbKhaiSinh.Size = new System.Drawing.Size(234, 231);
            this.grbKhaiSinh.TabIndex = 0;
            this.grbKhaiSinh.TabStop = false;
            this.grbKhaiSinh.Text = "Thông tin khai sinh";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rdNu);
            this.groupBox4.Controls.Add(this.rdNam);
            this.groupBox4.Location = new System.Drawing.Point(77, 73);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(151, 20);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // rdNu
            // 
            this.rdNu.AutoSize = true;
            this.rdNu.Location = new System.Drawing.Point(93, 2);
            this.rdNu.Name = "rdNu";
            this.rdNu.Size = new System.Drawing.Size(39, 17);
            this.rdNu.TabIndex = 4;
            this.rdNu.TabStop = true;
            this.rdNu.Text = "Nữ";
            this.rdNu.UseVisualStyleBackColor = true;
            // 
            // rdNam
            // 
            this.rdNam.AutoSize = true;
            this.rdNam.Location = new System.Drawing.Point(7, 2);
            this.rdNam.Name = "rdNam";
            this.rdNam.Size = new System.Drawing.Size(47, 17);
            this.rdNam.TabIndex = 3;
            this.rdNam.TabStop = true;
            this.rdNam.Text = "Nam";
            this.rdNam.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(4, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 13);
            this.label23.TabIndex = 26;
            this.label23.Text = "Giới tính";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 127);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "Nơi sinh";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(4, 152);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 13);
            this.label22.TabIndex = 23;
            this.label22.Text = "Số định danh";
            // 
            // txtbNoiSinh
            // 
            this.txtbNoiSinh.Location = new System.Drawing.Point(77, 124);
            this.txtbNoiSinh.Name = "txtbNoiSinh";
            this.txtbNoiSinh.Size = new System.Drawing.Size(151, 20);
            this.txtbNoiSinh.TabIndex = 6;
            // 
            // txtbSoDinhDanh
            // 
            this.txtbSoDinhDanh.Location = new System.Drawing.Point(77, 149);
            this.txtbSoDinhDanh.Name = "txtbSoDinhDanh";
            this.txtbSoDinhDanh.Size = new System.Drawing.Size(151, 20);
            this.txtbSoDinhDanh.TabIndex = 7;
            // 
            // grbCha
            // 
            this.grbCha.Controls.Add(this.txtbCMND_Cha);
            this.grbCha.Controls.Add(this.label16);
            this.grbCha.Controls.Add(this.dtpNgaySinh_Cha);
            this.grbCha.Controls.Add(this.txtbQueQuan_Cha);
            this.grbCha.Controls.Add(this.txtbQuocTich_Cha);
            this.grbCha.Controls.Add(this.txtbDanToc_Cha);
            this.grbCha.Controls.Add(this.txtbHoTen_Cha);
            this.grbCha.Controls.Add(this.label6);
            this.grbCha.Controls.Add(this.label7);
            this.grbCha.Controls.Add(this.label8);
            this.grbCha.Controls.Add(this.label9);
            this.grbCha.Controls.Add(this.label10);
            this.grbCha.Controls.Add(this.btn_ChooseCha);
            this.grbCha.Location = new System.Drawing.Point(285, 36);
            this.grbCha.Name = "grbCha";
            this.grbCha.Size = new System.Drawing.Size(234, 206);
            this.grbCha.TabIndex = 1;
            this.grbCha.TabStop = false;
            this.grbCha.Text = "Thông tin người cha";
            // 
            // txtbCMND_Cha
            // 
            this.txtbCMND_Cha.Location = new System.Drawing.Point(68, 47);
            this.txtbCMND_Cha.Name = "txtbCMND_Cha";
            this.txtbCMND_Cha.Size = new System.Drawing.Size(151, 20);
            this.txtbCMND_Cha.TabIndex = 12;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 49);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 13);
            this.label16.TabIndex = 45;
            this.label16.Text = "CMND";
            // 
            // dtpNgaySinh_Cha
            // 
            this.dtpNgaySinh_Cha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh_Cha.Location = new System.Drawing.Point(68, 100);
            this.dtpNgaySinh_Cha.Name = "dtpNgaySinh_Cha";
            this.dtpNgaySinh_Cha.Size = new System.Drawing.Size(151, 20);
            this.dtpNgaySinh_Cha.TabIndex = 14;
            // 
            // txtbQueQuan_Cha
            // 
            this.txtbQueQuan_Cha.Location = new System.Drawing.Point(68, 174);
            this.txtbQueQuan_Cha.Name = "txtbQueQuan_Cha";
            this.txtbQueQuan_Cha.Size = new System.Drawing.Size(151, 20);
            this.txtbQueQuan_Cha.TabIndex = 17;
            // 
            // txtbQuocTich_Cha
            // 
            this.txtbQuocTich_Cha.Location = new System.Drawing.Point(68, 149);
            this.txtbQuocTich_Cha.Name = "txtbQuocTich_Cha";
            this.txtbQuocTich_Cha.Size = new System.Drawing.Size(151, 20);
            this.txtbQuocTich_Cha.TabIndex = 16;
            // 
            // txtbDanToc_Cha
            // 
            this.txtbDanToc_Cha.Location = new System.Drawing.Point(68, 125);
            this.txtbDanToc_Cha.Name = "txtbDanToc_Cha";
            this.txtbDanToc_Cha.Size = new System.Drawing.Size(151, 20);
            this.txtbDanToc_Cha.TabIndex = 15;
            // 
            // txtbHoTen_Cha
            // 
            this.txtbHoTen_Cha.Location = new System.Drawing.Point(68, 74);
            this.txtbHoTen_Cha.Name = "txtbHoTen_Cha";
            this.txtbHoTen_Cha.Size = new System.Drawing.Size(151, 20);
            this.txtbHoTen_Cha.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Quê quán";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Quốc tịch";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "Dân tộc";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "Ngày sinh";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "Họ tên";
            // 
            // btn_ChooseCha
            // 
            this.btn_ChooseCha.Location = new System.Drawing.Point(47, 19);
            this.btn_ChooseCha.Name = "btn_ChooseCha";
            this.btn_ChooseCha.Size = new System.Drawing.Size(138, 23);
            this.btn_ChooseCha.TabIndex = 11;
            this.btn_ChooseCha.Text = "Chọn thông tin người cha";
            this.btn_ChooseCha.UseVisualStyleBackColor = true;
            this.btn_ChooseCha.Click += new System.EventHandler(this.btn_ChooseCha_Click);
            // 
            // grbMe
            // 
            this.grbMe.Controls.Add(this.txtbCMND_Me);
            this.grbMe.Controls.Add(this.label11);
            this.grbMe.Controls.Add(this.dtpNgaySinh_Me);
            this.grbMe.Controls.Add(this.txtbQueQuan_Me);
            this.grbMe.Controls.Add(this.txtbQuocTich_Me);
            this.grbMe.Controls.Add(this.txtbDanToc_Me);
            this.grbMe.Controls.Add(this.txtbHoTen_Me);
            this.grbMe.Controls.Add(this.label12);
            this.grbMe.Controls.Add(this.label13);
            this.grbMe.Controls.Add(this.label14);
            this.grbMe.Controls.Add(this.label15);
            this.grbMe.Controls.Add(this.label17);
            this.grbMe.Controls.Add(this.btn_ChooseMe);
            this.grbMe.Location = new System.Drawing.Point(542, 36);
            this.grbMe.Name = "grbMe";
            this.grbMe.Size = new System.Drawing.Size(234, 206);
            this.grbMe.TabIndex = 2;
            this.grbMe.TabStop = false;
            this.grbMe.Text = "Thông tin người mẹ";
            // 
            // txtbCMND_Me
            // 
            this.txtbCMND_Me.Location = new System.Drawing.Point(68, 47);
            this.txtbCMND_Me.Name = "txtbCMND_Me";
            this.txtbCMND_Me.Size = new System.Drawing.Size(151, 20);
            this.txtbCMND_Me.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(11, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 45;
            this.label11.Text = "CMND";
            // 
            // dtpNgaySinh_Me
            // 
            this.dtpNgaySinh_Me.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh_Me.Location = new System.Drawing.Point(68, 100);
            this.dtpNgaySinh_Me.Name = "dtpNgaySinh_Me";
            this.dtpNgaySinh_Me.Size = new System.Drawing.Size(151, 20);
            this.dtpNgaySinh_Me.TabIndex = 20;
            // 
            // txtbQueQuan_Me
            // 
            this.txtbQueQuan_Me.Location = new System.Drawing.Point(68, 174);
            this.txtbQueQuan_Me.Name = "txtbQueQuan_Me";
            this.txtbQueQuan_Me.Size = new System.Drawing.Size(151, 20);
            this.txtbQueQuan_Me.TabIndex = 23;
            // 
            // txtbQuocTich_Me
            // 
            this.txtbQuocTich_Me.Location = new System.Drawing.Point(68, 149);
            this.txtbQuocTich_Me.Name = "txtbQuocTich_Me";
            this.txtbQuocTich_Me.Size = new System.Drawing.Size(151, 20);
            this.txtbQuocTich_Me.TabIndex = 22;
            // 
            // txtbDanToc_Me
            // 
            this.txtbDanToc_Me.Location = new System.Drawing.Point(68, 125);
            this.txtbDanToc_Me.Name = "txtbDanToc_Me";
            this.txtbDanToc_Me.Size = new System.Drawing.Size(151, 20);
            this.txtbDanToc_Me.TabIndex = 21;
            // 
            // txtbHoTen_Me
            // 
            this.txtbHoTen_Me.Location = new System.Drawing.Point(68, 74);
            this.txtbHoTen_Me.Name = "txtbHoTen_Me";
            this.txtbHoTen_Me.Size = new System.Drawing.Size(151, 20);
            this.txtbHoTen_Me.TabIndex = 19;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 176);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 39;
            this.label12.Text = "Quê quán";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 151);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 38;
            this.label13.Text = "Quốc tịch";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 127);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 37;
            this.label14.Text = "Dân tộc";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 102);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 13);
            this.label15.TabIndex = 36;
            this.label15.Text = "Ngày sinh";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(10, 76);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 13);
            this.label17.TabIndex = 35;
            this.label17.Text = "Họ tên";
            // 
            // btn_ChooseMe
            // 
            this.btn_ChooseMe.Location = new System.Drawing.Point(47, 19);
            this.btn_ChooseMe.Name = "btn_ChooseMe";
            this.btn_ChooseMe.Size = new System.Drawing.Size(138, 23);
            this.btn_ChooseMe.TabIndex = 12;
            this.btn_ChooseMe.Text = "Chọn thông tin người mẹ";
            this.btn_ChooseMe.UseVisualStyleBackColor = true;
            this.btn_ChooseMe.Click += new System.EventHandler(this.btn_ChooseMe_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(270, 328);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(75, 23);
            this.btnThem.TabIndex = 5;
            this.btnThem.Text = "Thêm hồ sơ";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnXacNhan
            // 
            this.btnXacNhan.Location = new System.Drawing.Point(351, 328);
            this.btnXacNhan.Name = "btnXacNhan";
            this.btnXacNhan.Size = new System.Drawing.Size(97, 23);
            this.btnXacNhan.TabIndex = 6;
            this.btnXacNhan.Text = "Xác nhận hồ sơ";
            this.btnXacNhan.UseVisualStyleBackColor = true;
            this.btnXacNhan.Click += new System.EventHandler(this.btnXacNhan_Click);
            // 
            // btnDuyet
            // 
            this.btnDuyet.Location = new System.Drawing.Point(454, 328);
            this.btnDuyet.Name = "btnDuyet";
            this.btnDuyet.Size = new System.Drawing.Size(97, 23);
            this.btnDuyet.TabIndex = 7;
            this.btnDuyet.Text = "Duyệt hồ sơ";
            this.btnDuyet.UseVisualStyleBackColor = true;
            this.btnDuyet.Click += new System.EventHandler(this.btnDuyet_Click);
            // 
            // btnTraKQ
            // 
            this.btnTraKQ.Location = new System.Drawing.Point(557, 328);
            this.btnTraKQ.Name = "btnTraKQ";
            this.btnTraKQ.Size = new System.Drawing.Size(97, 23);
            this.btnTraKQ.TabIndex = 8;
            this.btnTraKQ.Text = "Trả kết quả";
            this.btnTraKQ.UseVisualStyleBackColor = true;
            this.btnTraKQ.Click += new System.EventHandler(this.btnTraKQ_Click);
            // 
            // dgrv_KhaiSinh
            // 
            this.dgrv_KhaiSinh.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrv_KhaiSinh.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaKhaiSinh,
            this.HoTen,
            this.NgaySinh,
            this.GioiTinh,
            this.DanToc,
            this.NoiSinh,
            this.SoDinhDanhCaNhan,
            this.QuocTich,
            this.QueQuan,
            this.HoTenCha,
            this.HoTenMe,
            this.TrangThai_MoTa,
            this.IDCha,
            this.NgayHen,
            this.NgayTra,
            this.IDMe,
            this.TrangThai});
            this.dgrv_KhaiSinh.Location = new System.Drawing.Point(12, 367);
            this.dgrv_KhaiSinh.Name = "dgrv_KhaiSinh";
            this.dgrv_KhaiSinh.Size = new System.Drawing.Size(782, 241);
            this.dgrv_KhaiSinh.TabIndex = 53;
            this.dgrv_KhaiSinh.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrv_KhaiSinh_CellClick);
            // 
            // MaKhaiSinh
            // 
            this.MaKhaiSinh.HeaderText = "ID";
            this.MaKhaiSinh.MinimumWidth = 6;
            this.MaKhaiSinh.Name = "MaKhaiSinh";
            this.MaKhaiSinh.Width = 125;
            // 
            // HoTen
            // 
            this.HoTen.HeaderText = "Họ tên";
            this.HoTen.MinimumWidth = 6;
            this.HoTen.Name = "HoTen";
            this.HoTen.Width = 125;
            // 
            // NgaySinh
            // 
            this.NgaySinh.HeaderText = "Ngày sinh";
            this.NgaySinh.MinimumWidth = 6;
            this.NgaySinh.Name = "NgaySinh";
            this.NgaySinh.Width = 125;
            // 
            // GioiTinh
            // 
            this.GioiTinh.HeaderText = "Giới tính";
            this.GioiTinh.MinimumWidth = 6;
            this.GioiTinh.Name = "GioiTinh";
            this.GioiTinh.Width = 125;
            // 
            // DanToc
            // 
            this.DanToc.HeaderText = "Dân tộc";
            this.DanToc.MinimumWidth = 6;
            this.DanToc.Name = "DanToc";
            this.DanToc.Width = 125;
            // 
            // NoiSinh
            // 
            this.NoiSinh.HeaderText = "Nơi sinh";
            this.NoiSinh.MinimumWidth = 6;
            this.NoiSinh.Name = "NoiSinh";
            this.NoiSinh.Width = 125;
            // 
            // SoDinhDanhCaNhan
            // 
            this.SoDinhDanhCaNhan.HeaderText = "Số định danh";
            this.SoDinhDanhCaNhan.MinimumWidth = 6;
            this.SoDinhDanhCaNhan.Name = "SoDinhDanhCaNhan";
            this.SoDinhDanhCaNhan.Width = 125;
            // 
            // QuocTich
            // 
            this.QuocTich.HeaderText = "Quốc tịch";
            this.QuocTich.MinimumWidth = 6;
            this.QuocTich.Name = "QuocTich";
            this.QuocTich.Width = 125;
            // 
            // QueQuan
            // 
            this.QueQuan.HeaderText = "Quê quán";
            this.QueQuan.MinimumWidth = 6;
            this.QueQuan.Name = "QueQuan";
            this.QueQuan.Width = 125;
            // 
            // HoTenCha
            // 
            this.HoTenCha.HeaderText = "Họ tên cha";
            this.HoTenCha.MinimumWidth = 6;
            this.HoTenCha.Name = "HoTenCha";
            this.HoTenCha.Width = 125;
            // 
            // HoTenMe
            // 
            this.HoTenMe.HeaderText = "Họ tên mẹ";
            this.HoTenMe.MinimumWidth = 6;
            this.HoTenMe.Name = "HoTenMe";
            this.HoTenMe.Width = 125;
            // 
            // TrangThai_MoTa
            // 
            this.TrangThai_MoTa.HeaderText = "Trạng thái";
            this.TrangThai_MoTa.MinimumWidth = 6;
            this.TrangThai_MoTa.Name = "TrangThai_MoTa";
            this.TrangThai_MoTa.Width = 125;
            // 
            // IDCha
            // 
            this.IDCha.HeaderText = "IDCha";
            this.IDCha.MinimumWidth = 6;
            this.IDCha.Name = "IDCha";
            this.IDCha.Visible = false;
            this.IDCha.Width = 125;
            // 
            // NgayHen
            // 
            this.NgayHen.HeaderText = "Ngày hẹn trả";
            this.NgayHen.MinimumWidth = 6;
            this.NgayHen.Name = "NgayHen";
            this.NgayHen.Width = 125;
            // 
            // NgayTra
            // 
            this.NgayTra.HeaderText = "Ngày trả";
            this.NgayTra.MinimumWidth = 6;
            this.NgayTra.Name = "NgayTra";
            this.NgayTra.Width = 125;
            // 
            // IDMe
            // 
            this.IDMe.HeaderText = "IDMe";
            this.IDMe.MinimumWidth = 6;
            this.IDMe.Name = "IDMe";
            this.IDMe.Visible = false;
            this.IDMe.Width = 125;
            // 
            // TrangThai
            // 
            this.TrangThai.HeaderText = "Mã trậng thái";
            this.TrangThai.MinimumWidth = 6;
            this.TrangThai.Name = "TrangThai";
            this.TrangThai.Visible = false;
            this.TrangThai.Width = 125;
            // 
            // cbTrangThai
            // 
            this.cbTrangThai.FormattingEnabled = true;
            this.cbTrangThai.Items.AddRange(new object[] {
            "Tất cả",
            "Tiếp nhận, chờ xác nhận",
            "Đã xác minh, chờ duyệt",
            "Không xác minh",
            "Đã duyệt",
            "Không duyệt",
            "Đã trả",
            "Đã thanh toán"});
            this.cbTrangThai.Location = new System.Drawing.Point(415, 292);
            this.cbTrangThai.Name = "cbTrangThai";
            this.cbTrangThai.Size = new System.Drawing.Size(152, 21);
            this.cbTrangThai.TabIndex = 54;
            this.cbTrangThai.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(357, 295);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Trạng thái";
            // 
            // lblTTTHS
            // 
            this.lblTTTHS.AutoSize = true;
            this.lblTTTHS.Location = new System.Drawing.Point(9, 9);
            this.lblTTTHS.Name = "lblTTTHS";
            this.lblTTTHS.Size = new System.Drawing.Size(90, 13);
            this.lblTTTHS.TabIndex = 55;
            this.lblTTTHS.Text = "Trạng thái hồ sơ: ";
            this.lblTTTHS.Visible = false;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Location = new System.Drawing.Point(105, 9);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(41, 13);
            this.lblTrangThai.TabIndex = 56;
            this.lblTrangThai.Text = "label20";
            this.lblTrangThai.Visible = false;
            // 
            // btn_TimKiem
            // 
            this.btn_TimKiem.Location = new System.Drawing.Point(573, 291);
            this.btn_TimKiem.Name = "btn_TimKiem";
            this.btn_TimKiem.Size = new System.Drawing.Size(75, 23);
            this.btn_TimKiem.TabIndex = 4;
            this.btn_TimKiem.Text = "Tìm kiếm";
            this.btn_TimKiem.UseVisualStyleBackColor = true;
            this.btn_TimKiem.Click += new System.EventHandler(this.btn_TimKiem_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Location = new System.Drawing.Point(677, 290);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(95, 23);
            this.btnHuy.TabIndex = 9;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // btnBackToMenu
            // 
            this.btnBackToMenu.Location = new System.Drawing.Point(677, 319);
            this.btnBackToMenu.Name = "btnBackToMenu";
            this.btnBackToMenu.Size = new System.Drawing.Size(95, 23);
            this.btnBackToMenu.TabIndex = 10;
            this.btnBackToMenu.Text = "Quay về Menu";
            this.btnBackToMenu.UseVisualStyleBackColor = true;
            this.btnBackToMenu.Click += new System.EventHandler(this.btnBackToMenu_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(36, 335);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "Ngày hẹn trả";
            // 
            // dtpHenTra
            // 
            this.dtpHenTra.CustomFormat = "dd/MM/yyyy";
            this.dtpHenTra.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHenTra.Location = new System.Drawing.Point(112, 331);
            this.dtpHenTra.Name = "dtpHenTra";
            this.dtpHenTra.Size = new System.Drawing.Size(117, 20);
            this.dtpHenTra.TabIndex = 60;
            // 
            // btnChonCongDan
            // 
            this.btnChonCongDan.Location = new System.Drawing.Point(35, 275);
            this.btnChonCongDan.Name = "btnChonCongDan";
            this.btnChonCongDan.Size = new System.Drawing.Size(64, 39);
            this.btnChonCongDan.TabIndex = 3;
            this.btnChonCongDan.Text = "Chọn công dân";
            this.btnChonCongDan.UseVisualStyleBackColor = true;
            this.btnChonCongDan.Click += new System.EventHandler(this.btnChonCongDan_Click);
            // 
            // lblNguoiYeuCau
            // 
            this.lblNguoiYeuCau.AutoSize = true;
            this.lblNguoiYeuCau.Location = new System.Drawing.Point(113, 288);
            this.lblNguoiYeuCau.Name = "lblNguoiYeuCau";
            this.lblNguoiYeuCau.Size = new System.Drawing.Size(79, 13);
            this.lblNguoiYeuCau.TabIndex = 62;
            this.lblNguoiYeuCau.Text = "Người yêu cầu:";
            // 
            // lblHoTenNguoiYeuCau
            // 
            this.lblHoTenNguoiYeuCau.AutoSize = true;
            this.lblHoTenNguoiYeuCau.Location = new System.Drawing.Point(187, 288);
            this.lblHoTenNguoiYeuCau.Name = "lblHoTenNguoiYeuCau";
            this.lblHoTenNguoiYeuCau.Size = new System.Drawing.Size(76, 13);
            this.lblHoTenNguoiYeuCau.TabIndex = 63;
            this.lblHoTenNguoiYeuCau.Text = "Nguyễn Văn A";
            // 
            // FormQLKhaiSinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 620);
            this.ControlBox = false;
            this.Controls.Add(this.lblHoTenNguoiYeuCau);
            this.Controls.Add(this.lblNguoiYeuCau);
            this.Controls.Add(this.btnChonCongDan);
            this.Controls.Add(this.dtpHenTra);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.btnBackToMenu);
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.btn_TimKiem);
            this.Controls.Add(this.lblTrangThai);
            this.Controls.Add(this.lblTTTHS);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.cbTrangThai);
            this.Controls.Add(this.dgrv_KhaiSinh);
            this.Controls.Add(this.btnTraKQ);
            this.Controls.Add(this.btnDuyet);
            this.Controls.Add(this.btnXacNhan);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.grbMe);
            this.Controls.Add(this.grbCha);
            this.Controls.Add(this.grbKhaiSinh);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormQLKhaiSinh";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng ký khai sinh";
            this.Load += new System.EventHandler(this.FormQLKhaiSinh_Load);
            this.Shown += new System.EventHandler(this.FormQLKhaiSinh_Shown);
            this.grbKhaiSinh.ResumeLayout(false);
            this.grbKhaiSinh.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.grbCha.ResumeLayout(false);
            this.grbCha.PerformLayout();
            this.grbMe.ResumeLayout(false);
            this.grbMe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrv_KhaiSinh)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbHoTen;
        private System.Windows.Forms.TextBox txtbDanToc;
        private System.Windows.Forms.TextBox txtbQuocTich;
        private System.Windows.Forms.TextBox txtbQueQuan;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh;
        private System.Windows.Forms.GroupBox grbKhaiSinh;
        private System.Windows.Forms.GroupBox grbCha;
        private System.Windows.Forms.TextBox txtbCMND_Cha;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh_Cha;
        private System.Windows.Forms.TextBox txtbQueQuan_Cha;
        private System.Windows.Forms.TextBox txtbQuocTich_Cha;
        private System.Windows.Forms.TextBox txtbDanToc_Cha;
        private System.Windows.Forms.TextBox txtbHoTen_Cha;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_ChooseCha;
        private System.Windows.Forms.GroupBox grbMe;
        private System.Windows.Forms.TextBox txtbCMND_Me;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh_Me;
        private System.Windows.Forms.TextBox txtbQueQuan_Me;
        private System.Windows.Forms.TextBox txtbQuocTich_Me;
        private System.Windows.Forms.TextBox txtbDanToc_Me;
        private System.Windows.Forms.TextBox txtbHoTen_Me;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btn_ChooseMe;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnXacNhan;
        private System.Windows.Forms.Button btnDuyet;
        private System.Windows.Forms.Button btnTraKQ;
        private System.Windows.Forms.DataGridView dgrv_KhaiSinh;
        private System.Windows.Forms.ComboBox cbTrangThai;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblTTTHS;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Button btn_TimKiem;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Button btnBackToMenu;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtbNoiSinh;
        private System.Windows.Forms.TextBox txtbSoDinhDanh;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rdNu;
        private System.Windows.Forms.RadioButton rdNam;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dtpHenTra;
        private System.Windows.Forms.Button btnChonCongDan;
        private System.Windows.Forms.Label lblNguoiYeuCau;
        private System.Windows.Forms.Label lblHoTenNguoiYeuCau;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaKhaiSinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTen;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgaySinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn GioiTinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn DanToc;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoiSinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoDinhDanhCaNhan;
        private System.Windows.Forms.DataGridViewTextBoxColumn QuocTich;
        private System.Windows.Forms.DataGridViewTextBoxColumn QueQuan;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTenCha;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTenMe;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai_MoTa;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDCha;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayHen;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayTra;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDMe;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai;
    }
}