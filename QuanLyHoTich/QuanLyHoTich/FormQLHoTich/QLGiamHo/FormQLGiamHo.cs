﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLHT.Entity;
using QLHT.Model;
using QLHT.Model.Dictionary;
using QuanLyHoTich.FormQLHoTich.QLGiamHo;

namespace QuanLyHoTich.FormQLHoTich
{
    public partial class FormQLGiamHo : Form
    {
        public int maCongDan1, maCongDan2, maCongDan3,maGiamHoSua;
       
        public static FormQLGiamHo instance;
        public static FormQLGiamHo Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormQLGiamHo();
                }
                return instance;
            }
        }
        public FormQLGiamHo()
        {
            InitializeComponent();
            GetQuyen();
            LoadDuLieu();
        }

        
        private void LoadDuLieu()
        {
            dataGridView1.Rows.Clear();
            List<GiamHo> dsGiamHo = giam.GetDataKSGiamHo();
            foreach (GiamHo giamho in dsGiamHo)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["MaGH"].Value = giamho.MaGH;
                dataGridView1.Rows[index].Cells["t1"].Value = giamho.IDNguoiGiamHo;
                dataGridView1.Rows[index].Cells["NgayDangKy"].Value = giamho.NgayDangKy.ToString("dd/MM/yyyy");
                dataGridView1.Rows[index].Cells["NgayHen"].Value = giamho.NgayHen.Value.ToString("dd/MM/yyyy");
                dataGridView1.Rows[index].Cells["t2"].Value = giamho.IDNguoiDuocGiamHo;
                dataGridView1.Rows[index].Cells["TrangThai"].Value = giamho.TrangThai==0?"Tiếp nhận":giamho.TrangThai==1?"Đã xác minh":giamho.TrangThai==2?"Xác minh thất bại":
                    giamho.TrangThai==3?"Đã duyệt":giamho.TrangThai==4?"Duyệt thất bại":giamho.TrangThai==5?"Xong":"Đã thanh toán";


                CongDan congDanDGH = giam.GetCongDanByMaCongDan((giamho.IDNguoiDuocGiamHo));
                CongDan congDanGH = giam.GetCongDanByMaCongDan((giamho.IDNguoiGiamHo));
                dataGridView1.Rows[index].Cells["IDNguoiDuocGiamHo"].Value = congDanDGH.HoTen;
                dataGridView1.Rows[index].Cells["IDNguoiGiamHo"].Value = congDanGH.HoTen;
            }
        }

        private void FormQLGiamHo_Load(object sender, EventArgs e)
        {

        }
        public CongDan congDanGiamHo = new CongDan();
        public bool isChooseCongDanGiamHo1 = false;
        public bool isChooseCongDanGiamHo3 = false;
        public bool isChooseCongDanGiamHo = false;

        private void button3_Click(object sender, EventArgs e)
        {
          
            hienThiCHonCongDan();
        }

        public void hienThiCHonCongDan()
        {
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.AllGioiTinh = true;
            FormQLCongDan.Instance.isContainKhaiSinh = false;
            FormQLCongDan.Instance.isContainAdult = true;
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                isChooseCongDanGiamHo = true;
                MessageBox.Show("Chọn thành công");
                congDanGiamHo = FormQLCongDan.Instance.congDan;

                textBox15.Text = String.IsNullOrEmpty(congDanGiamHo.CMND) ? "" : congDanGiamHo.CMND;
                textBox14.Text = String.IsNullOrEmpty(congDanGiamHo.HoTen) ? "" : congDanGiamHo.HoTen;
                dateTimePicker3.Value = congDanGiamHo.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(congDanGiamHo.NgaySinh);
                textBox13.Text = String.IsNullOrEmpty(congDanGiamHo.DanToc) ? "" : congDanGiamHo.DanToc;
                textBox12.Text = String.IsNullOrEmpty(congDanGiamHo.QuocTich) ? "" : congDanGiamHo.QuocTich;
                textBox11.Text = String.IsNullOrEmpty(congDanGiamHo.QueQuan) ? "" : congDanGiamHo.QueQuan;

                maCongDan2 = congDanGiamHo.MaCongDan;
            }
            FormQLCongDan.Instance.Close();
        }

        bool isChooseCongDanCha = false;
        
        public KhaiSinh khai = new KhaiSinh();

        private void button1_Click(object sender, EventArgs e)
        {
            
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.AllGioiTinh = true;
            FormQLCongDan.Instance.isContainKhaiSinh = true;
            FormQLCongDan.Instance.isContainAdult = false;
            FormQLCongDan.Instance.ShowDialog();
            
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                isChooseCongDanGiamHo1 = true;
                MessageBox.Show("Chọn thành công");
                congDanGiamHo = FormQLCongDan.Instance.congDan;
                textBox1.Text = String.IsNullOrEmpty(congDanGiamHo.CMND) ? "" : congDanGiamHo.CMND;
                textBox2.Text = String.IsNullOrEmpty(congDanGiamHo.HoTen) ? "" : congDanGiamHo.HoTen;
                dateTimePicker1.Value = congDanGiamHo.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(congDanGiamHo.NgaySinh);
                textBox3.Text = String.IsNullOrEmpty(congDanGiamHo.DanToc) ? "" : congDanGiamHo.DanToc;
                textBox4.Text = String.IsNullOrEmpty(congDanGiamHo.QuocTich) ? "" : congDanGiamHo.QuocTich;
                textBox5.Text = String.IsNullOrEmpty(congDanGiamHo.QueQuan) ? "" : congDanGiamHo.QueQuan;
                maCongDan1 =  congDanGiamHo.MaCongDan;
            }
            FormQLCongDan.Instance.Close();

            //FormListKS.Instance.isChonGiamHo = true;

            //FormListKS.Instance.ShowDialog();
            //if (FormListKS.Instance.chonThanhCong)
            //{
            //    isChooseCongDanCha = true;
            //    MessageBox.Show("Chọn thành công");
            //    khai = FormListKS.Instance.khaisinh;


            //    textBox2.Text = String.IsNullOrEmpty(khai.HoTen) ? "" : khai.HoTen;
            //    dateTimePicker1.Value = khai.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(khai.NgaySinh);
            //    textBox3.Text = String.IsNullOrEmpty(khai.DanToc) ? "" : khai.DanToc;
            //    textBox4.Text = String.IsNullOrEmpty(khai.QuocTich) ? "" : khai.QuocTich;
            //    textBox5.Text = String.IsNullOrEmpty(khai.QueQuan) ? "" : khai.QueQuan;
            //}
            //FormListKS.Instance.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormQLCongDan.Instance.isContainKhaiSinh = false;
            FormQLCongDan.Instance.isContainAdult = false;
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.AllGioiTinh = true;
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                isChooseCongDanGiamHo3 = true;
                MessageBox.Show("Chọn thành công");
                congDanGiamHo = FormQLCongDan.Instance.congDan;

                label12.Text = String.IsNullOrEmpty((congDanGiamHo.MaCongDan)+"") ? "" : (""+congDanGiamHo.MaCongDan);
                label9.Text = String.IsNullOrEmpty(congDanGiamHo.HoTen) ? "" : congDanGiamHo.HoTen;
                maCongDan3 = congDanGiamHo.MaCongDan;
            }
            FormQLCongDan.Instance.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FormMenu.Instance.Show();
            FormQLGiamHo.Instance.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }
        String Quyen = "";
        void SetFalseAllButton()
        {
            button7.Enabled = false;
            button8.Enabled = false;
            button9.Enabled = false;
            button10.Enabled = false;
        }

        public void GetQuyen()
        {
            Quyen = new CongChucModel().GetQuyen(Constants.MaCongChucDangNhap);
            //MessageBox.Show(Quyen);
            SetFalseAllButton();
            //Ktra 5 quyền
            

            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanTiepNhanVaTraKQ))
            {
               
                Constants.isTN_TKQ = true;
                button7.Enabled = true;
               
                
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatCongChucCapXa))
            {

                Constants.isCCCX = true;
                
                  button8.Enabled = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatLanhDao))
            {
                 button9.Enabled = true;
              
                Constants.isLD = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanVanPhong))
            {
                
                 button10.Enabled = true;
                Constants.isVP = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanKTTC))
            {
               
                Constants.isKTTC = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
           // MessageBox.Show(int.Parse(Constants.MaCongChucDangNhap) + "");
        }

        GiamHoModel giam =new GiamHoModel();

        private void button8_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn chắc chắn xác minh thông tin hồ sơ giám hộ này chứ?", "Xác nhận", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                
                
                    if (giam.UpdateTrangThai(maGiamHoSua, 1))
                    {
                        MessageBox.Show("Xác minh thành công!");

                        LoadDuLieu();
                    }
                    else
                    {
                        MessageBox.Show("Thao tác thất bại!");
                    }
                
                
            }
            else
            {
                if (giam.UpdateTrangThai(maGiamHoSua, 2))
                {
                    MessageBox.Show("Xác minh thất bại !");

                    LoadDuLieu();
                }
            }
               
            
          
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn chắc chắn duyệt thông tin hồ sơ giám hộ này chứ?", "Xác nhận", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {


                if (giam.UpdateTrangThaiDuyet(maGiamHoSua, int.Parse(Constants.MaCongChucDangNhap), 3))
                {
                    MessageBox.Show("Duyệt thành công!");

                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Duyệt thất bại!");
                }


            }
            else
            {
                if (giam.UpdateTrangThaiDuyet(maGiamHoSua, int.Parse(Constants.MaCongChucDangNhap), 4))
                {
                    MessageBox.Show("Duyệt thất bại !");

                    LoadDuLieu();
                }
            }

        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn chắc chắn duyệt thông tin hồ sơ giám hộ này chứ?", "Xác nhận", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {


                if (giam.UpdateTrangThaiTraKQ(maGiamHoSua, int.Parse(Constants.MaCongChucDangNhap), 5))
                {
                    MessageBox.Show("Trả hồ sơ thành công!");

                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Trả hồ sơ thất bại!");
                }


            }
        }

        public int idComBoBox;

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            idComBoBox = comboBox1.SelectedIndex;
            if (idComBoBox == 7)
            {
                LoadDuLieu();
            }
            else
            {
                dataGridView1.Rows.Clear();
                List<GiamHo> dsGiamHo = giam.GetDataKSGiamHoCBox(idComBoBox);
                foreach (GiamHo giamho in dsGiamHo)
                {
                    var index = dataGridView1.Rows.Add();
                    dataGridView1.Rows[index].Cells["MaGH"].Value = giamho.MaGH;
                    dataGridView1.Rows[index].Cells["t1"].Value = giamho.IDNguoiGiamHo;
                    dataGridView1.Rows[index].Cells["NgayDangKy"].Value = giamho.NgayDangKy.ToString("dd/MM/yyyy");
                    dataGridView1.Rows[index].Cells["NgayHen"].Value = giamho.NgayHen.Value.ToString("dd/MM/yyyy");
                    dataGridView1.Rows[index].Cells["t2"].Value = giamho.IDNguoiDuocGiamHo;
                    dataGridView1.Rows[index].Cells["TrangThai"].Value = giamho.TrangThai == 0 ? "Tiếp nhận" : giamho.TrangThai == 1 ? "Đã xác minh" : giamho.TrangThai == 2 ? "Xác minh thất bại" :
                        giamho.TrangThai == 3 ? "Đã duyệt" : giamho.TrangThai == 4 ? "Duyệt thất bại" : giamho.TrangThai == 5 ? "Xong" : "Đã thanh toán";


                    CongDan congDanDGH = giam.GetCongDanByMaCongDan((giamho.IDNguoiDuocGiamHo));
                    CongDan congDanGH = giam.GetCongDanByMaCongDan((giamho.IDNguoiGiamHo));
                    dataGridView1.Rows[index].Cells["IDNguoiDuocGiamHo"].Value = congDanDGH.HoTen;
                    dataGridView1.Rows[index].Cells["IDNguoiGiamHo"].Value = congDanGH.HoTen;
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
               

                int rowIndex = e.RowIndex;
                //MessageBox.Show(rowIndex + "");
                
                if (rowIndex >=0)
                {
                    maGiamHoSua = int.Parse(dataGridView1.Rows[rowIndex].Cells["MaGH"].Value.ToString());
                   // MessageBox.Show(maGiamHoSua + "");
                    //textBox1.Text = dataGridView1.Rows[rowIndex].Cells["IDNguoiDuocGiamHo"].Value.ToString();
                    //textBox12.Text=dataGridView1.Rows[rowIndex].Cells["IDNguoiGiamHo"].Value.ToString();
                    //txtbHoTen.Text = dgrv_CongDan.Rows[rowIndex].Cells["HoTen"].Value.ToString();
                    dateTimePicker2.Value = DateTime.ParseExact(dataGridView1.Rows[rowIndex].Cells["NgayHen"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);

                     
                     CongDan congDanDcGH = giam.GetCongDanByMaCongDan(int.Parse(dataGridView1.Rows[rowIndex].Cells["t2"].Value.ToString()));
                    CongDan congDanGH = giam.GetCongDanByMaCongDan(int.Parse(dataGridView1.Rows[rowIndex].Cells["t1"].Value.ToString()));

                    textBox1.Text = String.IsNullOrEmpty(congDanDcGH.CMND) ? "" : congDanDcGH.CMND;
                   
                    textBox2.Text = String.IsNullOrEmpty(congDanDcGH.HoTen) ? "" : congDanDcGH.HoTen;
                    dateTimePicker1.Value = congDanDcGH.NgaySinh;
                    textBox3.Text = String.IsNullOrEmpty(congDanDcGH.DanToc) ? "" : congDanDcGH.DanToc;
                    textBox4.Text = String.IsNullOrEmpty(congDanDcGH.QuocTich) ? "" : congDanDcGH.QuocTich;
                    textBox5.Text = String.IsNullOrEmpty(congDanDcGH.QueQuan) ? "" : congDanDcGH.QueQuan;

                    textBox15.Text = String.IsNullOrEmpty(congDanGH.CMND) ? "" : congDanGH.CMND;
                    textBox14.Text = String.IsNullOrEmpty(congDanGH.HoTen) ? "" : congDanGH.HoTen;
                    dateTimePicker3.Value = congDanGH.NgaySinh;
                    textBox13.Text = String.IsNullOrEmpty(congDanGH.DanToc) ? "" : congDanGH.DanToc;
                    textBox12.Text = String.IsNullOrEmpty(congDanGH.QuocTich) ? "" : congDanGH.QuocTich;
                    textBox11.Text = String.IsNullOrEmpty(congDanGH.QueQuan) ? "" : congDanGH.QueQuan;
                }
               
            }
            catch (Exception ex)
            {

            }
        }

        CongDanModel CongDanModel = new CongDanModel();
        private void button7_Click(object sender, EventArgs e)
        {
            if (!isChooseCongDanGiamHo || !isChooseCongDanGiamHo1||!isChooseCongDanGiamHo3)
            {
                MessageBox.Show("Mời chọn công dân");
            }
            else
            {

            
            if (MessageBox.Show("Bạn chắc chắn sẽ nhập thông tin hồ sơ giám hộ này chứ?", "Xác nhận", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {


                DateTime NgayHen = dateTimePicker2.Value;

                //if (!isChooseCongDanGiamHo)
                //{
                //    string CMND = "KhaiSinh";
                //    string HoTen = textBox2.Text.Trim();
                //    DateTime NgaySinh = dateTimePicker1.Value;
                //    string DanToc= textBox3.Text.Trim();
                //    string QuocTich = textBox4.Text.Trim();
                //    string QueQuan = textBox5.Text.Trim();
                //    CongDan congDanGHInsert = new CongDan(HoTen, NgaySinh, true, QueQuan, "", "", QuocTich, CMND, 0);



                //    string IDCĐuocGiamHo = CongDanModel.ThemCongDan(congDanGHInsert);
                //}
                //else
                {
                    if (giam.Insert(maCongDan2, maCongDan1, 1, NgayHen, maCongDan3, int.Parse(Constants.MaCongChucDangNhap), 0))
                    {
                        MessageBox.Show("Thêm hồ sơ đăng ký giám hộ thành công");
                            LoadDuLieu();

                    }
                }
            }
            }
            

        }
    }
}
