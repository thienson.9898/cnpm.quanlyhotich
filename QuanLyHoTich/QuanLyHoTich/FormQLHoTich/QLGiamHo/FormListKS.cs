﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLHT.Entity;
using QLHT.Model;

namespace QuanLyHoTich.FormQLHoTich.QLGiamHo
{

    
    public partial class FormListKS : Form
    {
        string dantocc, quoctich, quequan;
        DateTime ngay;
        string id;
        private static FormListKS instance;
        public static FormListKS Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormListKS();
                }
                return instance;
            }
        }
        public FormListKS()
        {
            InitializeComponent();
            showListKS();
        }

        private void showListKS()
        {
           
        }
        KhaiSinhModel KhaiSinhModel = new KhaiSinhModel();

        private void FormListKS_Load(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            List<KhaiSinh> dsGiamHo = KhaiSinhModel.GetDataKSGiamHo();
            foreach (KhaiSinh giamho in dsGiamHo)
            {
                var index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells["Column1"].Value = giamho.MaKhaiSinh;
                dataGridView1.Rows[index].Cells["Column2"].Value = giamho.HoTen;
                dataGridView1.Rows[index].Cells["Column3"].Value = giamho.NgaySinh.ToString("dd/MM/yyyy");
                dataGridView1.Rows[index].Cells["Column4"].Value = giamho.GioiTinh == true ? "Nam" : "Nữ";
                dataGridView1.Rows[index].Cells["Column5"].Value = giamho.DanToc;
                dataGridView1.Rows[index].Cells["Column7"].Value = giamho.QuocTich;
                dataGridView1.Rows[index].Cells["Column8"].Value = giamho.QueQuan;
                
                dataGridView1.Rows[index].Cells["Column10"].Value = giamho.SoDinhDanhCaNhan;

            }
        }

        public KhaiSinh khaisinh;
        public bool chonThanhCong = false;

        private void button2_Click(object sender, EventArgs e)
        {
            
            try
            {
                int.TryParse(id, out int MaCongDan);
                string HoTen = textBox2.Text.Trim();
               
                khaisinh = new KhaiSinh(MaCongDan,HoTen,quequan,quoctich,dantocc,ngay);
                chonThanhCong = true;
                
            }
            catch (Exception ex)
            {
                chonThanhCong = false;
            }
            finally
            {
                this.Hide();
            }
        }
        public bool isChonGiamHo = false;

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!isChonGiamHo)
                {
                    button2.Enabled = false;
                    button3.Enabled = true;
                }
                int rowIndex = e.RowIndex;
                if (rowIndex >= 0)
                {

                    id= dataGridView1.Rows[rowIndex].Cells["Column1"].Value.ToString();
                    textBox2.Text = dataGridView1.Rows[rowIndex].Cells["Column2"].Value.ToString();
                    button2.Enabled = true;
                    ngay = DateTime.ParseExact(dataGridView1.Rows[rowIndex].Cells["Column3"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                    
                    quequan = dataGridView1.Rows[rowIndex].Cells["Column8"].Value.ToString();
                   
                    dantocc = dataGridView1.Rows[rowIndex].Cells["Column5"].Value.ToString();
                    quoctich = dataGridView1.Rows[rowIndex].Cells["Column7"].Value.ToString();
                    
                }
            }
            catch
            {

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
