﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLHT.Model;
using QLHT.Entity;
using System.Diagnostics;
using QLHT.Model.Dictionary;

namespace QuanLyHoTich
{
    public partial class FormQLKhaiSinh : Form
    {
        private static FormQLKhaiSinh instance;
        public static FormQLKhaiSinh Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormQLKhaiSinh();
                }
                return instance;
            }
        }
        public FormQLKhaiSinh()
        {
            InitializeComponent();
            GetQuyen();
            Event_setFalseChonCongDan();
            dtpNgaySinh.CustomFormat = "dd/MM/yyyy";
            dtpNgaySinh_Cha.CustomFormat = "dd/MM/yyyy";
            dtpNgaySinh_Me.CustomFormat = "dd/MM/yyyy";
            LoadDuLieu();
            LoadDDLTrangThai();
        }

        private void LoadDDLTrangThai()
        {
            cbTrangThai.DataSource = TrangThaiModel.dsTrangThai();
            cbTrangThai.ValueMember = "MaTrangThai";
            cbTrangThai.DisplayMember = "MoTaTrangThai";
        }

        String Quyen = "";
        CongDanModel CongDanModel = new CongDanModel();
        KhaiSinhModel KhaiSinhModel = new KhaiSinhModel();
        TrangThaiModel TrangThaiModel = new TrangThaiModel();
        bool isChooseCongDanCha = false;
        bool isChooseCongDanMe = false;
        string MaCongDanYeuCau = "-1";
        KhaiSinh currentKhaiSinh = new KhaiSinh();
        int selectedRowIndex = -1;
        ThuTucModel ThuTucModel = new ThuTucModel();
        public void GetQuyen()
        {
            Quyen = new CongChucModel().GetQuyen(Constants.MaCongChucDangNhap);
            SetFalseAllButton();
            //Ktra 5 quyền
            Debug.WriteLine("Send to debug output.");

            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanTiepNhanVaTraKQ))
            {
                Debug.WriteLine("isTN_TKQ");
                Constants.isTN_TKQ = true;
                btnThem.Enabled = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatCongChucCapXa))
            {
                
                Constants.isCCCX = true;
                Debug.WriteLine("isCCCX");
                //  btnXacNhan.Enabled = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatLanhDao))
            {
                // btnDuyet.Enabled = true;
                Debug.WriteLine("isLD");
                Constants.isLD = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanVanPhong))
            {
                Debug.WriteLine("isVP");
                // btnTraKQ.Enabled = true;
                Constants.isVP = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanKTTC))
            {
                Debug.WriteLine("isKTTC");
                Constants.isKTTC = true;
            }
            //Get trạng thái hồ sơ để bật/tắt nút
        }

        void ToggleButton(bool btnThemHoSo, bool btnXacNhanKQ, bool btnDuyetKQ, bool btnTraKQ)
        {
            btnThem.Enabled = btnThemHoSo;
            btnXacNhan.Enabled = btnXacNhanKQ;
            btnDuyet.Enabled = btnDuyetKQ;
            this.btnTraKQ.Enabled = btnTraKQ;
        }

        void SetFalseAllButton()
        {
            btnThem.Enabled = false;
            btnXacNhan.Enabled = false;
            btnDuyet.Enabled = false;
            btnTraKQ.Enabled = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {
            FormMenu.Instance.Show();
            FormQLKhaiSinh.Instance.Close();
        }

        public CongDan congDanKhaiSinh = new CongDan();
        public CongDan congDanTmp = new CongDan();
        public CongDan congDanCha = new CongDan();
        public CongDan congDanMe = new CongDan();
        public bool chonThanhCong = false;

        private void btn_ChooseCha_Click(object sender, EventArgs e)
        {
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.GioiTinhNam = true;
            FormQLCongDan.Instance.isContainKhaiSinh = false;
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                isChooseCongDanCha = true;
                MessageBox.Show("Chọn thành công");
                congDanCha = FormQLCongDan.Instance.congDan;
                congDanTmp = null;
                txtbCMND_Cha.Text = String.IsNullOrEmpty(congDanCha.CMND) ? "" : congDanCha.CMND;
                txtbHoTen_Cha.Text = String.IsNullOrEmpty(congDanCha.HoTen) ? "" : congDanCha.HoTen;
                dtpNgaySinh_Cha.Value = congDanCha.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(congDanCha.NgaySinh);
                txtbDanToc_Cha.Text = String.IsNullOrEmpty(congDanCha.DanToc) ? "" : congDanCha.DanToc;
                txtbQuocTich_Cha.Text = String.IsNullOrEmpty(congDanCha.QuocTich) ? "" : congDanCha.QuocTich;
                txtbQueQuan_Cha.Text = String.IsNullOrEmpty(congDanCha.QueQuan) ? "" : congDanCha.QueQuan;
            }
            FormQLCongDan.Instance.Close();
        }



        private void btnThem_Click(object sender, EventArgs e)
        {
            if (ValidateKhaiSinh())
            {
                if (MessageBox.Show("Bạn chắc chắn sẽ nhập thông tin hồ sơ khai sinh này chứ?", "Xác nhận", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    string HoTen = txtbHoTen.Text.Trim();
                    string DanToc = txtbDanToc.Text.Trim();
                    DateTime NgaySinh = dtpNgaySinh.Value;
                    DateTime NgayHen = dtpHenTra.Value;
                    bool GioiTinh = rdNam.Checked ? true : rdNu.Checked ? false : true;
                    string NoiSinh = txtbNoiSinh.Text.Trim();
                    string SoDinhDanh = txtbSoDinhDanh.Text.Trim();
                    string QueQuan = txtbQueQuan.Text.Trim();
                    string QuocTich = txtbQuocTich.Text.Trim();
                    string MaCongDan = this.MaCongDanYeuCau;

                    if (!isChooseCongDanCha && !isChooseCongDanMe)
                    {
                        string CMND_Cha = txtbCMND_Cha.Text.Trim();
                        string HoTen_Cha = txtbHoTen_Cha.Text.Trim();
                        DateTime NgaySinh_Cha = dtpNgaySinh_Cha.Value;
                        string DanToc_Cha = txtbDanToc.Text.Trim();
                        string QuocTich_Cha = txtbQuocTich_Cha.Text.Trim();
                        string QueQuan_Cha = txtbQueQuan_Cha.Text.Trim();
                        CongDan congDanChaInsert = new CongDan(HoTen_Cha, NgaySinh_Cha, true, QueQuan_Cha, "", "", QuocTich_Cha, CMND_Cha, 0);

                        string CMND_Me = txtbCMND_Me.Text.Trim();
                        string HoTen_Me = txtbHoTen_Me.Text.Trim();
                        DateTime NgaySinh_Me = dtpNgaySinh_Me.Value;
                        string DanToc_Me = txtbDanToc.Text.Trim();
                        string QuocTich_Me = txtbQuocTich_Me.Text.Trim();
                        string QueQuan_Me = txtbQueQuan_Me.Text.Trim();
                        CongDan congDanMeInsert = new CongDan(HoTen_Me, NgaySinh_Me, false, QueQuan_Me, "", "", QuocTich_Me, CMND_Me, 0);

                        string IDCha = CongDanModel.ThemCongDan(congDanChaInsert);
                        string IDMe = CongDanModel.ThemCongDan(congDanMeInsert);

                        if (!String.IsNullOrEmpty(IDCha) && !String.IsNullOrEmpty(IDMe))
                        {
                            if (KhaiSinhModel.Insert(int.Parse(MaCongDan), int.Parse(Constants.MaCongChucDangNhap), int.Parse(IDCha), int.Parse(IDMe), NgaySinh, GioiTinh, NgayHen, HoTen, DanToc, NoiSinh, SoDinhDanh, QuocTich, QueQuan))
                            {
                                MessageBox.Show("Thêm hồ sơ đăng ký khai sinh thành công");
                                LoadDuLieu();
                            }
                        }
                    }//endif
                    else if (isChooseCongDanCha && !isChooseCongDanMe)
                    {
                        string CMND_Me = txtbCMND_Me.Text.Trim();
                        string HoTen_Me = txtbHoTen_Me.Text.Trim();
                        DateTime NgaySinh_Me = dtpNgaySinh_Me.Value;
                        string DanToc_Me = txtbDanToc.Text.Trim();
                        string QuocTich_Me = txtbQuocTich_Me.Text.Trim();
                        string QueQuan_Me = txtbQueQuan_Me.Text.Trim();
                        CongDan congDanMeInsert = new CongDan(HoTen_Me, NgaySinh_Me, false, QueQuan_Me, "", "", QuocTich_Me, CMND_Me, 0);

                        string IDMe = CongDanModel.ThemCongDan(congDanMeInsert);
                        int IDCha = congDanCha.MaCongDan;

                        if (!String.IsNullOrEmpty(IDMe))
                        {
                            if (KhaiSinhModel.Insert(int.Parse(MaCongDan), int.Parse(Constants.MaCongChucDangNhap), IDCha, int.Parse(IDMe), NgaySinh, GioiTinh, NgayHen, HoTen, DanToc, NoiSinh, SoDinhDanh, QuocTich, QueQuan))
                            {
                                MessageBox.Show("Thêm hồ sơ đăng ký khai sinh thành công");
                                LoadDuLieu();
                            }
                        }
                    }
                    else if (!isChooseCongDanCha && isChooseCongDanMe)
                    {
                        string CMND_Cha = txtbCMND_Cha.Text.Trim();
                        string HoTen_Cha = txtbHoTen_Cha.Text.Trim();
                        DateTime NgaySinh_Cha = dtpNgaySinh_Cha.Value;
                        string DanToc_Cha = txtbDanToc.Text.Trim();
                        string QuocTich_Cha = txtbQuocTich_Cha.Text.Trim();
                        string QueQuan_Cha = txtbQueQuan_Cha.Text.Trim();
                        CongDan congDanChaInsert = new CongDan(HoTen_Cha, NgaySinh_Cha, true, QueQuan_Cha, "", "", QuocTich_Cha, CMND_Cha, 0);

                        string IDCha = CongDanModel.ThemCongDan(congDanChaInsert);
                        int IDMe = congDanMe.MaCongDan;

                        if (!String.IsNullOrEmpty(IDCha))
                        {
                            if (KhaiSinhModel.Insert(int.Parse(MaCongDan), int.Parse(Constants.MaCongChucDangNhap), int.Parse(IDCha), IDMe, NgaySinh, GioiTinh, NgayHen, HoTen, DanToc, NoiSinh, SoDinhDanh, QuocTich, QueQuan))
                            {
                                MessageBox.Show("Thêm hồ sơ đăng ký khai sinh thành công");
                                LoadDuLieu();
                            }
                        }
                    }
                    else if (isChooseCongDanCha && isChooseCongDanMe)
                    {
                        int IDCha = congDanCha.MaCongDan;
                        int IDMe = congDanMe.MaCongDan;
                        if (KhaiSinhModel.Insert(IDCha, int.Parse(Constants.MaCongChucDangNhap), IDCha, IDMe, NgaySinh, GioiTinh, NgayHen, HoTen, DanToc, NoiSinh, SoDinhDanh, QuocTich, QueQuan))
                        {
                            MessageBox.Show("Thêm hồ sơ đăng ký khai sinh thành công");
                            LoadDuLieu();
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Vui lòng nhập đúng/đủ thông tin");
            }
        }
        /// <summary>
        /// Thêm validate nhé
        /// </summary>
        /// <returns></returns>
        private bool ValidateKhaiSinh()
        {
            bool check = true;
            if(lblHoTenNguoiYeuCau.Text == "")
            {
                return false;
            }
            foreach(Control ctl in grbKhaiSinh.Controls) {
                if(ctl is TextBox)
                {
                    var txtb = (TextBox)ctl;
                    if(txtb.Text == "")
                    {
                        return false;
                    }
                }
            }
            foreach (Control ctl in grbCha.Controls)
            {
                if (ctl is TextBox)
                {
                    var txtb = (TextBox)ctl;
                    if (txtb.Text == "")
                    {
                        return false;
                    }
                }
            }
            foreach (Control ctl in grbMe.Controls)
            {
                if (ctl is TextBox)
                {
                    var txtb = (TextBox)ctl;
                    if (txtb.Text == "")
                    {
                        return false;
                    }
                }
            }
            return check;
        }

        private void LoadDuLieu()
        {
            try
            {
                dgrv_KhaiSinh.Rows.Clear();
                List<KhaiSinh> dsKhaiSinh = KhaiSinhModel.GetData();
                foreach (var khaiSinh in dsKhaiSinh)
                {
                    var index = dgrv_KhaiSinh.Rows.Add();
                    dgrv_KhaiSinh.Rows[index].Cells["MaKhaiSinh"].Value = khaiSinh.MaKhaiSinh;
                    dgrv_KhaiSinh.Rows[index].Cells["HoTen"].Value = khaiSinh.HoTen;
                    dgrv_KhaiSinh.Rows[index].Cells["NgaySinh"].Value = khaiSinh.NgaySinh.ToString("dd/MM/yyyy"); 
                    dgrv_KhaiSinh.Rows[index].Cells["GioiTinh"].Value = khaiSinh.GioiTinh == true ? "Nam" : "Nữ"; 
                    dgrv_KhaiSinh.Rows[index].Cells["DanToc"].Value = khaiSinh.DanToc;
                    dgrv_KhaiSinh.Rows[index].Cells["QuocTich"].Value = khaiSinh.QuocTich;
                    dgrv_KhaiSinh.Rows[index].Cells["QueQuan"].Value = khaiSinh.QueQuan;
                    dgrv_KhaiSinh.Rows[index].Cells["NoiSinh"].Value = khaiSinh.NoiSinh;
                    dgrv_KhaiSinh.Rows[index].Cells["SoDinhDanhCaNhan"].Value = khaiSinh.SoDinhDanhCaNhan;
                    dgrv_KhaiSinh.Rows[index].Cells["HoTenMe"].Value = CongDanModel.GetHoTenByMaCongDan(khaiSinh.IDNguoiMe);
                    dgrv_KhaiSinh.Rows[index].Cells["HoTenCha"].Value = CongDanModel.GetHoTenByMaCongDan(khaiSinh.IDNguoiCha);
                    dgrv_KhaiSinh.Rows[index].Cells["IDMe"].Value = (khaiSinh.IDNguoiMe);
                    dgrv_KhaiSinh.Rows[index].Cells["IDCha"].Value = (khaiSinh.IDNguoiCha);
                    dgrv_KhaiSinh.Rows[index].Cells["TrangThai"].Value = (khaiSinh.TrangThai);
                    dgrv_KhaiSinh.Rows[index].Cells["TrangThai_MoTa"].Value = TrangThaiModel.GetTrangThaiVietTatByMaTrangThai(khaiSinh.TrangThai.ToString());
                    dgrv_KhaiSinh.Rows[index].Cells["NgayHen"].Value = khaiSinh.NgayHen == null ? "" : Convert.ToDateTime(khaiSinh.NgayHen).ToString("dd/MM/yyyy");
                    dgrv_KhaiSinh.Rows[index].Cells["NgayTra"].Value = khaiSinh.NgayTra==null? "": Convert.ToDateTime(khaiSinh.NgayTra).ToString("dd/MM/yyyy");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra!");
            }
        }

        private void btn_ChooseMe_Click(object sender, EventArgs e)
        {
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.GioiTinhNam = false;
            FormQLCongDan.Instance.isContainKhaiSinh = false;
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                isChooseCongDanMe = true;
                MessageBox.Show("Chọn thành công");
                congDanMe = FormQLCongDan.Instance.congDan;
                txtbCMND_Me.Text = String.IsNullOrEmpty(congDanMe.CMND) ? "" : congDanMe.CMND;
                txtbHoTen_Me.Text = String.IsNullOrEmpty(congDanMe.HoTen) ? "" : congDanMe.HoTen;
                dtpNgaySinh_Me.Value = congDanMe.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(congDanMe.NgaySinh);
                txtbDanToc_Me.Text = String.IsNullOrEmpty(congDanMe.DanToc) ? "" : congDanMe.DanToc;
                txtbQuocTich_Me.Text = String.IsNullOrEmpty(congDanMe.QuocTich) ? "" : congDanMe.QuocTich;
                txtbQueQuan_Me.Text = String.IsNullOrEmpty(congDanMe.QueQuan) ? "" : congDanMe.QueQuan;
            }
            FormQLCongDan.Instance.Close();
        }

        void Event_setFalseChonCongDan()
        {
            foreach (Control ctl in grbCha.Controls)
            {
                if (ctl is TextBox)
                {
                    var textBox = (TextBox)ctl;
                    textBox.Click += new EventHandler(this.setFalseToChonCongDanCha);
                }
                if (ctl is DateTimePicker)
                {
                    var dtp = (DateTimePicker)ctl;
                    dtp.MouseDown += new MouseEventHandler(this.setFalseToChonCongDanCha);
                }
            }
            foreach (Control ctl in grbMe.Controls)
            {
                if (ctl is TextBox)
                {
                    var textBox = (TextBox)ctl;
                    textBox.Click += new EventHandler(this.setFalseToChonCongDanMe);
                }
                if (ctl is DateTimePicker)
                {
                    var dtp = (DateTimePicker)ctl;
                    dtp.MouseDown += new MouseEventHandler(this.setFalseToChonCongDanMe);
                }
            }
        }

        private void setFalseToChonCongDanCha(object sender, EventArgs e)
        {
            isChooseCongDanCha = false;
        }
        private void setFalseToChonCongDanMe(object sender, EventArgs e)
        {
            isChooseCongDanMe = false;
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn xác nhận hồ sơ khai sinh này chứ?";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (KhaiSinhModel.UpdateTrangThai(currentKhaiSinh.MaKhaiSinh, Constants.MaCongChucDangNhap, "", (int)TrangThaiModel.TrangThai.XacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.XacMinh+"");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {
                if (KhaiSinhModel.UpdateTrangThai(currentKhaiSinh.MaKhaiSinh,  Constants.MaCongChucDangNhap, "", (int)TrangThaiModel.TrangThai.KhongXacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.KhongXacMinh+"");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }
        }

        private void btnDuyet_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn duyệt hồ sơ khai sinh này chứ?";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (KhaiSinhModel.UpdateTrangThai(currentKhaiSinh.MaKhaiSinh,  "", Constants.MaCongChucDangNhap, (int)TrangThaiModel.TrangThai.Duyet))
                {
                    //Cần UPDATE lại trạng thái cho công dân. Hàm này viết sau
                    string IDCongDanKhaiSinh = CongDanModel.ThemCongDan(congDanKhaiSinh);
                    CongDan congDanCha = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KhaiSinh["IDCha", selectedRowIndex].Value.ToString()));
                    congDanCha.TrangThai = (int)Constants.TrangThaiCongDan.DaDuyet;                    
                    CongDanModel.UpdateCongDan(congDanCha);
                    CongDan congDanMe = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KhaiSinh["IDMe", selectedRowIndex].Value.ToString()));
                    congDanMe.TrangThai = (int)Constants.TrangThaiCongDan.DaDuyet;
                    CongDanModel.UpdateCongDan(congDanMe);
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.Duyet+"");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {
                if (KhaiSinhModel.UpdateTrangThai(currentKhaiSinh.MaKhaiSinh, "", Constants.MaCongChucDangNhap, (int)TrangThaiModel.TrangThai.KhongDuyet))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.KhongDuyet + "");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }
        }

        private void btnTraKQ_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn trả kết quả hồ sơ khai sinh này chứ?";
            CustomYesNoCancelMsgBox.Instance.Yes = "OK";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                int kq = ThuTucModel.Insert("Đăng ký khai sinh cho " + currentKhaiSinh.HoTen + ", mã Khai sinh: " + currentKhaiSinh.MaKhaiSinh);
                if (kq!=-1)
                {
                    if (KhaiSinhModel.UpdateTrangThai(currentKhaiSinh.MaKhaiSinh, Constants.MaCongChucDangNhap, DateTime.Now, (int)TrangThaiModel.TrangThai.DaTra) && KhaiSinhModel.UpdateThuTuc(currentKhaiSinh.MaKhaiSinh, kq))
                    {
                        MessageBox.Show("Thao tác thành công!");
                        ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.DaTra + "");
                        LoadDuLieu();
                    }
                    else
                    {
                        MessageBox.Show("Thao tác thất bại!");
                    }
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {

            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }
        }

        private void FormQLKhaiSinh_Load(object sender, EventArgs e)
        {

        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            lblTrangThai.Enabled = false;
            lblTTTHS.Enabled = false;

            foreach (Control ctr in grbCha.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }
            foreach (Control ctr in grbMe.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }
            foreach (Control ctr in grbKhaiSinh.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }
            SetFalseAllButton();
            if (Constants.isTN_TKQ)
            {
                btnThem.Enabled = true;
            }
        }

        private void dgrv_KhaiSinh_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;
                if (rowIndex >= 0)
                {
                    currentKhaiSinh.MaKhaiSinh = String.IsNullOrEmpty(dgrv_KhaiSinh["MaKhaiSinh", rowIndex].Value.ToString()) ? 0 : int.Parse(dgrv_KhaiSinh["MaKhaiSinh", rowIndex].Value.ToString());
                    txtbHoTen.Text = String.IsNullOrEmpty(dgrv_KhaiSinh["HoTen", rowIndex].Value.ToString()) ? "" : dgrv_KhaiSinh["HoTen", rowIndex].Value.ToString();
                    currentKhaiSinh.HoTen = txtbHoTen.Text;
                    dtpNgaySinh.Value = DateTime.ParseExact(dgrv_KhaiSinh.Rows[rowIndex].Cells["NgaySinh"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                    rdNam.Checked = dgrv_KhaiSinh.Rows[rowIndex].Cells["GioiTinh"].Value.ToString() == "Nam" ? true : false;
                    rdNu.Checked = dgrv_KhaiSinh.Rows[rowIndex].Cells["GioiTinh"].Value.ToString() == "Nữ" ? true : false;
                    txtbDanToc.Text = String.IsNullOrEmpty(dgrv_KhaiSinh["DanToc", rowIndex].Value.ToString()) ? "" : dgrv_KhaiSinh["DanToc", rowIndex].Value.ToString();
                    txtbNoiSinh.Text = String.IsNullOrEmpty(dgrv_KhaiSinh["NoiSinh", rowIndex].Value.ToString()) ? "" : dgrv_KhaiSinh["NoiSinh", rowIndex].Value.ToString();
                    txtbSoDinhDanh.Text = String.IsNullOrEmpty(dgrv_KhaiSinh["SoDinhDanhCaNhan", rowIndex].Value.ToString()) ? "" : dgrv_KhaiSinh["SoDinhDanhCaNhan", rowIndex].Value.ToString();
                    txtbQuocTich.Text = String.IsNullOrEmpty(dgrv_KhaiSinh["QuocTich", rowIndex].Value.ToString()) ? "" : dgrv_KhaiSinh["QuocTich", rowIndex].Value.ToString();
                    txtbQueQuan.Text = String.IsNullOrEmpty(dgrv_KhaiSinh["QueQuan", rowIndex].Value.ToString()) ? "" : dgrv_KhaiSinh["QueQuan", rowIndex].Value.ToString();
                    dtpHenTra.Value = String.IsNullOrEmpty(dgrv_KhaiSinh.Rows[rowIndex].Cells["NgayHen"].Value.ToString()) ? DateTime.Now : DateTime.ParseExact(dgrv_KhaiSinh.Rows[rowIndex].Cells["NgayHen"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);

                    congDanKhaiSinh = new CongDan(txtbHoTen.Text, dtpNgaySinh.Value, rdNam.Checked ? true : false, txtbQueQuan.Text, "", txtbDanToc.Text, txtbQuocTich.Text, "KhaiSinh", 1);
                    selectedRowIndex = rowIndex;
                    CongDan congDanCha = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KhaiSinh["IDCha", rowIndex].Value.ToString()));
                    CongDan congDanMe = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KhaiSinh["IDMe", rowIndex].Value.ToString()));

                    txtbCMND_Cha.Text = String.IsNullOrEmpty(congDanCha.CMND) ? "" : congDanCha.CMND;
                    txtbHoTen_Cha.Text = String.IsNullOrEmpty(congDanCha.HoTen) ? "" : congDanCha.HoTen;
                    dtpNgaySinh_Cha.Value = congDanCha.NgaySinh;
                    txtbDanToc_Cha.Text = String.IsNullOrEmpty(congDanCha.DanToc) ? "" : congDanCha.DanToc;
                    txtbQuocTich_Cha.Text = String.IsNullOrEmpty(congDanCha.QuocTich) ? "" : congDanCha.QuocTich;
                    txtbQueQuan_Cha.Text = String.IsNullOrEmpty(congDanCha.QueQuan) ? "" : congDanCha.QueQuan;

                    txtbCMND_Me.Text = String.IsNullOrEmpty(congDanMe.CMND) ? "" : congDanMe.CMND;
                    txtbHoTen_Me.Text = String.IsNullOrEmpty(congDanMe.HoTen) ? "" : congDanMe.HoTen;
                    dtpNgaySinh_Me.Value = congDanMe.NgaySinh;
                    txtbDanToc_Me.Text = String.IsNullOrEmpty(congDanMe.DanToc) ? "" : congDanMe.DanToc;
                    txtbQuocTich_Me.Text = String.IsNullOrEmpty(congDanMe.QuocTich) ? "" : congDanMe.QuocTich;
                    txtbQueQuan_Me.Text = String.IsNullOrEmpty(congDanMe.QueQuan) ? "" : congDanMe.QueQuan;

                    lblTrangThai.Visible = true; lblTTTHS.Visible = true;
                    lblTrangThai.Text = String.IsNullOrEmpty(dgrv_KhaiSinh["TrangThai", rowIndex].Value.ToString()) ? "" : TrangThaiModel.GetTrangThaiByMaTrangThai(dgrv_KhaiSinh["TrangThai", rowIndex].Value.ToString());
                    ToggleButtonByTrangThai(dgrv_KhaiSinh["TrangThai", rowIndex].Value.ToString());
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void ToggleButtonByTrangThai(string TrangThai)
        {
            int iTrangThai = int.Parse(TrangThai);
            SetFalseAllButton();
            if (Constants.isCCCX)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.TiepNhan)
                {
                    btnXacNhan.Enabled = true;
                }
            }
            if (Constants.isLD)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.XacMinh)
                {
                    btnDuyet.Enabled = true;
                }
            }
            if (Constants.isVP)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.Duyet)
                {
                    btnTraKQ.Enabled = true;
                }
            }
        }

        private void FormQLKhaiSinh_Shown(object sender, EventArgs e)
        {
            dtpHenTra.Value = DateTime.Now.Date.AddDays(5);
            lblHoTenNguoiYeuCau.Text = "";
        }

        private void btnChonCongDan_Click(object sender, EventArgs e)
        {
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.AllGioiTinh = true;
            //FormQLCongDan.Instance.isContainAdult = false;
            //FormQLCongDan.Instance.isContainKhaiSinh= true;

            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                MessageBox.Show("Chọn thành công");
                congDanTmp = FormQLCongDan.Instance.congDan;
                this.MaCongDanYeuCau = congDanTmp.MaCongDan.ToString();
                lblHoTenNguoiYeuCau.Text = String.IsNullOrEmpty(congDanTmp.HoTen) ? "" : congDanTmp.HoTen;
                congDanTmp = null;
            }
            FormQLCongDan.Instance.Close();
        }

        private void btn_TimKiem_Click(object sender, EventArgs e)
        {
            string HoTen = txtbHoTen.Text.Trim();
            object NgaySinh = null;
            int GioiTinh = rdNam.Checked ? 1 : rdNu.Checked ? 0 : -1;
            string DanToc = txtbDanToc.Text.Trim();
            string QuocTich = txtbQuocTich.Text.Trim();
            string QueQuan = txtbQueQuan.Text.Trim();
            string NoiSinh = txtbNoiSinh.Text.Trim();
            string SoDinhDanh = txtbSoDinhDanh.Text.Trim();
            int TrangThai = -1;
            try
            {
                TrangThai = int.Parse(cbTrangThai.SelectedValue.ToString());
            }
            catch
            {
                TrangThai = -1;
            }
            object[] param = new object[] { HoTen, NgaySinh, GioiTinh, DanToc, QuocTich, QueQuan, NoiSinh, SoDinhDanh, TrangThai};
            
            try
            {
                dgrv_KhaiSinh.Rows.Clear();
                List<KhaiSinh> dsKhaiSinh = KhaiSinhModel.TimKiem(param);
                foreach (var khaiSinh in dsKhaiSinh)
                {
                    int GTKS = khaiSinh.GioiTinh ? 1 : 0;
                    if(GioiTinh == -1) {
                        var index = dgrv_KhaiSinh.Rows.Add();
                        dgrv_KhaiSinh.Rows[index].Cells["MaKhaiSinh"].Value = khaiSinh.MaKhaiSinh;
                        dgrv_KhaiSinh.Rows[index].Cells["HoTen"].Value = khaiSinh.HoTen;
                        dgrv_KhaiSinh.Rows[index].Cells["NgaySinh"].Value = khaiSinh.NgaySinh.ToString("dd/MM/yyyy");
                        dgrv_KhaiSinh.Rows[index].Cells["GioiTinh"].Value = khaiSinh.GioiTinh == true ? "Nam" : "Nữ";
                        dgrv_KhaiSinh.Rows[index].Cells["DanToc"].Value = khaiSinh.DanToc;
                        dgrv_KhaiSinh.Rows[index].Cells["QuocTich"].Value = khaiSinh.QuocTich;
                        dgrv_KhaiSinh.Rows[index].Cells["QueQuan"].Value = khaiSinh.QueQuan;
                        dgrv_KhaiSinh.Rows[index].Cells["NoiSinh"].Value = khaiSinh.NoiSinh;
                        dgrv_KhaiSinh.Rows[index].Cells["SoDinhDanhCaNhan"].Value = khaiSinh.SoDinhDanhCaNhan;
                        dgrv_KhaiSinh.Rows[index].Cells["HoTenMe"].Value = CongDanModel.GetHoTenByMaCongDan(khaiSinh.IDNguoiMe);
                        dgrv_KhaiSinh.Rows[index].Cells["HoTenCha"].Value = CongDanModel.GetHoTenByMaCongDan(khaiSinh.IDNguoiCha);
                        dgrv_KhaiSinh.Rows[index].Cells["IDMe"].Value = (khaiSinh.IDNguoiMe);
                        dgrv_KhaiSinh.Rows[index].Cells["IDCha"].Value = (khaiSinh.IDNguoiCha);
                        dgrv_KhaiSinh.Rows[index].Cells["TrangThai"].Value = (khaiSinh.TrangThai);
                        dgrv_KhaiSinh.Rows[index].Cells["TrangThai_MoTa"].Value = TrangThaiModel.GetTrangThaiVietTatByMaTrangThai(khaiSinh.TrangThai.ToString());
                        dgrv_KhaiSinh.Rows[index].Cells["NgayHen"].Value = khaiSinh.NgayHen == null ? "" : Convert.ToDateTime(khaiSinh.NgayHen).ToString("dd/MM/yyyy");
                        dgrv_KhaiSinh.Rows[index].Cells["NgayTra"].Value = khaiSinh.NgayTra == null ? "" : Convert.ToDateTime(khaiSinh.NgayTra).ToString("dd/MM/yyyy");
                    }
                    else if (GioiTinh != -1 && GioiTinh == GTKS)
                    {
                        var index = dgrv_KhaiSinh.Rows.Add();
                        dgrv_KhaiSinh.Rows[index].Cells["MaKhaiSinh"].Value = khaiSinh.MaKhaiSinh;
                        dgrv_KhaiSinh.Rows[index].Cells["HoTen"].Value = khaiSinh.HoTen;
                        dgrv_KhaiSinh.Rows[index].Cells["NgaySinh"].Value = khaiSinh.NgaySinh.ToString("dd/MM/yyyy");
                        dgrv_KhaiSinh.Rows[index].Cells["GioiTinh"].Value = khaiSinh.GioiTinh == true ? "Nam" : "Nữ";
                        dgrv_KhaiSinh.Rows[index].Cells["DanToc"].Value = khaiSinh.DanToc;
                        dgrv_KhaiSinh.Rows[index].Cells["QuocTich"].Value = khaiSinh.QuocTich;
                        dgrv_KhaiSinh.Rows[index].Cells["QueQuan"].Value = khaiSinh.QueQuan;
                        dgrv_KhaiSinh.Rows[index].Cells["NoiSinh"].Value = khaiSinh.NoiSinh;
                        dgrv_KhaiSinh.Rows[index].Cells["SoDinhDanhCaNhan"].Value = khaiSinh.SoDinhDanhCaNhan;
                        dgrv_KhaiSinh.Rows[index].Cells["HoTenMe"].Value = CongDanModel.GetHoTenByMaCongDan(khaiSinh.IDNguoiMe);
                        dgrv_KhaiSinh.Rows[index].Cells["HoTenCha"].Value = CongDanModel.GetHoTenByMaCongDan(khaiSinh.IDNguoiCha);
                        dgrv_KhaiSinh.Rows[index].Cells["IDMe"].Value = (khaiSinh.IDNguoiMe);
                        dgrv_KhaiSinh.Rows[index].Cells["IDCha"].Value = (khaiSinh.IDNguoiCha);
                        dgrv_KhaiSinh.Rows[index].Cells["TrangThai"].Value = (khaiSinh.TrangThai);
                        dgrv_KhaiSinh.Rows[index].Cells["TrangThai_MoTa"].Value = TrangThaiModel.GetTrangThaiVietTatByMaTrangThai(khaiSinh.TrangThai.ToString());
                        dgrv_KhaiSinh.Rows[index].Cells["NgayHen"].Value = khaiSinh.NgayHen == null ? "" : Convert.ToDateTime(khaiSinh.NgayHen).ToString("dd/MM/yyyy");
                        dgrv_KhaiSinh.Rows[index].Cells["NgayTra"].Value = khaiSinh.NgayTra == null ? "" : Convert.ToDateTime(khaiSinh.NgayTra).ToString("dd/MM/yyyy");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra!");
            }
        }
    }
}
