﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLHT.Model;
using QLHT.Entity;
using System.Diagnostics;

namespace QuanLyHoTich
{
    public partial class FormQLKetHon : Form
    {
        private static FormQLKetHon instance;
        public static FormQLKetHon Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormQLKetHon();
                }
                return instance;
            }
        }
        public FormQLKetHon()
        {
            InitializeComponent();
            GetQuyen();
            Event_setFalseChonCongDan();
            dtpNgaySinh_Chong.CustomFormat = "dd/MM/yyyy";
            dtpNgaySinh_Vo.CustomFormat = "dd/MM/yyyy";
            LoadDuLieu();

        }
           void Event_setFalseChonCongDan()
        {
            foreach (Control ctl in grbChong.Controls)
            {
                if (ctl is TextBox)
                {
                    var textBox = (TextBox)ctl;
                    textBox.Click += new EventHandler(this.setFalseToChonCongDanCha);
                }
                if (ctl is DateTimePicker)
                {
                    var dtp = (DateTimePicker)ctl;
                    dtp.MouseDown += new MouseEventHandler(this.setFalseToChonCongDanCha);
                }
            }
            foreach (Control ctl in grbVo.Controls)
            {
                if (ctl is TextBox)
                {
                    var textBox = (TextBox)ctl;
                    textBox.Click += new EventHandler(this.setFalseToChonCongDanMe);
                }
                if (ctl is DateTimePicker)
                {
                    var dtp = (DateTimePicker)ctl;
                    dtp.MouseDown += new MouseEventHandler(this.setFalseToChonCongDanMe);
                }
            }
        }
        String Quyen = "";
        CongDanModel CongDanModel = new CongDanModel();
        KetHonModel KetHonModel = new KetHonModel();
        TrangThaiModel TrangThaiModel = new TrangThaiModel();
        bool isChooseCongDanChong = false;
        bool isChooseCongDanVo = false;
        string MaCongDanYeuCau = "-1";
        KetHon currentKetHon = new KetHon();
        int selectedRowIndex = -1;

        public void GetQuyen()
        {
            Quyen = new CongChucModel().GetQuyen(Constants.MaCongChucDangNhap);
            SetFalseAllButton();
            //Ktra 5 quyền
            Debug.WriteLine("Send to debug output.");

            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanTiepNhanVaTraKQ))
            {
                Debug.WriteLine("isTN_TKQ");
                Constants.isTN_TKQ = true;
                btnThem.Enabled = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatCongChucCapXa))
            {
                
                Constants.isCCCX = true;
                Debug.WriteLine("isCCCX");
                //  btnXacNhan.Enabled = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatLanhDao))
            {
                // btnDuyet.Enabled = true;
                Debug.WriteLine("isLD");
                Constants.isLD = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanVanPhong))
            {
                Debug.WriteLine("isVP");
                // btnTraKQ.Enabled = true;
                Constants.isVP = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanKTTC))
            {
                Debug.WriteLine("isKTTC");
                Constants.isKTTC = true;
            }
            //Get trạng thái hồ sơ để bật/tắt nút
        }

        void ToggleButton(bool btnThemHoSo, bool btnXacNhanKQ, bool btnDuyetKQ, bool btnTraKQ)
        {
            btnThem.Enabled = btnThemHoSo;
            btnXacNhan.Enabled = btnXacNhanKQ;
            btnDuyet.Enabled = btnDuyetKQ;
            this.btnTraKQ.Enabled = btnTraKQ;
        }

        void SetFalseAllButton()
        {
            btnThem.Enabled = false;
            btnXacNhan.Enabled = false;
            btnDuyet.Enabled = false;
            btnTraKQ.Enabled = false;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {
            FormMenu.Instance.Show();
            FormQLKetHon.Instance.Close();
        }

        public CongDan congDanKetHon = new CongDan();
        public CongDan congDanTmp = new CongDan();
        public CongDan congDanChong = new CongDan();
        public CongDan congDanVo = new CongDan();
        public bool chonThanhCong = false;

        private void btn_ChooseChong_Click(object sender, EventArgs e)
        {
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.GioiTinhNam = true;
            FormQLCongDan.Instance.isContainKetHon = false;
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                isChooseCongDanChong = true;
                MessageBox.Show("Chọn thành công");
                congDanChong = FormQLCongDan.Instance.congDan;
                congDanTmp = null;
                txtbCMND_Chong.Text = String.IsNullOrEmpty(congDanChong.CMND) ? "" : congDanChong.CMND;
                txtbHoTen_Chong.Text = String.IsNullOrEmpty(congDanChong.HoTen) ? "" : congDanChong.HoTen;
                dtpNgaySinh_Chong.Value = congDanChong.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(congDanChong.NgaySinh);
                txtbDanToc_Chong.Text = String.IsNullOrEmpty(congDanChong.DanToc) ? "" : congDanChong.DanToc;
                txtbQuocTich_Chong.Text = String.IsNullOrEmpty(congDanChong.QuocTich) ? "" : congDanChong.QuocTich;
                txtbQueQuan_Chong.Text = String.IsNullOrEmpty(congDanChong.QueQuan) ? "" : congDanChong.QueQuan;
            }
            FormQLCongDan.Instance.Close();
        }



        private void btnThem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn chắc chắn sẽ nhập thông tin hồ sơ kết hôn này chứ?", "Xác nhận", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
           
                DateTime NgayHen = dtpHenTra.Value;
                string MaCongDan = this.MaCongDanYeuCau;                

                if (!isChooseCongDanChong && !isChooseCongDanVo)
                {
                    string CMND_Chong = txtbCMND_Chong.Text.Trim();
                    string HoTen_Chong = txtbHoTen_Chong.Text.Trim();
                    DateTime NgaySinh_Chong = dtpNgaySinh_Chong.Value;
                    string DanToc_Chong = txtbDanToc_Chong.Text.Trim();
                    string QuocTich_Chong = txtbQuocTich_Chong.Text.Trim();
                    string QueQuan_Chong = txtbQueQuan_Chong.Text.Trim();
                    CongDan congDanChongInsert = new CongDan(HoTen_Chong, NgaySinh_Chong, true, QueQuan_Chong, "", "", QuocTich_Chong, CMND_Chong, 0);

                    string CMND_Vo = txtbCMND_Vo.Text.Trim();
                    string HoTen_Vo = txtbHoTen_Vo.Text.Trim();
                    DateTime NgaySinh_Vo = dtpNgaySinh_Vo.Value;
                    string DanToc_Vo = txtbDanToc_Vo.Text.Trim();
                    string QuocTich_Vo = txtbQuocTich_Vo.Text.Trim();
                    string QueQuan_Vo = txtbQueQuan_Vo.Text.Trim();
                    CongDan congDanVoInsert = new CongDan(HoTen_Vo, NgaySinh_Vo, false, QueQuan_Vo, "", "", QuocTich_Vo, CMND_Vo, 0);

                    string IDNguoiChong = CongDanModel.ThemCongDan(congDanChongInsert);
                    string IDNguoiVo = CongDanModel.ThemCongDan(congDanVoInsert);

                    if (!String.IsNullOrEmpty(IDNguoiChong) && !String.IsNullOrEmpty(IDNguoiVo))
                    {
                        if (KetHonModel.Insert(int.Parse(MaCongDan), int.Parse(Constants.MaCongChucDangNhap), int.Parse(IDNguoiChong), int.Parse(IDNguoiVo), NgayHen))
                        {
                            MessageBox.Show("Thêm hồ sơ đăng ký kết hôn thành công");
                            LoadDuLieu();
                        }
                    }
                }//endif
                else if (isChooseCongDanChong && !isChooseCongDanVo)
                {
                    string CMND_Vo = txtbCMND_Vo.Text.Trim();
                    string HoTen_Vo = txtbHoTen_Vo.Text.Trim();
                    DateTime NgaySinh_Vo = dtpNgaySinh_Vo.Value;
                    string DanToc_Vo = txtbDanToc_Vo.Text.Trim();
                    string QuocTich_Vo = txtbQuocTich_Vo.Text.Trim();
                    string QueQuan_Vo = txtbQueQuan_Vo.Text.Trim();
                    CongDan congDanVoInsert = new CongDan(HoTen_Vo, NgaySinh_Vo, false, QueQuan_Vo, "", "", QuocTich_Vo, CMND_Vo, 0);

                    string IDNguoiVo = CongDanModel.ThemCongDan(congDanVoInsert);
                    int IDNguoiChong = congDanChong.MaCongDan;

                    if (!String.IsNullOrEmpty(IDNguoiVo))
                    {
                        if (KetHonModel.Insert(int.Parse(MaCongDan), int.Parse(Constants.MaCongChucDangNhap), IDNguoiChong, int.Parse(IDNguoiVo),  NgayHen))
                        {
                            MessageBox.Show("Thêm hồ sơ đăng ký kết hôn thành công");
                            LoadDuLieu();
                        }
                    }
                }
                else if (!isChooseCongDanChong && isChooseCongDanVo)
                {
                    string CMND_Chong = txtbCMND_Chong.Text.Trim();
                    string HoTen_Chong = txtbHoTen_Chong.Text.Trim();
                    DateTime NgaySinh_Chong = dtpNgaySinh_Chong.Value;
                    string DanToc_Chong = txtbDanToc_Chong.Text.Trim();
                    string QuocTich_Chong = txtbQuocTich_Chong.Text.Trim();
                    string QueQuan_Chong = txtbQueQuan_Chong.Text.Trim();
                    CongDan congDanChongInsert = new CongDan(HoTen_Chong, NgaySinh_Chong, true, QueQuan_Chong, "", "", QuocTich_Chong, CMND_Chong, 0);

                    string IDNguoiChong = CongDanModel.ThemCongDan(congDanChongInsert);
                    int IDNguoiVo = congDanVo.MaCongDan;

                    if (!String.IsNullOrEmpty(IDNguoiChong))
                    {
                        if (KetHonModel.Insert(int.Parse(MaCongDan), int.Parse(Constants.MaCongChucDangNhap), int.Parse(IDNguoiChong), IDNguoiVo, NgayHen))
                        {
                            MessageBox.Show("Thêm hồ sơ đăng ký kết hôn thành công");
                            LoadDuLieu();
                        }
                    }
                }
                else if (isChooseCongDanChong && isChooseCongDanVo)
                {
                    int IDNguoiChong = congDanChong.MaCongDan;
                    int IDNguoiVo = congDanVo.MaCongDan;
                    if (KetHonModel.Insert(IDNguoiChong, int.Parse(Constants.MaCongChucDangNhap), IDNguoiChong, IDNguoiVo, NgayHen))
                    {
                        MessageBox.Show("Thêm hồ sơ đăng ký kết hôn thành công");
                        LoadDuLieu();
                    }
                }
            }
        }

        private void LoadDuLieu()
        {
            try
            {
                dgrv_KetHon.Rows.Clear();
                List<KetHon> dsKetHon = KetHonModel.GetData();
                foreach (var ketHon in dsKetHon)
                {
                    var index = dgrv_KetHon.Rows.Add();
                    dgrv_KetHon.Rows[index].Cells["MaKH"].Value = ketHon.MaKH;
                    dgrv_KetHon.Rows[index].Cells["HoTenVo"].Value = CongDanModel.GetHoTenByMaCongDan_KetHon(ketHon.IDNguoiVo);
                    dgrv_KetHon.Rows[index].Cells["HoTenChong"].Value = CongDanModel.GetHoTenByMaCongDan_KetHon(ketHon.IDNguoiChong);
                    dgrv_KetHon.Rows[index].Cells["IDNguoiVo"].Value = (ketHon.IDNguoiVo);
                    dgrv_KetHon.Rows[index].Cells["IDNguoiChong"].Value = (ketHon.IDNguoiChong);
                    dgrv_KetHon.Rows[index].Cells["TrangThai"].Value = (ketHon.TrangThai);
                    dgrv_KetHon.Rows[index].Cells["TrangThai_MoTa"].Value = TrangThaiModel.GetTrangThaiVietTatByMaTrangThai(ketHon.TrangThai.ToString());
                    dgrv_KetHon.Rows[index].Cells["NgayHen"].Value = ketHon.NgayHen == null ? "" : Convert.ToDateTime(ketHon.NgayHen).ToString("dd/MM/yyyy");
                    dgrv_KetHon.Rows[index].Cells["NgayTra"].Value = ketHon.NgayTra==null? "": Convert.ToDateTime(ketHon.NgayTra).ToString("dd/MM/yyyy");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi xảy ra!");
            }
        }

     
  

        private void setFalseToChonCongDanCha(object sender, EventArgs e)
        {
            isChooseCongDanChong = false;
        }
        private void setFalseToChonCongDanMe(object sender, EventArgs e)
        {
            isChooseCongDanVo = false;
        }

        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn xác nhận hồ sơ kết hôn này chứ?";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (KetHonModel.UpdateTrangThai(currentKetHon.MaKH, Constants.MaCongChucDangNhap, "", (int)TrangThaiModel.TrangThai.XacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.XacMinh+"");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {
                if (KetHonModel.UpdateTrangThai(currentKetHon.MaKH,  Constants.MaCongChucDangNhap, "", (int)TrangThaiModel.TrangThai.KhongXacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.KhongXacMinh+"");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }
        }

        private void btnDuyet_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn duyệt hồ sơ kết hôn này chứ?";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (KetHonModel.UpdateTrangThai(currentKetHon.MaKH,  "", Constants.MaCongChucDangNhap, (int)TrangThaiModel.TrangThai.Duyet))
                {
                    //Cần UPDATE lại trạng thái cho công dân. Hàm này viết sau
                    CongDan congDanChong = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KetHon["IDNguoiChong", selectedRowIndex].Value.ToString()));
                    congDanChong.TrangThai = (int)Constants.TrangThaiCongDan.DaDuyet;                    
                    CongDanModel.UpdateCongDan(congDanChong);
                    CongDan congDanVo = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KetHon["IDNguoiVo", selectedRowIndex].Value.ToString()));
                    congDanVo.TrangThai = (int)Constants.TrangThaiCongDan.DaDuyet;
                    CongDanModel.UpdateCongDan(congDanVo);
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.Duyet+"");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {
                if (KetHonModel.UpdateTrangThai(currentKetHon.MaKH, "", Constants.MaCongChucDangNhap, (int)TrangThaiModel.TrangThai.KhongDuyet))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.KhongDuyet + "");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }
        }

        private void btnTraKQ_Click(object sender, EventArgs e)
        {
            CustomYesNoCancelMsgBox.Instance.title = "Bạn trả kết quả hồ sơ kết hôn này chứ?";
            CustomYesNoCancelMsgBox.Instance.Yes = "OK";
            CustomYesNoCancelMsgBox.Instance.ShowDialog();
            int result = CustomYesNoCancelMsgBox.Instance.DialogResult;
            CustomYesNoCancelMsgBox.Instance.Close();
            if (result == (int)Constants.CustomDialogResult.Yes)
            {
                if (KetHonModel.UpdateTrangThai(currentKetHon.MaKH, Constants.MaCongChucDangNhap, DateTime.Now, (int)TrangThaiModel.TrangThai.XacMinh))
                {
                    MessageBox.Show("Thao tác thành công!");
                    ToggleButtonByTrangThai((int)TrangThaiModel.TrangThai.DaTra + "");
                    LoadDuLieu();
                }
                else
                {
                    MessageBox.Show("Thao tác thất bại!");
                }
            }
            if (result == (int)Constants.CustomDialogResult.No)
            {

            }
            if (result == (int)Constants.CustomDialogResult.Cancel)
            {

            }
        }

    

        private void btnHuy_Click(object sender, EventArgs e)
        {
            lblTrangThai.Enabled = false;
            lblTTTHS.Enabled = false;

            foreach (Control ctr in grbChong.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }
            foreach (Control ctr in grbVo.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }
          
            SetFalseAllButton();
            if (Constants.isTN_TKQ)
            {
                btnThem.Enabled = true;
            }



        }

        private void dgrv_KetHon_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;
                if (rowIndex >= 0)
                {
                    currentKetHon.MaKH = String.IsNullOrEmpty(dgrv_KetHon["MaKH", rowIndex].Value.ToString()) ? 0 : int.Parse(dgrv_KetHon["MaKH", rowIndex].Value.ToString());
            
                    dtpHenTra.Value = String.IsNullOrEmpty(dgrv_KetHon.Rows[rowIndex].Cells["NgayHen"].Value.ToString()) ? DateTime.Now : DateTime.ParseExact(dgrv_KetHon.Rows[rowIndex].Cells["NgayHen"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);

                    CongDan congDanChong = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KetHon["IDNguoiChong", rowIndex].Value.ToString()));
                    CongDan congDanVo = CongDanModel.GetCongDanByMaCongDan(int.Parse(dgrv_KetHon["IDNguoiVo", rowIndex].Value.ToString()));

                    txtbCMND_Chong.Text = String.IsNullOrEmpty(congDanChong.CMND) ? "" : congDanChong.CMND;
                    txtbHoTen_Chong.Text = String.IsNullOrEmpty(congDanChong.HoTen) ? "" : congDanChong.HoTen;
                    dtpNgaySinh_Chong.Value = congDanChong.NgaySinh;
                    txtbDanToc_Chong.Text = String.IsNullOrEmpty(congDanChong.DanToc) ? "" : congDanChong.DanToc;
                    txtbQuocTich_Chong.Text = String.IsNullOrEmpty(congDanChong.QuocTich) ? "" : congDanChong.QuocTich;
                    txtbQueQuan_Chong.Text = String.IsNullOrEmpty(congDanChong.QueQuan) ? "" : congDanChong.QueQuan;

                    txtbCMND_Vo.Text = String.IsNullOrEmpty(congDanVo.CMND) ? "" : congDanVo.CMND;
                    txtbHoTen_Vo.Text = String.IsNullOrEmpty(congDanVo.HoTen) ? "" : congDanVo.HoTen;
                    dtpNgaySinh_Vo.Value = congDanVo.NgaySinh;
                    txtbDanToc_Vo.Text = String.IsNullOrEmpty(congDanVo.DanToc) ? "" : congDanVo.DanToc;
                    txtbQuocTich_Vo.Text = String.IsNullOrEmpty(congDanVo.QuocTich) ? "" : congDanVo.QuocTich;
                    txtbQueQuan_Vo.Text = String.IsNullOrEmpty(congDanVo.QueQuan) ? "" : congDanVo.QueQuan;

                    lblTrangThai.Visible = true; lblTTTHS.Visible = true;
                    lblTrangThai.Text = String.IsNullOrEmpty(dgrv_KetHon["TrangThai", rowIndex].Value.ToString()) ? "" : TrangThaiModel.GetTrangThaiByMaTrangThai(dgrv_KetHon["TrangThai", rowIndex].Value.ToString());
                    ToggleButtonByTrangThai(dgrv_KetHon["TrangThai", rowIndex].Value.ToString());
                    selectedRowIndex = rowIndex;
                }
            }
            catch(Exception ex)
            {

            }
        }

        private void ToggleButtonByTrangThai(string TrangThai)
        {
            int iTrangThai = int.Parse(TrangThai);
            SetFalseAllButton();
            if (Constants.isCCCX)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.TiepNhan)
                {
                    btnXacNhan.Enabled = true;
                }
            }
            if (Constants.isLD)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.XacMinh)
                {
                    btnDuyet.Enabled = true;
                }
            }
            if (Constants.isVP)
            {
                if (iTrangThai == (int)TrangThaiModel.TrangThai.Duyet)
                {
                    btnTraKQ.Enabled = true;
                }
            }
        }

    

        private void btn_TimKiem_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void FormQLKetHon_Load(object sender, EventArgs e)
        {
          
        }

        private void FormQLKetHon_Shown(object sender, EventArgs e)
        {
            dtpHenTra.Value = DateTime.Now.Date.AddDays(5);
       
        }

        private void btn_ChooseVo_Click(object sender, EventArgs e)
        {
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.GioiTinhNam = false;
            FormQLCongDan.Instance.isContainKhaiSinh = false;
            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                isChooseCongDanVo = true;
                MessageBox.Show("Chọn thành công");
                congDanVo = FormQLCongDan.Instance.congDan;
                txtbCMND_Vo.Text = String.IsNullOrEmpty(congDanVo.CMND) ? "" : congDanVo.CMND;
                txtbHoTen_Vo.Text = String.IsNullOrEmpty(congDanVo.HoTen) ? "" : congDanVo.HoTen;
                dtpNgaySinh_Vo.Value = congDanVo.NgaySinh == null ? DateTime.MinValue : Convert.ToDateTime(congDanVo.NgaySinh);
                txtbDanToc_Vo.Text = String.IsNullOrEmpty(congDanVo.DanToc) ? "" : congDanVo.DanToc;
                txtbQuocTich_Vo.Text = String.IsNullOrEmpty(congDanVo.QuocTich) ? "" : congDanVo.QuocTich;
                txtbQueQuan_Vo.Text = String.IsNullOrEmpty(congDanVo.QueQuan) ? "" : congDanVo.QueQuan;
            }
            FormQLCongDan.Instance.Close();
        }

        private void btnChonCongDan_Click(object sender, EventArgs e)
        {
            FormQLCongDan.Instance.isChonCongDan = true;
            FormQLCongDan.Instance.txtTitle = "Chọn thông tin công dân";
            FormQLCongDan.Instance.AllGioiTinh = true;
            //FormQLCongDan.Instance.isContainAdult = false;
            //FormQLCongDan.Instance.isContainKhaiSinh= true;

            FormQLCongDan.Instance.ShowDialog();
            if (FormQLCongDan.Instance.chonThanhCong)
            {
                MessageBox.Show("Chọn thành công");
                congDanTmp = FormQLCongDan.Instance.congDan;
                this.MaCongDanYeuCau = congDanTmp.MaCongDan.ToString();
                lblHoTenNguoiYeuCau.Text = String.IsNullOrEmpty(congDanTmp.HoTen) ? "" : congDanTmp.HoTen;
                congDanTmp = null;
            }
            FormQLCongDan.Instance.Close();
        }
    }
}
