﻿namespace QuanLyHoTich
{
    partial class FormQLKetHon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grbChong = new System.Windows.Forms.GroupBox();
            this.txtbCMND_Chong = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dtpNgaySinh_Chong = new System.Windows.Forms.DateTimePicker();
            this.txtbQueQuan_Chong = new System.Windows.Forms.TextBox();
            this.txtbQuocTich_Chong = new System.Windows.Forms.TextBox();
            this.txtbDanToc_Chong = new System.Windows.Forms.TextBox();
            this.txtbHoTen_Chong = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_ChooseChong = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnXacNhan = new System.Windows.Forms.Button();
            this.btnDuyet = new System.Windows.Forms.Button();
            this.btnTraKQ = new System.Windows.Forms.Button();
            this.dgrv_KetHon = new System.Windows.Forms.DataGridView();
            this.MaKH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDNguoiChong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTenChong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDNguoiVo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTenVo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrangThai_MoTa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayHen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayTra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TrangThai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.lblTTTHS = new System.Windows.Forms.Label();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.btn_TimKiem = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.btnBackToMenu = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.dtpHenTra = new System.Windows.Forms.DateTimePicker();
            this.grbVo = new System.Windows.Forms.GroupBox();
            this.txtbCMND_Vo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpNgaySinh_Vo = new System.Windows.Forms.DateTimePicker();
            this.txtbQueQuan_Vo = new System.Windows.Forms.TextBox();
            this.txtbQuocTich_Vo = new System.Windows.Forms.TextBox();
            this.txtbDanToc_Vo = new System.Windows.Forms.TextBox();
            this.txtbHoTen_Vo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btn_ChooseVo = new System.Windows.Forms.Button();
            this.ketHonBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblHoTenNguoiYeuCau = new System.Windows.Forms.Label();
            this.lblNguoiYeuCau = new System.Windows.Forms.Label();
            this.btnChonCongDan = new System.Windows.Forms.Button();
            this.grbChong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrv_KetHon)).BeginInit();
            this.grbVo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ketHonBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grbChong
            // 
            this.grbChong.Controls.Add(this.txtbCMND_Chong);
            this.grbChong.Controls.Add(this.label16);
            this.grbChong.Controls.Add(this.dtpNgaySinh_Chong);
            this.grbChong.Controls.Add(this.txtbQueQuan_Chong);
            this.grbChong.Controls.Add(this.txtbQuocTich_Chong);
            this.grbChong.Controls.Add(this.txtbDanToc_Chong);
            this.grbChong.Controls.Add(this.txtbHoTen_Chong);
            this.grbChong.Controls.Add(this.label6);
            this.grbChong.Controls.Add(this.label7);
            this.grbChong.Controls.Add(this.label8);
            this.grbChong.Controls.Add(this.label9);
            this.grbChong.Controls.Add(this.label10);
            this.grbChong.Controls.Add(this.btn_ChooseChong);
            this.grbChong.Location = new System.Drawing.Point(27, 36);
            this.grbChong.Name = "grbChong";
            this.grbChong.Size = new System.Drawing.Size(326, 210);
            this.grbChong.TabIndex = 47;
            this.grbChong.TabStop = false;
            this.grbChong.Text = "Thông tin người chồng";
            // 
            // txtbCMND_Chong
            // 
            this.txtbCMND_Chong.Location = new System.Drawing.Point(85, 49);
            this.txtbCMND_Chong.Name = "txtbCMND_Chong";
            this.txtbCMND_Chong.Size = new System.Drawing.Size(149, 20);
            this.txtbCMND_Chong.TabIndex = 46;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 49);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(39, 13);
            this.label16.TabIndex = 45;
            this.label16.Text = "CMND";
            // 
            // dtpNgaySinh_Chong
            // 
            this.dtpNgaySinh_Chong.CustomFormat = "dd/MM/yyyy";
            this.dtpNgaySinh_Chong.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh_Chong.Location = new System.Drawing.Point(85, 102);
            this.dtpNgaySinh_Chong.Name = "dtpNgaySinh_Chong";
            this.dtpNgaySinh_Chong.Size = new System.Drawing.Size(149, 20);
            this.dtpNgaySinh_Chong.TabIndex = 44;
            // 
            // txtbQueQuan_Chong
            // 
            this.txtbQueQuan_Chong.Location = new System.Drawing.Point(85, 176);
            this.txtbQueQuan_Chong.Name = "txtbQueQuan_Chong";
            this.txtbQueQuan_Chong.Size = new System.Drawing.Size(149, 20);
            this.txtbQueQuan_Chong.TabIndex = 43;
            // 
            // txtbQuocTich_Chong
            // 
            this.txtbQuocTich_Chong.Location = new System.Drawing.Point(85, 150);
            this.txtbQuocTich_Chong.Name = "txtbQuocTich_Chong";
            this.txtbQuocTich_Chong.Size = new System.Drawing.Size(149, 20);
            this.txtbQuocTich_Chong.TabIndex = 42;
            // 
            // txtbDanToc_Chong
            // 
            this.txtbDanToc_Chong.Location = new System.Drawing.Point(85, 127);
            this.txtbDanToc_Chong.Name = "txtbDanToc_Chong";
            this.txtbDanToc_Chong.Size = new System.Drawing.Size(149, 20);
            this.txtbDanToc_Chong.TabIndex = 41;
            // 
            // txtbHoTen_Chong
            // 
            this.txtbHoTen_Chong.Location = new System.Drawing.Point(85, 76);
            this.txtbHoTen_Chong.Name = "txtbHoTen_Chong";
            this.txtbHoTen_Chong.Size = new System.Drawing.Size(149, 20);
            this.txtbHoTen_Chong.TabIndex = 40;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 39;
            this.label6.Text = "Quê quán";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Quốc tịch";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "Dân tộc";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "Ngày sinh";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "Họ tên";
            // 
            // btn_ChooseChong
            // 
            this.btn_ChooseChong.Location = new System.Drawing.Point(47, 19);
            this.btn_ChooseChong.Name = "btn_ChooseChong";
            this.btn_ChooseChong.Size = new System.Drawing.Size(152, 23);
            this.btn_ChooseChong.TabIndex = 34;
            this.btn_ChooseChong.Text = "Chọn thông tin người chồng";
            this.btn_ChooseChong.UseVisualStyleBackColor = true;
            this.btn_ChooseChong.Click += new System.EventHandler(this.btn_ChooseChong_Click);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(272, 304);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(74, 23);
            this.btnThem.TabIndex = 49;
            this.btnThem.Text = "Thêm hồ sơ";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnXacNhan
            // 
            this.btnXacNhan.Location = new System.Drawing.Point(353, 304);
            this.btnXacNhan.Name = "btnXacNhan";
            this.btnXacNhan.Size = new System.Drawing.Size(95, 23);
            this.btnXacNhan.TabIndex = 50;
            this.btnXacNhan.Text = "Xác nhận hồ sơ";
            this.btnXacNhan.UseVisualStyleBackColor = true;
            this.btnXacNhan.Click += new System.EventHandler(this.btnXacNhan_Click);
            // 
            // btnDuyet
            // 
            this.btnDuyet.Location = new System.Drawing.Point(456, 304);
            this.btnDuyet.Name = "btnDuyet";
            this.btnDuyet.Size = new System.Drawing.Size(95, 23);
            this.btnDuyet.TabIndex = 51;
            this.btnDuyet.Text = "Duyệt hồ sơ";
            this.btnDuyet.UseVisualStyleBackColor = true;
            this.btnDuyet.Click += new System.EventHandler(this.btnDuyet_Click);
            // 
            // btnTraKQ
            // 
            this.btnTraKQ.Location = new System.Drawing.Point(560, 304);
            this.btnTraKQ.Name = "btnTraKQ";
            this.btnTraKQ.Size = new System.Drawing.Size(95, 23);
            this.btnTraKQ.TabIndex = 52;
            this.btnTraKQ.Text = "Trả kết quả";
            this.btnTraKQ.UseVisualStyleBackColor = true;
            this.btnTraKQ.Click += new System.EventHandler(this.btnTraKQ_Click);
            // 
            // dgrv_KetHon
            // 
            this.dgrv_KetHon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrv_KetHon.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaKH,
            this.IDNguoiChong,
            this.HoTenChong,
            this.IDNguoiVo,
            this.HoTenVo,
            this.TrangThai_MoTa,
            this.NgayHen,
            this.NgayTra,
            this.TrangThai});
            this.dgrv_KetHon.Location = new System.Drawing.Point(12, 342);
            this.dgrv_KetHon.Name = "dgrv_KetHon";
            this.dgrv_KetHon.RowHeadersWidth = 51;
            this.dgrv_KetHon.Size = new System.Drawing.Size(776, 268);
            this.dgrv_KetHon.TabIndex = 53;
            this.dgrv_KetHon.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrv_KetHon_CellClick);
            // 
            // MaKH
            // 
            this.MaKH.HeaderText = "MaKH";
            this.MaKH.MinimumWidth = 6;
            this.MaKH.Name = "MaKH";
            this.MaKH.Width = 125;
            // 
            // IDNguoiChong
            // 
            this.IDNguoiChong.HeaderText = "IDNguoiChong";
            this.IDNguoiChong.MinimumWidth = 6;
            this.IDNguoiChong.Name = "IDNguoiChong";
            this.IDNguoiChong.Width = 125;
            // 
            // HoTenChong
            // 
            this.HoTenChong.HeaderText = "Họ tên chồng";
            this.HoTenChong.MinimumWidth = 6;
            this.HoTenChong.Name = "HoTenChong";
            this.HoTenChong.Width = 125;
            // 
            // IDNguoiVo
            // 
            this.IDNguoiVo.HeaderText = "IDNguoiVo";
            this.IDNguoiVo.MinimumWidth = 6;
            this.IDNguoiVo.Name = "IDNguoiVo";
            this.IDNguoiVo.Width = 125;
            // 
            // HoTenVo
            // 
            this.HoTenVo.HeaderText = "Họ tên vợ";
            this.HoTenVo.MinimumWidth = 6;
            this.HoTenVo.Name = "HoTenVo";
            this.HoTenVo.Width = 125;
            // 
            // TrangThai_MoTa
            // 
            this.TrangThai_MoTa.HeaderText = "Trạng thái";
            this.TrangThai_MoTa.MinimumWidth = 6;
            this.TrangThai_MoTa.Name = "TrangThai_MoTa";
            this.TrangThai_MoTa.Width = 125;
            // 
            // NgayHen
            // 
            this.NgayHen.HeaderText = "Ngày hẹn trả";
            this.NgayHen.MinimumWidth = 6;
            this.NgayHen.Name = "NgayHen";
            this.NgayHen.Width = 125;
            // 
            // NgayTra
            // 
            this.NgayTra.HeaderText = "Ngày trả";
            this.NgayTra.MinimumWidth = 6;
            this.NgayTra.Name = "NgayTra";
            this.NgayTra.Width = 125;
            // 
            // TrangThai
            // 
            this.TrangThai.HeaderText = "Mã trậng thái";
            this.TrangThai.MinimumWidth = 6;
            this.TrangThai.Name = "TrangThai";
            this.TrangThai.Visible = false;
            this.TrangThai.Width = 125;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Tất cả",
            "Tiếp nhận, chờ xác nhận",
            "Đã xác minh, chờ duyệt",
            "Không xác minh",
            "Đã duyệt",
            "Không duyệt",
            "Đã trả",
            "Đã thanh toán"});
            this.comboBox1.Location = new System.Drawing.Point(417, 267);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(150, 21);
            this.comboBox1.TabIndex = 54;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(359, 271);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Trạng thái";
            // 
            // lblTTTHS
            // 
            this.lblTTTHS.AutoSize = true;
            this.lblTTTHS.Location = new System.Drawing.Point(9, 9);
            this.lblTTTHS.Name = "lblTTTHS";
            this.lblTTTHS.Size = new System.Drawing.Size(90, 13);
            this.lblTTTHS.TabIndex = 55;
            this.lblTTTHS.Text = "Trạng thái hồ sơ: ";
            this.lblTTTHS.Visible = false;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Location = new System.Drawing.Point(105, 9);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(41, 13);
            this.lblTrangThai.TabIndex = 56;
            this.lblTrangThai.Text = "label20";
            this.lblTrangThai.Visible = false;
            // 
            // btn_TimKiem
            // 
            this.btn_TimKiem.Location = new System.Drawing.Point(575, 266);
            this.btn_TimKiem.Name = "btn_TimKiem";
            this.btn_TimKiem.Size = new System.Drawing.Size(74, 23);
            this.btn_TimKiem.TabIndex = 57;
            this.btn_TimKiem.Text = "Tìm kiếm";
            this.btn_TimKiem.UseVisualStyleBackColor = true;
            this.btn_TimKiem.Click += new System.EventHandler(this.btn_TimKiem_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Location = new System.Drawing.Point(680, 266);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(94, 23);
            this.btnHuy.TabIndex = 58;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // btnBackToMenu
            // 
            this.btnBackToMenu.Location = new System.Drawing.Point(680, 295);
            this.btnBackToMenu.Name = "btnBackToMenu";
            this.btnBackToMenu.Size = new System.Drawing.Size(94, 23);
            this.btnBackToMenu.TabIndex = 59;
            this.btnBackToMenu.Text = "Quay về Menu";
            this.btnBackToMenu.UseVisualStyleBackColor = true;
            this.btnBackToMenu.Click += new System.EventHandler(this.btnBackToMenu_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(38, 310);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(68, 13);
            this.label19.TabIndex = 28;
            this.label19.Text = "Ngày hẹn trả";
            // 
            // dtpHenTra
            // 
            this.dtpHenTra.CustomFormat = "dd/MM/yyyy";
            this.dtpHenTra.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHenTra.Location = new System.Drawing.Point(114, 306);
            this.dtpHenTra.Name = "dtpHenTra";
            this.dtpHenTra.Size = new System.Drawing.Size(116, 20);
            this.dtpHenTra.TabIndex = 60;
            // 
            // grbVo
            // 
            this.grbVo.Controls.Add(this.txtbCMND_Vo);
            this.grbVo.Controls.Add(this.label2);
            this.grbVo.Controls.Add(this.dtpNgaySinh_Vo);
            this.grbVo.Controls.Add(this.txtbQueQuan_Vo);
            this.grbVo.Controls.Add(this.txtbQuocTich_Vo);
            this.grbVo.Controls.Add(this.txtbDanToc_Vo);
            this.grbVo.Controls.Add(this.txtbHoTen_Vo);
            this.grbVo.Controls.Add(this.label3);
            this.grbVo.Controls.Add(this.label4);
            this.grbVo.Controls.Add(this.label5);
            this.grbVo.Controls.Add(this.label11);
            this.grbVo.Controls.Add(this.label13);
            this.grbVo.Controls.Add(this.btn_ChooseVo);
            this.grbVo.Location = new System.Drawing.Point(388, 36);
            this.grbVo.Name = "grbVo";
            this.grbVo.Size = new System.Drawing.Size(326, 210);
            this.grbVo.TabIndex = 64;
            this.grbVo.TabStop = false;
            this.grbVo.Text = "Thông tin người vợ";
            // 
            // txtbCMND_Vo
            // 
            this.txtbCMND_Vo.Location = new System.Drawing.Point(85, 49);
            this.txtbCMND_Vo.Name = "txtbCMND_Vo";
            this.txtbCMND_Vo.Size = new System.Drawing.Size(149, 20);
            this.txtbCMND_Vo.TabIndex = 46;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 45;
            this.label2.Text = "CMND";
            // 
            // dtpNgaySinh_Vo
            // 
            this.dtpNgaySinh_Vo.CustomFormat = "dd/MM/yyyy";
            this.dtpNgaySinh_Vo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgaySinh_Vo.Location = new System.Drawing.Point(85, 102);
            this.dtpNgaySinh_Vo.Name = "dtpNgaySinh_Vo";
            this.dtpNgaySinh_Vo.Size = new System.Drawing.Size(149, 20);
            this.dtpNgaySinh_Vo.TabIndex = 44;
            // 
            // txtbQueQuan_Vo
            // 
            this.txtbQueQuan_Vo.Location = new System.Drawing.Point(85, 176);
            this.txtbQueQuan_Vo.Name = "txtbQueQuan_Vo";
            this.txtbQueQuan_Vo.Size = new System.Drawing.Size(149, 20);
            this.txtbQueQuan_Vo.TabIndex = 43;
            // 
            // txtbQuocTich_Vo
            // 
            this.txtbQuocTich_Vo.Location = new System.Drawing.Point(85, 150);
            this.txtbQuocTich_Vo.Name = "txtbQuocTich_Vo";
            this.txtbQuocTich_Vo.Size = new System.Drawing.Size(149, 20);
            this.txtbQuocTich_Vo.TabIndex = 42;
            // 
            // txtbDanToc_Vo
            // 
            this.txtbDanToc_Vo.Location = new System.Drawing.Point(85, 127);
            this.txtbDanToc_Vo.Name = "txtbDanToc_Vo";
            this.txtbDanToc_Vo.Size = new System.Drawing.Size(149, 20);
            this.txtbDanToc_Vo.TabIndex = 41;
            // 
            // txtbHoTen_Vo
            // 
            this.txtbHoTen_Vo.Location = new System.Drawing.Point(85, 76);
            this.txtbHoTen_Vo.Name = "txtbHoTen_Vo";
            this.txtbHoTen_Vo.Size = new System.Drawing.Size(149, 20);
            this.txtbHoTen_Vo.TabIndex = 40;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Quê quán";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Quốc tịch";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "Dân tộc";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "Ngày sinh";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 76);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Họ tên";
            // 
            // btn_ChooseVo
            // 
            this.btn_ChooseVo.Location = new System.Drawing.Point(47, 19);
            this.btn_ChooseVo.Name = "btn_ChooseVo";
            this.btn_ChooseVo.Size = new System.Drawing.Size(152, 23);
            this.btn_ChooseVo.TabIndex = 34;
            this.btn_ChooseVo.Text = "Chọn thông tin người vợ";
            this.btn_ChooseVo.UseVisualStyleBackColor = true;
            this.btn_ChooseVo.Click += new System.EventHandler(this.btn_ChooseVo_Click);
            // 
            // ketHonBindingSource
            // 
            this.ketHonBindingSource.DataMember = "KetHon";
            // 
            // lblHoTenNguoiYeuCau
            // 
            this.lblHoTenNguoiYeuCau.AutoSize = true;
            this.lblHoTenNguoiYeuCau.Location = new System.Drawing.Point(180, 271);
            this.lblHoTenNguoiYeuCau.Name = "lblHoTenNguoiYeuCau";
            this.lblHoTenNguoiYeuCau.Size = new System.Drawing.Size(76, 13);
            this.lblHoTenNguoiYeuCau.TabIndex = 67;
            this.lblHoTenNguoiYeuCau.Text = "Nguyễn Văn A";
            // 
            // lblNguoiYeuCau
            // 
            this.lblNguoiYeuCau.AutoSize = true;
            this.lblNguoiYeuCau.Location = new System.Drawing.Point(106, 271);
            this.lblNguoiYeuCau.Name = "lblNguoiYeuCau";
            this.lblNguoiYeuCau.Size = new System.Drawing.Size(79, 13);
            this.lblNguoiYeuCau.TabIndex = 66;
            this.lblNguoiYeuCau.Text = "Người yêu cầu:";
            // 
            // btnChonCongDan
            // 
            this.btnChonCongDan.Location = new System.Drawing.Point(28, 258);
            this.btnChonCongDan.Name = "btnChonCongDan";
            this.btnChonCongDan.Size = new System.Drawing.Size(64, 39);
            this.btnChonCongDan.TabIndex = 65;
            this.btnChonCongDan.Text = "Chọn công dân";
            this.btnChonCongDan.UseVisualStyleBackColor = true;
            this.btnChonCongDan.Click += new System.EventHandler(this.btnChonCongDan_Click);
            // 
            // FormQLKetHon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 616);
            this.ControlBox = false;
            this.Controls.Add(this.lblHoTenNguoiYeuCau);
            this.Controls.Add(this.lblNguoiYeuCau);
            this.Controls.Add(this.btnChonCongDan);
            this.Controls.Add(this.grbVo);
            this.Controls.Add(this.dtpHenTra);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.btnBackToMenu);
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.btn_TimKiem);
            this.Controls.Add(this.lblTrangThai);
            this.Controls.Add(this.lblTTTHS);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.dgrv_KetHon);
            this.Controls.Add(this.btnTraKQ);
            this.Controls.Add(this.btnDuyet);
            this.Controls.Add(this.btnXacNhan);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.grbChong);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormQLKetHon";
            this.Text = "Đăng ký kết hôn";
            this.Load += new System.EventHandler(this.FormQLKetHon_Load);
            this.Shown += new System.EventHandler(this.FormQLKetHon_Shown);
            this.grbChong.ResumeLayout(false);
            this.grbChong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrv_KetHon)).EndInit();
            this.grbVo.ResumeLayout(false);
            this.grbVo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ketHonBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox grbChong;
        private System.Windows.Forms.TextBox txtbCMND_Chong;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh_Chong;
        private System.Windows.Forms.TextBox txtbQueQuan_Chong;
        private System.Windows.Forms.TextBox txtbQuocTich_Chong;
        private System.Windows.Forms.TextBox txtbDanToc_Chong;
        private System.Windows.Forms.TextBox txtbHoTen_Chong;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_ChooseChong;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnXacNhan;
        private System.Windows.Forms.Button btnDuyet;
        private System.Windows.Forms.Button btnTraKQ;
        private System.Windows.Forms.DataGridView dgrv_KetHon;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblTTTHS;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Button btn_TimKiem;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Button btnBackToMenu;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DateTimePicker dtpHenTra;
        private System.Windows.Forms.GroupBox grbVo;
        private System.Windows.Forms.TextBox txtbCMND_Vo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpNgaySinh_Vo;
        private System.Windows.Forms.TextBox txtbQueQuan_Vo;
        private System.Windows.Forms.TextBox txtbQuocTich_Vo;
        private System.Windows.Forms.TextBox txtbDanToc_Vo;
        private System.Windows.Forms.TextBox txtbHoTen_Vo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_ChooseVo;

        private System.Windows.Forms.BindingSource ketHonBindingSource;
   
        private System.Windows.Forms.DataGridViewTextBoxColumn MaKH;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDNguoiChong;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTenChong;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDNguoiVo;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTenVo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai_MoTa;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayHen;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayTra;
        private System.Windows.Forms.DataGridViewTextBoxColumn TrangThai;
        private System.Windows.Forms.Label lblHoTenNguoiYeuCau;
        private System.Windows.Forms.Label lblNguoiYeuCau;
        private System.Windows.Forms.Button btnChonCongDan;
    }
}