﻿namespace QuanLyHoTich.FormQLTaiKhoan
{
    partial class FormCapTaiKhoan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtbMatKhau = new System.Windows.Forms.TextBox();
            this.txtbMatKhau1 = new System.Windows.Forms.TextBox();
            this.txtTaiKhoan = new System.Windows.Forms.TextBox();
            this.lblTK = new System.Windows.Forms.Label();
            this.lblTenCongChuc = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnHuy = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(139, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(120, 20);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Cấp tài khoản";
            // 
            // txtbMatKhau
            // 
            this.txtbMatKhau.Location = new System.Drawing.Point(207, 125);
            this.txtbMatKhau.Name = "txtbMatKhau";
            this.txtbMatKhau.PasswordChar = '*';
            this.txtbMatKhau.Size = new System.Drawing.Size(176, 20);
            this.txtbMatKhau.TabIndex = 2;
            // 
            // txtbMatKhau1
            // 
            this.txtbMatKhau1.Location = new System.Drawing.Point(207, 161);
            this.txtbMatKhau1.Name = "txtbMatKhau1";
            this.txtbMatKhau1.PasswordChar = '*';
            this.txtbMatKhau1.Size = new System.Drawing.Size(176, 20);
            this.txtbMatKhau1.TabIndex = 3;
            // 
            // txtTaiKhoan
            // 
            this.txtTaiKhoan.Location = new System.Drawing.Point(207, 89);
            this.txtTaiKhoan.Name = "txtTaiKhoan";
            this.txtTaiKhoan.Size = new System.Drawing.Size(176, 20);
            this.txtTaiKhoan.TabIndex = 1;
            // 
            // lblTK
            // 
            this.lblTK.AutoSize = true;
            this.lblTK.Location = new System.Drawing.Point(52, 51);
            this.lblTK.Name = "lblTK";
            this.lblTK.Size = new System.Drawing.Size(154, 13);
            this.lblTK.TabIndex = 4;
            this.lblTK.Text = "Cấp tài khoản cho công chức: ";
            // 
            // lblTenCongChuc
            // 
            this.lblTenCongChuc.AutoSize = true;
            this.lblTenCongChuc.Location = new System.Drawing.Point(212, 51);
            this.lblTenCongChuc.Name = "lblTenCongChuc";
            this.lblTenCongChuc.Size = new System.Drawing.Size(86, 13);
            this.lblTenCongChuc.TabIndex = 5;
            this.lblTenCongChuc.Text = "lblTenCongChuc";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tài khoản";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Mật khẩu";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Nhập lại mật khẩu";
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(108, 212);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnHuy
            // 
            this.btnHuy.Location = new System.Drawing.Point(215, 212);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(75, 23);
            this.btnHuy.TabIndex = 5;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // FormCapTaiKhoan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 256);
            this.ControlBox = false;
            this.Controls.Add(this.btnHuy);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblTenCongChuc);
            this.Controls.Add(this.lblTK);
            this.Controls.Add(this.txtTaiKhoan);
            this.Controls.Add(this.txtbMatKhau1);
            this.Controls.Add(this.txtbMatKhau);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormCapTaiKhoan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cấp tài khoản";
            this.Shown += new System.EventHandler(this.FormCapTaiKhoan_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtbMatKhau;
        private System.Windows.Forms.TextBox txtbMatKhau1;
        private System.Windows.Forms.TextBox txtTaiKhoan;
        private System.Windows.Forms.Label lblTK;
        private System.Windows.Forms.Label lblTenCongChuc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnHuy;
    }
}