﻿using QLHT.Entity;
using QLHT.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHoTich.FormQLTaiKhoan
{
    public partial class FormCapTaiKhoan : Form
    {
        private static FormCapTaiKhoan instance;

        public FormCapTaiKhoan()
        {
            InitializeComponent();
        }

        public static FormCapTaiKhoan Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormCapTaiKhoan();
                }
                return instance;
            }
        }
        public string title;
        public string tenCongChuc;
        public int IDCongChuc = -1;
        //Mặc định là cấp tk, truyền tham số dưới để lấy lại mk cho tk
        public bool isLayLaiMatKhau = false;
        public bool isDoiMatKhau = false;
        public bool isCapTaiKhoan = false;
        CongChucModel congChucModel = new CongChucModel();
        public int MaCongChuc = -1;
        public CongChuc CongChuc;
        private void FormCapTaiKhoan_Shown(object sender, EventArgs e)
        {
            if (isLayLaiMatKhau)
            {
                title = "Lấy lại mật khẩu";
                lblTK.Text = "Lấy lại mật khẩu cho công chức: ";                
            }
            else if (isDoiMatKhau)
            {
                title = "Đổi mật khẩu";
                lblTK.Text = "Đổi mật khẩu cho công chức: ";
            }
            else if (isCapTaiKhoan)
            {
                title = "Đổi mật khẩu";
                lblTK.Text = "Cấp tài khoản cho công chức: ";
            }
            lblTitle.Text = title;
            this.Text = title;
            CongChuc = congChucModel.GetCongChucByMaCongChuc(IDCongChuc);
            if (!String.IsNullOrEmpty(CongChuc.HoTen))
            {
                lblTenCongChuc.Text = CongChuc.HoTen;
            }
            if (!String.IsNullOrEmpty(CongChuc.TaiKhoan))
            {
                txtTaiKhoan.Enabled = false;
                txtTaiKhoan.Text = CongChuc.TaiKhoan;
            }
            else
            {
                txtTaiKhoan.Enabled = true;
                txtTaiKhoan.Text = "";
            }
            if (IDCongChuc == -1)
            {
                txtTaiKhoan.Enabled = true;
                txtTaiKhoan.Text = "";
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            FormCapTaiKhoan.Instance.Close();
        }
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            string matKhau = txtbMatKhau.Text.Trim();
            string matKhau1 = txtbMatKhau1.Text.Trim();
            if (CheckTaiKhoan(CongChuc.MaCongChuc, txtTaiKhoan.Text.Trim()))
            {
                if (!String.IsNullOrEmpty(matKhau) && matKhau.Equals(matKhau1))
                {
                    matKhau = CreateMD5(matKhau);
                    if (isLayLaiMatKhau)
                    {
                        if (congChucModel.CapNhatMatKhau(CongChuc.MaCongChuc, matKhau))
                        {
                            MessageBox.Show("Thao tác thành công!");
                            FormQLCongChuc.Instance.LoadDuLieu();
                            FormCapTaiKhoan.Instance.Close();
                        }
                        else
                            MessageBox.Show("Thao tác không thành công!");
                    }
                    else if (isDoiMatKhau)
                    {
                        if (congChucModel.CapNhatMatKhau(CongChuc.MaCongChuc, matKhau))
                        {
                            MessageBox.Show("Thao tác thành công!");
                            FormQLCongChuc.Instance.LoadDuLieu();
                            FormCapTaiKhoan.Instance.Close();
                        }
                        else
                            MessageBox.Show("Thao tác không thành công!");
                    }
                    else if (isCapTaiKhoan)
                    {
                        string TaiKhoan = txtTaiKhoan.Text.Trim();
                        if (!String.IsNullOrEmpty(TaiKhoan))
                        {
                            if (congChucModel.CapTaiKhoan(CongChuc.MaCongChuc, TaiKhoan, matKhau))
                            {
                                MessageBox.Show("Thao tác thành công!");
                                FormQLCongChuc.Instance.LoadDuLieu();
                                FormCapTaiKhoan.Instance.Close();
                            }
                            else
                                MessageBox.Show("Thao tác không thành công!");
                        }
                        else
                        {
                            MessageBox.Show("Không được để trống trường tài khoản!");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Mật khẩu không khớp!");
                }
            }
            else
            {
                MessageBox.Show("Trùng tài khoản!");
            }
        }

        private bool CheckTaiKhoan(int maCongChuc, string Taikhoan)
        {
            if (congChucModel.CheckTaiKhoan(maCongChuc, Taikhoan))
                return true;
            return false;
        }
    }
}
