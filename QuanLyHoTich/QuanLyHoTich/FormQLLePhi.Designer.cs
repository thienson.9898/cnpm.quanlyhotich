﻿namespace QuanLyHoTich
{
    partial class FormQLLePhi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLyDo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCapNhapCT = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnXoa2 = new System.Windows.Forms.Button();
            this.cbCongChuc = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMaLePhi = new System.Windows.Forms.TextBox();
            this.btnThemCT = new System.Windows.Forms.Button();
            this.cbCongDan = new System.Windows.Forms.ComboBox();
            this.btnCapNhap = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btnXoa1 = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.txtMaChiTietLePhi = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSoTien = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnHuy = new System.Windows.Forms.Button();
            this.cbLePhi = new System.Windows.Forms.ComboBox();
            this.cbThuTuc = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnHuyLP = new System.Windows.Forms.Button();
            this.dtpNgayLap = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnBackToMenu = new System.Windows.Forms.Button();
            this.dgv_dsLePhi = new System.Windows.Forms.DataGridView();
            this.MaLePhi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaCongDan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTenCongDan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaCongChuc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTenCongChuc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayLap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_dsCTLePhi = new System.Windows.Forms.DataGridView();
            this.MaCTLePhi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaThuTuc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HoTenThuTuc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaLePhiCT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoTien = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LyDo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_dsLePhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_dsCTLePhi)).BeginInit();
            this.SuspendLayout();
            // 
            // txtLyDo
            // 
            this.txtLyDo.Location = new System.Drawing.Point(173, 227);
            this.txtLyDo.Margin = new System.Windows.Forms.Padding(4);
            this.txtLyDo.Name = "txtLyDo";
            this.txtLyDo.Size = new System.Drawing.Size(339, 22);
            this.txtLyDo.TabIndex = 49;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 230);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 17);
            this.label4.TabIndex = 48;
            this.label4.Text = "Lý do nộp";
            // 
            // btnCapNhapCT
            // 
            this.btnCapNhapCT.Location = new System.Drawing.Point(164, 294);
            this.btnCapNhapCT.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapNhapCT.Name = "btnCapNhapCT";
            this.btnCapNhapCT.Size = new System.Drawing.Size(100, 28);
            this.btnCapNhapCT.TabIndex = 47;
            this.btnCapNhapCT.Text = "Cập nhật";
            this.btnCapNhapCT.UseVisualStyleBackColor = true;
            this.btnCapNhapCT.Click += new System.EventHandler(this.btnCapNhapCT_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 129);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 17);
            this.label3.TabIndex = 45;
            this.label3.Text = "Mã lệ phí";
            // 
            // btnXoa2
            // 
            this.btnXoa2.Location = new System.Drawing.Point(302, 294);
            this.btnXoa2.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa2.Name = "btnXoa2";
            this.btnXoa2.Size = new System.Drawing.Size(100, 28);
            this.btnXoa2.TabIndex = 9;
            this.btnXoa2.Text = "Xóa";
            this.btnXoa2.UseVisualStyleBackColor = true;
            // 
            // cbCongChuc
            // 
            this.cbCongChuc.FormattingEnabled = true;
            this.cbCongChuc.Location = new System.Drawing.Point(135, 126);
            this.cbCongChuc.Name = "cbCongChuc";
            this.cbCongChuc.Size = new System.Drawing.Size(346, 24);
            this.cbCongChuc.TabIndex = 51;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 184);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 43;
            this.label5.Text = "Ngày lập";
            // 
            // txtMaLePhi
            // 
            this.txtMaLePhi.Location = new System.Drawing.Point(135, 19);
            this.txtMaLePhi.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaLePhi.Name = "txtMaLePhi";
            this.txtMaLePhi.Size = new System.Drawing.Size(346, 22);
            this.txtMaLePhi.TabIndex = 38;
            // 
            // btnThemCT
            // 
            this.btnThemCT.Location = new System.Drawing.Point(38, 294);
            this.btnThemCT.Margin = new System.Windows.Forms.Padding(4);
            this.btnThemCT.Name = "btnThemCT";
            this.btnThemCT.Size = new System.Drawing.Size(100, 28);
            this.btnThemCT.TabIndex = 9;
            this.btnThemCT.Text = "Thêm";
            this.btnThemCT.UseVisualStyleBackColor = true;
            this.btnThemCT.Click += new System.EventHandler(this.btnThemCT_Click);
            // 
            // cbCongDan
            // 
            this.cbCongDan.FormattingEnabled = true;
            this.cbCongDan.Location = new System.Drawing.Point(135, 70);
            this.cbCongDan.Name = "cbCongDan";
            this.cbCongDan.Size = new System.Drawing.Size(346, 24);
            this.cbCongDan.TabIndex = 50;
            // 
            // btnCapNhap
            // 
            this.btnCapNhap.Location = new System.Drawing.Point(161, 294);
            this.btnCapNhap.Margin = new System.Windows.Forms.Padding(4);
            this.btnCapNhap.Name = "btnCapNhap";
            this.btnCapNhap.Size = new System.Drawing.Size(100, 28);
            this.btnCapNhap.TabIndex = 47;
            this.btnCapNhap.Text = "Cập nhật";
            this.btnCapNhap.UseVisualStyleBackColor = true;
            this.btnCapNhap.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 129);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 17);
            this.label9.TabIndex = 45;
            this.label9.Text = "Mã công chức";
            // 
            // btnXoa1
            // 
            this.btnXoa1.Location = new System.Drawing.Point(285, 294);
            this.btnXoa1.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa1.Name = "btnXoa1";
            this.btnXoa1.Size = new System.Drawing.Size(100, 28);
            this.btnXoa1.TabIndex = 9;
            this.btnXoa1.Text = "Xóa";
            this.btnXoa1.UseVisualStyleBackColor = true;
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(39, 294);
            this.btnThem.Margin = new System.Windows.Forms.Padding(4);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(100, 28);
            this.btnThem.TabIndex = 9;
            this.btnThem.Text = "Thêm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // txtMaChiTietLePhi
            // 
            this.txtMaChiTietLePhi.Location = new System.Drawing.Point(173, 20);
            this.txtMaChiTietLePhi.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaChiTietLePhi.Name = "txtMaChiTietLePhi";
            this.txtMaChiTietLePhi.Size = new System.Drawing.Size(339, 22);
            this.txtMaChiTietLePhi.TabIndex = 38;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(28, 23);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 17);
            this.label10.TabIndex = 37;
            this.label10.Text = "Mã chi tiết lệ phí";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 37;
            this.label2.Text = "Mã lệ phí";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(907, 75);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(140, 17);
            this.label12.TabIndex = 57;
            this.label12.Text = "Quản lý chi tiết lệ phí";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 70);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Mã công dân";
            // 
            // txtSoTien
            // 
            this.txtSoTien.Location = new System.Drawing.Point(173, 180);
            this.txtSoTien.Margin = new System.Windows.Forms.Padding(4);
            this.txtSoTien.Name = "txtSoTien";
            this.txtSoTien.Size = new System.Drawing.Size(339, 22);
            this.txtSoTien.TabIndex = 51;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnHuy);
            this.panel1.Controls.Add(this.cbLePhi);
            this.panel1.Controls.Add(this.cbThuTuc);
            this.panel1.Controls.Add(this.txtSoTien);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txtLyDo);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.btnCapNhapCT);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btnXoa2);
            this.panel1.Controls.Add(this.btnThemCT);
            this.panel1.Controls.Add(this.txtMaChiTietLePhi);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Location = new System.Drawing.Point(683, 96);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(578, 326);
            this.panel1.TabIndex = 58;
            // 
            // btnHuy
            // 
            this.btnHuy.Location = new System.Drawing.Point(434, 294);
            this.btnHuy.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(100, 28);
            this.btnHuy.TabIndex = 54;
            this.btnHuy.Text = "Hủy";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // cbLePhi
            // 
            this.cbLePhi.FormattingEnabled = true;
            this.cbLePhi.Location = new System.Drawing.Point(173, 127);
            this.cbLePhi.Name = "cbLePhi";
            this.cbLePhi.Size = new System.Drawing.Size(339, 24);
            this.cbLePhi.TabIndex = 53;
            // 
            // cbThuTuc
            // 
            this.cbThuTuc.FormattingEnabled = true;
            this.cbThuTuc.Location = new System.Drawing.Point(173, 71);
            this.cbThuTuc.Name = "cbThuTuc";
            this.cbThuTuc.Size = new System.Drawing.Size(339, 24);
            this.cbThuTuc.TabIndex = 52;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(38, 183);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 17);
            this.label13.TabIndex = 50;
            this.label13.Text = "Số tiền";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(38, 70);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 17);
            this.label11.TabIndex = 1;
            this.label11.Text = "Mã thủ tục";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(275, 75);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 17);
            this.label8.TabIndex = 50;
            this.label8.Text = "Quản lý lệ phí";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnHuyLP);
            this.panel2.Controls.Add(this.dtpNgayLap);
            this.panel2.Controls.Add(this.cbCongChuc);
            this.panel2.Controls.Add(this.cbCongDan);
            this.panel2.Controls.Add(this.btnCapNhap);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.btnXoa1);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.btnThem);
            this.panel2.Controls.Add(this.txtMaLePhi);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(13, 96);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(549, 326);
            this.panel2.TabIndex = 56;
            // 
            // btnHuyLP
            // 
            this.btnHuyLP.Location = new System.Drawing.Point(421, 294);
            this.btnHuyLP.Margin = new System.Windows.Forms.Padding(4);
            this.btnHuyLP.Name = "btnHuyLP";
            this.btnHuyLP.Size = new System.Drawing.Size(100, 28);
            this.btnHuyLP.TabIndex = 53;
            this.btnHuyLP.Text = "Hủy";
            this.btnHuyLP.UseVisualStyleBackColor = true;
            this.btnHuyLP.Click += new System.EventHandler(this.btnHuyLP_Click);
            // 
            // dtpNgayLap
            // 
            this.dtpNgayLap.CustomFormat = "dd/MM/yyyy";
            this.dtpNgayLap.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNgayLap.Location = new System.Drawing.Point(135, 179);
            this.dtpNgayLap.Margin = new System.Windows.Forms.Padding(4);
            this.dtpNgayLap.Name = "dtpNgayLap";
            this.dtpNgayLap.Size = new System.Drawing.Size(346, 22);
            this.dtpNgayLap.TabIndex = 52;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(907, 426);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 17);
            this.label1.TabIndex = 55;
            this.label1.Text = "Chi tiết lệ phí";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(246, 426);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 17);
            this.label6.TabIndex = 54;
            this.label6.Text = "Danh sách lệ phí";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(528, 35);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(165, 25);
            this.lblTitle.TabIndex = 51;
            this.lblTitle.Text = "QUẢN LÝ LỆ PHÍ";
            // 
            // btnBackToMenu
            // 
            this.btnBackToMenu.Location = new System.Drawing.Point(577, 535);
            this.btnBackToMenu.Margin = new System.Windows.Forms.Padding(4);
            this.btnBackToMenu.Name = "btnBackToMenu";
            this.btnBackToMenu.Size = new System.Drawing.Size(116, 28);
            this.btnBackToMenu.TabIndex = 63;
            this.btnBackToMenu.Text = "Quay về Menu";
            this.btnBackToMenu.UseVisualStyleBackColor = true;
            this.btnBackToMenu.Click += new System.EventHandler(this.btnBackToMenu_Click);
            // 
            // dgv_dsLePhi
            // 
            this.dgv_dsLePhi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_dsLePhi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaLePhi,
            this.MaCongDan,
            this.HoTenCongDan,
            this.MaCongChuc,
            this.HoTenCongChuc,
            this.NgayLap});
            this.dgv_dsLePhi.Location = new System.Drawing.Point(13, 447);
            this.dgv_dsLePhi.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_dsLePhi.Name = "dgv_dsLePhi";
            this.dgv_dsLePhi.Size = new System.Drawing.Size(549, 253);
            this.dgv_dsLePhi.TabIndex = 64;
            this.dgv_dsLePhi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_dsLePhi_CellClick);
            // 
            // MaLePhi
            // 
            this.MaLePhi.HeaderText = "ID";
            this.MaLePhi.Name = "MaLePhi";
            this.MaLePhi.ReadOnly = true;
            this.MaLePhi.Width = 20;
            // 
            // MaCongDan
            // 
            this.MaCongDan.HeaderText = "Mã công dân";
            this.MaCongDan.Name = "MaCongDan";
            // 
            // HoTenCongDan
            // 
            this.HoTenCongDan.HeaderText = "Tên công dân";
            this.HoTenCongDan.Name = "HoTenCongDan";
            this.HoTenCongDan.ReadOnly = true;
            // 
            // MaCongChuc
            // 
            this.MaCongChuc.HeaderText = "Mã công chức";
            this.MaCongChuc.Name = "MaCongChuc";
            // 
            // HoTenCongChuc
            // 
            this.HoTenCongChuc.HeaderText = "Tên công chức";
            this.HoTenCongChuc.Name = "HoTenCongChuc";
            this.HoTenCongChuc.ReadOnly = true;
            // 
            // NgayLap
            // 
            this.NgayLap.HeaderText = "Ngày lập";
            this.NgayLap.Name = "NgayLap";
            this.NgayLap.ReadOnly = true;
            // 
            // dgv_dsCTLePhi
            // 
            this.dgv_dsCTLePhi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_dsCTLePhi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaCTLePhi,
            this.MaThuTuc,
            this.HoTenThuTuc,
            this.MaLePhiCT,
            this.SoTien,
            this.LyDo});
            this.dgv_dsCTLePhi.Location = new System.Drawing.Point(701, 447);
            this.dgv_dsCTLePhi.Margin = new System.Windows.Forms.Padding(4);
            this.dgv_dsCTLePhi.Name = "dgv_dsCTLePhi";
            this.dgv_dsCTLePhi.Size = new System.Drawing.Size(549, 253);
            this.dgv_dsCTLePhi.TabIndex = 65;
            this.dgv_dsCTLePhi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_dsCTLePhi_CellClick);
            // 
            // MaCTLePhi
            // 
            this.MaCTLePhi.HeaderText = "ID";
            this.MaCTLePhi.Name = "MaCTLePhi";
            this.MaCTLePhi.ReadOnly = true;
            this.MaCTLePhi.Width = 20;
            // 
            // MaThuTuc
            // 
            this.MaThuTuc.HeaderText = "Mã thủ tục";
            this.MaThuTuc.Name = "MaThuTuc";
            // 
            // HoTenThuTuc
            // 
            this.HoTenThuTuc.HeaderText = "Tên thủ tục";
            this.HoTenThuTuc.Name = "HoTenThuTuc";
            this.HoTenThuTuc.ReadOnly = true;
            // 
            // MaLePhiCT
            // 
            this.MaLePhiCT.HeaderText = "Mã lệ phí";
            this.MaLePhiCT.Name = "MaLePhiCT";
            // 
            // SoTien
            // 
            this.SoTien.HeaderText = "Số Tiền";
            this.SoTien.Name = "SoTien";
            this.SoTien.ReadOnly = true;
            // 
            // LyDo
            // 
            this.LyDo.HeaderText = "Lý do";
            this.LyDo.Name = "LyDo";
            // 
            // FormQLLePhi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 713);
            this.Controls.Add(this.dgv_dsCTLePhi);
            this.Controls.Add(this.dgv_dsLePhi);
            this.Controls.Add(this.btnBackToMenu);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblTitle);
            this.Name = "FormQLLePhi";
            this.Text = "FormQLLePhi";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_dsLePhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_dsCTLePhi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtLyDo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCapNhapCT;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnXoa2;
        private System.Windows.Forms.ComboBox cbCongChuc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMaLePhi;
        private System.Windows.Forms.Button btnThemCT;
        private System.Windows.Forms.ComboBox cbCongDan;
        private System.Windows.Forms.Button btnCapNhap;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnXoa1;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.TextBox txtMaChiTietLePhi;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSoTien;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnBackToMenu;
        private System.Windows.Forms.ComboBox cbLePhi;
        private System.Windows.Forms.ComboBox cbThuTuc;
        private System.Windows.Forms.DataGridView dgv_dsLePhi;
        private System.Windows.Forms.DateTimePicker dtpNgayLap;
        private System.Windows.Forms.DataGridView dgv_dsCTLePhi;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaLePhi;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCongDan;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTenCongDan;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCongChuc;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTenCongChuc;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayLap;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaCTLePhi;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaThuTuc;
        private System.Windows.Forms.DataGridViewTextBoxColumn HoTenThuTuc;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaLePhiCT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoTien;
        private System.Windows.Forms.DataGridViewTextBoxColumn LyDo;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Button btnHuyLP;
    }
}