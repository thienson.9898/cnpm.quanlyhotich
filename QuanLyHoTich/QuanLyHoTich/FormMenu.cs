﻿using QLHT.Model;
using QuanLyHoTich.FormQLTaiKhoan;
using QuanLyHoTich.FormQLHoTich;
using QuanLyHoTich.FormQLThayDoiCaiChinhBoSungHoTich;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHoTich
{
    public partial class FormMenu : Form
    {
        private static FormMenu instance;
        public static FormMenu Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormMenu();
                }
                return instance;
            }
        }
        CongChucModel congChucModel = new CongChucModel();
        public FormMenu()
        {
            InitializeComponent();
        }

        private void DangXuatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDangNhap.Instance.Show();
            FormMenu.Instance.Close();
        }

        private void QLKhaiSinhToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormQLKhaiSinh.Instance.Show();
            FormMenu.Instance.Hide();
        }

        private void btn_QLCongDan_Click(object sender, EventArgs e)
        {
            FormQLCongDan.Instance.Show();
            FormMenu.Instance.Hide();
        }

        private void FormMenu_Load(object sender, EventArgs e)
        {

        }

        private void QLKhaiTuToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormQLKhaiTu.Instance.Show();
            FormMenu.Instance.Hide();


        }

        private void QLCongChucToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormQLCongChuc.Instance.Show();
            FormMenu.Instance.Hide();
        }

        private void DoiMatKhauToolStripMenuItem1_Click(object sender, EventArgs e)
        {
           
        }
        public void GetQuyen()
        {
            string Quyen = new CongChucModel().GetQuyen(Constants.MaCongChucDangNhap);
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanTiepNhanVaTraKQ))
            {
                Constants.isTN_TKQ = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatCongChucCapXa))
            {
                Constants.isCCCX = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatLanhDao))
            {
                Constants.isLD = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanVanPhong))
            {
                // btnTraKQ.Enabled = true;
                Constants.isVP = true;
            }
            if (Quyen.Contains(BoPhanModel.TenVietTatBoPhanKTTC))
            {
                Constants.isKTTC = true;
            }
            //Get trạng thái hồ sơ để bật/tắt nút
        }

        private void FormMenu_Shown(object sender, EventArgs e)
        {
            GetQuyen();
            lblLoiChao.Text = "Chào " + congChucModel.GetCongChucByMaCongChuc(int.Parse(Constants.MaCongChucDangNhap)).HoTen;
            if(Constants.isLD)
            {
                QLCongChucToolStripMenuItem.Enabled = true;
            }
            else
            {
                QLCongChucToolStripMenuItem.Enabled = false;
            }
        }

        private void CapTaiKhoanToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //isGetCongChucChuaCoTaiKhoan 
            FormQLCongChuc.Instance.isGetCongChucChuaCoTaiKhoan = true;
            FormQLCongChuc.Instance.Show();
        }

        private void DoiMatKhauToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            FormCapTaiKhoan.Instance.IDCongChuc = int.Parse(Constants.MaCongChucDangNhap);
            FormCapTaiKhoan.Instance.isDoiMatKhau = true;
            FormCapTaiKhoan.Instance.ShowDialog();
        }

        private void QLGiamHoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormQLHoTich.FormQLGiamHo.Instance.Show();
            FormMenu.Instance.Hide();
        }

        private void QLKetHonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            FormQLKetHon.Instance.Show();
            FormMenu.Instance.Hide();
        }

        private void btn_QLLePhi_Click(object sender, EventArgs e)
        {
            FormQLLePhi.Instance.Show();
            FormMenu.Instance.Hide();
        }

        private void btnQLBCTK_Click(object sender, EventArgs e)
        {
            FormThongKeLePhi.Instance.Show();
            FormMenu.Instance.Hide();
        }

        private void QLThayDoiHoTichToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormQLThayDoiBoSung.Instance.Show();
            FormMenu.Instance.Hide();
        }
    }
}
