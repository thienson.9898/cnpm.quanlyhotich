﻿namespace QuanLyHoTich
{
    partial class FormMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMenu));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.QLCongChucToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DangXuatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.DoiMatKhauToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.CapTaiKhoanToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.khóaTàiKhoảnToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.QLKhaiSinhToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.QLKhaiTuToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.QLKetHonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.QLGiamHoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.QLThayDoiHoTichToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_QLLePhi = new System.Windows.Forms.ToolStripLabel();
            this.btnQLBCTK = new System.Windows.Forms.ToolStripLabel();
            this.btn_QLCongDan = new System.Windows.Forms.ToolStripLabel();
            this.lblLoiChao = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(19, 27);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(506, 315);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(185, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(186, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "QUẢN LÝ HỘ TỊCH";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Location = new System.Drawing.Point(128, 69);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(538, 357);
            this.panel2.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(351, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "NHÓM 1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2,
            this.toolStripDropDownButton3,
            this.btn_QLLePhi,
            this.btnQLBCTK,
            this.btn_QLCongDan});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(798, 25);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.QLCongChucToolStripMenuItem,
            this.DangXuatToolStripMenuItem});
            this.toolStripDropDownButton1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(70, 22);
            this.toolStripDropDownButton1.Text = "Hệ thống";
            // 
            // QLCongChucToolStripMenuItem
            // 
            this.QLCongChucToolStripMenuItem.Name = "QLCongChucToolStripMenuItem";
            this.QLCongChucToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.QLCongChucToolStripMenuItem.Text = "Quản lý công chức";
            this.QLCongChucToolStripMenuItem.Click += new System.EventHandler(this.QLCongChucToolStripMenuItem_Click);
            // 
            // DangXuatToolStripMenuItem
            // 
            this.DangXuatToolStripMenuItem.Name = "DangXuatToolStripMenuItem";
            this.DangXuatToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.DangXuatToolStripMenuItem.Text = "Đăng xuất";
            this.DangXuatToolStripMenuItem.Click += new System.EventHandler(this.DangXuatToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DoiMatKhauToolStripMenuItem1,
            this.CapTaiKhoanToolStripMenuItem1,
            this.khóaTàiKhoảnToolStripMenuItem1});
            this.toolStripDropDownButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton2.Image")));
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(113, 22);
            this.toolStripDropDownButton2.Text = "Quản lý tài khoản";
            // 
            // DoiMatKhauToolStripMenuItem1
            // 
            this.DoiMatKhauToolStripMenuItem1.Name = "DoiMatKhauToolStripMenuItem1";
            this.DoiMatKhauToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.DoiMatKhauToolStripMenuItem1.Text = "Đổi mật khẩu";
            this.DoiMatKhauToolStripMenuItem1.Click += new System.EventHandler(this.DoiMatKhauToolStripMenuItem1_Click_1);
            // 
            // CapTaiKhoanToolStripMenuItem1
            // 
            this.CapTaiKhoanToolStripMenuItem1.Name = "CapTaiKhoanToolStripMenuItem1";
            this.CapTaiKhoanToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.CapTaiKhoanToolStripMenuItem1.Text = "Cấp tài khoản";
            this.CapTaiKhoanToolStripMenuItem1.Click += new System.EventHandler(this.CapTaiKhoanToolStripMenuItem1_Click);
            // 
            // khóaTàiKhoảnToolStripMenuItem1
            // 
            this.khóaTàiKhoảnToolStripMenuItem1.Name = "khóaTàiKhoảnToolStripMenuItem1";
            this.khóaTàiKhoảnToolStripMenuItem1.Size = new System.Drawing.Size(153, 22);
            this.khóaTàiKhoảnToolStripMenuItem1.Text = "Khóa tài khoản";
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.QLKhaiSinhToolStripMenuItem1,
            this.QLKhaiTuToolStripMenuItem1,
            this.QLKetHonToolStripMenuItem,
            this.QLGiamHoToolStripMenuItem1,
            this.QLThayDoiHoTichToolStripMenuItem});
            this.toolStripDropDownButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton3.Image")));
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(101, 22);
            this.toolStripDropDownButton3.Text = "Quản lý hộ tịch";
            // 
            // QLKhaiSinhToolStripMenuItem1
            // 
            this.QLKhaiSinhToolStripMenuItem1.Name = "QLKhaiSinhToolStripMenuItem1";
            this.QLKhaiSinhToolStripMenuItem1.Size = new System.Drawing.Size(267, 22);
            this.QLKhaiSinhToolStripMenuItem1.Text = "Quản lý khai sinh";
            this.QLKhaiSinhToolStripMenuItem1.Click += new System.EventHandler(this.QLKhaiSinhToolStripMenuItem1_Click);
            // 
            // QLKhaiTuToolStripMenuItem1
            // 
            this.QLKhaiTuToolStripMenuItem1.Name = "QLKhaiTuToolStripMenuItem1";
            this.QLKhaiTuToolStripMenuItem1.Size = new System.Drawing.Size(267, 22);
            this.QLKhaiTuToolStripMenuItem1.Text = "Quản lý khai tử";
            this.QLKhaiTuToolStripMenuItem1.Click += new System.EventHandler(this.QLKhaiTuToolStripMenuItem1_Click);
            // 
            // QLKetHonToolStripMenuItem
            // 
            this.QLKetHonToolStripMenuItem.Name = "QLKetHonToolStripMenuItem";
            this.QLKetHonToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.QLKetHonToolStripMenuItem.Text = "Quản lý kết hôn";
            this.QLKetHonToolStripMenuItem.Click += new System.EventHandler(this.QLKetHonToolStripMenuItem_Click);
            // 
            // QLGiamHoToolStripMenuItem1
            // 
            this.QLGiamHoToolStripMenuItem1.Name = "QLGiamHoToolStripMenuItem1";
            this.QLGiamHoToolStripMenuItem1.Size = new System.Drawing.Size(267, 22);
            this.QLGiamHoToolStripMenuItem1.Text = "Quản lý giám hộ";
            this.QLGiamHoToolStripMenuItem1.Click += new System.EventHandler(this.QLGiamHoToolStripMenuItem1_Click);
            // 
            // QLThayDoiHoTichToolStripMenuItem
            // 
            this.QLThayDoiHoTichToolStripMenuItem.Name = "QLThayDoiHoTichToolStripMenuItem";
            this.QLThayDoiHoTichToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.QLThayDoiHoTichToolStripMenuItem.Text = "Quản lý thay đổi và cải chính hộ tịch";
            this.QLThayDoiHoTichToolStripMenuItem.Click += new System.EventHandler(this.QLThayDoiHoTichToolStripMenuItem_Click);
            // 
            // btn_QLLePhi
            // 
            this.btn_QLLePhi.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btn_QLLePhi.Name = "btn_QLLePhi";
            this.btn_QLLePhi.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btn_QLLePhi.Size = new System.Drawing.Size(145, 25);
            this.btn_QLLePhi.Text = "Quản lý thanh toán lệ phí";
            this.btn_QLLePhi.Click += new System.EventHandler(this.btn_QLLePhi_Click);
            // 
            // btnQLBCTK
            // 
            this.btnQLBCTK.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btnQLBCTK.Name = "btnQLBCTK";
            this.btnQLBCTK.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btnQLBCTK.Size = new System.Drawing.Size(147, 25);
            this.btnQLBCTK.Text = "Quản lý báo cáo thống kê";
            this.btnQLBCTK.Click += new System.EventHandler(this.btnQLBCTK_Click);
            // 
            // btn_QLCongDan
            // 
            this.btn_QLCongDan.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btn_QLCongDan.Name = "btn_QLCongDan";
            this.btn_QLCongDan.Padding = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btn_QLCongDan.Size = new System.Drawing.Size(105, 25);
            this.btn_QLCongDan.Text = "Quản lý công dân";
            this.btn_QLCongDan.Click += new System.EventHandler(this.btn_QLCongDan_Click);
            // 
            // lblLoiChao
            // 
            this.lblLoiChao.AutoSize = true;
            this.lblLoiChao.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoiChao.Location = new System.Drawing.Point(302, 449);
            this.lblLoiChao.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.lblLoiChao.Name = "lblLoiChao";
            this.lblLoiChao.Size = new System.Drawing.Size(90, 25);
            this.lblLoiChao.TabIndex = 2;
            this.lblLoiChao.Text = "Chào .....";
            // 
            // FormMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 501);
            this.ControlBox = false;
            this.Controls.Add(this.lblLoiChao);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu chức năng";
            this.Shown += new System.EventHandler(this.FormMenu_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel btn_QLLePhi;
        private System.Windows.Forms.ToolStripLabel btnQLBCTK;
        private System.Windows.Forms.ToolStripLabel btn_QLCongDan;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem DangXuatToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem CapTaiKhoanToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem khóaTàiKhoảnToolStripMenuItem1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem QLKhaiSinhToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem QLKhaiTuToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem QLKetHonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem QLGiamHoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem QLThayDoiHoTichToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem QLCongChucToolStripMenuItem;
        private System.Windows.Forms.Label lblLoiChao;
        private System.Windows.Forms.ToolStripMenuItem DoiMatKhauToolStripMenuItem1;
    }
}