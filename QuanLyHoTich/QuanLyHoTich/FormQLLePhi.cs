﻿using QLHT.Entity;
using QLHT.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHoTich
{
    public partial class FormQLLePhi : Form
    {
        public string str = @"Data Source=.\SQLEXPRESS;Initial Catalog=qlhotich;Integrated Security=True";
        public bool isChonLePhi = false;
        LePhiModel lePhiModel = new LePhiModel();
        CTLPModel ctlpModel = new CTLPModel();
        private static FormQLLePhi instance;
        public static FormQLLePhi Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormQLLePhi();
                }
                return instance;
            }
        }

        public FormQLLePhi()
        {
            InitializeComponent();
            loadMaCongDan();
            loadMaCongChuc();
            loadMaThuTuc();
            loadMaLePhi();
            LoadDuLieu();
            loadChiTietLePhi();
            txtMaLePhi.Enabled = false;
            txtMaChiTietLePhi.Enabled = false;
            btnXoa1.Enabled = false;
            btnXoa2.Enabled = false;
            btnCapNhap.Enabled = false;
            btnCapNhapCT.Enabled = false;

        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {
            FormQLLePhi.Instance.Dispose();
            FormMenu.Instance.Show();
        }

        public void loadMaCongDan()
        {
            using (SqlConnection conn = new SqlConnection(str))
            {
                try
                {
                    string query = "Select MaCongDan from CongDan";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataTable tb = new DataTable();
                    da.Fill(tb);
                    cbCongDan.DataSource = tb;
                    cbCongDan.ValueMember = tb.Columns[0].ColumnName;
                    cbCongDan.DisplayMember = tb.Columns[0].ColumnName;
                }
                catch (Exception)
                {
                    MessageBox.Show("Error!");
                }
            }
        }

        public void loadMaCongChuc()
        {
            using (SqlConnection conn = new SqlConnection(str))
            {
                try
                {
                    string query = "Select MaCongChuc from CongChuc";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataTable tb = new DataTable();
                    da.Fill(tb);
                    cbCongChuc.DataSource = tb;
                    cbCongChuc.ValueMember = tb.Columns[0].ColumnName;
                    cbCongChuc.DisplayMember = tb.Columns[0].ColumnName;
                }
                catch (Exception)
                {
                    MessageBox.Show("Error!");
                }
            }
        }

        public void loadMaThuTuc()
        {
            using (SqlConnection conn = new SqlConnection(str))
            {
                try
                {
                    string query = "Select MaThuTuc from ThuTuc";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataTable tb = new DataTable();
                    da.Fill(tb);
                    cbThuTuc.DataSource = tb;
                    cbThuTuc.ValueMember = tb.Columns[0].ColumnName;
                    cbThuTuc.DisplayMember = tb.Columns[0].ColumnName;
                }
                catch (Exception)
                {
                    MessageBox.Show("Error!");
                }
            }
        }

        public void loadMaLePhi()
        {
            using (SqlConnection conn = new SqlConnection(str))
            {
                try
                {
                    string query = "Select MaLePhi from LePhi";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataTable tb = new DataTable();
                    da.Fill(tb);
                    cbLePhi.DataSource = tb;
                    cbLePhi.ValueMember = tb.Columns[0].ColumnName;
                    cbLePhi.DisplayMember = tb.Columns[0].ColumnName;
                }
                catch (Exception)
                {
                    MessageBox.Show("Error!");
                }
            }
        }

        CTLP ctlp = new CTLP();
        public void loadChiTietLePhi()
        {
            dgv_dsCTLePhi.Rows.Clear();
            List<CTLP> dsCTLePhi = ctlpModel.GetData();
            foreach (CTLP ctlp in dsCTLePhi)
            {
                var index = dgv_dsCTLePhi.Rows.Add();
                dgv_dsCTLePhi.Rows[index].Cells["MaCTLePhi"].Value = ctlp.MaCTLePhi;
                dgv_dsCTLePhi.Rows[index].Cells["MaThuTuc"].Value = ctlp.MaThuTuc;
                dgv_dsCTLePhi.Rows[index].Cells["HoTenThuTuc"].Value = ctlp.HoTenTT;
                dgv_dsCTLePhi.Rows[index].Cells["MaLePhiCT"].Value = ctlp.MaLePhi;
                dgv_dsCTLePhi.Rows[index].Cells["SoTien"].Value = ctlp.SoTienNop;
                dgv_dsCTLePhi.Rows[index].Cells["LyDo"].Value = ctlp.LyDoNop;
            }
        }

        LePhi lePhi = new LePhi();
        public void LoadDuLieu()
        {
            dgv_dsLePhi.Rows.Clear();
            List<LePhi> dsLePhi = lePhiModel.GetData();
            foreach (LePhi lePhi in dsLePhi)
            {
                var index = dgv_dsLePhi.Rows.Add();
                dgv_dsLePhi.Rows[index].Cells["MaLePhi"].Value = lePhi.MaLePhi;
                dgv_dsLePhi.Rows[index].Cells["MaCongDan"].Value = lePhi.MaCongDan;
                dgv_dsLePhi.Rows[index].Cells["MaCongChuc"].Value = lePhi.MaCongChuc;
                dgv_dsLePhi.Rows[index].Cells["HoTenCongDan"].Value = lePhi.HoTenCD;
                dgv_dsLePhi.Rows[index].Cells["HoTenCongChuc"].Value = lePhi.HoTenCC;
                dgv_dsLePhi.Rows[index].Cells["NgayLap"].Value = lePhi.NgayLap.ToString("dd/MM/yyyy");
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            int MaCongDan = Convert.ToInt32(cbCongDan.SelectedValue);
            int MaCongChuc = Convert.ToInt32(cbCongChuc.SelectedValue);
            DateTime NgayLap = dtpNgayLap.Value;
            LePhi lePhi = new LePhi(MaCongDan, MaCongChuc, NgayLap);
            if (!String.IsNullOrEmpty(lePhiModel.ThemLePhi(lePhi)))
            {
                MessageBox.Show("Thêm lệ phí thành công");
                LoadDuLieu();
            }
        }

        private void dgv_dsLePhi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnCapNhap.Enabled = true;
            try
            {
                if (!isChonLePhi)
                {
                    btnThem.Enabled = false;
                    btnCapNhap.Enabled = true;
                }
                int rowIndex = e.RowIndex;
                if (rowIndex >= 0)
                {
                    txtMaLePhi.Text = dgv_dsLePhi.Rows[rowIndex].Cells["MaLePhi"].Value.ToString();
                    dtpNgayLap.Value = DateTime.ParseExact(dgv_dsLePhi.Rows[rowIndex].Cells["NgayLap"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                    cbCongChuc.SelectedValue = dgv_dsLePhi.Rows[rowIndex].Cells["MaCongChuc"].Value.ToString();
                    cbCongDan.SelectedValue = dgv_dsLePhi.Rows[rowIndex].Cells["MaCongDan"].Value.ToString();
                }
            }
            catch
            {

            }
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            int.TryParse(txtMaLePhi.Text.Trim(), out int MaLePhi);
            int MaCongDan = Convert.ToInt32(cbCongDan.SelectedValue);
            int MaCongChuc = Convert.ToInt32(cbCongChuc.SelectedValue);
            DateTime NgayLap = dtpNgayLap.Value;
            LePhi lePhi = new LePhi(MaLePhi,MaCongDan, MaCongChuc, NgayLap);
            if (lePhiModel.UpdateLePhi(lePhi))
            {
                MessageBox.Show("Cập nhật lệ phí thành công");
                btnCapNhap.Enabled = false;
                btnThem.Enabled = true;
                txtMaLePhi.Text = null;
                LoadDuLieu();
            }
        }

        private void btnThemCT_Click(object sender, EventArgs e)
        {
            int MaLePhi = Convert.ToInt32(cbLePhi.SelectedValue);
            int MaThuTuc = Convert.ToInt32(cbThuTuc.SelectedValue);
            string LyDoNop = String.IsNullOrEmpty(txtLyDo.Text.Trim()) ? "N/A" : txtLyDo.Text.Trim();
            float SoTienNop = String.IsNullOrEmpty(txtSoTien.Text.Trim()) ? 0 : float.Parse(txtSoTien.Text.Trim());
            CTLP ctlp = new CTLP(MaThuTuc, MaLePhi, LyDoNop, SoTienNop);
            if (!String.IsNullOrEmpty(ctlpModel.ThemCTLePhi(ctlp)))
            {
                MessageBox.Show("Thêm chi tiết lệ phí thành công");
                txtSoTien.Text = null;
                txtLyDo.Text = null;
                loadChiTietLePhi();
            }
        }

        private void dgv_dsCTLePhi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!isChonLePhi)
                {
                    btnThemCT.Enabled = false;
                    btnCapNhapCT.Enabled = true;
                }
                int rowIndex = e.RowIndex;
                if (rowIndex >= 0)
                {
                    txtMaChiTietLePhi.Text = dgv_dsCTLePhi.Rows[rowIndex].Cells["MaCTLePhi"].Value.ToString();
                    cbThuTuc.SelectedValue = dgv_dsCTLePhi.Rows[rowIndex].Cells["MaThuTuc"].Value.ToString();
                    cbLePhi.SelectedValue = dgv_dsCTLePhi.Rows[rowIndex].Cells["MaLePhiCT"].Value.ToString();
                    txtSoTien.Text = dgv_dsCTLePhi.Rows[rowIndex].Cells["SoTien"].Value.ToString();
                    txtLyDo.Text = dgv_dsCTLePhi.Rows[rowIndex].Cells["LyDo"].Value.ToString();
                }
            }
            catch
            {

            }
        }

        private void btnCapNhapCT_Click(object sender, EventArgs e)
        {
            int.TryParse(txtMaChiTietLePhi.Text.Trim(), out int MaCTLePhi);
            int MaLePhi = Convert.ToInt32(cbLePhi.SelectedValue);
            int MaThuTuc = Convert.ToInt32(cbThuTuc.SelectedValue);
            string LyDoNop = txtLyDo.Text.Trim();
            float SoTienNop = float.Parse(txtSoTien.Text.Trim());
            CTLP ctlp = new CTLP(MaCTLePhi,MaThuTuc, MaLePhi, LyDoNop, SoTienNop);
            if (ctlpModel.UpdateCTLePhi(ctlp))
            {
                MessageBox.Show("Cập nhật chi tiết lệ phí thành công");
                btnCapNhapCT.Enabled = false;
                btnThemCT.Enabled = true;
                txtSoTien.Text = null;
                txtLyDo.Text = null;
                loadChiTietLePhi();
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            //clear
            txtSoTien.Text = "";
            txtLyDo.Text = "";
            txtMaChiTietLePhi.Text = "";
            if (!isChonLePhi)
            {
                btnThemCT.Enabled = true;
                btnCapNhapCT.Enabled = false;
            }
        }

        private void btnHuyLP_Click(object sender, EventArgs e)
        {
            txtMaLePhi.Text = "";
            if (!isChonLePhi)
            {
                btnThem.Enabled = true;
                btnCapNhap.Enabled = false;
            }
        }
    }
}
