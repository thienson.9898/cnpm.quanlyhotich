﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLHT.Model;
using QLHT.Entity;

namespace QuanLyHoTich
{
    public partial class FormQLCongDan : Form
    {
        private static FormQLCongDan instance;
        public static FormQLCongDan Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormQLCongDan();
                }
                return instance;
            }
        }
        CongDanModel CongDanModel = new CongDanModel();

        public  bool isFormThayDoiClick = false;
        public bool isChonCongDan = false;
        public string txtTitle = "";
        public bool AllGioiTinh = false;
        public bool GioiTinhNam = true;//Mac Dinh check nam
        public CongDan congDan;
        public bool isFromQLKhaiSinh;
        public bool isContainKhaiSinh = false;
        public bool isContainKetHon = false;
        public bool isContainAdult = true;
        public bool chonThanhCong = false;
        //
        private string _containKhaiSinh = "-1";
        private string _containAdult = "1";

        public FormQLCongDan()
        {
            InitializeComponent();
            dtpNgaySinh.CustomFormat = "dd/MM/yyyy";

            if (isChonCongDan)
            {
                // Fixbug lần 1 nvHieu
                if(isFormThayDoiClick==false)
                {
                    if (!String.IsNullOrEmpty(txtTitle))
                    {
                        lblTitle.Text = txtTitle;
                    }
                    if (GioiTinhNam)
                    {
                        rdNam.Checked = true;
                    }
                    else
                    {
                        rdNu.Checked = true;
                    }
                    if (AllGioiTinh)
                    {
                        rdNam.Checked = false;
                        rdNu.Checked = false;
                    }
                }               
                /////////////
                btnHuy.Text = "Hủy chọn";
                btnThem.Enabled = false;
                btnCapNhat.Enabled = false;
            }
            LoadDuLieu();
        }

        private void LoadDuLieu(string ContainKhaiSinh = "-1", string ContainAdult = "1")
        {
            int.TryParse(txtbMaCongDan.Text.Trim(), out int MaCongDan);
            string HoTen = txtbHoTen.Text.Trim();
            string NgaySinh = "";
            //string NgaySinh = dtpNgaySinh.Value.ToString("MM/dd/yyyy");
            string GioiTinh = rdNam.Checked ? "1" : rdNu.Checked ? "0" : "-1";
            string QueQuan = txtbQueQuan.Text.Trim();
            string ThuongTru = txtbThuongTru.Text.Trim();
            string DanToc = txtbDanToc.Text.Trim();
            string QuocTich = txtbQuocTich.Text.Trim();
            string CMND = txtbCMND.Text.Trim();

            dgrv_CongDan.Rows.Clear();
            List<CongDan> dsCongDan = CongDanModel.GetData(HoTen, NgaySinh, GioiTinh, QueQuan, ThuongTru, DanToc, QuocTich, CMND, ContainKhaiSinh, ContainAdult);
            foreach (CongDan cd in dsCongDan)
            {
                var index = dgrv_CongDan.Rows.Add();
                dgrv_CongDan.Rows[index].Cells["MaCongDan"].Value = cd.MaCongDan;
                dgrv_CongDan.Rows[index].Cells["HoTen"].Value = cd.HoTen;
                dgrv_CongDan.Rows[index].Cells["NgaySinh"].Value = cd.NgaySinh.ToString("dd/MM/yyyy");
                dgrv_CongDan.Rows[index].Cells["GioiTinh"].Value = cd.GioiTinh == true ? "Nam" : "Nữ";
                dgrv_CongDan.Rows[index].Cells["QueQuan"].Value = cd.QueQuan;
                dgrv_CongDan.Rows[index].Cells["ThuongTru"].Value = cd.ThuongTru;
                dgrv_CongDan.Rows[index].Cells["DanToc"].Value = cd.DanToc;
                dgrv_CongDan.Rows[index].Cells["QuocTich"].Value = cd.QuocTich;
                dgrv_CongDan.Rows[index].Cells["CMND"].Value = cd.CMND;
            }
        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {
            FormQLCongDan.Instance.Dispose();
            FormMenu.Instance.Show();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            foreach (Control ctr in this.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }//
            if (!isChonCongDan)
            {
                btnThem.Enabled = true;
                btnCapNhat.Enabled = false;
            }
            else
            {
                FormQLKhaiSinh.Instance.chonThanhCong = false;
                this.Close();
            }
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            int.TryParse(txtbMaCongDan.Text.Trim(), out int MaCongDan);
            string HoTen = txtbHoTen.Text.Trim();
            DateTime NgaySinh = dtpNgaySinh.Value;
            bool GioiTinh = rdNam.Checked ? true : false;
            string QueQuan = txtbQueQuan.Text.Trim();
            string ThuongTru = txtbThuongTru.Text.Trim();
            string DanToc = txtbDanToc.Text.Trim();
            string QuocTich = txtbQuocTich.Text.Trim();
            string CMND = txtbCMND.Text.Trim();
            CongDan congDan = new CongDan(MaCongDan, HoTen, NgaySinh, GioiTinh, QueQuan, ThuongTru, DanToc, QuocTich, CMND, 1);
            if (CongDanModel.UpdateCongDan(congDan))
            {
                MessageBox.Show("Cập nhật dữ liệu thành công");
                btnHuy_Click(null, null);
                LoadDuLieu();
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if (ValidateCongDan())
            {
                string HoTen = txtbHoTen.Text.Trim();
                DateTime NgaySinh = dtpNgaySinh.Value;
                bool GioiTinh = rdNam.Checked ? true : rdNu.Checked ? false : true;
                string QueQuan = txtbQueQuan.Text.Trim();
                string ThuongTru = txtbThuongTru.Text.Trim();
                string DanToc = txtbDanToc.Text.Trim();
                string QuocTich = txtbQuocTich.Text.Trim();
                string CMND = txtbCMND.Text.Trim();
                CongDan congDan = new CongDan(HoTen, NgaySinh, GioiTinh, QueQuan, ThuongTru, DanToc, QuocTich, CMND, 1);
                if (!String.IsNullOrEmpty(CongDanModel.ThemCongDan(congDan)))
                {
                    MessageBox.Show("Thêm dữ liệu thành công");
                    btnHuy_Click(null, null);
                    LoadDuLieu();
                }
            }
        }

        private bool ValidateCongDan()
        {
            throw new NotImplementedException();
        }

        private void dgrv_CongDan_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!isChonCongDan)
                {
                    btnThem.Enabled = false;
                    btnCapNhat.Enabled = true;
                }
                int rowIndex = e.RowIndex;
                if (rowIndex >= 0)
                {
                    txtbMaCongDan.Text = dgrv_CongDan.Rows[rowIndex].Cells["MaCongDan"].Value.ToString();
                    txtbHoTen.Text = dgrv_CongDan.Rows[rowIndex].Cells["HoTen"].Value.ToString();
                    dtpNgaySinh.Value = DateTime.ParseExact(dgrv_CongDan.Rows[rowIndex].Cells["NgaySinh"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                    rdNam.Checked = dgrv_CongDan.Rows[rowIndex].Cells["GioiTinh"].Value.ToString() == "Nam" ? true : false;
                    rdNu.Checked = dgrv_CongDan.Rows[rowIndex].Cells["GioiTinh"].Value.ToString() == "Nữ" ? true : false;
                    txtbQueQuan.Text = dgrv_CongDan.Rows[rowIndex].Cells["QueQuan"].Value.ToString();
                    txtbThuongTru.Text = dgrv_CongDan.Rows[rowIndex].Cells["ThuongTru"].Value.ToString();
                    txtbDanToc.Text = dgrv_CongDan.Rows[rowIndex].Cells["DanToc"].Value.ToString();
                    txtbQuocTich.Text = dgrv_CongDan.Rows[rowIndex].Cells["QuocTich"].Value.ToString();
                    txtbCMND.Text = dgrv_CongDan.Rows[rowIndex].Cells["CMND"].Value.ToString();
                }
            }
            catch
            {

            }
        }

        private void btn_TimKiem_Click(object sender, EventArgs e)
        {
            int.TryParse(txtbMaCongDan.Text.Trim(), out int MaCongDan);
            string HoTen = txtbHoTen.Text.Trim();
            string NgaySinh = "";
            //string NgaySinh = dtpNgaySinh.Value.ToString("MM/dd/yyyy");
            string GioiTinh = rdNam.Checked ? "1" : rdNu.Checked ? "0" : "-1";
            string QueQuan = txtbQueQuan.Text.Trim();
            string ThuongTru = txtbThuongTru.Text.Trim();
            string DanToc = txtbDanToc.Text.Trim();
            string QuocTich = txtbQuocTich.Text.Trim();
            string CMND = txtbCMND.Text.Trim();

            dgrv_CongDan.Rows.Clear();
            List<CongDan> dsCongDan = CongDanModel.GetData(HoTen, NgaySinh, GioiTinh, QueQuan, ThuongTru, DanToc, QuocTich, CMND, _containKhaiSinh, _containAdult);
            foreach (CongDan cd in dsCongDan)
            {
                var index = dgrv_CongDan.Rows.Add();
                dgrv_CongDan.Rows[index].Cells["MaCongDan"].Value = cd.MaCongDan;
                dgrv_CongDan.Rows[index].Cells["HoTen"].Value = cd.HoTen;
                dgrv_CongDan.Rows[index].Cells["NgaySinh"].Value = cd.NgaySinh.ToString("dd/MM/yyyy");
                dgrv_CongDan.Rows[index].Cells["GioiTinh"].Value = cd.GioiTinh == true ? "Nam" : "Nữ";
                dgrv_CongDan.Rows[index].Cells["QueQuan"].Value = cd.QueQuan;
                dgrv_CongDan.Rows[index].Cells["ThuongTru"].Value = cd.ThuongTru;
                dgrv_CongDan.Rows[index].Cells["DanToc"].Value = cd.DanToc;
                dgrv_CongDan.Rows[index].Cells["QuocTich"].Value = cd.QuocTich;
                dgrv_CongDan.Rows[index].Cells["CMND"].Value = cd.CMND;
            }
        }
        public bool isBackToFormThayDoi = false;
        public bool isChonNguoiYeuCau = false;
        public bool isChonNguoiDeNghi = false;
        private void btnChon_Click(object sender, EventArgs e)
        {         
            try
            {               
                int.TryParse(txtbMaCongDan.Text.Trim(), out int MaCongDan);
                string HoTen = txtbHoTen.Text.Trim();
                DateTime NgaySinh = dtpNgaySinh.Value;
                bool GioiTinh = rdNam.Checked ? true : false;
                string QueQuan = txtbQueQuan.Text.Trim();
                string ThuongTru = txtbThuongTru.Text.Trim();
                string DanToc = txtbDanToc.Text.Trim();
                string QuocTich = txtbQuocTich.Text.Trim();
                string CMND = txtbCMND.Text.Trim();
                congDan = new CongDan(MaCongDan, HoTen, NgaySinh, GioiTinh, QueQuan, ThuongTru, DanToc, QuocTich, CMND, 1);
                chonThanhCong = true;
                if (isFormThayDoiClick == true)
                {
                    isBackToFormThayDoi = true;
                }
                //FormQLKhaiSinh.Instance.chonThanhCong = true;
                //FormQLKhaiSinh.Instance.congDanTmp = congDan;
            }
            catch (Exception ex)
            {
                chonThanhCong = false;
            }
            finally
            {                        
                this.Hide();
            }
        }

        private void FormQLCongDan_Shown(object sender, EventArgs e)
        {
            
                if (isChonCongDan)
                {
                    btnChon.Enabled = true;
                // Fixbug lần 1 nvHieu
                    if(isFormThayDoiClick == false)
                    {
                        if ((isContainKetHon && isContainKhaiSinh && !isContainAdult) || (isContainKhaiSinh && !isContainAdult))
                        {
                            _containKhaiSinh = "1";
                            _containAdult = "-1";
                            //LoadDuLieu(_containKhaiSinh, _containAdult);
                            btn_TimKiem_Click(null, null);
                        }
                        if ((isContainKetHon && isContainKhaiSinh && isContainAdult) || (isContainKhaiSinh && isContainAdult))
                        {
                            _containKhaiSinh = "1";
                            _containAdult = "1";
                            //LoadDuLieu(_containKhaiSinh, _containAdult);
                            btn_TimKiem_Click(null, null);
                        }
                        if ((!isContainKetHon && !isContainKhaiSinh && isContainAdult) || (!isContainKhaiSinh && isContainAdult))
                        {
                            _containKhaiSinh = "-1";
                            _containAdult = "1";
                            //LoadDuLieu(_containKhaiSinh, _containAdult);
                            btn_TimKiem_Click(null, null);
                        }

                        if (!String.IsNullOrEmpty(txtTitle))
                        {
                            lblTitle.Text = txtTitle;
                        }
                        if (!AllGioiTinh)
                        {
                            if (GioiTinhNam)
                            {
                                rdNam.Checked = true;
                                btn_TimKiem_Click(null, null);
                                grbGioiTinh.Enabled = false;
                            }
                            else
                            {
                                rdNu.Checked = true;
                                btn_TimKiem_Click(null, null);
                                grbGioiTinh.Enabled = false;
                            }
                        }
                    }
                 ///////////////////
                    btnHuy.Text = "Hủy chọn";
                    btnThem.Enabled = false;
                    btnCapNhat.Enabled = false;
                
            }
           
        }

        private void FormQLCongDan_Load(object sender, EventArgs e)
        {

        }
    }
}
