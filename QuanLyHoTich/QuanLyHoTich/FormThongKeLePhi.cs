﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using QLHT.Entity;
using QLHT.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;

namespace QuanLyHoTich
{
    public partial class FormThongKeLePhi : Form
    {
        public string str = @"Data Source=.\SQLEXPRESS;Initial Catalog=qlhotich;Integrated Security=True";
        private static FormThongKeLePhi instance;
        public static FormThongKeLePhi Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormThongKeLePhi();
                }
                return instance;
            }
        }
        public FormThongKeLePhi()
        {
            InitializeComponent();
        }

        private void btnThangNam_Click(object sender, EventArgs e)
        {
            if (cbThang.Text != "")
            {
                using (SqlConnection cnn = new SqlConnection(str))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "Proc_tinhTheoThangNam_ThongKe";
                        DateTime dt = DateTime.Now;
                        cmd.Parameters.AddWithValue("@thang", int.Parse(cbThang.Text));
                        cmd.Parameters.AddWithValue("@nam", String.IsNullOrEmpty(cbNam.Text.Trim()) ? dt.Year : int.Parse(cbNam.Text));
                        using (SqlDataAdapter ad = new SqlDataAdapter(cmd))
                        {
                            DataTable tb = new DataTable();
                            ad.Fill(tb);
                            dgvThongKeLePhi.DataSource = tb;
                        }
                    }
                }
                float TongLP = 0;
                for (int i = 0; i < dgvThongKeLePhi.RowCount - 1; i++)
                {
                    TongLP += float.Parse(dgvThongKeLePhi.Rows[i].Cells[5].Value.ToString());
                }
                CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                string tongLePhi = TongLP.ToString("#,###", cul.NumberFormat);

                label_LePhi.Text = tongLePhi;
            }
            else
            {
                using (SqlConnection cnn = new SqlConnection(str))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "Proc_tinhTheoNam_ThongKe";
                        DateTime dt = DateTime.Now;
                        cmd.Parameters.AddWithValue("@nam", String.IsNullOrEmpty(cbNam.Text.Trim()) ? dt.Year : int.Parse(cbNam.Text));
                        using (SqlDataAdapter ad = new SqlDataAdapter(cmd))
                        {
                            DataTable tb = new DataTable();
                            ad.Fill(tb);
                            dgvThongKeLePhi.DataSource = tb;
                        }
                    }
                }
                float TongLP = 0;
                for (int i = 0; i < dgvThongKeLePhi.RowCount - 1; i++)
                {
                    TongLP += float.Parse(dgvThongKeLePhi.Rows[i].Cells[5].Value.ToString());
                }

                CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN"); 
                string tongLePhi = TongLP.ToString("#,###", cul.NumberFormat);

                label_LePhi.Text = tongLePhi;
            }
        }

        private void btnKhoangNgay_Click(object sender, EventArgs e)
        {
            using (SqlConnection cnn = new SqlConnection(str))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_tinhTheoKhoangNgay";
                    cmd.Parameters.AddWithValue("@tu", dtpStart.Value);
                    cmd.Parameters.AddWithValue("@den", dtpEnd.Value);
                    using (SqlDataAdapter ad = new SqlDataAdapter(cmd))
                    {
                        DataTable tb = new DataTable();
                        ad.Fill(tb);
                        dgvThongKeLePhi.DataSource = tb;
                    }
                }
            }
            float TongLP = 0;
            for (int i = 0; i < dgvThongKeLePhi.RowCount - 1; i++)
            {
                TongLP += float.Parse(dgvThongKeLePhi.Rows[i].Cells[3].Value.ToString());
            }
            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   
            string tongLePhi = TongLP.ToString("#,###", cul.NumberFormat);

            label_LePhi.Text = tongLePhi;
        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {
            FormThongKeLePhi.Instance.Dispose();
            FormMenu.Instance.Show();
        }
    }
}
