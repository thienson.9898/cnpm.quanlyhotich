﻿using QLHT.Entity;
using QLHT.Model;
using QuanLyHoTich.FormHeThong;
using QuanLyHoTich.FormQLTaiKhoan;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHoTich
{
    public partial class FormQLCongChuc : Form
    {
        public FormQLCongChuc()
        {
            InitializeComponent();
        }
        CongChucModel CongChucModel = new CongChucModel();
        private static FormQLCongChuc instance;
        public static FormQLCongChuc Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormQLCongChuc();
                }
                return instance;
            }
        }
        CongChuc congChuc = new CongChuc();
        CongChuc currentCongChuc = new CongChuc();
        List<int> listID = new List<int>();
        BoPhan_CongChucModel BoPhan_CongChucModel = new BoPhan_CongChucModel();
        public bool isGetCongChucChuaCoTaiKhoan = false;
        public void LoadDuLieu()
        {
            int.TryParse(txtbMaCongChuc.Text.Trim(), out int MaCongDan);
            string HoTen = txtbHoTen.Text.Trim();
            string NgaySinh = "";
            //string NgaySinh = dtpNgaySinh.Value.ToString("MM/dd/yyyy");
            string GioiTinh = rdNam.Checked ? "1" : rdNu.Checked ? "0" : "-1";
            string QueQuan = txtbQueQuan.Text.Trim();
            string ChucVu = txtbChucVu.Text.Trim();
            string CMND = txtbCMND.Text.Trim();
            string TaiKhoan = txtbTaiKhoan.Text.Trim();
            List<CongChuc> dsCongChuc;
            dgrv_CongChuc.Rows.Clear();
            if (isGetCongChucChuaCoTaiKhoan)
            {
                dsCongChuc = CongChucModel.isGetCongChucChuaCoTaiKhoan();
            }else
            {
                dsCongChuc = CongChucModel.GetData();
            }
            foreach (CongChuc congChuc in dsCongChuc)
            {
                var index = dgrv_CongChuc.Rows.Add();
                dgrv_CongChuc.Rows[index].Cells["MaCongChuc"].Value = congChuc.MaCongChuc;
                dgrv_CongChuc.Rows[index].Cells["HoTen"].Value = congChuc.HoTen;
                dgrv_CongChuc.Rows[index].Cells["NgaySinh"].Value = congChuc.NgaySinh.ToString("dd/MM/yyyy");
                dgrv_CongChuc.Rows[index].Cells["GioiTinh"].Value = congChuc.GioiTinh == true ? "Nam" : "Nữ";
                dgrv_CongChuc.Rows[index].Cells["QueQuan"].Value = congChuc.QueQuan;
                dgrv_CongChuc.Rows[index].Cells["ChucVu"].Value = congChuc.ChucVu;
                dgrv_CongChuc.Rows[index].Cells["TaiKhoan"].Value = congChuc.TaiKhoan;
                dgrv_CongChuc.Rows[index].Cells["CMND"].Value = congChuc.CMND;
            }
            btnHuy_Click(null, null);
        }

        private void FormQLCongChuc_Load(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            CongChuc congChucInsert = new CongChuc();
            congChucInsert.HoTen = txtbHoTen.Text;
            congChucInsert.NgaySinh = dtpNgaySinh.Value;
            congChucInsert.GioiTinh = rdNam.Checked ? true : false;
            congChucInsert.QueQuan = txtbQueQuan.Text;
            congChucInsert.ChucVu = txtbChucVu.Text;
            congChucInsert.CMND = txtbCMND.Text;
            if (listID.Count > 0)
            {
                string IDCongChuc = CongChucModel.ThemCongChuc(congChucInsert);
                if (!String.IsNullOrEmpty(IDCongChuc))
                {
                    BoPhan_CongChucModel.Insert(int.Parse(IDCongChuc), listID);
                    MessageBox.Show("Thao tác thành công!");
                    LoadDuLieu();
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa chọn bộ phận cho công chức!");
            }
        }

        private void btnCapTaiKhoan_Click(object sender, EventArgs e)
        {
            if (btnCapTaiKhoan.Text == "Cấp tài khoản")
            {
                FormCapTaiKhoan.Instance.IDCongChuc = currentCongChuc.MaCongChuc;
                FormCapTaiKhoan.Instance.isCapTaiKhoan = true;
                FormCapTaiKhoan.Instance.ShowDialog();
            }
            else if (btnCapTaiKhoan.Text == "Lấy lại mật khẩu")
            {
                FormCapTaiKhoan.Instance.IDCongChuc = currentCongChuc.MaCongChuc;
                FormCapTaiKhoan.Instance.isLayLaiMatKhau = true;
                FormCapTaiKhoan.Instance.ShowDialog();
            }
        }

        private void dgrv_CongChuc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int rowIndex = e.RowIndex;
                if (rowIndex >= 0)
                {
                    

                    currentCongChuc.MaCongChuc = String.IsNullOrEmpty(dgrv_CongChuc["MaCongChuc", rowIndex].Value.ToString()) ? 0 : int.Parse(dgrv_CongChuc["MaCongChuc", rowIndex].Value.ToString());
                    txtbHoTen.Text = String.IsNullOrEmpty(dgrv_CongChuc["HoTen", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["HoTen", rowIndex].Value.ToString();
                    dtpNgaySinh.Value = DateTime.ParseExact(dgrv_CongChuc.Rows[rowIndex].Cells["NgaySinh"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                    rdNam.Checked = dgrv_CongChuc.Rows[rowIndex].Cells["GioiTinh"].Value.ToString() == "Nam" ? true : false;
                    rdNu.Checked = dgrv_CongChuc.Rows[rowIndex].Cells["GioiTinh"].Value.ToString() == "Nữ" ? true : false;
                    txtbChucVu.Text = String.IsNullOrEmpty(dgrv_CongChuc["ChucVu", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["ChucVu", rowIndex].Value.ToString();
                    txtbQueQuan.Text = String.IsNullOrEmpty(dgrv_CongChuc["QueQuan", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["QueQuan", rowIndex].Value.ToString();
                    txtbCMND.Text = String.IsNullOrEmpty(dgrv_CongChuc["CMND", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["CMND", rowIndex].Value.ToString();
                    txtbMaCongChuc.Text = String.IsNullOrEmpty(dgrv_CongChuc["MaCongChuc", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["MaCongChuc", rowIndex].Value.ToString();
                    txtbTaiKhoan.Text = String.IsNullOrEmpty(dgrv_CongChuc["TaiKhoan", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["TaiKhoan", rowIndex].Value.ToString();
                    listID = BoPhan_CongChucModel.GetIDsBoPhanByMaCongChuc(currentCongChuc.MaCongChuc);
                    //txtbDanToc.Text = String.IsNullOrEmpty(dgrv_CongChuc["DanToc", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["DanToc", rowIndex].Value.ToString();
                    //txtbNoiSinh.Text = String.IsNullOrEmpty(dgrv_CongChuc["NoiSinh", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["NoiSinh", rowIndex].Value.ToString();
                    //txtbSoDinhDanh.Text = String.IsNullOrEmpty(dgrv_CongChuc["SoDinhDanhCaNhan", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["SoDinhDanhCaNhan", rowIndex].Value.ToString();
                    //txtbQuocTich.Text = String.IsNullOrEmpty(dgrv_CongChuc["QuocTich", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["QuocTich", rowIndex].Value.ToString();
                    //txtbQueQuan.Text = String.IsNullOrEmpty(dgrv_CongChuc["QueQuan", rowIndex].Value.ToString()) ? "" : dgrv_CongChuc["QueQuan", rowIndex].Value.ToString();
                    //dtpHenTra.Value = String.IsNullOrEmpty(dgrv_CongChuc.Rows[rowIndex].Cells["NgayHen"].Value.ToString()) ? DateTime.Now : DateTime.ParseExact(dgrv_CongChuc.Rows[rowIndex].Cells["NgayHen"].Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
                    btnCapNhat.Enabled = true;
                    btnThem.Enabled = false;
                    if (isGetCongChucChuaCoTaiKhoan)
                    {
                        btnThem.Enabled = false;
                        btnCapNhat.Enabled = false;
                        btnCapTaiKhoan.Enabled = true;
                        if (String.IsNullOrEmpty(txtbTaiKhoan.Text))
                        {
                            btnCapTaiKhoan.Text = "Cấp tài khoản";
                        }
                        else
                        {
                            btnCapTaiKhoan.Text = "Lấy lại mật khẩu";
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnChonBoPhan_Click(object sender, EventArgs e)
        {
            FormQLCongChuc_BoPhan.Instance.IDCongChuc = currentCongChuc.MaCongChuc;
            FormQLCongChuc_BoPhan.Instance.ShowDialog();
            listID = FormQLCongChuc_BoPhan.Instance.listIDBoPhan;
            FormQLCongChuc_BoPhan.Instance.Close();
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            CongChuc congChucUpdate = new CongChuc();
            congChucUpdate.MaCongChuc = int.Parse(txtbMaCongChuc.Text);
            congChucUpdate.HoTen = txtbHoTen.Text;
            congChucUpdate.NgaySinh = dtpNgaySinh.Value;
            congChucUpdate.GioiTinh = rdNam.Checked ? true : false;
            congChucUpdate.QueQuan = txtbQueQuan.Text;
            congChucUpdate.ChucVu = txtbChucVu.Text;
            congChucUpdate.CMND = txtbCMND.Text;
            if (listID.Count > 0)
            {
                if (!String.IsNullOrEmpty(CongChucModel.CapNhatCongChuc(congChucUpdate)))
                {
                    BoPhan_CongChucModel.Insert((congChucUpdate.MaCongChuc), listID);
                    MessageBox.Show("Thao tác thành công!");
                    LoadDuLieu();
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa chọn bộ phận cho công chức!");
            }
        }

        private void btnBackToMenu_Click(object sender, EventArgs e)
        {
            FormMenu.Instance.Show();
            FormQLCongChuc.Instance.Close();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            foreach (Control ctr in FormQLCongChuc.Instance.Controls)
            {
                if (ctr is TextBox)
                {
                    ((TextBox)ctr).Text = String.Empty;
                }
            }
            btnThem.Enabled = true;
            btnCapNhat.Enabled = false;
            btnCapTaiKhoan.Enabled = false;
        }

        private void FormQLCongChuc_Shown(object sender, EventArgs e)
        {
            if (isGetCongChucChuaCoTaiKhoan)
            {
                lblTitle.Text = "Cấp tài khoản cho công chức";
                btnThem.Enabled = false;
                btnHuy.Enabled = false;
                btnChonBoPhan.Enabled = false;
                this.Text = "Cấp tài khoản";
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            dgrv_CongChuc.Rows.Clear();
            object HoTen = txtbHoTen.Text.ToString().Trim() == "" ? null : txtbHoTen.Text.ToString().Trim();
            object GioiTinh = rdNam.Checked ? 1 : rdNu.Checked ? 0 : -1;
            object QueQuan = txtbQueQuan.Text.ToString().Trim() == "" ? null : txtbQueQuan.Text.ToString().Trim();
            object ChucVu = txtbChucVu.Text.ToString().Trim() == "" ? null : txtbChucVu.Text.ToString().Trim();
            var dsCongChuc = CongChucModel.TimKiem(HoTen, GioiTinh, QueQuan, ChucVu);
            foreach (CongChuc congChuc in dsCongChuc)
            {
                var index = dgrv_CongChuc.Rows.Add();
                dgrv_CongChuc.Rows[index].Cells["MaCongChuc"].Value = congChuc.MaCongChuc;
                dgrv_CongChuc.Rows[index].Cells["HoTen"].Value = congChuc.HoTen;
                dgrv_CongChuc.Rows[index].Cells["NgaySinh"].Value = congChuc.NgaySinh.ToString("dd/MM/yyyy");
                dgrv_CongChuc.Rows[index].Cells["GioiTinh"].Value = congChuc.GioiTinh == true ? "Nam" : "Nữ";
                dgrv_CongChuc.Rows[index].Cells["QueQuan"].Value = congChuc.QueQuan;
                dgrv_CongChuc.Rows[index].Cells["ChucVu"].Value = congChuc.ChucVu;
                dgrv_CongChuc.Rows[index].Cells["TaiKhoan"].Value = congChuc.TaiKhoan;
                dgrv_CongChuc.Rows[index].Cells["CMND"].Value = congChuc.CMND;
            }
        }
    }
}
