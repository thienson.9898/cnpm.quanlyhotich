﻿using QLHT.Entity;
using QLHT.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyHoTich.FormHeThong
{
    public partial class FormQLCongChuc_BoPhan : Form
    {
        public static FormQLCongChuc_BoPhan Instance
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new FormQLCongChuc_BoPhan();
                }
                return instance;
            }
        }
        private static FormQLCongChuc_BoPhan instance;
        public List<BoPhan> dsBoPhan;
        public List<int> listIDBoPhan;
        public List<int> listIDBoPhanBackup;
        public int IDCongChuc=-1;
        BoPhan_CongChucModel BoPhan_CongChucModel = new BoPhan_CongChucModel();

        public FormQLCongChuc_BoPhan()
        {
            InitializeComponent();
            LayoutBoPhan.MaximumSize = new Size(LayoutBoPhan.Width, LayoutBoPhan.Height);
            LayoutBoPhan.VerticalScroll.Enabled = true;
            LayoutBoPhan.AutoScroll = true;
        }


        private void FormCongChuc_BoPhan_Load(object sender, EventArgs e)
        {

        }

        private void FormCongChuc_BoPhan_Shown(object sender, EventArgs e)
        {
            LoadData();
            InitForm();
        }

        void LoadData()
        {
            dsBoPhan = BoPhanModel.GetData();
        }
        void InitForm()
        {
            foreach(BoPhan bp in dsBoPhan)
            {
                LayoutBoPhan.RowCount += 1;
                LayoutBoPhan.RowStyles.Add(new RowStyle(SizeType.AutoSize));                

                Label lbl = new Label();
                lbl.Margin = new Padding(6);
                lbl.Name = "lbl" + bp.TenVietTat;
                lbl.Text = bp.TenBoPhan;
                lbl.AutoSize = true;
                lbl.Dock = DockStyle.Top;
                lbl.FlatStyle = FlatStyle.Popup;

                CheckBox chk = new CheckBox();
                chk.Dock = DockStyle.Top;
                chk.FlatStyle = FlatStyle.Popup;
                chk.Name = "chk" + bp.TenVietTat;
                chk.Tag = bp.MaBoPhan+"";

                LayoutBoPhan.Controls.Add(lbl, 0, LayoutBoPhan.RowCount);
                LayoutBoPhan.Controls.Add(chk, 1, LayoutBoPhan.RowCount);
            }
            if (IDCongChuc != -1)
            {
                List<BoPhan_CongChuc> dsBoPhan_CongChuc = BoPhan_CongChucModel.GetData(IDCongChuc);
                if(dsBoPhan_CongChuc!=null && dsBoPhan_CongChuc.Count > 0)
                {
                    foreach(var bp_cc in dsBoPhan_CongChuc)
                    {
                        foreach (Control ctl in LayoutBoPhan.Controls)
                        {
                            if (ctl is CheckBox)
                            {
                                var chk = (CheckBox)ctl;
                                if(chk.Tag.ToString() == bp_cc.MaBoPhan.ToString())
                                {
                                    chk.Checked = true;
                                }                                
                            }
                        }
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            listIDBoPhan = new List<int>();
            foreach (Control ctl in LayoutBoPhan.Controls)
            {
                if (ctl is CheckBox)
                {
                    var chk = (CheckBox)ctl;
                    if (chk.Checked)
                        listIDBoPhan.Add(int.Parse(chk.Tag.ToString()));
                    continue;
                }
            }
            if (listIDBoPhan.Count > 0)
            {
                this.Hide();
                MessageBox.Show("Chọn thành công!");
            }
            else
            {
                MessageBox.Show("Mời bạn chọn 1 trong số các bộ phận!");
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
