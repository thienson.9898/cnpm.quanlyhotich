﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyHoTich.FormHeThong;

namespace QuanLyHoTich
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormDangNhap());
            //Application.Run(new FormQLHoTich.FormQLKhaiTu());

            //Application.Run(new FormQLKhaiSinh());
            //Application.Run(new FormQLCongChuc_BoPhan());
        }
    }
}
