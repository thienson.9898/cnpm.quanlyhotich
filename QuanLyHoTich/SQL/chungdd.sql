﻿--chungdd
--Quản lý lệ phí

--get data
create PROC Proc_LePhi_GetData
AS
BEGIN
    SELECT lp.MaLePhi,lp.NgayLap,lp.MaCongDan,lp.MaCongChuc,cd.MaCongDan,cd.HoTen as HoTenCD,cc.MaCongChuc,cc.HoTen as HoTenCC from LePhi as lp,CongDan as cd,CongChuc as cc where lp.MaCongDan = cd.MaCongDan and lp.MaCongChuc = cc.MaCongChuc
END

--add
create PROC Proc_LePhi_Insert @MaCongDan int,
                            @MaCongChuc int,
                            @NgayLap DATETIME
                        
AS BEGIN
    INSERT INTO dbo.LePhi
            ( MaCongDan ,
              MaCongChuc,
              NgayLap
            )
    VALUES  ( @MaCongDan , 
              @MaCongChuc , 
              @NgayLap
            )
        Select scope_identity()
END

--update
CREATE PROC Proc_LePhi_Update @MaCongDan int,
                            @MaCongChuc int, 
                            @NgayLap DATETIME, 
                            @MaLePhi int
AS BEGIN 
    UPDATE dbo.LePhi SET MaCongDan = @MaCongDan,
                            MaCongChuc = @MaCongChuc,
                            NgayLap = @NgayLap
    WHERE MaLePhi = @MaLePhi
END

--chi tiết lệ phí
--get data
create PROC Proc_CTLePhi_GetData
AS
BEGIN
    SELECT ctlp.MaCTLePhi,ctlp.MaThuTuc,tt.TenThuTuc,ctlp.MaLePhi, ctlp.LyDoNop , ctlp.SoTienNop
    from CTLP as ctlp,LePhi as lp,ThuTuc as tt 
    where ctlp.MaThuTuc = tt.MaThuTuc and ctlp.MaLePhi = lp.MaLePhi
END

--add
create PROC Proc_CTLePhi_Insert @MaThuTuc int,
                            @MaLePhi int,
                            @SoTienNop FLOAT,
                            @LyDoNop NVARCHAR(255)
                        
AS BEGIN
    INSERT INTO dbo.CTLP
            ( MaThuTuc ,
              MaLePhi,
              SoTienNop,
              LyDoNop
            )
    VALUES  ( @MaThuTuc , 
              @MaLePhi , 
              @SoTienNop,
              @LyDoNop
            )
        Select scope_identity()
END

--update
CREATE PROC Proc_CTLePhi_Update @MaThuTuc int,
                            @MaLePhi int, 
                            @SoTienNop FLOAT,
                            @LyDoNop NVARCHAR(255),
                            @MaCTLePhi int
AS BEGIN 
    UPDATE dbo.CTLP SET     MaThuTuc = @MaThuTuc,
                            MaLePhi = @MaLePhi,
                            SoTienNop = @SoTienNop,
                            LyDoNop = @LyDoNop
    WHERE MaCTLePhi = @MaCTLePhi
END

--thống kê theo tháng năm
create PROC Proc_tinhTheoThangNam_ThongKe
@thang INT,
@nam int
AS
SELECT ctlp.MaCTLePhi,ctlp.MaThuTuc,tt.TenThuTuc,ctlp.MaLePhi, ctlp.LyDoNop , ctlp.SoTienNop, lp.NgayLap
    from CTLP as ctlp,LePhi as lp,ThuTuc as tt 
    where ctlp.MaThuTuc = tt.MaThuTuc and ctlp.MaLePhi = lp.MaLePhi
 AND  YEAR(lp.NgayLap)=@nam 
AND MONTH(lp.NgayLap)= @thang

--thống kê theo năm
create PROC Proc_tinhTheoNam_ThongKe
@nam int
AS
SELECT ctlp.MaCTLePhi,ctlp.MaThuTuc,tt.TenThuTuc,ctlp.MaLePhi, ctlp.LyDoNop , ctlp.SoTienNop, lp.NgayLap
    from CTLP as ctlp,LePhi as lp,ThuTuc as tt 
    where ctlp.MaThuTuc = tt.MaThuTuc and ctlp.MaLePhi = lp.MaLePhi
 AND  YEAR(lp.NgayLap)=2020 

--thống kê theo khoảng ngày
create PROC sp_tinhTheoKhoangNgay
@tu datetime,@den DATETIME
AS
SELECT ctlp.MaCTLePhi,ctlp.MaThuTuc,tt.TenThuTuc,ctlp.MaLePhi, ctlp.LyDoNop , ctlp.SoTienNop, lp.NgayLap
    from CTLP as ctlp,LePhi as lp,ThuTuc as tt 
    where ctlp.MaThuTuc = tt.MaThuTuc and ctlp.MaLePhi = lp.MaLePhi
AND (lp.NgayLap)>@tu AND (lp.NgayLap)<@den