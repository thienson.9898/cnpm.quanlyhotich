
---------------------Proc1------
ALTER PROC Proc_KhaiSinh_GetDataKSGiamHo
								
AS 
	SELECT MaKhaiSinh,HoTen,NgaySinh,GioiTinh,DanToc,QuocTich,QueQuan,SoDinhDanHCaNhan FROM KhaiSinh

	GO;
	
	
---------------------Proc2------
CREATE PROCEDURE proc_insertGiamHo
	@idNgGiamHo int,
	@idNgDcGiamHo int,
	@maThuTuc int,
	
	@ngHen DateTime,
	
	@maCongDan int,
	@maCongCHucTN int,
	@trangThai int
	

AS
	INSERT INTO GiamHo(IDNguoiGiamHo,IDNguoiDuocGiamHo,MaThuTuc,NgayDangKy,NgayHen,MaCongDan,MaCongChucTiepNhan,TrangThai)
	VALUES(@idNgGiamHo,@idNgDcGiamHo,@maThuTuc,GETDATE(),@ngHen,@maCongDan,@maCongCHucTN,@trangThai);
GO;

---------------------Proc3------
CREATE PROCEDURE Proc_GiamHoCD_getDATA
	@MaCongDan int
	
AS
	SELECT * from CongDan where MaCongDan=@MaCongDan
	
	GO;
---------------------Proc4------
	
CREATE PROC Proc_GIamHo_GetData
as
Select MaGH,IDNguoiGiamHo,IDNguoiDuocGiamHo,NgayDangKy,NgayHen,TrangThai from GiamHo
GO;
---------------------Proc5------
CREATE PROCEDURE Proc_GiamHo_UpdateTrangThai
	@MaGH int,
	@TrangThai int
	
AS
	UPDATE GiamHo SET TrangThai=@TrangThai where MaGH=@MaGH
	
GO;

---------------------Proc6------
CREATE PROCEDURE Proc_GiamHo_UpdateTrangThaiTraKQ
	@MaGH int,
	@TrangThai int,
	@MaCongChucTiepNhan int
	
	
AS
	UPDATE GiamHo SET TrangThai=@TrangThai,MaCongChucTiepNhan=@MaCongChucTiepNhan,NgayTra=GETDATE() where MaGH=@MaGH
	
GO;
---------------------Proc7------
CREATE PROCEDURE Proc_GiamHo_UpdateTrangThaiDuyetGH
	@MaGH int,
	@TrangThai int,
	@MaLanhDao int
	
AS
	UPDATE GiamHo SET TrangThai=@TrangThai,MaLanhDao=@MaLanhDao where MaGH=@MaGH

GO;
---------------------Proc8------
CREATE PROC Proc_GIamHo_GetDataComBoBox
@TrangThai int
as
Select MaGH,IDNguoiGiamHo,IDNguoiDuocGiamHo,NgayDangKy,NgayHen,TrangThai from GiamHo
where TrangThai=@TrangThai

GO;