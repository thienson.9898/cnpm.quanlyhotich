﻿--Procedure
Create Proc Proc_CongDan_Insert @HoTen NVARCHAR(255), 
							@NgaySinh DATETIME, 
							@GioiTinh BIT, 
							@QueQuan NVARCHAR(255), 
							@ThuongTru NVARCHAR(255), 
							@DanToc NVARCHAR(255) = N'Kinh', 
							@QuocTich NVARCHAR(255), 
							@CMND NVARCHAR(25), 
							@TrangThai int = 0
AS BEGIN 
	INSERT INTO dbo.CongDan
	        ( HoTen ,
	          NgaySinh ,
	          GioiTinh ,
	          QueQuan ,
			  ThuongTru,
	          DanToc ,
	          QuocTich ,
	          CMND, 
			  TrangThai
	        )
	VALUES  ( @HoTen , 
	          @NgaySinh , 
	          @GioiTinh , 
	          @QueQuan , 
	          @ThuongTru, 
			  @DanToc, 
			  @QuocTich,
			  @CMND,
			  @TrangThai
	        )
	Select scope_identity()
END;
Go

Create Proc Proc_CongDan_Update @HoTen NVARCHAR(255), 
							@NgaySinh DATETIME, 
							@GioiTinh BIT, 
							@QueQuan NVARCHAR(255), 
							@ThuongTru NVARCHAR(255), 
							@DanToc NVARCHAR(255) = N'Kinh', 
							@QuocTich NVARCHAR(255), 
							@CMND NVARCHAR(25), 
							@TrangThai int = 0,
							@MaCongDan int
AS BEGIN 
	UPDATE dbo.CongDan SET HoTen = @HoTen,
							NgaySinh = @NgaySinh,
							GioiTinh = @GioiTinh,
							QueQuan = @QueQuan,
							ThuongTru = @ThuongTru,
							DanToc = @DanToc,
							QuocTich = @QuocTich,
							CMND = @CMND,
							TrangThai = @TrangThai
	WHERE MaCongDan = @MaCongDan
END

GO

Create Procedure Proc_CongDan_GetData 
							@MaCongDan INT = -1,
							@HoTen NVARCHAR(255) = '', 
							@NgaySinh DATETIME = '' , 
							@GioiTinh INT = -1, 
							@QueQuan NVARCHAR(255) = '', 
							@ThuongTru NVARCHAR(255) = '', 
							@DanToc NVARCHAR(255) = '', 
							@QuocTich NVARCHAR(255) = '', 
							@CMND NVARCHAR(25) = '', 
							@ContainKhaiSinh INT = -1,
							@ContainAdult INT = -1,
							@TrangThai int = -1,
							@Offset INT = -1,
							@Fetch INT = -1
AS BEGIN
	DECLARE @Query AS NVARCHAR(MAX)
	DECLARE @ParamList AS NVARCHAR(max)
	SET @Query = 'Select * from CongDan where (1=1) '
	IF(@MaCongDan !=-1)
		SET @Query += ' AND (MaCongDan = @MaCongDan) '
	IF(@HoTen != '')
		BEGIN
			SET @HoTen = '%'+@HoTen+'%'
			SET @Query += ' AND (HoTen like @HoTen) '
		END
		
	IF (@NgaySinh != '')
		SET @Query += ' AND (NgaySinh = @NgaySinh) '
	IF (@GioiTinh !=-1)
		SET @Query += ' AND (GioiTinh = @GioiTinh) '
	IF (@QueQuan != '')
		BEGIN
			SET @QueQuan = '%'+@QueQuan+'%'
			SET @Query += ' AND (QueQuan like @QueQuan) '
		END
	IF (@ThuongTru != '')
		BEGIN
			SET @ThuongTru = '%'+@ThuongTru+'%'
		SET @Query += ' AND (ThuongTru like @ThuongTru) '
		END
	IF (@DanToc != '')
		SET @Query += ' AND (DanToc = @DanToc) '
	IF (@QuocTich != '')
		SET @Query += ' AND (QuocTich = @QuocTich) '
	IF (@CMND != '')
		SET @Query += ' AND (CMND = @CMND) '
	IF(@ContainKhaiSinh = -1)
		SET @Query += ' AND (CMND != ''KhaiSinh'') '
	IF(@ContainAdult = -1)
		SET @Query += ' AND (CMND = ''KhaiSinh'') '
	IF (@TrangThai !=-1)
		SET @Query += ' AND (TrangThai = @TrangThai) '
	IF (@Offset != -1 AND @Fetch !=-1)
		SET @Query += 'ORDER BY MaCongDan OFFSET '+@Offset+'ROWS FETCH NEXT '+@Fetch+' ROWS ONLY '
	SET @ParamList = '@MaCongDan INT,
							@HoTen NVARCHAR(255), 
							@NgaySinh DATETIME, 
							@GioiTinh INT, 
							@QueQuan NVARCHAR(255), 
							@ThuongTru NVARCHAR(255), 
							@DanToc NVARCHAR(255), 
							@QuocTich NVARCHAR(255), 
							@CMND NVARCHAR(25), 
							@ContainKhaiSinh int,
							@ContainAdult int,
							@TrangThai INT, 
							@Offset INT,
							@Fetch INT'
	EXEC SP_EXECUTESQL @Query, @ParamList ,@MaCongDan,@HoTen, @NgaySinh, @GioiTinh, @QueQuan, @ThuongTru, @DanToc, @QuocTich, @CMND, @ContainKhaiSinh, @ContainAdult, @TrangThai, @Offset, @Fetch
END


GO
-------------------------------
--Công Chức-- 1. Thêm công chức----2. Sửa công chức
Create Proc Proc_CongChuc_Insert @HoTen nvarchar(255),
							@NgaySinh DATETIME,
							@GioiTinh BIT,
							@QueQuan NVARCHAR(255),
							@ChucVu NVARCHAR(255),
							@CMND NVARCHAR(25),
							@TaiKhoan NVARCHAR(255) = null,
							@Pass NVARCHAR(255) = null,
							@Quyen NVARCHAR(255) = null,
							@TrangThai NVARCHAR(255)
AS BEGIN
	INSERT INTO dbo.CongChuc
	        ( HoTen ,
	          NgaySinh ,
	          GioiTinh ,
	          QueQuan ,
	          ChucVu ,
	          CMND ,
	          TaiKhoan ,
	          Pass ,
	          Quyen ,
	          TrangThai
	        )
	VALUES  ( @HoTen , -- HoTen - nvarchar(255)
	          @NgaySinh , -- NgaySinh - datetime
	          @GioiTinh , -- GioiTinh - bit
	          @QueQuan , -- QueQuan - nvarchar(255)
	          @ChucVu , -- ChucVu - nvarchar(255)
	          @CMND , -- CMND - nvarchar(25)
	          @TaiKhoan, -- TaiKhoan - nvarchar(255)
	          @Pass, -- Pass - nvarchar(255)
	          @Quyen, -- Quyen - nvarchar(255)
	          @TrangThai-- TrangThai - nvarchar(255)
	        )
		Select scope_identity()
END

GO
Create Proc Proc_CongChuc_Update @HoTen nvarchar(255),
							@NgaySinh DATETIME,
							@GioiTinh BIT,
							@QueQuan NVARCHAR(255),
							@ChucVu NVARCHAR(255),
							@CMND NVARCHAR(25),							
							@TrangThai NVARCHAR(255),
							@MaCongChuc INT
AS
BEGIN
	UPDATE CongChuc SET
							Hoten = @HoTen ,
							NgaySinh = @NgaySinh ,
							GioiTinh= @GioiTinh ,
							QueQuan= @QueQuan ,
							ChucVu= @ChucVu ,
							CMND= @CMND ,							
							TrangThai= @TrangThai
	WHERE MaCongChuc = @MaCongChuc

END;
GO

Create Procedure Proc_CongChuc_GetData
							@MaCongChuc INT = -1,
							@HoTen NVARCHAR(255) = NULL, 
							@NgaySinh DATETIME = NULL , 
							@GioiTinh BIT = NULL, 
							@QueQuan NVARCHAR(255) = NULL, 
							@ChucVu NVARCHAR(255) = NULL,
							@CMND NVARCHAR(25) = NULL, 
							@TaiKhoan NVARCHAR(255) = NULL,
							@Quyen NVARCHAR(255) = NULL,							
							@TrangThai NVARCHAR(255) = NULL
AS BEGIN
	DECLARE @Query AS NVARCHAR(MAX)
	DECLARE @ParamList AS NVARCHAR(max)
	SET @Query = 'SELECT [MaCongChuc]
				  ,[HoTen]
				  ,[NgaySinh]
				  ,[GioiTinh]
				  ,[QueQuan]
				  ,[ChucVu]
				  ,[CMND]
				  ,[TaiKhoan]      
				  ,[Quyen]
				  ,[TrangThai]
			  FROM [qlhotich].[dbo].[CongChuc] where (1=1) '
	IF(@MaCongChuc != -1)
		SET @Query += ' AND (MaCongChuc like @MaCongChuc) '
	IF(@HoTen IS NOT NULL)
		BEGIN
			SET @HoTen = '%'+@HoTen+'%'
			SET @Query += ' AND (HoTen like @HoTen) '
		END
		
	IF (@NgaySinh IS NOT NULL)
		SET @Query += ' AND (NgaySinh = @NgaySinh) '
	IF (@GioiTinh IS NOT NULL)
		SET @Query += ' AND (GioiTinh = @GioiTinh) '
	IF (@QueQuan IS NOT NULL)
		BEGIN
			SET @QueQuan = '%'+@QueQuan+'%'
			SET @Query += ' AND (QueQuan like @QueQuan) '
		END
	IF (@ChucVu IS NOT NULL)
			SET @Query += ' AND (ChucVu like @ChucVu) '
	IF (@CMND IS NOT NULL)
		SET @Query += ' AND (CMND = @CMND) '
	IF (@TaiKhoan IS NOT NULL)
		SET @Query += ' AND (TaiKhoan = @TaiKhoan) '
	IF (@Quyen IS NOT NULL)
		SET @Query += ' AND (Quyen = @Quyen) '
	IF (@TrangThai IS NOT NULL)
		SET @Query += ' AND (TrangThai = @TrangThai) '
		  SET @ParamList = '@MaCongChuc int,
							@HoTen NVARCHAR(255), 
							@NgaySinh DATETIME, 
							@GioiTinh BIT, 
							@QueQuan NVARCHAR(255), 
							@ChucVu NVARCHAR(255),
							@CMND NVARCHAR(25), 
							@TaiKhoan Nvarchar(255),
							@Quyen NVARCHAR(255),							
							@TrangThai NVARCHAR(255)'
	EXEC SP_EXECUTESQL @Query, @ParamList, @MaCongChuc,@HoTen, @NgaySinh,@GioiTinh,@QueQuan, @ChucVu,@CMND, @TaiKhoan, @Quyen,	@TrangThai
END


 GO

 --Proc_CongChuc_DangNhap 'Quang','1'
 
Create Proc Proc_CongChuc_DangNhap @TaiKhoan NVARCHAR(255), @Pass NVARCHAR(255)
AS
BEGIN
	SELECT MaCongChuc FROM dbo.CongChuc WHERE TaiKhoan = @TaiKhoan AND Pass = @Pass
END;

GO
Create Proc Proc_BoPhan_CongChuc_GetData @MaBoPhan int = 0, @MaCongChuc int = 0
AS
BEGIN
	DECLARE @Query AS NVARCHAR(MAX)
	DECLARE @ParamList AS NVARCHAR(max)

	SET @Query = 'SELECT BoPhan_CongChuc.MaBoPhan, MaCongChuc, TenBoPhan, TenVietTat FROM BoPhan, BoPhan_CongChuc WHERE (1=1) and BoPhan.MaBoPhan = BoPhan_CongChuc.MaBoPhan ' 
	IF(@MaBoPhan !=0)
		SET @Query += ' AND MaBoPhan = @MaBoPhan '
	IF(@MaCongChuc !=0)
		SET @Query += ' AND MaCongChuc = @MaCongChuc '
	SET @ParamList = '@MaBoPhan int, @MaCongChuc int'
	EXEC SP_EXECUTESQL @Query, @ParamList, @MaBoPhan, @MaCongChuc
END


GO
/**/
Create Proc Proc_KhaiSinh_Insert @HoTen NVARCHAR(255), 
									@NgaySinh DATETIME,
									@GioiTinh BIT,
									@DanToc NVARCHAR(255) = N'Kinh',
									@QuocTich NVARCHAR(255) = N'Việt Nam',
									@QueQuan NVARCHAR(255) = N'Ninh Khang',
									@NoiSinh NVARCHAR(255) = N'Không rõ',
									@SoDinhDanhCaNhan NVARCHAR(255) = '000',
									@IDNguoiCha INT,
									@IDNguoiMe INT,
									@MaThuTuc INT,	
									@NgayHen DATETIME,								
									@MaCongDan INT,
									@MaCongChucTiepNhan INT									
AS
BEGIN
	INSERT INTO dbo.KhaiSinh
	        ( HoTen ,
	          NgaySinh ,
	          GioiTinh ,
	          DanToc ,
	          QuocTich ,
	          QueQuan ,
			  NoiSinh,
			  SoDinhDanhCaNhan,
	          IDNguoiMe ,
	          IDNguoiCha ,
	          MaThuTuc ,
	          NgayDangKy ,
			  NgayHen,
	          MaCongDan ,
	          MaCongChucTiepNhan ,
	          TrangThai
	        )
	VALUES  ( @HoTen , -- HoTen - nvarchar(255)
	          @NgaySinh , -- NgaySinh - datetime
	          @GioiTinh , -- GioiTinh - bit
	          @DanToc , -- DanToc - nvarchar(255)
	          @QuocTich , -- QuocTich - nvarchar(255)
	          @QueQuan , -- QueQuan - nvarchar(255)
			  @NoiSinh,
			  @SoDinhDanhCaNhan,
	          @IDNguoiMe , -- IDNguoiMe - int
	          @IDNguoiCha , -- IDNguoiCha - int
	          @MaThuTuc , -- MaThuTuc - int
	          GETDATE() , -- NgayDangKy - datetime
	          @NgayHen,
			  @MaCongDan , -- MaCongDan - int
	          @MaCongChucTiepNhan , -- MaCongChucTiepNhan - int
	          0  -- TrangThai - int
	        )
END;
GO

Create Proc Proc_KhaiSinh_GetData @MaKhaiSinh INT = -1, @HoTen NVARCHAR(255) = '', @NgaySinh DATETIME ='', @GioiTinh INT = -1, @DanToc NVARCHAR(255) = '',
								@QuocTich NVARCHAR(255) = '', @QueQuan NVARCHAR(255) = '', @NoiSinh NVARCHAR(255) = '', @SoDinhDanhCaNhan NVARCHAR(255) = '',
								@MaCongDan INT = -1, @MaCongChucTiepNhan INT = -1, @MaCongChucXacMinh INT = -1, @MaLanhDao INT = -1, @MaCongChucVanPhong INT = -1,
								@MaCongChucKTTC INT = -1, @TrangThai INT = -1
AS BEGIN
	DECLARE @Query AS NVARCHAR(max)
	DECLARE @ListParam AS NVARCHAR(max)
	SET @Query = N' Select * from KhaiSinh where (1=1) '
	IF(@MaKhaiSinh !=-1)
	BEGIN
		SET @Query += ' AND (MaKhaiSinh = @MaKhaiSinh) '
	END
	IF(@HoTen != '')
	BEGIN
		SET @HoTen = '%'+@HoTen+'%'
		SET @Query += ' AND (HoTen like @HoTen) '
	END
	IF(@NgaySinh != '')
	BEGIN
		SET @Query += ' AND (NgaySinh = @NgaySinh) '
	END
	IF(@DanToc !='')
		SET @Query += ' AND (DanToc = @DanToc) '
	IF(@QuocTich != '')
		SET @Query += ' AND (QuocTich = @QuocTich) '
	IF(@QueQuan != '')
	BEGIN
		SET @QueQuan = '%'+@QueQuan+'%'
		SET @Query += ' AND (QueQuan like @QueQuan) '
	END
	IF(@NoiSinh != '')
	BEGIN
		SET @NoiSinh = '%'+@NoiSinh+'%'
		SET @Query += ' AND (NoiSinh like @NoiSinh) '
	END
	IF(@SoDinhDanhCaNhan != '')
		SET @Query += ' AND (SoDinhDanhCaNhan = @SoDinhDanhCaNhan) '
	IF(@MaCongDan != -1)
		SET @Query += ' AND (MaCongDan = @MaCongDan) '
	IF(@MaCongChucTiepNhan != -1)
		SET @Query += ' AND (MaCongChucTiepNhan = @MaCongChucTiepNhan) '
	IF(@MaCongChucXacMinh != -1)
		SET @Query += ' AND (MaCongChucXacMinh = @MaCongChucXacMinh) '
	IF(@MaLanhDao != -1)
		SET @Query += ' AND (MaLanhDao = @MaLanhDao) '
	IF(@MaCongChucVanPhong != -1)
		SET @Query += ' AND (MaCongChucVanPhong = @MaCongChucVanPhong) '
	IF(@MaCongChucKTTC != -1)
		SET @Query += ' AND (MaCongChucKTTC = @MaCongChucKTTC) '
	IF(@TrangThai != -1)
		SET @Query += ' AND (TrangThai = @TrangThai) '
	SET @ListParam = '@MaKhaiSinh INT, @HoTen NVARCHAR(255), @NgaySinh DATETIME, @GioiTinh INT, @DanToc NVARCHAR(255),
								@QuocTich NVARCHAR(255), @QueQuan NVARCHAR(255), @NoiSinh NVARCHAR(255), @SoDinhDanhCaNhan NVARCHAR(255),
								@MaCongDan INT, @MaCongChucTiepNhan INT, @MaCongChucXacMinh INT, @MaLanhDao INT, @MaCongChucVanPhong INT,
								@MaCongChucKTTC INT, @TrangThai INT'
	EXECUTE SP_EXECUTESQL @Query, @ListParam, @MaKhaiSinh , @HoTen , @NgaySinh , @GioiTinh , @DanToc ,
								@QuocTich , @QueQuan , @NoiSinh , @SoDinhDanhCaNhan ,
								@MaCongDan , @MaCongChucTiepNhan , @MaCongChucXacMinh , @MaLanhDao , @MaCongChucVanPhong ,
								@MaCongChucKTTC , @TrangThai 
END

GO

Create Proc Proc_KhaiSinh_UpdateTrangThai @MaKhaiSinh INT,
								@MaCongChucXacMinh INT = -1,
								@MaLanhDao INT = -1,
								@MaCongChucVanPhong INT = -1,
								@MaCongChucKTTC INT = -1, 
								@NgayTra DATETIME = '',
								@TrangThai INT
AS BEGIN
	IF(@MaCongChucXacMinh != -1)
	BEGIN
		UPDATE dbo.KhaiSinh SET MaCongChucXacMinh = @MaCongChucXacMinh, TrangThai = @TrangThai WHERE MaKhaiSinh = @MaKhaiSinh
	END
	IF(@MaLanhDao != -1)
	BEGIN
		UPDATE dbo.KhaiSinh SET MaLanhDao = @MaLanhDao, TrangThai = @TrangThai WHERE MaKhaiSinh = @MaKhaiSinh
	END
	IF(@MaCongChucVanPhong != -1)
	BEGIN
		UPDATE dbo.KhaiSinh SET MaCongChucVanPhong = @MaCongChucVanPhong, NgayTra = @NgayTra ,TrangThai = @TrangThai WHERE MaKhaiSinh = @MaKhaiSinh
	END
	IF(@MaCongChucKTTC != -1)
	BEGIN
		UPDATE dbo.KhaiSinh SET MaCongChucKTTC = @MaCongChucKTTC, TrangThai = @TrangThai WHERE MaKhaiSinh = @MaKhaiSinh
	END
END


GO

Create Proc Proc_BoPhan_GetData AS SELECT * FROM dbo.BoPhan
GO


Create Proc Proc_BoPhan_CongChuc_Insert @MaBoPhan INT, @MaCongChuc INT 
AS INSERT INTO dbo.BoPhan_CongChuc
        ( MaBoPhan, MaCongChuc )
VALUES  ( @MaBoPhan, -- MaBoPhan - int
          @MaCongChuc  -- MaCongChuc - int
          )

GO

Create Proc Proc_BoPhan_CongChuc_DeleteByCongChuc @MaCongChuc INT  AS DELETE dbo.BoPhan_CongChuc WHERE MaCongChuc = @MaCongChuc
GO

Create Proc Proc_CongChuc_CapTaiKhoan @MaCongChuc int, @TaiKhoan NVARCHAR(255), @Pass NVARCHAR(255)
AS UPDATE dbo.CongChuc SET TaiKhoan = @TaiKhoan, Pass = @Pass WHERE MaCongChuc = @MaCongChuc
Go


Create Proc Proc_CongChuc_CapNhatMatKhau @MaCongChuc int, @Pass NVARCHAR(255)
AS UPDATE dbo.CongChuc SET Pass = @Pass WHERE MaCongChuc = @MaCongChuc
GO


create PROC Proc_ThuTuc_Insert @TenThuTuc NVARCHAR(255)
AS
BEGIN
INSERT INTO dbo.ThuTuc
        ( TenThuTuc )
VALUES  ( @TenThuTuc  -- TenThuTuc - nvarchar(255)
          )
		  Select scope_identity()
END

CREATE PROC Proc_KhaiSinh_UpdateThuTuc @MaKhaiSinh INT, @MaThuTuc INT
AS
BEGIN
UPDATE dbo.KhaiSinh SET MaThuTuc = @MaThuTuc WHERE MaKhaiSinh = @MaKhaiSinh
END

---- Chạy cái này 08/04/2020 -----

ALTER TABLE dbo.CongChuc DROP CONSTRAINT UQ_TK


----Trả ra 0: Thì thêm. Trả ra >0: Trùng -> k thêm
CREATE PROC Proc_CongChuc_CheckTK @MaCongChuc int, @TaiKhoan NVARCHAR(255)
AS 
BEGIN
	SELECT Count (MaCongChuc) FROM dbo.CongChuc WHERE TaiKhoan = @TaiKhoan AND MaCongChuc != @MaCongChuc
END
