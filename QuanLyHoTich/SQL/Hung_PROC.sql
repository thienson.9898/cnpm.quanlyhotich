create Proc Proc_KetHon_Insert
									@IDNguoiChong INT,
									@IDNguoiVo INT,
									@MaThuTuc INT,	
									@NgayHen DATETIME,								
									@MaCongDan INT,
									@MaCongChucTiepNhan INT									
AS
BEGIN
	INSERT INTO dbo.KetHon
	        ( 
	          IDNguoiChong ,
	          IDNguoiVo ,
	          MaThuTuc ,
	          NgayDangKy ,
			  NgayHen,
	          MaCongDan ,
	          MaCongChucTiepNhan ,
	          TrangThai
	        )
	VALUES  (
	          @IDNguoiChong , -- IDNguoiMe - int
	          @IDNguoiVo , -- IDNguoiCha - int
	          @MaThuTuc , -- MaThuTuc - int
	          GETDATE() , -- NgayDangKy - datetime
	          @NgayHen,
			  @MaCongDan , -- MaCongDan - int
	          @MaCongChucTiepNhan , -- MaCongChucTiepNhan - int
	          0  -- TrangThai - int
	        )
END;
GO

create PROC Proc_KetHon_GetData @MaKH INT = -1,
								@MaCongDan INT = -1, @MaCongChucTiepNhan INT = -1, @MaCongChucXacMinh INT = -1, @MaLanhDao INT = -1, @MaCongChucVanPhong INT = -1,
								@MaCongChucKTTC INT = -1, @TrangThai INT = -1
AS BEGIN
	DECLARE @Query AS NVARCHAR(max)
	DECLARE @ListParam AS NVARCHAR(max)
	SET @Query = N' Select * from KetHon where (1=1) '
	IF(@MaKH !=-1)
	BEGIN
		SET @Query += ' AND (MaKH = @MaKH) '
	END
	IF(@MaCongDan != -1)
		SET @Query += ' AND (MaCongDan = @MaCongDan) '
	IF(@MaCongChucTiepNhan != -1)
		SET @Query += ' AND (MaCongChucTiepNhan = @MaCongChucTiepNhan) '
	IF(@MaCongChucXacMinh != -1)
		SET @Query += ' AND (MaCongChucXacMinh = @MaCongChucXacMinh) '
	IF(@MaLanhDao != -1)
		SET @Query += ' AND (MaLanhDao = @MaLanhDao) '
	IF(@MaCongChucVanPhong != -1)
		SET @Query += ' AND (MaCongChucVanPhong = @MaCongChucVanPhong) '
	IF(@MaCongChucKTTC != -1)
		SET @Query += ' AND (MaCongChucKTTC = @MaCongChucKTTC) '
	IF(@TrangThai != -1)
		SET @Query += ' AND (TrangThai = @TrangThai) '
	SET @ListParam = '@MaKH INT,
								@MaCongDan INT, @MaCongChucTiepNhan INT, @MaCongChucXacMinh INT, @MaLanhDao INT, @MaCongChucVanPhong INT,
								@MaCongChucKTTC INT, @TrangThai INT'
	EXECUTE SP_EXECUTESQL @Query, @ListParam, @MaKH ,
								@MaCongDan , @MaCongChucTiepNhan , @MaCongChucXacMinh , @MaLanhDao , @MaCongChucVanPhong ,
								@MaCongChucKTTC , @TrangThai 
END

GO

CREATE PROC Proc_KetHon_UpdateTrangThai @MaKH INT,
								@MaCongChucXacMinh INT = -1,
								@MaLanhDao INT = -1,
								@MaCongChucVanPhong INT = -1,
								@MaCongChucKTTC INT = -1, 
								@NgayTra DATETIME = '',
								@TrangThai INT
AS BEGIN
	IF(@MaCongChucXacMinh != -1)
	BEGIN
		UPDATE dbo.KetHon SET MaCongChucXacMinh = @MaCongChucXacMinh, TrangThai = @TrangThai WHERE  MaKH = @MaKH
	END
	IF(@MaLanhDao != -1)
	BEGIN
		UPDATE dbo.KetHon SET MaLanhDao = @MaLanhDao, TrangThai = @TrangThai WHERE MaKH = @MaKH
	END
	IF(@MaCongChucVanPhong != -1)
	BEGIN
		UPDATE dbo.KetHon SET MaCongChucVanPhong = @MaCongChucVanPhong, NgayTra = @NgayTra ,TrangThai = @TrangThai WHERE MaKH = @MaKH
	END
	IF(@MaCongChucKTTC != -1)
	BEGIN
		UPDATE dbo.KetHon SET MaCongChucKTTC = @MaCongChucKTTC, TrangThai = @TrangThai WHERE MaKH = @MaKH
	END
	END