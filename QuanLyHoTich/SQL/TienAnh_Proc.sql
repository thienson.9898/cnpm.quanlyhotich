﻿--Proc

select * from CongDan where MaCongDan=11
select * from KhaiTu

CREATE Proc Proc_KhaiTu_Insert		@IdNguoiChet INT, 
									@NoiCuTruCuoiCung NVARCHAR(255) = 'Không rõ',
									@ThoiGianChet DATETIME,									
									@NoiChet NVARCHAR(255) =N'Không rõ',
									@NguyenNhan NVARCHAR(255)= N'Không rõ',
									@MaThuTuc INT,
									@NgayHen DATETIME,
									@MaCongDan INT,
									@MaCongChucTiepNhan INT
									
						
																
AS
BEGIN
	INSERT INTO dbo.KhaiTu
	        ( IDNguoiChet ,
			NoiCuTruCuoiCung,
			ThoiGianChet,
			NoiChet,
			NguyenNhan,
			MaThuTuc,
			NgayDangKy,
			NgayHen,			
			MaCongDan,
			MaCongChucTiepNhan,
			TrangThai         
	        )
	VALUES  (	@IdNguoiChet , -- Mã Công Dân Tử - nvarchar(255)
				@NoiCuTruCuoiCung,
				@ThoiGianChet,
				@NoiChet,
				@NguyenNhan,
				@MaThuTuc,
				GETDATE(),
				@NgayHen,
				@MaCongDan,
				@MaCongChucTiepNhan,	   
	          0  -- TrangThai - int
	        )
END;

GO

Select * from KhaiTu join CongDan on KhaiTu.IdNguoiChet=CongDan.MaCongDan where (1=1)
exec Proc_KhaiTu_GetData
ALTER PROC Proc_KhaiTu_GetData @MaKT INT=-1,
								@MaCongDan INT = -1, @MaCongChucTiepNhan INT = -1, @MaCongChucXacMinh INT = -1, @MaLanhDao INT = -1, @MaCongChucVanPhong INT = -1,
								@MaCongChucKTTC INT = -1, @TrangThai INT = -1
AS BEGIN
	DECLARE @Query AS NVARCHAR(max)
	DECLARE @ListParam AS NVARCHAR(max)
	SET @Query = N' Select * from KhaiTu  where (1=1) '
	IF(@MaKT !=-1)
	BEGIN
		SET @Query += ' AND (MaKT = @MaKT) '
	END
	IF(@MaCongDan != -1)
		SET @Query += ' AND (MaCongDan = @MaCongDan) '

	IF(@MaCongChucTiepNhan != -1)
		SET @Query += ' AND (MaCongChucTiepNhan = @MaCongChucTiepNhan) '
	IF(@MaCongChucXacMinh != -1)
		SET @Query += ' AND (MaCongChucXacMinh = @MaCongChucXacMinh) '
	IF(@MaLanhDao != -1)
		SET @Query += ' AND (MaLanhDao = @MaLanhDao) '
	IF(@MaCongChucVanPhong != -1)
		SET @Query += ' AND (MaCongChucVanPhong = @MaCongChucVanPhong) '
	IF(@MaCongChucKTTC != -1)
		SET @Query += ' AND (MaCongChucKTTC = @MaCongChucKTTC) '
	IF(@TrangThai != -1)
		SET @Query += ' AND (TrangThai = @TrangThai) '
	SET @ListParam = '@MaKT INT, @MaCongDan INT, @MaCongChucTiepNhan INT, @MaCongChucXacMinh INT, @MaLanhDao INT, @MaCongChucVanPhong INT,
								@MaCongChucKTTC INT, @TrangThai INT'
	EXECUTE SP_EXECUTESQL @Query, @ListParam, @MaKT ,
								@MaCongDan , @MaCongChucTiepNhan , @MaCongChucXacMinh , @MaLanhDao , @MaCongChucVanPhong ,
								@MaCongChucKTTC , @TrangThai 
END



GO
 
--- Update Trang Thai

CREATE PROC Proc_KhaiTu_UpdateTrangThai @MaKT INT,
								@MaCongChucXacMinh INT = -1,
								@TrangThai INT,
								@MaLanhDao INT = -1,
								@MaCongChucVanPhong INT = -1,
								@MaCongChucKTTC INT = -1, 
								@NgayTra DATETIME = ''
							
AS BEGIN
	IF(@MaCongChucXacMinh != -1)
	BEGIN
		UPDATE dbo.KhaiTu SET MaCongChucXacMinh = @MaCongChucXacMinh, TrangThai = @TrangThai WHERE MaKT = @MaKT
	END
	IF(@MaLanhDao != -1)
	BEGIN
		UPDATE dbo.KhaiTu SET MaLanhDao = @MaLanhDao, TrangThai = @TrangThai WHERE MaKT = @MaKT
	END
	IF(@MaCongChucVanPhong != -1)
	BEGIN
		UPDATE dbo.KhaiTu SET MaCongChucVanPhong = @MaCongChucVanPhong, NgayTra = @NgayTra ,TrangThai = @TrangThai WHERE MaKT = @MaKT
	END
	IF(@MaCongChucKTTC != -1)
	BEGIN
		UPDATE dbo.KhaiTu SET MaCongChucKTTC = @MaCongChucKTTC, TrangThai = @TrangThai WHERE MaKT = @MaKT
	END
END


exec Proc_KhaiTu_UpdateTrangThai 1,1,1

select * from KhaiTu


create proc TimKiemKhaiTu 
@TrangThai nvarchar(255)
as
begin
select *from KhaiTu where TrangThai=@TrangThai
end

exec TimKiemKhaiTu 3
 