﻿ ---Proc
 Create proc Insert_DangKyThayDoi
 (		
		  @MaCongDanYC int,
		  @MaCongDanDN int,
		  @QuanHeVoiNDN NVARCHAR(255),
		  @MucCanThayDoi NVARCHAR(50),
		  @LyDo nvarchar(500),
		  @GiaTriCu NVARCHAR(255),
		  @GiaTriMoi NVARCHAR(255),
		  @MaThuTuc INT,		 
		  @NgayHen DATETIME,
		  @MaCongChucTiepNhan INT
 )
 as
 begin
      Insert into DangKyThayDoi
	     (MaCongDanYC ,
		  MaCongDanDN ,
		  QuanHeVoiNDN ,
		  MucCanThayDoi,
		  LyDo,
		  GiaTriCu,
		  GiaTriMoi ,
		  MaThuTuc ,
		  NgayDangKy,
		  NgayHen,
		  MaCongChucTiepNhan , 
		  TrangThai)
	  values(@MaCongDanYC ,
		  @MaCongDanDN ,
		  @QuanHeVoiNDN ,
		  @MucCanThayDoi,
		  @LyDo,
		  @GiaTriCu,
		  @GiaTriMoi ,
		  @MaThuTuc ,
		  Getdate(),
		  @NgayHen,	  
		  @MaCongChucTiepNhan , 
		  0)

 end


 Exec Insert_DangKyThayDoi @MaCongDanYC = '1',
          @MaCongDanDN = '2', 
		  @QuanHeVoiNDN = N'Anh trai',
		  @MucCanThayDoi = N'Quốc tịch',
		  @LyDo = N'Thay đổi',
		  @GiaTriCu = '',
		  @GiaTriMoi = '',
		  @MaThuTuc = '1',	 
		  @NgayHen = '08/09/2020',
		  @NgayTra = '08-09-2020',
		  @MaCongChucTiepNhan = '1', 
 ---
 Create proc select_DSDangKyThayDoi
 as
 begin
   Select * from DangKyThayDoi
 end

 ---
  create proc select_DSDangKyThayDoi_ChiTiet
 as
 begin
   Select DangKyThayDoi.MaCongDanYC,DangKyThayDoi.MaCongDanDN,DangKyThayDoi.QuanHeVoiNDN,
   DangKyThayDoi.NgayHen,CongDan.MaCongDan,CongDan.HoTen,CongDan.GioiTinh,CongDan.NgaySinh,CongDan.QueQuan,
   CongDan.QuocTich from DangKyThayDoi,CongDan  
 end

 ---
 update_DangKyThayDoi_Update_TrangThai @MaThayDoi = 5, @MaCongChucXacMinh = 3, @TrangThai = 1
 Create PROC update_DangKyThayDoi_Update_TrangThai (
                                @MaThayDoi INT,
								@MaCongChucXacMinh INT = -1,
								@MaLanhDao INT = -1,
								@MaCongChucVanPhong INT = -1,
								@MaCongChucKTTC INT = -1, 
								@NgayTra DATETIME = '',
								@TrangThai INT,
								@MaCongDanDN int = -1,
								@HoTen nvarchar(500) = '',
								@NgaySinh datetime = '',
								@DanToc nvarchar(500) = '',
								@GioiTinh bit = '',
								@QueQuan nvarchar(500)='',
								@QuocTich nvarchar(500)='')
AS BEGIN
    --DECLARE @TT AS int
	IF(@MaCongChucXacMinh != -1)
	BEGIN
		UPDATE dbo.DangKyThayDoi SET MaCongChucXacMinh = @MaCongChucXacMinh, TrangThai = @TrangThai WHERE MaThayDoi = @MaThayDoi
	END
	IF(@MaLanhDao != -1)
	BEGIN
		UPDATE dbo.DangKyThayDoi SET MaLanhDao = @MaLanhDao, TrangThai = @TrangThai WHERE MaThayDoi = @MaThayDoi
		--Set @TT = @TrangThai
		      update CongDan set HoTen = @HoTen , NgaySinh = @NgaySinh , GioiTinh = @GioiTinh , DanToc = @DanToc,
			  QueQuan = @QueQuan , QuocTich = @QuocTich where CongDan.MaCongDan = @MaCongDanDN
			  --Select CongDan.CMND from CongDan 
			  --update KhaiSinh set HoTen = @HoTen , NgaySinh = @NgaySinh , GioiTinh = @GioiTinh , DanToc = @DanToc,
			  --QueQuan = @QueQuan , QuocTich = @QuocTich where (Select CongDan.CMND from CongDan where CongDan.CMND = N'KhaiSinh') = 1
	END
	IF(@MaCongChucVanPhong != -1)
	BEGIN
		UPDATE dbo.DangKyThayDoi SET MaCongChucVanPhong = @MaCongChucVanPhong, NgayTra = @NgayTra ,TrangThai = @TrangThai WHERE MaThayDoi = @MaThayDoi
	END
	IF(@MaCongChucKTTC != -1)
	BEGIN
		UPDATE dbo.DangKyThayDoi SET MaCongChucKTTC = @MaCongChucKTTC, TrangThai = @TrangThai WHERE MaThayDoi = @MaThayDoi
	END
END

Proc_CongDan_GetData 2, @ContainKhaiSinh  = -1,
							@ContainAdult = 1


--
create proc select_MaTrangThai
as
begin
    Select TrangThai.MaTrangThai,TrangThai.TrangThai from TrangThai
end

---
create proc select_DSDangKyThayDoi_TheoMa (@MaTrangThai int)
as
begin
 Select * from DangKyThayDoi where DangKyThayDoi.TrangThai = @MaTrangThai 
end


---
create proc update_CongDan_FromDangKyThayDoi(
                                @MaCongDanDN int,
								@HoTen nvarchar(500) = '',
								@NgaySinh datetime = '',
								@DanToc nvarchar(500) = '',
								@GioiTinh bit = '',
								@QueQuan nvarchar(500)='',
								@QuocTich nvarchar(500)='')
as
begin
   
    update CongDan set HoTen = @HoTen , NgaySinh = @NgaySinh , DanToc = @DanToc , 
	GioiTinh = @GioiTinh , QueQuan = @QueQuan , QuocTich = @QuocTich 
	where MaCongDan = @MaCongDanDN
end

--
create proc select_CMND_CongDan(@MaCongDan int)
as
begin
Select CMND from CongDan where MaCongDan = @MaCongDan
end

---

create proc update_KhaiSinh_FromDangKyThayDoi(                            
								@CMND nvarchar(100),
                                @HoTen nvarchar(500) = '',
								@NgaySinh datetime = '',
								@DanToc nvarchar(500) = '',
								@GioiTinh bit = '',
								@QueQuan nvarchar(500)='',
								@QuocTich nvarchar(500)='')
as
begin
      update KhaiSinh set HoTen = @HoTen , NgaySinh = @NgaySinh , DanToc = @DanToc , 
	  GioiTinh = @GioiTinh , QueQuan = @QueQuan , QuocTich = @QuocTich where MaKhaiSinh = @CMND
end

---